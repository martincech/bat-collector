﻿using Communication.DataSource;

namespace Communication.Tests.Connectivity.Modbus
{
    public partial class ModbusSourceTests
    {
        public interface ITestActionInvocation
        {
            void InvokeAction(byte v);
            void InvokeAction(IDataSource source);
        }
    }
}
