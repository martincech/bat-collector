﻿using System;
using Communication.Connectivity.Modbus;
using Communication.DataSource.Modbus;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Modbus.Bat2Scale.Registers.Types;

namespace Communication.Tests.Connectivity.Modbus
{
    public partial class ModbusSourceTests
    {
        private class MockDataSource2 : IModbusDataSource
        {
            #region Implementation of IDataSource

            public event EventHandler<TimeSample> NewSourceData
            {
                add { }
                remove { }
            }
            public string Id { get; private set; }
            public DeviceType Type { get; private set; }

            #endregion

            /// <inheritdoc />
            public event EventHandler<bool> CommunicationOkStatusChanged
            {
                add { }
                remove { }
            }

            public event EventHandler<ModbusResponse> ResponseRead
            {
                add { }
                remove { }
            }

            /// <inheritdoc />
            public void Activate()
            {
            }

            /// <inheritdoc />
            public void Deactivate()
            {
            }

            /// <inheritdoc />
            public byte Address { get; private set; }

            public void GetVersion()
            {
                throw new NotImplementedException();
            }

            public void GetScaleName()
            {
                throw new NotImplementedException();
            }

            public void SetScaleName(string name)
            {
                throw new NotImplementedException();
            }

            public void GetConfiguration()
            {
                throw new NotImplementedException();
            }

            public void SetSavingParameters(SavingParameters parameters)
            {
                throw new NotImplementedException();
            }

            public void GetSavingParameters()
            {
                throw new NotImplementedException();
            }

            public void GetFlock(byte flockNumber)
            {
                throw new NotImplementedException();
            }

            public void SetFlock(Flock flock)
            {
                throw new NotImplementedException();
            }

            public void StartWeighing(Flock flock, WeighingParameters parameters)
            {
                throw new NotImplementedException();
            }

            public void StopWeighing()
            {
                throw new NotImplementedException();
            }

            public void PauseWeighing(bool pause)
            {
                throw new NotImplementedException();
            }

            public void GetWeighingStatus()
            {
                throw new NotImplementedException();
            }

            public void GetWeighingParameters()
            {
                throw new NotImplementedException();
            }

            /// <inheritdoc />
            public void Dispose() { }
        }


    }
}
