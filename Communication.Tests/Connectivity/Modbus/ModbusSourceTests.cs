﻿using System;
using System.Linq;
using Communication.Connectivity.Modbus;
using Communication.Connectivity.Modbus.LineRequest;
using Communication.DataSource;
using Communication.DataSource.Modbus;
using Moq;
using Veit.Mqueue;
using Xunit;

namespace Communication.Tests.Connectivity.Modbus
{
    public partial class ModbusSourceTests
    {
        private readonly ModbusConnectivitySource uut;
        private readonly Mock<IProcessQueue> mockModbusQueue;
        private Func<ModbusScanRequest, bool> modbusScanRequestAction;

        public ModbusSourceTests()
        {
            mockModbusQueue = new Mock<IProcessQueue>();
            mockModbusQueue.Setup(
               q => q.AddProcessing(It.IsAny<Func<ModbusScanRequest, bool>>()))
               .Callback<Func<ModbusScanRequest, bool>>(a => modbusScanRequestAction = a);
            mockModbusQueue.Setup(q => q.Enqueue(It.Is<object>(o => o is ModbusScanRequest), It.IsAny<Priority>()))
#pragma warning disable CC0031 // Check for null before calling a delegate
               .Callback<object,Priority>((o, p) => modbusScanRequestAction((ModbusScanRequest)o));
#pragma warning restore CC0031 // Check for null before calling a delegate
            uut = new ModbusConnectivitySource(mockModbusQueue.Object, "test");
        }

        [Fact]
        public void ScanAction_Registered()
        {
            mockModbusQueue.Verify(q => q.AddProcessing(It.IsAny<Func<ModbusScanRequest, bool>>()), Times.Once);
        }

        [Fact]
        public void ScanAction_Invoked()
        {
            mockModbusQueue.Verify(q => q.Enqueue(It.IsAny<ModbusScanRequest>(), It.IsAny<Priority>()), Times.Never);
            uut.ScanForDevices(new ModbusScanRequest { AddressFrom = 0, AddressTo = 255 }, null);
            mockModbusQueue.Verify(q => q.Enqueue(It.IsAny<ModbusScanRequest>(), It.IsAny<Priority>()), Times.Once);
        }

        [Fact]
        public void SupportedDataSources_AllSupportedSourceRegistered()
        {
            uut.AddSupportedDataSource(i => new MockDataSource1(i.ToString()));
            uut.AddSupportedDataSource(i => new MockDataSource2());
            Assert.Contains(typeof(MockDataSource1), uut.SupportedDataSources);
            Assert.Contains(typeof(MockDataSource2), uut.SupportedDataSources);
        }

        [Fact]
        public void SupportedDataSources_SameSourcesRegistration_ThrowsException()
        {
#pragma warning disable IDE0039 // Use local function
            Action act = () => uut.AddSupportedDataSource(j => new MockDataSource1(j.ToString()));
#pragma warning restore IDE0039 // Use local function
            act.Invoke();   //first action => register OK
            Assert.Throws<ArgumentException>(act);  //same registration => exception
        }

        [Fact]
        public void SupportedDataSources_ExceptionThrownForSimpleInterfaceType()
        {
#pragma warning disable IDE0039 // Use local function
            Action act = () => uut.AddSupportedDataSource<IDataSource>(j => new MockDataSource1(j.ToString()));
#pragma warning restore IDE0039 // Use local function
            Assert.Throws<ArgumentException>(act);
        }

        [Fact]
        public void ScanAction_FoundDevices_ForFullAddressRange()
        {
            //setup
            mockModbusQueue.Verify(q => q.Enqueue(It.Is<object>(o => o is ModbusScanRequest), It.IsAny<Priority>()), Times.Never);
            var deviceCount = AddDevicesToAdresses(0, 255);

            Assert.Equal(deviceCount, uut.ConnectedSources.Count());
        }

        private int AddDevicesToAdresses(byte addressFrom, byte addressTo,
           Action<byte> actionForCreated = null,
           Action<IDataSource> actionForConnected = null,
           Action<IDataSource> actionForDisconnected = null)
        {
            //register a device to be found on each of the scanning addresses
            var mockConnectedInvocation = new Mock<ITestActionInvocation>();
            SetupMockTestAction(mockConnectedInvocation);
            uut.AddSupportedDataSource(i =>
            {
                actionForCreated?.Invoke(i);
                return new MockDataSource1(i.ToString());
            });

            uut.SourceConnected += (sender, source) =>
            {
                actionForConnected?.Invoke(source);
                mockConnectedInvocation.Object.InvokeAction(source);
            };
            uut.SourceDisconnected += (sender, source) =>
            {
                actionForDisconnected?.Invoke(source);
            };

            //test
            var deviceCount = addressTo - addressFrom + 1;
            var request = new ModbusScanRequest { AddressFrom = addressFrom, AddressTo = addressTo };
            uut.ScanForDevices(request, null);

            mockConnectedInvocation.Verify(s => s.InvokeAction(It.IsAny<IDataSource>()), Times.Exactly(deviceCount));
            return deviceCount;
        }

        [Fact]
        public void ScanAction_Executed_ForAllAdressesInRange()
        {
            //setup
            mockModbusQueue.Verify(q => q.Enqueue(It.Is<object>(o => o is ModbusScanRequest), It.IsAny<Priority>()), Times.Never);
            //register a device to be found on each of the scanning addresses
            var dumyAction = new Mock<ITestActionInvocation>();
            const int addressFrom = 0;
            const int addressTo = 5;
            var deviceCount = AddDevicesToAdresses(addressFrom, addressTo, i => dumyAction.Object.InvokeAction(i));

            //verify
            mockModbusQueue.Verify(q => q.Enqueue(It.IsAny<ModbusScanRequest>(), It.IsAny<Priority>()), Times.Once); //scan action executed only once

            for (var i = addressFrom; i <= addressTo; i++) //scan executed on
            {
                var i1 = i;
                dumyAction.Verify(s => s.InvokeAction(It.Is<byte>(v => v == i1)), Times.Once); // single address checked only once
                Assert.Contains(uut.ConnectedSources, d => d.Id == i1.ToString());
            }
            dumyAction.Verify(s => s.InvokeAction(It.IsAny<byte>()), Times.Exactly(deviceCount));
        }

        [Fact]
        public void ScanAction_Executed_Twice_DevicesNotDubled()
        {
            //setup
            mockModbusQueue.Verify(q => q.Enqueue(It.Is<object>(o => o is ModbusScanRequest), It.IsAny<Priority>()), Times.Never);
            //register a device to be found on each of the scanning addresses
            var constructAction = new Mock<ITestActionInvocation>();
            SetupMockTestAction(constructAction);
            var connectedAction = new Mock<ITestActionInvocation>();
            SetupMockTestAction(connectedAction);
            var disconnectedAction = new Mock<ITestActionInvocation>();
            SetupMockTestAction(disconnectedAction);

            const int addressFrom = 0;
            const int addressTo = 5;
            var deviceCount = AddDevicesToAdresses(addressFrom, addressTo,
               i => constructAction.Object.InvokeAction(i),
               source => connectedAction.Object.InvokeAction(source),
               source => disconnectedAction.Object.InvokeAction(source));

            var request = new ModbusScanRequest { AddressFrom = addressFrom, AddressTo = addressTo };
            uut.ScanForDevices(request, null);

            //verify
            Assert.Equal(deviceCount, uut.ConnectedSources.Count()); //number of devices found
            mockModbusQueue.Verify(q => q.Enqueue(It.IsAny<ModbusScanRequest>(), It.IsAny<Priority>()), Times.Exactly(2)); //scan action executed twice

            var j = 0;
            for (var i = addressFrom; i <= addressTo; i++) //scan executed on
            {
                var i1 = i;
                constructAction.Verify(s => s.InvokeAction(It.Is<byte>(v => v == i1)), Times.Exactly(2)); // device create twice on single address
                connectedAction.Verify(s => s.InvokeAction(It.Is<IDataSource>(d => d == uut.ConnectedSources.ElementAt(j))), Times.Once); // connected reported once for this device
                disconnectedAction.Verify(s => s.InvokeAction(It.Is<IDataSource>(d => d == uut.ConnectedSources.ElementAt(j))), Times.Never); // disconnected never reported for newly created devices in between the scan requests
                Assert.Contains(uut.ConnectedSources, d => d.Id == i1.ToString());
                j++;
            }
            constructAction.Verify(s => s.InvokeAction(It.IsAny<byte>()), Times.Exactly(deviceCount * 2)); // constructed reported twice for all devices
            disconnectedAction.Verify(s => s.InvokeAction(It.IsAny<IDataSource>()), Times.Once);// connected reported once only in between the scan requests
        }

        private static void SetupMockTestAction(Mock<ITestActionInvocation> mock)
        {
            mock.SetupAllProperties();
            mock.Setup(s => s.InvokeAction(It.IsAny<IDataSource>()));
        }

        [Fact]
        public void ScanAction_Executed_ForAllAdressesInRange_AndForAllRegisteredTypes()
        {
            //prepare
            var dumyActionForMock1 = new Mock<ITestActionInvocation>();
            var dumyActionForMock2 = new Mock<ITestActionInvocation>();
            //register a device but it wont be found on any address
            uut.AddSupportedDataSource<MockDataSource1>(i =>
            {
                dumyActionForMock1.Object.InvokeAction(i);
                return null;
            });
            uut.AddSupportedDataSource<MockDataSource2>(i => {
                dumyActionForMock2.Object.InvokeAction(i);
                return null;
            });

            //test
            const int addressFrom = 0;
            const int addressTo = 5;
            const int deviceCount = addressTo - addressFrom + 1;
            var request = new ModbusScanRequest { AddressFrom = addressFrom, AddressTo = addressTo };
            uut.ScanForDevices(request, null);

            //verify
            Assert.False(uut.ConnectedSources.Any());
            for (var i = addressFrom; i <= addressTo; i++) //scan executed on
            {
                var i1 = i;
                dumyActionForMock1.Verify(s => s.InvokeAction(It.Is<byte>(v => v == i1)), Times.Once); // single address checked only once
                dumyActionForMock2.Verify(s => s.InvokeAction(It.Is<byte>(v => v == i1)), Times.Once); // single address checked only once
            }
            dumyActionForMock1.Verify(s => s.InvokeAction(It.IsAny<byte>()), Times.Exactly(deviceCount));
            dumyActionForMock2.Verify(s => s.InvokeAction(It.IsAny<byte>()), Times.Exactly(deviceCount));
        }

        [Fact]
        public void ScanAction_DoesNotCheckOtherTypes_WhenFirstRegisteredTypeFound()
        {
            //prepare
            var dumyActionForMock1 = new Mock<ITestActionInvocation>();
            var dumyActionForMock2 = new Mock<ITestActionInvocation>();
            //register a device but it wont be found on any address var dumyActionForMock1 = new Mock<ITestActionInvocation>();s
            uut.AddSupportedDataSource(i =>
            {
                dumyActionForMock1.Object.InvokeAction(i);
                return new MockDataSource1(i.ToString());
            });
            uut.AddSupportedDataSource<MockDataSource2>(i => {
                dumyActionForMock2.Object.InvokeAction(i);
                return null;
            });

            //test
            const int addressFrom = 0;
            const int addressTo = 5;
            const int deviceCount = addressTo - addressFrom + 1;
            var request = new ModbusScanRequest { AddressFrom = addressFrom, AddressTo = addressTo };
            uut.ScanForDevices(request, null);


            //verify
            Assert.Equal(deviceCount, uut.ConnectedSources.Count()); //number of devices found
            for (var i = addressFrom; i <= addressTo; i++) //scan executed on
            {
                var i1 = i;
                dumyActionForMock1.Verify(s => s.InvokeAction(It.Is<byte>(v => v == i1)), Times.Once); // single address checked only once
                dumyActionForMock2.Verify(s => s.InvokeAction(It.Is<byte>(v => v == i1)), Times.Never); // single address checked only once
            }
            dumyActionForMock1.Verify(s => s.InvokeAction(It.IsAny<byte>()), Times.Exactly(deviceCount));
            dumyActionForMock2.Verify(s => s.InvokeAction(It.IsAny<byte>()), Times.Never);
        }



        [Fact]
        public void ActiveSources_IsEmpty_AfterConnected()
        {
            const int addressFrom = 0;
            const int addressTo = 5;
            var deviceCount = AddDevicesToAdresses(addressFrom, addressTo);


            Assert.Equal(deviceCount, uut.ConnectedSources.Count()); //number of devices found
            Assert.Empty(uut.ActiveSources); //number of devices found
        }

        [Fact]
        public void ActiveSources_Connected_CanBeActivated()
        {
            const int addressFrom = 0;
            const int addressTo = 5;
            var deviceCount = AddDevicesToAdresses(addressFrom, addressTo);
            var device = uut.ConnectedSources.First();
            uut.ActivateSource(device as IModbusDataSource);
            Assert.Equal(deviceCount, uut.ConnectedSources.Count()); //number of devices found
            Assert.Single(uut.ActiveSources);
        }
        [Fact]
        public void ActiveSources_Disconnected_CannotBeActivated()
        {
            const int addressFrom = 0;
            const int addressTo = 5;
            var deviceCount = AddDevicesToAdresses(addressFrom, addressTo);
            var device = new Mock<IModbusDataSource>();
            uut.ActivateSource(device.Object);
            Assert.Equal(deviceCount, uut.ConnectedSources.Count()); //number of devices found
            Assert.Empty(uut.ActiveSources);
        }
        [Fact]
        public void ActiveSources_IsReconected_After_Rescan()
        {
            const int addressFrom = 0;
            const int addressTo = 5;
            AddDevicesToAdresses(addressFrom, addressTo);
            uut.ActivateSource(uut.ConnectedSources.First() as IModbusDataSource);
            Assert.Single(uut.ActiveSources);
            uut.ScanForDevices(new ModbusScanRequest { AddressFrom = addressFrom, AddressTo = addressTo }, null);

            Assert.Single(uut.ActiveSources); //number of devices activated

        }
    }
}
