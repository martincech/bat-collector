﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity;
using Communication.EventToQueue;
using Communication.Manager;
using Moq;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Mqueue;
using Xunit;

namespace Communication.Tests.EventToQueue
{
    public class QueableSourceManagerEventsTests
    {
        private readonly ManagerEventsToQueue uut;
        private readonly List<Mock<IQueue>> mockRepositories;
        private readonly Mock<ISourceManager> mockManager;
        private readonly List<IConnectivitySource> mockConnectivitySources;

        public QueableSourceManagerEventsTests()
        {
            mockRepositories = new List<Mock<IQueue>>
            {
                SetupMockRepository(),
                SetupMockRepository(),
                SetupMockRepository()
            };
            mockConnectivitySources = new List<IConnectivitySource>();
            mockManager = new Mock<ISourceManager>();
            mockManager.SetupGet(m => m.ConnectedSources).Returns(mockConnectivitySources);

            uut = new ManagerEventsToQueue(mockManager.Object, mockRepositories.Select(r => r.Object).ToList());
        }

        private static Mock<IQueue> SetupMockRepository()
        {
            var repository = new Mock<IQueue>();
            var queue = new List<object>();
            repository.Setup(r => r.Queue()).Returns(queue);
            repository.Setup(r => r.Enqueue(It.IsAny<object>(), Priority.Normal))
                      .Callback<object,Priority>((o,p) => queue.Add(o));
            return repository;
        }


        [Fact]
        public void RepositoriesProperty_ContainsAllRepos()
        {
            mockRepositories.ForEach(r => Assert.Contains(r.Object, uut.GetRepositories()));
        }

        [Fact]
        public void RepositoriesProperty_ContainsAllRepos_AfterAddRepository()
        {
            var newRepo = SetupMockRepository();
            mockRepositories.Add(newRepo);
            uut.Add(newRepo.Object);
            Assert.Contains(newRepo.Object, uut.GetRepositories());
            mockRepositories.ForEach(r => Assert.Contains(r.Object, uut.GetRepositories()));
        }

        [Fact]
        public void RepositoriesProperty_DoesNotContainRepo_AfterRemoved()
        {
            uut.Remove(mockRepositories.Last().Object);
            Assert.DoesNotContain(mockRepositories.Last().Object, uut.GetRepositories());
        }


        [Fact]
        public void ConstructorThrowsExcetion_OnNullManager()
        {
            void act() => new ManagerEventsToQueue(null, mockRepositories.Select(r => r.Object).ToList());
            Assert.Throws<ArgumentNullException>((Action)act);
        }

        [Fact]
        public void ConnectivitySourceConnected_InfoQueued()
        {
            Assert.True(mockRepositories.All(r => !r.Object.Queue().Any()));
            InvokeConnectivitySourceConnected(out var connectivitySource);
            Assert.True(mockRepositories.All(r => r.Object.Queue().Count() == 1));
        }

        [Fact]
        public void ConnectivitySourceConnected_InfoQueued_ValidDeviceInfoObjects()
        {
            Assert.True(mockRepositories.All(r => !r.Object.Queue().Any()));
            InvokeConnectivitySourceConnected(out var connectivitySource);
            mockRepositories.ForEach(r =>
            {
                var info = r.Object.Queue().FirstOrDefault();
                Assert.NotNull(info);
                Assert.Equal(typeof(SourceConnectivityInfo), info.GetType());
            });
        }

        [Fact]
        public void ConnectivitySourceConnected_InfoQueued_ValidDeviceInfoProperties()
        {
            Assert.True(mockRepositories.All(r => !r.Object.Queue().Any()));
            InvokeConnectivitySourceConnected(out var mockSource);
            var connectivitySource = mockSource.Object;
            mockRepositories.ForEach(r =>
            {
                var info = r.Object.Queue().FirstOrDefault() as SourceConnectivityInfo;
                Assert.NotNull(info);
                Assert.Equal(connectivitySource.Id, info.Source);
                Assert.True(info.Device.Connected);
                Assert.Equal(connectivitySource.Type.ToString(), info.Device.Type);
                Assert.True(string.IsNullOrEmpty(info.Device.SensorUid));
            });
        }

        [Fact]
        public void ConnectivitySourceDisconnected_InfoNotQueued_WhenPreviouslyNotConnected()
        {
            InvokeConnectivitySourceDisconnected(new Mock<IConnectivitySource>());
            Assert.True(mockRepositories.All(r => !r.Object.Queue().Any()));
        }

        [Fact]
        public void ConnectivitySourceDisconnected_InfoQueued()
        {
            InvokeConnectivitySourceConnected(out var mockSource);
            InvokeConnectivitySourceDisconnected(mockSource);
            mockRepositories.ForEach(r =>
            {
                Assert.Equal(2, r.Object.Queue().Count());
                var connectedInfo = r.Object.Queue().First() as SourceConnectivityInfo;
                var disconnectedInfo = r.Object.Queue().Last() as SourceConnectivityInfo;
                Assert.NotNull(connectedInfo);
                Assert.NotNull(disconnectedInfo);
                Assert.True(connectedInfo.Device.Connected);
                Assert.False(disconnectedInfo.Device.Connected);
            });
        }

        [Fact]
        public void NothingQueued_ForRemovedRepository()
        {
            var removedRepo = mockRepositories.Last();
            uut.Remove(removedRepo.Object);
            InvokeConnectivitySourceConnected(out var mockSource);

            Assert.True(mockRepositories.Except(new[] { removedRepo }).All(r => r.Object.Queue().Count() == 1));
            Assert.Empty(removedRepo.Object.Queue());
        }

        [Fact]
        public void Queued_ForRemovedAndAddedRepository()
        {
            var removedRepo = mockRepositories.Last();
            uut.Remove(removedRepo.Object);
            InvokeConnectivitySourceConnected(out var mockSource);
            uut.Add(removedRepo.Object);
            InvokeConnectivitySourceConnected(out mockSource);

            Assert.True(mockRepositories.Except(new[] { removedRepo }).All(r => r.Object.Queue().Count() == 2));
            Assert.Single(removedRepo.Object.Queue());
        }

        private void InvokeConnectivitySourceConnected(out Mock<IConnectivitySource> mockSource)
        {
            mockSource = new Mock<IConnectivitySource>();
            mockSource.SetupAllProperties();
            mockSource.SetupGet(source => source.Id).Returns("ConnectivitySourceId" + new Random().Next(200));
            mockSource.SetupGet(source => source.Type).Returns(SourceType.None);
            mockConnectivitySources.Add(mockSource.Object);
            mockManager.Raise(connectivity => connectivity.SourceConnected += null, mockManager.Object, mockSource.Object);
        }

        private void InvokeConnectivitySourceDisconnected(IMock<IConnectivitySource> connectivitySourceMock)
        {
            if (mockConnectivitySources.Contains(connectivitySourceMock.Object))
            {
                mockConnectivitySources.Remove(connectivitySourceMock.Object);
            }
            mockManager.Raise(connectivity => connectivity.SourceDisconnected += null, mockManager.Object, connectivitySourceMock.Object);
        }

    }
}
