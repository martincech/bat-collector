﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity.Modbus.LineRequest;
using Communication.DataSource.Modbus;
using Moq;
using Utilities.Timer;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Bat.Common.Sample.Weight;
using Veit.Bat.Modbus.Bat2Scale.Registers;
using Veit.Mqueue;
using Xunit;

namespace Communication.Tests.DataSource
{
    public class Bat2ModbusDataSourceTests
    {
        private readonly Bat2ModbusDataSource uut;

        private readonly Mock<IProcessQueue> mockActionQueue;
        private readonly Mock<IBat2ModbusRegisterRw> mockRegisterDecoder;
        private const byte UUT_ID = 1;
        private List<BirdWeight> birdWeights;
        private StatisticSample statistics;

        private Func<ModbusReadSamplesRequest, bool> modbusReadSamplesRequest;
        private Func<ModbusReadStatisticsRequest, bool> modbusReadStatisticsRequest;

        private readonly Mock<ITimer> mockTimer;
        private const int expectedTimersCount = 2;  //read today statistic timer and reading all statistics

        public Bat2ModbusDataSourceTests()
        {
            mockActionQueue = new Mock<IProcessQueue>();
            mockRegisterDecoder = new Mock<IBat2ModbusRegisterRw>();
            SetupNewSourceDataTests();
            mockTimer = new Mock<ITimer>();
            uut = new Bat2ModbusDataSource(
               mockRegisterDecoder.Object,
               mockActionQueue.Object,
               UUT_ID, Guid.NewGuid(), "cID",
               null,
               () => mockTimer.Object);
        }

        [Fact]
        public void Id_IsSameAsConstructorParameter()
        {
            Assert.Equal(UUT_ID, uut.Address);
        }

        [Fact]
        public void Type_IsBat2Modbus()
        {
            Assert.Equal(DeviceType.Bat2Cable, uut.Type);
        }

        [Fact]
        public void ReadWeights_InvokedOnRwObject_WhenReadSampleRequest_Queued()
        {
            mockActionQueue.Object.Enqueue(new ModbusReadSamplesRequest { Address = UUT_ID });
            mockRegisterDecoder.Verify(rd => rd.ReadLastBirdWeights(), Times.Once);
        }

        [Fact]
        public void ReadTodayStats_InvokedOnRwObject_WhenReadStatisticsRequest_Queued()
        {
            mockActionQueue.Object.Enqueue(new ModbusReadStatisticsRequest { Address = UUT_ID });
            mockRegisterDecoder.Verify(rd => rd.ReadTodayStatistics(), Times.Once);
        }


        [Fact]
        public void OnNewSourceData_InvokedForNewBirdWeights_OnlyOnce()
        {
            var invocationCount = 0;
            uut.NewSourceData += (sender, sample) =>
            {
                invocationCount++;
                Assert.Same(birdWeights.First(), sample);
            };
            mockActionQueue.Object.Enqueue(new ModbusReadSamplesRequest { Address = UUT_ID });
            Assert.Equal(1, invocationCount);
        }

        [Fact]
        public void OnNewSourceData_InvokedForNewStatistics_OnlyOnce()
        {
            var invocationCount = 0;
            uut.NewSourceData += (sender, sample) =>
            {
                invocationCount++;
                Assert.Same(statistics, sample);
            };
            mockActionQueue.Object.Enqueue(new ModbusReadStatisticsRequest { Address = UUT_ID });
            Assert.Equal(1, invocationCount);
        }

        [Fact]
        public void ActivationStartsTimer()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V153);
            uut.Activate();
            mockTimer.Verify(t => t.Start(), Times.Exactly(expectedTimersCount));
        }

        [Fact]
        public void DeactivationStopsTimer()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V153);
            uut.Activate();
            mockTimer.Verify(t => t.Start(), Times.Exactly(expectedTimersCount));
            uut.Deactivate();
            mockTimer.Verify(t => t.Stop(), Times.Exactly(expectedTimersCount));
        }

        [Fact]
        public void DisposeStopsTimer()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V153);
            uut.Activate();
            using (uut)
            {

            }
            mockTimer.Verify(t => t.Stop(), Times.Exactly(expectedTimersCount));
        }

        [Fact]
        public void Activation_ReadStatsAction_Invoked_OnBat2_Version_111()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V111);
            Activate_Verification<ModbusReadStatisticsRequest, ModbusReadSamplesRequest>();
        }
        [Fact]
        public void ReadWeightsAction_Invoked_OnBat2_Version_111_Repeatly()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V111);
            Activate_Verification<ModbusReadStatisticsRequest, ModbusReadSamplesRequest>(5);
        }

        [Fact]
        public void Activation_ReadStatsAction_Invoked_OnBat2_Version_150()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V150);
            Activate_Verification<ModbusReadStatisticsRequest, ModbusReadSamplesRequest>();
        }

        [Fact]
        public void ReadWeightsAction_Invoked_OnBat2_Version_150_Repeatly()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V150);
            Activate_Verification<ModbusReadStatisticsRequest, ModbusReadSamplesRequest>(5);
        }

        [Fact]
        public void Activation_ReadWeightsAction_Invoked_OnBat2_Version_153()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V153);
            Activate_Verification<ModbusReadStatisticsRequest, ModbusReadSamplesRequest>();
        }

        [Fact]
        public void ReadWeightsAction_Invoked_OnBat2_Version_153_Repeatly()
        {
            mockRegisterDecoder.Setup(d => d.Version).Returns(Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version.V153);
            Activate_Verification<ModbusReadStatisticsRequest, ModbusReadSamplesRequest>(5);
        }


        private void Activate_Verification<T1, T2>(int times = 0)
        {
            Assert.NotSame(typeof(T1), typeof(T2));
            mockActionQueue.Verify(q => q.Enqueue(It.IsAny<T1>(), It.IsAny<Priority>()), Times.Never);
            mockActionQueue.Verify(q => q.Enqueue(It.IsAny<T2>(), It.IsAny<Priority>()), Times.Never);
            uut.Activate();
            for (var i = 0; i < times; i++)
            {
                mockTimer.Raise(t => t.Elapsed += null, mockTimer.Object, null);
            }
            mockActionQueue.Verify(q => q.Enqueue(It.IsAny<T1>(), It.IsAny<Priority>()), Times.Exactly(times));
            mockActionQueue.Verify(q => q.Enqueue(It.IsAny<T2>(), It.IsAny<Priority>()), Times.Never);
        }


        private void SetupNewSourceDataTests()
        {
            birdWeights = new List<BirdWeight>
            {
            new BirdWeight(20)
            };
            statistics = new StatisticSample();
            mockActionQueue
               .Setup(q => q.AddProcessing(It.IsAny<Func<ModbusReadSamplesRequest, bool>>()))
               .Callback<Func<ModbusReadSamplesRequest, bool>>(a => modbusReadSamplesRequest = a)
               .Returns(true);
#pragma warning disable CC0031 // Check for null before calling a delegate
            mockActionQueue
               .Setup(q => q.Enqueue(It.IsAny<ModbusReadSamplesRequest>(), Priority.Normal))
               .Callback<object,Priority>((r,p) => modbusReadSamplesRequest((ModbusReadSamplesRequest)r))
               .Returns(true);
            mockActionQueue
               .Setup(q => q.AddProcessing(It.IsAny<Func<ModbusReadStatisticsRequest, bool>>()))
               .Callback<Func<ModbusReadStatisticsRequest, bool>>(a => modbusReadStatisticsRequest = a)
               .Returns(true);
            mockActionQueue
               .Setup(q => q.Enqueue(It.IsAny<ModbusReadStatisticsRequest>(), Priority.Normal))
               .Callback<object,Priority>((r,p) => modbusReadStatisticsRequest((ModbusReadStatisticsRequest)r))
               .Returns(true);
#pragma warning restore CC0031 // Check for null before calling a delegate

            mockRegisterDecoder.Setup(rd => rd.ReadLastBirdWeights()).Returns(true);
            mockRegisterDecoder.SetupGet(rd => rd.BirdWeights).Returns(birdWeights);
            mockRegisterDecoder.Setup(rd => rd.ReadTodayStatistics()).Returns(true);
            mockRegisterDecoder.SetupGet(rd => rd.Statistics).Returns(statistics);
        }
    }
}
