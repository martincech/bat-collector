﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Services.Client;
using Xunit;

namespace Services.Tests.UnitTests
{
    public class ChannelFactoryExtensionsTests : IDisposable
    {
        private readonly ServiceHost host;
        private static bool _event1Invoked;
        private static bool _event2Invoked;
        private static bool _event3Invoked;
        private IChannelFactory<ITestServiceEvents> factory;

        public ChannelFactoryExtensionsTests()
        {
            host = new ServiceHost(new TestServiceEventsService());
            host.AddServiceEndpoint(typeof(ITestServiceEvents), PublishingService.NetPipeBinding,
               PublishingService.ENDPOINT_NET_PIPE);

            host.Open();
            _event1Invoked = _event2Invoked = _event3Invoked = false;
        }

        public void Dispose()
        {
            factory.Close();
            host.Close();
            GC.SuppressFinalize(this);
        }


        [Fact]
        public void ChannelFactoryTest()
        {
            factory = new ChannelFactory<ITestServiceEvents>(PublishingService.NetPipeBinding,
               PublishingService.ENDPOINT_NET_PIPE);

            factory.RemoteAction(ts => ts.OnEvent1());
            factory.RemoteAction(ts => ts.OnEvent2(0));
            factory.RemoteAction(ts => ts.OnEvent3(0, "a"));
            Assert.True(_event1Invoked);
            Assert.True(_event2Invoked);
            Assert.True(_event3Invoked);
            _event1Invoked = _event2Invoked = _event3Invoked = false;
            factory.RemoteAction(ts => ts.OnEvent1());
            factory.RemoteAction(ts => ts.OnEvent2(0));
            factory.RemoteAction(ts => ts.OnEvent3(0, "a"));
            Assert.True(_event1Invoked);
            Assert.True(_event2Invoked);
            Assert.True(_event3Invoked);
        }


        [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
        public class TestServiceEventsService : ITestServiceEvents
        {
            #region Implementation of ITestServiceEvents

            public void OnEvent1()
            {
                _event1Invoked = true;
            }

            public void OnEvent2(int number)
            {
                _event2Invoked = true;
            }

            public void OnEvent3(int number, string text)
            {
                _event3Invoked = true;
            }

            #endregion
        }
    }
}
