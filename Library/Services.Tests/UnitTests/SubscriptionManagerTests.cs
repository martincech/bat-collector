﻿using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using Services.PublishSubscribe;
using Xunit;

namespace Services.Tests.UnitTests
{
    public class SubscriptionManagerTests : IDisposable
    {
        private readonly SubscribtionClient subscribtionClient;
        private SubscribtionPersistentClient persistentClient;
        private readonly Subscription unitUnderTest;
        private const string DUMMY_ADDRESS = "net.pipe://callback";

        public SubscriptionManagerTests()
        {
            unitUnderTest = new Subscription();
            Subscription.StartService(unitUnderTest);
            subscribtionClient = new SubscribtionClient(
               new InstanceContext(new EventsCallback()),
               PublishingService.NetPipeBinding, new EndpointAddress(Subscription.ENDPOINT));
            CreatePersistentClient();
        }


        public void Dispose()
        {
            subscribtionClient.Close();
            persistentClient.Close();
            Subscription.StopService();
            GC.SuppressFinalize(this);
        }

        private void CreatePersistentClient()
        {
            persistentClient = new SubscribtionPersistentClient(
               PublishingService.NetPipeBinding, new EndpointAddress(Subscription.ENDPOINT_PERSISTENT));
        }

        [Fact]
        public void SubscriptionManager_Created()
        {
            Assert.NotNull(Subscription.Host);
        }


        [Fact]
        public void Subscribe_ThrowException_WhenInvalidAddressSpecified()
        {
            Assert.NotNull(typeof(ITestServiceEvents).GetMethods().First());
            void act() => persistentClient.Subscribe("address", typeof(ITestServiceEvents).GetMethods().First().Name);
            Assert.ThrowsAny<FaultException>((Action)act);
        }

        [Fact]
        public void Subscribe_ThrowException_WhenUnsuportedAddressSpecified()
        {
            Assert.NotNull(typeof(ITestServiceEvents).GetMethods().First());
            void act() => persistentClient.Subscribe("ftp://address", typeof(ITestServiceEvents).GetMethods().First().Name);
            Assert.ThrowsAny<FaultException>((Action)act);
        }

        [Fact]
        public void Subscribe_ThrowException_WhenInvalidOperationSpecified()
        {
            const string name = "StupidName";
            var methodNames = typeof(ITestServiceEvents).GetMethods().Select(info => info.Name).ToList();

            Assert.DoesNotContain(methodNames, (o) => o.Equals(name));
            void act() => persistentClient.Subscribe(DUMMY_ADDRESS, name);
            Assert.ThrowsAny<FaultException>((Action)act);
        }

        [Fact]
        public void Subscribe_And_UnSubscribted_Persistently()
        {
            Assert.NotNull(typeof(ITestServiceEvents).GetMethods().First());
            var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
            persistentClient.Subscribe(DUMMY_ADDRESS, methodName);
            using (var dataContext = new PublishSubscribeDataContext())
            {
                var query = from subscriber in dataContext.Subscribers
                            where subscriber.Address == DUMMY_ADDRESS &&
                                  subscriber.Operation == methodName
                            select subscriber;
                Assert.True(query.Any(), "Method not registered!");
                persistentClient.Close();
                Subscription.StopService();
                Subscription.StartService(unitUnderTest);
                CreatePersistentClient();
                Assert.True(query.Any(), "Method is not saved permanently!");
                persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
                Assert.False(query.Any(), "Method still saved even though unsubscribe called!");
            }
        }

        [Fact]
        public void Subscribe_And_UnSubscribted_Transiently()
        {
            var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
            var transientStore = unitUnderTest.MTransientStore;
            Assert.NotNull(transientStore);
            Assert.True(transientStore.ContainsKey(methodName));
            transientStore.TryGetValue(methodName, out var clientList);
            Assert.NotNull(clientList);
            Assert.False(clientList.Any());

            subscribtionClient.Subscribe(methodName);
            transientStore.TryGetValue(methodName, out clientList);
            Assert.NotNull(clientList);
            Assert.True(clientList.Any(), "Client not subscribted!");

            subscribtionClient.Unsubscribe(methodName);
            transientStore.TryGetValue(methodName, out clientList);
            Assert.NotNull(clientList);
            Assert.False(clientList.Any(), "Client not unsubscribted!");
        }

        [Fact]
        public void InterfaceType_MethodsInherited()
        {
            var transientStore = unitUnderTest.MTransientStore;
            foreach (var methodInfo in typeof(ITestServiceEvents).GetMethods())
            {
                Assert.True(transientStore.ContainsKey(methodInfo.Name));
            }
        }

        [Fact]
        public void GetSubscribersFromAddress_Returs_Subscribtion()
        {
            var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
            var methodName2 = typeof(ITestServiceEvents).GetMethods().Last().Name;
            persistentClient.Subscribe(DUMMY_ADDRESS, methodName);
            persistentClient.Subscribe(DUMMY_ADDRESS, methodName2);

            Assert.Contains(unitUnderTest.GetAllSubscribersFromAddress(DUMMY_ADDRESS), subscription => subscription.Address == DUMMY_ADDRESS && subscription.Operation == methodName);
            Assert.Contains(unitUnderTest.GetAllSubscribersFromAddress(DUMMY_ADDRESS), subscription => subscription.Address == DUMMY_ADDRESS && subscription.Operation == methodName2);
            Assert.True(unitUnderTest.GetAllSubscribersFromAddress(DUMMY_ADDRESS).Count() == 2);
            persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
            persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName2);
        }


        [Fact]
        public void GetAllSubscribers_Returns_Address()
        {
            var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
            persistentClient.Subscribe(DUMMY_ADDRESS, methodName);
            Assert.Contains(unitUnderTest.PersistentSubscribers, subscription => subscription.Address == DUMMY_ADDRESS && subscription.Operation == methodName);
            Assert.True(unitUnderTest.PersistentSubscribers.Count() == 1);
            persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
        }

        [Fact]
        public void GetSubscribersToOperation_Returns_Address()
        {
            var methodName = typeof(ITestServiceEvents).GetMethods().First().Name;
            persistentClient.Subscribe(DUMMY_ADDRESS, methodName);

            Assert.Contains(unitUnderTest.PersistentSubscribersToOperation(methodName), address => address == DUMMY_ADDRESS);
            Assert.True(unitUnderTest.PersistentSubscribersToOperation(methodName).Count() == 1);
            persistentClient.Unsubscribe(DUMMY_ADDRESS, methodName);
        }
    }
}
