﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Services.Tests.UnitTests
{
    public class PublishingService
    {
        public static Binding NetPipeBinding { get; } = new NetNamedPipeBinding
        {
            Security = { Mode = NetNamedPipeSecurityMode.None },
            TransactionFlow = true,
            ReceiveTimeout = TimeSpan.MaxValue
        };

        public const string ENDPOINT_MSMQ = "net.msmq://localhost/private/ITestServiceEventsQueued";
        public const string ENDPOINT_NET_PIPE = "net.pipe://localhost/ITestServiceEvents";

        public static ServiceHost Host { get; set; }
    }
}
