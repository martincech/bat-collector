﻿using System.ServiceModel;

namespace Services.Tests
{
    [ServiceContract]
    public interface ITestServiceEvents
    {
        [OperationContract(IsOneWay = true)]
        void OnEvent1();
        [OperationContract(IsOneWay = true)]
        void OnEvent2(int number);
        [OperationContract(IsOneWay = true)]
        void OnEvent3(int number, string text);
    }

    [ServiceContract]
    public interface ITestServiceEventsInherited : ITestServiceEvents
    {
    }
}
