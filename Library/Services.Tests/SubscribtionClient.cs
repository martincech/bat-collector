﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Xunit;

namespace Services.Tests
{
    internal class SubscribtionClient : DuplexClientBase<ITestContractContract>, ITestContractContract
    {

        public SubscribtionClient(InstanceContext callbackInstance) :
           base(callbackInstance)
        {
        }

        public SubscribtionClient(InstanceContext callbackInstance, string endpointConfigurationName) :
           base(callbackInstance, endpointConfigurationName)
        {
        }

        public SubscribtionClient(InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) :
           base(callbackInstance, endpointConfigurationName, remoteAddress)
        {
        }

        public SubscribtionClient(InstanceContext callbackInstance, string endpointConfigurationName, EndpointAddress remoteAddress) :
           base(callbackInstance, endpointConfigurationName, remoteAddress)
        {
        }

        public SubscribtionClient(InstanceContext callbackInstance, Binding binding, EndpointAddress remoteAddress) :
           base(callbackInstance, binding, remoteAddress)
        {
        }

        #region Implementation of ISubscriptionContract

        /// <summary>
        /// Subscribe for all events
        /// </summary>
        public void Subscribe()
        {
            Channel.Subscribe();
        }

        /// <summary>
        /// Subscribe for concrete event
        /// </summary>
        /// <param name="eventOperation">event operation name</param>
        public void Subscribe(string eventOperation)
        {
            Channel.Subscribe(eventOperation);
        }

        /// <summary>
        /// Unsubscribe for concrete event
        /// </summary>
        /// <param name="eventOperation">event operation name</param>
        public void Unsubscribe(string eventOperation)
        {
            Channel.Unsubscribe(eventOperation);
        }

        /// <summary>
        /// Unsubscribe for all events
        /// </summary>
        public void Unsubscribe()
        {
            Channel.Unsubscribe();
        }

        #endregion

        public new void Close()
        {
            if (State == CommunicationState.Opened)
            {
                base.Close();
            }
            else
            {
                Abort();
            }
            Assert.True(State == CommunicationState.Closed);
        }
    }
}
