﻿using System;
using System.ServiceModel;
using System.Threading;
using Services.PublishSubscribe;
using Services.PublishSubscribe.Contracts;
using Services.Tests.UnitTests;
using Xunit;

namespace Services.Tests
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
    internal class Subscription
      : SubscriptionManager<ITestServiceEvents>,
      ITestContractContract
    {
        #region Public stuff

        public const string ENDPOINT = "net.pipe://localhost/ITestContractContract";
        public const string ENDPOINT_PERSISTENT = "net.pipe://localhost/IPersistentSubscriptionContract";

        public static ServiceHost Host;

        #endregion


        public Subscription()
        {
        }

        public static void StartService(Subscription sub)
        {
            if (Host == null)
            {
                try
                {
                    Host = new ServiceHost(sub);
                    Host.AddServiceEndpoint(typeof(ITestContractContract), PublishingService.NetPipeBinding, ENDPOINT);
                    Host.AddServiceEndpoint(typeof(IPersistentSubscriptionContract), PublishingService.NetPipeBinding, ENDPOINT_PERSISTENT);
                    Host.Open();
                }
                catch (Exception ex)
                {
                    Assert.True(false, $"Exception when creating server: {ex.Message}");
                }
            }
            Assert.True(Host.State == CommunicationState.Opened);
        }

        public static void StopService()
        {
            if (Host == null)
            {
                return;
            }
            using (var ev = new ManualResetEvent(false))
            {
                var handler = new EventHandler(delegate { ev.Set(); });
                Host.Closed += handler;
                Host.Close();
                ev.WaitOne();
                Host.Closed -= handler;
                Assert.True(Host.State == CommunicationState.Closed);
                Host = null;
            }
        }
    }
}
