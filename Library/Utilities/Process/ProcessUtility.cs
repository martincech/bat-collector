﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;

namespace Utilities.Process
{
    public class ProcessUtility
    {
        const int MAX_INSTALL_DELAY = 1000 * 60 * 2;
        const string WIN_INSTALLER = "msiexec";
        const string PRODUCTS_REG_KEY = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Products";

        [Flags]
        public enum AccessTokenRights : uint
        {
            STANDARD_RIGHTS_READ = 0x00020000,
            TOKEN_ASSIGN_PRIMARY = 0x0001,
            TOKEN_DUPLICATE = 0x0002,
            TOKEN_IMPERSONATE = 0x0004,
            TOKEN_QUERY = 0x0008,
            TOKEN_QUERY_SOURCE = 0x0010,
            TOKEN_ADJUST_PRIVILEGES = 0x0020,
            TOKEN_ADJUST_GROUPS = 0x0040,
            TOKEN_ADJUST_DEFAULT = 0x0080,
            TOKEN_ADJUST_SESSIONID = 0x0100,
            TOKEN_READ = (STANDARD_RIGHTS_READ | TOKEN_QUERY),
            TOKEN_ALL_ACCESS = TOKEN_ASSIGN_PRIMARY |
                               TOKEN_DUPLICATE | TOKEN_IMPERSONATE | TOKEN_QUERY | TOKEN_QUERY_SOURCE |
                               TOKEN_ADJUST_PRIVILEGES | TOKEN_ADJUST_GROUPS | TOKEN_ADJUST_DEFAULT |
                               TOKEN_ADJUST_SESSIONID
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct StartupInfo
        {
            public int Cb { get; set; }
            public string LpReserved { get; set; }
            public string LpDesktop { get; set; }
            public string LpTitle { get; set; }
            public int DwX { get; set; }
            public int DwY { get; set; }
            public int DwXSize { get; set; }
            public int DwXCountChars { get; set; }
            public int DwYCountChars { get; set; }
            public int DwFillAttribute { get; set; }
            public int DwFlags { get; set; }
            public short WShowWindow { get; set; }
            public short CbReserved2 { get; set; }
            public IntPtr LpReserved2 { get; set; }
            public IntPtr HStdInput { get; set; }
            public IntPtr HStdOutput { get; set; }
            public IntPtr HStdError { get; set; }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ProcessInformation
        {
            public IntPtr HProcess { get; set; }
            public IntPtr HThread { get; set; }
            public int DwProcessID { get; set; }
            public int DwThreadID { get; set; }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SecurityAttributes
        {
            public int Length { get; set; }
            public IntPtr LpSecurityDescriptor { get; set; }
            public bool BInheritHandle { get; set; }
        }

        public enum SecurityImpersonationLevel
        {
            SecurityAnonymous,
            SecurityIdentification,
            SecurityImpersonation,
            SecurityDelegation
        }


        public enum Logon
        {
            WithProfile = 1,
            NetCredentialsOnly
        }

        public enum Creation
        {
            DefaultErrorMode = 0x04000000,
            NewConsole = 0x00000010,
            NewProcessGroup = 0x00000200,
            SeparateWOWVDM = 0x00000800,
            Suspended = 0x00000004,
            UnicodeEnvironment = 0x00000400,
            ExtendedStartupInfoPresent = 0x00080000
        }

        public static bool CloseHandleWrapper(IntPtr handle)
        {
            if (handle == IntPtr.Zero)
            {
                throw new ArgumentNullException(nameof(handle));
            }
            return CloseHandle(handle);
        }

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.StdCall)]
        [SuppressUnmanagedCodeSecurity]
        private static extern bool CloseHandle(
           IntPtr handle
        );


        public static bool DuplicateTokenWrapper(SafeHandle hExistingToken,
           int dwDesiredAccess,
           ref SecurityAttributes lpThreadAttributes,
           int impersonationLevel,
           int dwTokenType,
           out SafeFileHandle phNewToken)
        {
            if (hExistingToken == null)
            {
                throw new ArgumentNullException(nameof(hExistingToken));
            }
            return DuplicateTokenEx(hExistingToken, dwDesiredAccess, ref lpThreadAttributes, impersonationLevel, dwTokenType, out phNewToken);
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        [SuppressUnmanagedCodeSecurity]
        private static extern bool DuplicateTokenEx(
           SafeHandle hExistingToken,
           int dwDesiredAccess,
           ref SecurityAttributes lpThreadAttributes,
           int ImpersonationLevel,
           int dwTokenType,
           out SafeFileHandle phNewToken
        );


        public static bool OpenProcessTokenWrapper(IntPtr processHandle, uint desiredAccess, out SafeFileHandle tokenHandle)
        {
            if (processHandle == IntPtr.Zero)
            {
                throw new ArgumentNullException(nameof(processHandle));
            }
            return OpenProcessToken(processHandle, desiredAccess, out tokenHandle);
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool OpenProcessToken(
           IntPtr ProcessHandle,
           uint DesiredAccess,
           out SafeFileHandle TokenHandle
        );


        public static bool CreateProcessAsUserWrapper(
            ProcessArguments args,
            ref SecurityAttributes lpProcessAttributes,
            ref SecurityAttributes lpThreadAttributes,
            ref StartupInfo lpStartupInfo,
            out ProcessInformation lpProcessInformation)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }
            if (args.HToken == null)
            {
                throw new MemberAccessException(nameof(args.HToken));
            }
            if (string.IsNullOrEmpty(args.LpApplicationName))
            {
                throw new MemberAccessException(nameof(args.LpApplicationName));
            }
            return CreateProcessAsUser(args.HToken,
                                       args.LpApplicationName,
                                       args.LpCommandLine,
                                       ref lpProcessAttributes,
                                       ref lpThreadAttributes,
                                       args.BInheritHandles,
                                       args.DwCreationFlags,
                                       args.LpEnvironment,
                                       args.LpCurrentDirectory,
                                       ref lpStartupInfo,
                                       out lpProcessInformation);
        }

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode, EntryPoint = "CreateProcessAsUserW", CallingConvention = CallingConvention.StdCall)]
        [SuppressUnmanagedCodeSecurity]
        private static extern bool CreateProcessAsUser(
            SafeHandle hToken,
            string lpApplicationName,
            string lpCommandLine,
            ref SecurityAttributes lpProcessAttributes,
            ref SecurityAttributes lpThreadAttributes,
            bool bInheritHandles,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref StartupInfo lpStartupInfo,
            out ProcessInformation lpProcessInformation
        );

        public static System.Diagnostics.Process GetSingleProcess(string name)
        {
            try
            {
                var procesess = System.Diagnostics.Process.GetProcessesByName(name);
                var count = procesess.Length;
                if (count == 0)
                {
                    return null;
                }
                return count > 1 ? null : procesess[0];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool StopProcess(string name)
        {
            var proc = GetSingleProcess(name);
            if (proc == null)
            {
                return false;
            }
            else
            {
                proc.Kill();
                proc.WaitForExit(MAX_INSTALL_DELAY);
                proc.Dispose();
            }

            return GetSingleProcess(name) == null;
        }

        public static void LaunchApp(string fileName, string arguments = "", ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal)
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = arguments,
                WindowStyle = windowStyle
            };

            using (var exeProcess = System.Diagnostics.Process.Start(startInfo))
            {
                exeProcess.WaitForExit(MAX_INSTALL_DELAY);
            }
        }

        public static void InstallMsi(string fullPath, string installPath = null)
        {
            var arguments = "/qn";
            if (!string.IsNullOrEmpty(installPath))
            {
                arguments += $" TARGETDIR=\"{installPath}\"";
            }

            LaunchApp(WIN_INSTALLER, $"/i \"{fullPath}\" {arguments}");
        }

        public static void UninstallMsi(string guid)
        {
            LaunchApp(WIN_INSTALLER, $"/x {guid} /qn");
        }

        public static InstallApplicationProperties GetInstallApplicationProperties(string applicationName)
        {
            var applicationProperties = new InstallApplicationProperties
            {
                DisplayName = applicationName
            };

            //works on 32 and 64 bit OS
            var key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64).OpenSubKey(PRODUCTS_REG_KEY);

            foreach (var tempKeyName in key.GetSubKeyNames())
            {
                var tempKey = key.OpenSubKey(tempKeyName + "\\InstallProperties");
                if (tempKey != null &&
                    string.Equals(Convert.ToString(tempKey.GetValue("DisplayName")), applicationName, StringComparison.CurrentCultureIgnoreCase))
                {
                    var uninstallStr = Convert.ToString(tempKey.GetValue("UninstallString"));
                    uninstallStr = uninstallStr.Replace("/I", "");
                    uninstallStr = uninstallStr.Replace("MsiExec.exe", "").Trim();

                    applicationProperties.DisplayVersion = Convert.ToString(tempKey.GetValue("DisplayVersion"));
                    applicationProperties.InstallLocation = Convert.ToString(tempKey.GetValue("InstallLocation"));
                    applicationProperties.UninstallString = uninstallStr;
                    break;
                }
            }
            return applicationProperties;
        }

        public static void RunAsUser(string applicationPath)
        {
            var processInfo = new ProcessInformation();
            try
            {
                if (!File.Exists(applicationPath)) return;
                var uiProcess = GetSingleProcess("explorer");
                var duplTokenHandle = GetProcessToken(uiProcess.Handle, out var lpTokenAttributes, out var startInfo);

                var procArgs = new ProcessArguments
                {
                    HToken = duplTokenHandle,
                    LpApplicationName = applicationPath,
                    LpCurrentDirectory = Path.GetPathRoot(applicationPath)
                };

                if (!CreateProcessAsUserWrapper(
                        procArgs,
                         ref lpTokenAttributes,
                         ref lpTokenAttributes,
                         ref startInfo,
                         out processInfo
                         ))
                {
                    throw new InvalidOperationException($"Could not create process as user. Win32 Error Code: {Marshal.GetLastWin32Error()}");
                }
            }
            finally
            {   //close handles
                if (processInfo.HProcess != IntPtr.Zero)
                {
                    CloseHandleWrapper(processInfo.HProcess);
                }
                if (processInfo.HThread != IntPtr.Zero)
                {
                    CloseHandleWrapper(processInfo.HThread);
                }
            }
        }

        static SafeFileHandle GetProcessToken(IntPtr processHandle, out ProcessUtility.SecurityAttributes lpTokenAttributes, out StartupInfo startInfo)
        {
            if (!OpenProcessTokenWrapper(processHandle, (uint)AccessTokenRights.TOKEN_ALL_ACCESS, out var tokenHandle))
            {
                throw new InvalidOperationException($"Could not get process token. Win32 Error Code: {Marshal.GetLastWin32Error()}");
            }

            startInfo = new StartupInfo();
            startInfo.Cb = Marshal.SizeOf(startInfo);

            // Setting security attributes
            lpTokenAttributes = new ProcessUtility.SecurityAttributes();
            lpTokenAttributes.Length = Marshal.SizeOf(lpTokenAttributes);
            lpTokenAttributes.LpSecurityDescriptor = IntPtr.Zero;
            lpTokenAttributes.BInheritHandle = true;

            if (!DuplicateTokenWrapper(tokenHandle, (int)AccessTokenRights.TOKEN_ALL_ACCESS,
                     ref lpTokenAttributes, (int)SecurityImpersonationLevel.SecurityImpersonation,
                     (int)AccessTokenRights.TOKEN_ASSIGN_PRIMARY, out var duplTokenHandle))
            {
                throw new InvalidOperationException($"Could not duplicate process token. Win32 Error Code: {Marshal.GetLastWin32Error()}");
            }

            return duplTokenHandle;
        }
    }
}
