﻿using System;
using System.Runtime.InteropServices;

namespace Utilities.Process
{
    public class ProcessArguments
    {
        public ProcessArguments()
        {
            LpCommandLine = string.Empty;
            LpEnvironment = IntPtr.Zero;
        }

        public SafeHandle HToken { get; set; }
        public string LpApplicationName { get; set; }
        public string LpCommandLine { get; set; }
        public bool BInheritHandles { get; set; }
        public uint DwCreationFlags { get; set; }
        public IntPtr LpEnvironment { get; set; }
        public string LpCurrentDirectory { get; set; }
    }
}
