﻿namespace Utilities.Process
{
    public class InstallApplicationProperties
    {
        public string DisplayName { get; set; }
        public string DisplayVersion { get; set; }
        public string InstallLocation { get; set; }
        public string UninstallString { get; set; }
    }
}
