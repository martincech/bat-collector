﻿using System;

namespace Utilities.Timer
{
    public class TimerAdapter : System.Timers.Timer, ITimer
    {
        public static Func<ITimer> Factory
        {
            get { return () => new TimerAdapter(); }
        }
    }
}
