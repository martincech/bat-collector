﻿using System.Timers;

namespace Utilities.Timer
{
    /// <summary>
    /// Purpose of this class is merely to provide and interface to the Timer like functionality to be able to test classes which use timers
    /// <see cref="TimerAdapter"/> which implements this interface
    /// </summary>
    public interface ITimer
    {
        void Start();
        void Stop();
        double Interval { get; set; }
        bool AutoReset { get; set; }
        bool Enabled { get; set; }

        event ElapsedEventHandler Elapsed;
    }
}
