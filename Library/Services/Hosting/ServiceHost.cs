﻿using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using Services.DataContracts;
using Services.QueuedProcessing;

namespace Services.Hosting
{
    /// <summary>
    /// Strongly typed service host with additional functionality such as enabling MEX, installing specific resolvers for data contracts, creating queues for <see cref="NetMsmqBinding"/>.
    /// </summary>
    /// <typeparam name="T">Service contract to be used by this <see cref="ServiceHost"/></typeparam>
    public class ServiceHost<T> : ServiceHost
    {

        #region Additional public interface methods

        /// <summary>
        /// Enable meta data exchange for this contract. Optionally enable meta data exchanging through HTTP.
        /// Can only call before opening the host
        /// </summary>
        /// <param name="enableHttpGet">optionally enable exchange through HTTP protocol</param>
        /// <exception cref="InvalidOperationException">when host is already opened</exception>
        public void EnableMetadataExchange(bool enableHttpGet = true)
        {
            if (State == CommunicationState.Opened)
            {
                throw new InvalidOperationException("Host is already opened");
            }

            if (HasMexEndpoint)
            {
                return;
            }
            var metadataBehavior = Description.Behaviors.Find<ServiceMetadataBehavior>();

            if (metadataBehavior == null)
            {
                metadataBehavior = new ServiceMetadataBehavior();
                Description.Behaviors.Add(metadataBehavior);

                if (BaseAddresses.Any(uri => uri.Scheme == "http"))
                {
                    metadataBehavior.HttpGetEnabled = enableHttpGet;
                }

                if (BaseAddresses.Any(uri => uri.Scheme == "https"))
                {
                    metadataBehavior.HttpsGetEnabled = enableHttpGet;
                }
            }
            AddAllMexEndPoints();
        }

        /// <summary>
        /// Tells whether this <see cref="ServiceHost"/> has some Mex endpoints or not
        /// </summary>
        public bool HasMexEndpoint
        {
            get
            {
                return Description.Endpoints.Any(endpoint => endpoint.Contract.ContractType == typeof(IMetadataExchange));
            }
        }

        /// <summary>
        /// Get <see cref="ServiceThrottle"/>
        /// </summary>
        public ServiceThrottle Throttle
        {
            get
            {
                var dispatcher = OperationContext.Current.Host.ChannelDispatchers[0] as ChannelDispatcher;
                Debug.Assert(dispatcher != null);
                return dispatcher.ServiceThrottle;
            }
        }

        /// <summary>
        /// Enable/disable including exceptions in faults messages for this host.
        /// Can only call before opening the host
        /// </summary>
        public bool IncludeExceptionDetailInFaults
        {
            set
            {
                if (State == CommunicationState.Opened)
                {
                    throw new InvalidOperationException("Host is already opened");
                }
                var debuggingBehavior = Description.Behaviors.Find<ServiceBehaviorAttribute>();
                debuggingBehavior.IncludeExceptionDetailInFaults = value;
            }
            get
            {
                var debuggingBehavior = Description.Behaviors.Find<ServiceBehaviorAttribute>();
                return debuggingBehavior.IncludeExceptionDetailInFaults;
            }
        }

        /// <summary>
        /// Enable <see cref="ServiceSecurityAuditBehavior"/>
        /// Can only set before opening the host
        /// </summary>
        public bool SecurityAuditEnabled
        {
            get
            {
                var securityAudit = Description.Behaviors.Find<ServiceSecurityAuditBehavior>();
                if (securityAudit != null)
                {
                    return securityAudit.MessageAuthenticationAuditLevel == AuditLevel.SuccessOrFailure &&
                           securityAudit.ServiceAuthorizationAuditLevel == AuditLevel.SuccessOrFailure;
                }
                return false;
            }
            set
            {
                if (State == CommunicationState.Opened)
                {
                    throw new InvalidOperationException("Host is already opened");
                }
                var securityAudit = Description.Behaviors.Find<ServiceSecurityAuditBehavior>();
                if (securityAudit == null && value)
                {
                    securityAudit = new ServiceSecurityAuditBehavior
                    {
                        MessageAuthenticationAuditLevel = AuditLevel.SuccessOrFailure,
                        ServiceAuthorizationAuditLevel = AuditLevel.SuccessOrFailure
                    };
                    Description.Behaviors.Add(securityAudit);
                }
            }
        }

        /// <summary>
        /// Singleton instance of service for this <see cref="ServiceHost"/>
        /// </summary>
        public virtual T Singleton
        {
            get
            {
                if (SingletonInstance == null)
                {
                    return default;
                }
                Debug.Assert(SingletonInstance is T);
                return (T)SingletonInstance;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ServiceHost class for interface of type <see cref="T"/>.
        /// </summary>
        public ServiceHost() : base(typeof(T))
        {
        }

        /// <summary>
        /// Initializes a new instance of the ServiceHost class for interface of type <see cref="T"/> to listen on specific addresses.
        /// </summary>
        /// <param name="baseAddresses">address to be listen on</param>
        public ServiceHost(params string[] baseAddresses)
           : base(typeof(T), baseAddresses.Select(address => new Uri(address)).ToArray())
        {
        }

        /// <summary>
        /// Initializes a new instance of the ServiceHost class for interface of type <see cref="T"/> to listen on specific addresses.
        /// </summary>
        /// <param name="baseAddresses">address to be listen on</param>
        public ServiceHost(params Uri[] baseAddresses) : base(typeof(T), baseAddresses)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ServiceHost class for interface of type <see cref="T"/> to listen on specific addresses
        /// and using specific singleton as resolver for queries.
        /// </summary>
        /// <param name="singleton">singleton object to be used as resolver</param>
        /// <param name="baseAddresses">address to be listen on</param>
        public ServiceHost(T singleton, params string[] baseAddresses)
           : base(singleton, baseAddresses.Select(address => new Uri(address)).ToArray())
        {
        }

        /// <summary>
        /// Initializes a new instance of the ServiceHost class for interface of type <see cref="T"/>
        ///  using specific singleton as resolver for queries.
        /// </summary>
        /// <param name="singleton">singleton object to be used as resolver</param>
        public ServiceHost(T singleton) : base(singleton)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ServiceHost class for interface of type <see cref="T"/> to listen on specific addresses
        /// and using specific singleton as resolver for queries.
        /// </summary>
        /// <param name="singleton">singleton object to be used as resolver</param>
        /// <param name="baseAddresses">address to be listen on</param>
        public ServiceHost(T singleton, params Uri[] baseAddresses) : base(singleton, baseAddresses)
        {
        }

        #endregion

        #region Private helpers

        private void AddAllMexEndPoints()
        {
            Debug.Assert(!HasMexEndpoint);

            foreach (var baseAddress in BaseAddresses)
            {
                Binding binding = null;
                switch (baseAddress.Scheme)
                {
                    case "net.tcp":
                        binding = MetadataExchangeBindings.CreateMexTcpBinding();
                        break;
                    case "net.pipe":
                        binding = MetadataExchangeBindings.CreateMexNamedPipeBinding();
                        break;
                    case "http":
                        binding = MetadataExchangeBindings.CreateMexHttpBinding();
                        break;
                    case "https":
                        binding = MetadataExchangeBindings.CreateMexHttpsBinding();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException($"Unexpected Case: {baseAddress.Scheme}");
                }
                if (binding != null)
                {
                    AddServiceEndpoint(typeof(IMetadataExchange), binding, "MEX");
                }
            }
        }

        [Conditional("DEBUG")]
        private void PurgeQueues()
        {
            foreach (var endpoint in Description.Endpoints)
            {
                endpoint.PurgeQueue();
            }
        }

        #endregion

        #region ServiceHost overrides

        protected override void OnOpening()
        {
            this.AddGenericResolver();

            foreach (var endpoint in Description.Endpoints)
            {
                endpoint.VerifyQueue();
            }
            base.OnOpening();
        }

        protected override void OnClosing()
        {
            PurgeQueues();
            base.OnClosing();
        }

        #endregion

    }
}
