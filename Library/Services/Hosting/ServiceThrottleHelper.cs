﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Services.Hosting
{
    /// <summary>
    /// Bunch of extension methods for <see cref="ServiceHost"/> to set throttling behavior.
    /// </summary>
    public static class ServiceThrottleHelper
    {
        ///         ///<summary>
        ///  Can only call before opening the host
        ///</summary>
        /// <param name="host"></param>
        /// <param name="maxCalls"></param>
        /// <param name="maxSessions"></param>
        /// <param name="maxInstances"></param>
        public static void SetThrottle(this ServiceHost host, int maxCalls, int maxSessions, int maxInstances)
        {
            var throttle = new ServiceThrottlingBehavior
            {
                MaxConcurrentCalls = maxCalls,
                MaxConcurrentSessions = maxSessions,
                MaxConcurrentInstances = maxInstances
            };
            host.SetThrottle(throttle);
        }

        /// <summary>
        /// Can only call before opening the host
        /// </summary>
        /// <param name="host"></param>
        /// <param name="serviceThrottle"></param>
        /// <param name="overrideConfig"></param>
        public static void SetThrottle(this ServiceHost host, ServiceThrottlingBehavior serviceThrottle, bool overrideConfig)
        {
            if (host.State == CommunicationState.Opened)
            {
                throw new InvalidOperationException("Host is already opened");
            }
            var throttle = host.Description.Behaviors.Find<ServiceThrottlingBehavior>();
            if (throttle == null)
            {
                host.Description.Behaviors.Add(serviceThrottle);
                return;
            }
            if (!overrideConfig)
            {
                return;
            }
            host.Description.Behaviors.Remove(throttle);
            host.Description.Behaviors.Add(serviceThrottle);
        }

        /// <summary>
        /// Can only call before opening the host. Does not override configuration values if present
        /// </summary>
        /// <param name="host"></param>
        /// <param name="serviceThrottle"></param>
        public static void SetThrottle(this ServiceHost host, ServiceThrottlingBehavior serviceThrottle)
        {
            host.SetThrottle(serviceThrottle, false);
        }
    }
}
