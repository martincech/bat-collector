﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using NLog;
using Services.DataContracts;

namespace Services.Hosting
{
    /// <summary>
    /// InProcFactory is designed to streamline and automate in-proc hosting. It takes a service and hosts it up as a WCF service using <see cref="ServiceHost"/>.
    /// It uses <see cref="NetNamedPipeBinding"/> for communication between client and server side.
    /// </summary>
    public static class InProcFactory
    {
        #region Properties/fields

        internal static readonly string BaseAddress = "net.pipe://localhost/" + Guid.NewGuid();

        internal static readonly Binding Binding;

        internal static readonly Dictionary<Type, Dictionary<Type, Tuple<ServiceHost, EndpointAddress>>> Hosts =
           new Dictionary<Type, Dictionary<Type, Tuple<ServiceHost, EndpointAddress>>>();

        internal static readonly Dictionary<Type, ServiceThrottlingBehavior> Throttles =
           new Dictionary<Type, ServiceThrottlingBehavior>();

        internal static readonly Dictionary<Type, object> Singletons = new Dictionary<Type, object>();

        internal static readonly Dictionary<Type, List<ICommunicationObject>> Clients = new Dictionary<Type, List<ICommunicationObject>>();

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public access

        /// <summary>
        /// Create strongly typed <see cref="IClientFactory&lt;S, I&gt;"/> for creating in-proc services.
        /// </summary>
        /// <typeparam name="S">type of service class to use for specific contract</typeparam>
        /// <typeparam name="I">service contract type</typeparam>
        /// <returns>new factory</returns>
        public static IClientFactory<S, I> CreateClientFactory<S, I>()
           where S : class, I
           where I : class
        {
            return new InProcFactory<S, I>();
        }

        /// <summary>
        /// Stops all services which were created up until now. It is not necessary to call this function, since it is automatically executed when application process ends.
        /// </summary>
        /// <typeparam name="S">type of service to be stopped</typeparam>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void StopService<S>()
           where S : class
        {
            // close clients connected to host
            foreach (
               var clientType in
                  Hosts.Where(kv => kv.Key == typeof(S)).Select(kv => kv.Value).SelectMany(dict => dict.Keys))
            {
                while (Clients.ContainsKey(clientType))
                {
                    var closeMethod = typeof(InProcFactory).GetMethod(nameof(CloseClient), BindingFlags.NonPublic | BindingFlags.Static)
                       .MakeGenericMethod(clientType);
                    closeMethod.Invoke(null, new object[] { Clients[clientType].First() });
                }
            }

            // close service host itself
            foreach (
               var record in
                  Hosts.Where(kv => kv.Key == typeof(S)).Select(kv => kv.Value).SelectMany(endpoint => endpoint.Values))
            {
                try
                {
                    switch(record.Item1.State)
                    {
                        case CommunicationState.Faulted:
                            record.Item1.Abort();
                            break;
                        default:
                            record.Item1.Close();
                            break;
                    }
                }
                catch (Exception e)
                {
                    record.Item1.Abort();
                    logger.Warn(e, "Stop service exception");
                }
            }
            Hosts.Remove(typeof(S));
        }

        #endregion

        #region Static constructor

        static InProcFactory()
        {
            try
            {
                Binding = new NetNamedPipeBinding(typeof(InProcFactory).Name);
            }
            catch (Exception)
            {
                Binding = new NetNamedPipeBinding();
            }
            finally
            {
                ((NetNamedPipeBinding)Binding).TransactionFlow = true;
            }

            AppDomain.CurrentDomain.ProcessExit += (sender, args) => StopAllServices();
        }

        #endregion

        #region Private & internal methods

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal static void StopAllServices()
        {
            while (Hosts.Any())
            {
                var stopServiceMethod =
                   typeof(InProcFactory).GetMethod(nameof(StopService), BindingFlags.Public | BindingFlags.Static)
                      .MakeGenericMethod(Hosts.First().Key);
                stopServiceMethod.Invoke(null, null);
            }
        }

        #region Creation methods

        /// <summary>
        /// Get address of server for connection. Create and open <see cref="ServiceHost"/> when
        /// service is not running yet.
        /// </summary>
        /// <typeparam name="S">service server</typeparam>
        /// <typeparam name="I">contract</typeparam>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        internal static EndpointAddress GetAddress<S, I>()
           where I : class
           where S : class, I
        {
            while (true)
            {
                if (Hosts.ContainsKey(typeof(S)))
                {
                    Debug.Assert(Hosts[typeof(S)].ContainsKey(typeof(I)));

                    // end of cycle
                    return Hosts[typeof(S)][typeof(I)].Item2;
                }

                ServiceHost<S> host;
                if (Singletons.ContainsKey(typeof(S)))
                {
                    var singleton = Singletons[typeof(S)] as S;
                    Debug.Assert(singleton != null);
                    host = new ServiceHost<S>(singleton);
                }
                else
                {
                    host = new ServiceHost<S>();
                }

                var contracts = GetContracts<S>();
                Debug.Assert(contracts.Any());

                Hosts[typeof(S)] = new Dictionary<Type, Tuple<ServiceHost, EndpointAddress>>();

                foreach (var contract in contracts)
                {
                    var address = BaseAddress + Guid.NewGuid() + "_" + contract;

                    Hosts[typeof(S)][contract] = new Tuple<ServiceHost, EndpointAddress>(host, new EndpointAddress(address));
                    host.AddServiceEndpoint(contract, Binding, address);
                }

                if (Throttles.ContainsKey(typeof(S)))
                {
                    host.SetThrottle(Throttles[typeof(S)]);
                }
                host.Open();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal static I CreateClient<S, I>()
           where I : class
           where S : class, I
        {
            var factory = new ChannelFactory<I>(Binding, GetAddress<S, I>());
            factory.AddGenericResolver();
            var channel = factory.CreateChannel();
            AddClient<I>(channel as ICommunicationObject);
            return channel;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal static I CreateClient<S, I>(InstanceContext context)
           where I : class
           where S : class, I
        {
            var factory = new DuplexChannelFactory<I>(context, Binding, GetAddress<S, I>());
            factory.AddGenericResolver();
            var channel = factory.CreateChannel();
            AddClient<I>(channel as ICommunicationObject);
            return channel;
        }

        #endregion

        #region Destroy methods

        [MethodImpl(MethodImplOptions.Synchronized)]
        internal static void CloseClient<I>(I client)
           where I : class
        {
            var proxy = client as ICommunicationObject;
            Debug.Assert(proxy != null);
            try
            {
                switch (proxy.State)
                {
                    case CommunicationState.Opened:
                        proxy.Close();
                        break;
                    case CommunicationState.Faulted:
                        proxy.Abort();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException($"Unexpected Case: {proxy.State}");
                }
            }
            catch (Exception)
            {
                proxy.Abort();
            }
            RemoveClient<I>(proxy);
        }

        #endregion

        #region Private helpers

        private static Type[] GetContracts<S>()
        {
            Debug.Assert(typeof(S).IsClass);

            var interfaces = typeof(S).GetInterfaces();

            return
               interfaces.Where(type => type.GetCustomAttributes(typeof(ServiceContractAttribute), false).Any()).ToArray();
        }

        private static void AddClient<I>(ICommunicationObject client)
           where I : class
        {
            Debug.Assert(client != null);
            if (!Clients.ContainsKey(typeof(I)))
            {
                Clients.Add(typeof(I), new List<ICommunicationObject>());
            }
            Clients[typeof(I)].Add(client);
        }

        private static void RemoveClient<I>(ICommunicationObject client)
           where I : class
        {
            Debug.Assert(client != null);
            if (Clients.ContainsKey(typeof(I)))
            {
                Clients[typeof(I)].Remove(client);
                if (Clients[typeof(I)].Count == 0)
                {
                    Clients.Remove(typeof(I));
                }
            }
        }

        #endregion

        #endregion
    }

    /// <summary>
    /// Strongly typed implementation of <see cref="IClientFactory&lt;S, I&gt;"/> to be created by non-generic <see cref="InProcFactory"/>
    /// </summary>
    /// <typeparam name="S"></typeparam>
    /// <typeparam name="I"></typeparam>
    internal class InProcFactory<S, I> : IClientFactory<S, I>
       where S : class, I
       where I : class
    {
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SetThrottle(ServiceThrottlingBehavior throttle)
        {
            if (InProcFactory.Hosts.ContainsKey(typeof(S)))
            {
                throw new InvalidOperationException("Host is already opened");
            }
            InProcFactory.Throttles[typeof(S)] = throttle;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void MaxThrottle()
        {
            SetThrottle(int.MaxValue, int.MaxValue, int.MaxValue);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SetThrottle(int maxCalls, int maxSessions, int maxInstances)
        {
            var throttle = new ServiceThrottlingBehavior
            {
                MaxConcurrentCalls = maxCalls,
                MaxConcurrentSessions = maxSessions,
                MaxConcurrentInstances = maxInstances
            };
            SetThrottle(throttle);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SetSingleton(S singleton)
        {
            if (InProcFactory.Hosts.ContainsKey(typeof(S)))
            {
                throw new InvalidOperationException("Host is already opened");
            }
            if (InProcFactory.Singletons.ContainsKey(typeof(S)))
            {
                InProcFactory.Singletons.Remove(typeof(S));
            }
            InProcFactory.Singletons.Add(typeof(S), singleton);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public I CreateInstance()
        {
            return InProcFactory.CreateClient<S, I>();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public I CreateInstance(InstanceContext context)
        {
            return InProcFactory.CreateClient<S, I>(context);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public I CreateInstance(object callback)
        {
            var context = new InstanceContext(callback);
            return CreateInstance(context);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void CloseProxy(I instance)
        {
            InProcFactory.CloseClient(instance);
        }
    }
}
