﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Services.Hosting
{
    /// <summary>
    /// Client factory interface, contains methods for creating a client proxy for some service.
    /// <typeparam name="I">Contract interface</typeparam>
    /// </summary>
    public interface IClientFactory<in S, I>
       where S : class, I
       where I : class
    {
        /// <summary>
        /// Set throttling for all created instances of <see cref="I"/>
        /// Can only call before creating any instance of the service
        /// </summary>
        /// <param name="throttle">Throttle to use</param>
        /// <exception cref="InvalidOperationException">when host is already opened</exception>
        void SetThrottle(ServiceThrottlingBehavior throttle);

        /// <summary>
        /// Can only call before creating any instance of the service
        /// </summary>
        /// <param name="maxCalls">Maximum available calls</param>
        /// <param name="maxSessions">Maximum available sessions</param>
        /// <param name="maxInstances">Maximum available instances</param>
        /// <exception cref="InvalidOperationException">when host is already opened</exception>
        void SetThrottle(int maxCalls, int maxSessions, int maxInstances);

        /// <summary>
        /// Set maximum possible throttling for all created instances of <see cref="I"/>
        /// Can only call before creating any instance of the service
        /// </summary>
        /// <exception cref="InvalidOperationException">when host is already opened</exception>
        void MaxThrottle();


        /// <summary>
        /// Set singleton service object to be used by all created clients.
        /// Can only call before creating any instance of the service
        /// </summary>
        /// <param name="singleton">singleton service object to be used</param>
        /// <exception cref="InvalidOperationException">when host is already opened</exception>
        void SetSingleton(S singleton);

        /// <summary>
        /// Create instance of non-duplex client proxy.
        /// </summary>
        /// <returns>Created client</returns>
        I CreateInstance();

        /// <summary>
        /// Create instance of duplex client proxy.
        /// </summary>
        /// <param name="context">callback context</param>
        /// <returns>Created client</returns>
        I CreateInstance(InstanceContext context);

        /// <summary>
        /// Create instance of duplex client proxy.
        /// </summary>
        /// <param name="callback">callback object</param>
        /// <returns>Created client</returns>
        I CreateInstance(object callback);

        /// <summary>
        /// Close previously created client proxy.
        /// </summary>
        /// <param name="instance">Client proxy to be closed</param>
        void CloseProxy(I instance);
    }
}
