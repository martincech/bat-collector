﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Channels;
using NLog;
using Services.Bindings;
using Services.Extensions;
using Services.PublishSubscribe.Contracts;

namespace Services.PublishSubscribe
{
    /// <summary>
    ///    SubscriptionManager for set of subscribers in Subscribe/Publish pattern.
    /// </summary>
    /// <typeparam name="T">Type of service to use subscription manager for</typeparam>
    [BindingRequirement(TransactionFlowEnabled = true)]
    public abstract class SubscriptionManager<T> :
       ISubscriptionContract,
       IPersistentSubscriptionContract
       where T : class
    {
        #region Private Fields

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        ///    Transient storage for transient subscribers.
        ///    Key is name of method from <see cref="T" />, Value are subscribed client of type <see cref="T" />
        /// </summary>
        internal readonly Dictionary<string, List<T>> MTransientStore;

        internal string ContractName
        {
            get { return typeof(T).Name; }
        }

        private readonly bool servesPersistent;
        private readonly string servesPersistentErrorMessage;

        #endregion

        #region Constructors

        protected SubscriptionManager()
        {
            MTransientStore = new Dictionary<string, List<T>>();
            var methods = GetOperations();
            foreach (var methodName in methods)
            {
                MTransientStore.Add(methodName, new List<T>());
            }

            try
            {
                using (var db = new PublishSubscribeDataContext())
                {
                    db.Database.CreateIfNotExists();
                }
                servesPersistent = true;
            }
            catch (Exception e)
            {
                servesPersistentErrorMessage = e.Message;
                servesPersistent = false;
            }
        }

        #endregion

        #region Transient subscriptions management

        /// <summary>
        ///    Subscribe for all events
        /// </summary>
        public void Subscribe()
        {
            var methods = GetOperations();
            foreach (var method in methods)
                Subscribe(method);
        }

        /// <summary>
        ///    Subscribe client for operation callback.
        /// </summary>
        /// <param name="eventOperation">event to be subscribed or empty for all events </param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Subscribe(string eventOperation)
        {
            VerifyOperation(eventOperation);
            var subscriber = CurrentCallBack;
            if (!string.IsNullOrEmpty(eventOperation))
            {
                AddTransient(subscriber, eventOperation);
            }
            else
            {
                var methods = GetOperations();
                foreach (var methodName in methods)
                {
                    AddTransient(subscriber, methodName);
                }
            }
        }

        private static T CurrentCallBack
        {
            get { return OperationContext.Current.GetCallbackChannel<T>(); }
        }

        /// <summary>
        ///    Unsubscripted client for operation callback.
        /// </summary>
        /// <param name="eventOperation">event to be unsubscripted or empty for all events </param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Unsubscribe(string eventOperation)
        {
            VerifyOperation(eventOperation);
            var subscriber = CurrentCallBack;
            if (!string.IsNullOrEmpty(eventOperation))
            {
                RemoveTransient(subscriber, eventOperation);
            }
            else
            {
                var methods = GetOperations();
                foreach (var methodName in methods)
                {
                    RemoveTransient(subscriber, methodName);
                }
            }
        }

        /// <summary>
        ///    Unsubscripted for all events
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Unsubscribe()
        {
            var methods = GetOperations();
            foreach (var method in methods)
                Unsubscribe(method);
        }

        public IEnumerable<T> TransientSubscribers
        {
            get
            {
                var ops = GetOperations();
                var res = new List<T>();
                foreach (var op in ops)
                    res.AddRange(TransientSubscribersForOperation(op));
                return res.Distinct();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private IEnumerable<T> TransientSubscribersForOperation(string eventOperation)
        {
            if (string.IsNullOrEmpty(eventOperation) ||
                !MTransientStore.ContainsKey(eventOperation)) return new T[] { };
            var list = MTransientStore[eventOperation];
            return list.ToArray();
        }

        /// <summary>
        ///    Add new subscriber to list of all subscribers for specified operation
        /// </summary>
        /// <param name="subscriber">Subscriber to be added to list</param>
        /// <param name="eventOperation">operation to be subscribed</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void AddTransient(T subscriber, string eventOperation)
        {
            var list = MTransientStore[eventOperation];
            if (list.Contains(subscriber))
                return;
            list.Add(subscriber);
        }

        /// <summary>
        ///    Remove subscriber from list of existing subscribers for specified operation
        /// </summary>
        /// <param name="subscriber">Subscriber to be removed from list</param>
        /// <param name="eventOperation">operation to be unsubscripted </param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void RemoveTransient(T subscriber, string eventOperation)
        {
            var list = MTransientStore[eventOperation];
            list.Remove(subscriber);
        }

        protected void Publish(string methodName, params object[] args)
        {
            PublishTransient(methodName, args);
            PublishPersistent(methodName, args);
        }

        /// <summary>
        ///    Publish for persistent subscribers
        /// </summary>
        /// <param name="methodName">name of method</param>
        /// <param name="args">parameters of method</param>
        private void PublishPersistent(string methodName, object[] args)
        {
            var subscribers = GetPersistentList(methodName);
            Publish(subscribers, true, methodName, args);
        }

        /// <summary>
        ///    Publish for transient subscribers
        /// </summary>
        /// <param name="methodName">name of method</param>
        /// <param name="args">parameters of method</param>
        private void PublishTransient(string methodName, object[] args)
        {
            var subscribers = TransientSubscribersForOperation(methodName);
            Publish(subscribers, false, methodName, args);
        }

        private void Publish(IEnumerable<T> subscribers, bool persistentSubscribers, string methodName,
           object[] args)
        {
            // start new thread for every subscriber
            foreach (var subscriber in subscribers)
            {
                try
                {
                    Invoke(subscriber, methodName, args);
                    if (!persistentSubscribers) return;
                    if (subscriber is IDisposable sub)
                    {
                        sub.Dispose();
                    }
                }
                catch (TargetInvocationException ex)
                {
                    // subscriber don't live anymore - unsubscripted him
                    if (ClientHangoutException(ex.InnerException))
                    {
                        if (!persistentSubscribers)
                        {
                            RemoveTransient(subscriber, methodName);
                        }
                    }
                    else
                    {
                        logger.Warn(ex, "WCF Publish exception");
                        throw;
                    }
                }
            }
        }

        private static bool ClientHangoutException(Exception ex)
        {
            return ex is ObjectDisposedException ||
                   ex is CommunicationObjectAbortedException ||
                   ex is CommunicationObjectFaultedException;
        }

        private static void Invoke(T subscriber, string methodName, object[] args)
        {
            Debug.Assert(subscriber != null);
            var type = typeof(T);
            var methodInfo = type.GetMethodR(methodName);
            Debug.Assert(methodInfo != null);
            methodInfo.Invoke(subscriber, args);
        }

        #endregion

        #region Persistent subscriptions management

        /// <summary>
        ///    Check database whether it contains some records.
        /// </summary>
        /// <param name="address">address to be checked</param>
        /// <param name="eventsContract">contract name to be checked</param>
        /// <param name="eventOperation">operation name to be checked</param>
        /// <returns>true/false</returns>
        private static bool ContainsPersistent(string address, string eventsContract, string eventOperation)
        {
            using (var dataContext = new PublishSubscribeDataContext())
            {
                try
                {
                    return
                       dataContext.Subscribers.Any(
                          item => item.Address == address && item.Contract == eventsContract &&
                                  item.Operation == eventOperation);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        /// <summary>
        ///    Adds record to database.
        /// </summary>
        /// <param name="address">address of client</param>
        /// <param name="eventsContract">contract to be added</param>
        /// <param name="eventOperation">operation to be added</param>
        private static void AddPersistent(string address, string eventsContract, string eventOperation)
        {
            if (ContainsPersistent(address, eventsContract, eventOperation))
                return;
            using (var dataContext = new PublishSubscribeDataContext())
            {
                dataContext.Subscribers.Add(new Subscriber
                {
                    Address = address,
                    Operation = eventOperation,
                    Contract = eventsContract
                });
                dataContext.SaveChanges();
            }
        }

        /// <summary>
        ///    Remove records from database
        /// </summary>
        /// <param name="address">address of client</param>
        /// <param name="eventsContract">contract to be removed</param>
        /// <param name="eventOperation">operation to be removed</param>
        private static void RemovePersistent(string address, string eventsContract, string eventOperation)
        {
            using (var dataContext = new PublishSubscribeDataContext())
            {
                var removedSubscribers =
                   from subscriber in dataContext.Subscribers
                   where
                   subscriber.Address == address && subscriber.Contract == eventsContract &&
                   subscriber.Operation == eventOperation
                   select subscriber;
                foreach (var removedSubscriber in removedSubscribers)
                    dataContext.Subscribers.Remove(removedSubscriber);
                dataContext.SaveChanges();
            }
        }

        /// <summary>
        ///    Convert Database representation of subscribers to WCF transferable representation
        /// </summary>
        /// <param name="subscribers">list of db objects</param>
        /// <returns>list of wcf objects</returns>
        private static IEnumerable<PersistentSubscription> Convert(IEnumerable<Subscriber> subscribers)
        {
            return subscribers
               .Select(subscriber => new PersistentSubscription
               {
                   Address = subscriber.Address,
                   Operation = subscriber.Operation
               }).ToList();
        }

        /// <summary>
        ///    Get addresses subscribers subscribed to specific contract and operations
        /// </summary>
        /// <param name="eventOperation">operation name</param>
        /// <returns>addresses of subscribers</returns>
        private IEnumerable<string> SubscribersToOperation(string eventOperation)
        {
            IEnumerable<string> subscribers;

            using (var dataContext = new PublishSubscribeDataContext())
            {
                subscribers = (from subscriber in dataContext.Subscribers
                               where subscriber.Contract == ContractName && subscriber.Operation == eventOperation
                               select subscriber.Address).ToArray();
            }

            return subscribers.ToArray();
        }

        internal IEnumerable<T> GetPersistentList(string eventOperation)
        {
            if (!servesPersistent) return new List<T>();
            var addresses = SubscribersToOperation(eventOperation).ToList();

            var subscribers = new List<T>(addresses.Count);

            foreach (var address in addresses)
            {
                var binding = GetBindingFromAddress(address);
                var factory = new ChannelFactory<T>(binding, new EndpointAddress(address));
                var proxy = factory.CreateChannel();
                subscribers.Add(proxy);
            }
            return subscribers;
        }


        /// <summary>
        ///    Get list of all subscribers
        /// </summary>
        /// <returns>subscribers</returns>
        internal IEnumerable<PersistentSubscription> PersistentSubscribers
        {
            get
            {
                if (!servesPersistent) return new List<PersistentSubscription>();
                IEnumerable<Subscriber> subscribers;

                using (var dataContext = new PublishSubscribeDataContext())
                {
                    subscribers = (
                       from subscriber in dataContext.Subscribers
                       where subscriber.Contract == ContractName
                       select subscriber).ToArray();
                }
                return Convert(subscribers);
            }
        }

        /// <summary>
        ///    Get addresses subscribers subscribed to specific contract and operations
        /// </summary>
        /// <param name="eventOperation">operation name</param>
        /// <returns>addresses of subscribers</returns>
        internal IEnumerable<string> PersistentSubscribersToOperation(string eventOperation)
        {
            return !servesPersistent ? new List<string>() : SubscribersToOperation(eventOperation);
        }

        /// <summary>
        ///    Get all subscriptions from specific address
        /// </summary>
        /// <param name="address">address string</param>
        /// <returns>all existing subscriptions</returns>
        internal IEnumerable<PersistentSubscription> GetAllSubscribersFromAddress(string address)
        {
            if (!servesPersistent) return new List<PersistentSubscription>();
            VerifyAddress(address);

            IEnumerable<Subscriber> subscribers;
            using (var dataContext = new PublishSubscribeDataContext())
            {
                subscribers = (from subscriber in dataContext.Subscribers
                               where subscriber.Address == address &&
                                     subscriber.Contract == ContractName
                               select subscriber).ToArray();
            }
            return Convert(subscribers);
        }

        /// <summary>
        ///    Subscribe for all events
        /// </summary>
        /// <param name="address">Address where should events be sent</param>
        [OperationBehavior(TransactionScopeRequired = true)]
        public void SubscribeAll(string address)
        {
            var methods = GetOperations();
            foreach (var method in methods)
                Subscribe(address, method);
        }

        /// <summary>
        ///    Permanently subscribe client from address to specific contract and operation.
        /// </summary>
        /// <param name="address">address of client</param>
        /// <param name="eventOperation">operation from contract to be subscribed</param>
        [OperationBehavior(TransactionScopeRequired = true)]
        public void Subscribe(string address, string eventOperation)
        {
            VerifyAddress(address);
            VerifyOperation(eventOperation);
            VerifyPersistency();
            if (!string.IsNullOrEmpty(eventOperation))
            {
                AddPersistent(address, ContractName, eventOperation);
            }
            else
            {
                var methods = GetOperations();
                void addPersistent(string methodName) => AddPersistent(address, ContractName, methodName);
                foreach (var item in methods)
                {
                    addPersistent(item);
                }
            }
        }


        /// <summary>
        ///    Permanently unsubscripted client from address from specific contract and operation.
        /// </summary>
        /// <param name="address">address of client</param>
        /// <param name="eventOperation">operation from contract to be unsubscripted</param>
        [OperationBehavior(TransactionScopeRequired = true)]
        public void Unsubscribe(string address, string eventOperation)
        {
            VerifyAddress(address);
            VerifyOperation(eventOperation);
            VerifyPersistency();

            if (!string.IsNullOrEmpty(eventOperation))
            {
                RemovePersistent(address, ContractName, eventOperation);
            }
            else
            {
                var methods = GetOperations();
                void removePersistent(string methodName) => RemovePersistent(address, ContractName, methodName);
                foreach (var item in methods)
                {
                    removePersistent(item);
                }
            }
        }


        /// <summary>
        ///    Unsubscripted for all events
        /// </summary>
        /// <param name="address">Address where should events be sent</param>
        [OperationBehavior(TransactionScopeRequired = true)]
        public void UnsubscribeAll(string address)
        {
            var methods = GetOperations();
            foreach (var method in methods)
                Unsubscribe(address, method);
        }

        #endregion

        #region Helper methods

        /// <summary>
        ///    Verify address for valid format and for valid type of binding
        ///    /// Exception is thrown when verification failed.
        /// </summary>
        /// <param name="address">address to be verified</param>
        /// ///
        /// <exception cref="T:System.ArgumentException">Invalid format of address.</exception>
        /// /// ///
        /// <exception cref="T:System.ArgumentOutOfRangeException">Nonsupporting address type.</exception>
        private static void VerifyAddress(string address)
        {
            if (!Uri.IsWellFormedUriString(address, UriKind.Absolute))
                throw new ArgumentException("Invalid address format specification");
            if (address.StartsWith("http:") || address.StartsWith("https:"))
                return;
            if (address.StartsWith("net.tcp:"))
                return;
            if (address.StartsWith("net.pipe:"))
                return;
            if (address.StartsWith("net.msmq:"))
                return;
            throw new ArgumentOutOfRangeException(nameof(address), @"Unsupported protocol specified");
        }

        /// <summary>
        ///    Verify event operation name, whether contract contains this name.
        ///    Exception is thrown when verification failed.
        /// </summary>
        /// <param name="eventOperation">operation to be verified</param>
        /// <exception cref="T:System.InvalidOperationException">When operation is not valid.</exception>
        private void VerifyOperation(string eventOperation)
        {
            if (GetOperations().All(op => op != eventOperation))
                throw new InvalidOperationException($"{eventOperation} is not valid operation of {ContractName} contract");
        }

        private void VerifyPersistency()
        {
            if (!servesPersistent)
                throw new InvalidOperationException(
                   $"Operation cannot be performed, permanent storage for service is unavailable! Message: {servesPersistentErrorMessage}");
        }

        /// <summary>
        ///    Create binding from address
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        internal static Binding GetBindingFromAddress(string address)
        {
            if (address.StartsWith("http:") || address.StartsWith("https:"))
            {
                var binding = new WSHttpBinding { ReliableSession = { Enabled = true }, TransactionFlow = true };
                return binding;
            }
            if (address.StartsWith("net.tcp:"))
            {
                var binding = new NetTcpBinding { ReliableSession = { Enabled = true }, TransactionFlow = true };
                return binding;
            }
            if (address.StartsWith("net.pipe:"))
            {
                var binding = new NetNamedPipeBinding { TransactionFlow = true };
                return binding;
            }
            if (address.StartsWith("net.msmq:"))
            {
                var binding = new NetMsmqBinding { Security = { Mode = NetMsmqSecurityMode.None } };
                return binding;
            }
            Debug.Assert(false, "Unsupported binding specified");
            return null;
        }

        /// <summary>
        ///    Returns list of names of available operations for <see cref="T" />.
        /// </summary>
        /// <returns>list of names</returns>
        private static IEnumerable<string> GetOperations()
        {
            return typeof(T).GetMethodsR().Select(method => method.Name);
        }

        #endregion
    }
}
