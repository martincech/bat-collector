﻿using System.Data.Entity;

namespace Services.PublishSubscribe
{
    public class PublishSubscribeDataContext : DbContext
    {
        public PublishSubscribeDataContext()
           : base("name=PublishSubscribeDataContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Do nothing
        }

        public virtual DbSet<Subscriber> Subscribers { get; set; }
    }
}
