﻿namespace Services.PublishSubscribe
{
    /// <summary>
    ///    Data contract for persistent subscribers
    /// </summary>
    public struct PersistentSubscription
    {
        /// <summary>
        ///    Address of subscriber
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        ///    Subscribed operation
        /// </summary>
        public string Operation { get; set; }
    }
}
