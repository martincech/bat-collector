﻿using System.ComponentModel.DataAnnotations;

namespace Services.PublishSubscribe
{
    public class Subscriber
    {
        [Key]
        public int Id { get; set; }
        public string Address { get; set; }
        public string Operation { get; set; }
        public string Contract { get; set; }
    }
}
