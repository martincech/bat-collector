﻿using System.ServiceModel;

namespace Services.PublishSubscribe.Contracts
{
    /// <summary>
    /// Specifies contract for general event subscription/unsubscripted
    /// Subscription is not persistent when application ends.
    /// </summary>
    [ServiceContract]
    public interface ISubscriptionContract
    {
        /// <summary>
        /// Subscribe for all events
        /// </summary>
        [OperationContract(Name = "SubscribeAll")]
        void Subscribe();

        /// <summary>
        /// Subscribe for concrete event
        /// </summary>
        /// <param name="eventOperation">event operation name</param>
        [OperationContract]
        void Subscribe(string eventOperation);

        /// <summary>
        /// Unsubscripted for concrete event
        /// </summary>
        /// <param name="eventOperation">event operation name</param>
        [OperationContract]
        void Unsubscribe(string eventOperation);

        /// <summary>
        /// Unsubscripted for all events
        /// </summary>
        [OperationContract(Name = "UnsubscribeAll")]
        void Unsubscribe();
    }
}
