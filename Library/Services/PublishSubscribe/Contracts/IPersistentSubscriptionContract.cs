﻿using System.ServiceModel;

namespace Services.PublishSubscribe.Contracts
{
    /// <summary>
    /// Subscribe for concrete event.
    /// Subscription is PERSISTENT even when application ends.
    /// </summary>
    [ServiceContract]
    public interface IPersistentSubscriptionContract
    {
        /// <summary>
        /// Subscribe for all events
        /// </summary>
        /// <param name="address">Address where should events be sent</param>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void SubscribeAll(string address);

        /// <summary>
        /// Subscribe for specific event
        /// </summary>
        /// <param name="address">Address where should events be sent</param>
        /// <param name="eventOperation">event operation name</param>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void Subscribe(string address, string eventOperation);

        /// <summary>
        /// Unsubscripted for specific event
        /// </summary>
        /// <param name="address">Address which will be unsubscripted</param>
        /// <param name="eventOperation">event operation name</param>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void Unsubscribe(string address, string eventOperation);

        /// <summary>
        /// Unsubscripted for all events
        /// </summary>
        /// <param name="address">Address where should events be sent</param>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void UnsubscribeAll(string address);
    }
}
