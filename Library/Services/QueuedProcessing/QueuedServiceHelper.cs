﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Messaging;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;

namespace Services.QueuedProcessing
{
    /// <summary>
    /// This class helps to create message queues for MSMQ endpoints.
    /// </summary>
    public static class QueuedServiceHelper
    {
        #region Static methods

        /// <summary>
        /// Verify all queues existence for all MSMQ endpoints from configuration file.
        /// Create queues when they don't exist yet.
        /// </summary>
        public static void VerifyQueues()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var sectionGroup = ServiceModelSectionGroup.GetSectionGroup(config);
            if (sectionGroup == null) return;

            foreach (ChannelEndpointElement endpointElement in sectionGroup.Client.Endpoints)
            {
                if (endpointElement.Binding != "netMsmqBinding") continue;

                var queue = GetQueueFromUri(endpointElement.Address);

                if (!MessageQueue.Exists(queue))
                {
                    MessageQueue.Create(queue, true);
                }
            }
        }

        /// <summary>
        /// Verify queue existence for specified endpoint configuration name.
        /// Create queue when it doesn't exists yet.
        /// </summary>
        /// <typeparam name="T">Channel type</typeparam>
        /// <param name="endpointName">endpoint configuration name</param>
        public static void VerifyQueue<T>(string endpointName) where T : class
        {
            using (var factory = new ChannelFactory<T>(endpointName))
            {
                factory.Endpoint.VerifyQueue();
            }
        }

        /// <summary>
        /// Delete existing queue by endpoint name.
        /// </summary>
        /// <param name="endpointName">endpoint configuration name</param>
        public static void DeleteQueue(string endpointName)
        {
            var queueName = GetQueueFromUri(new Uri(endpointName));
            if (!MessageQueue.Exists(queueName)) return;

            MessageQueue.Delete(queueName);
        }

        /// <summary>
        /// Verify queue existence for empty endpoint name.
        /// Create queue when it doesn't exists yet.
        /// </summary>
        /// <typeparam name="T">Channel type</typeparam>
        public static void VerifyQueue<T>() where T : class
        {
            VerifyQueue<T>("");
        }

        #endregion

        #region Extension methods

        /// <summary>
        /// Extension method. Verifies whether queue for specified endpoint exists and create one when not
        /// Create queue when it doesn't exists yet.
        /// </summary>
        /// <param name="endpoint">General endpoint, but only for <see cref="NetMsmqBinding"/> types is queue actually created</param>
        public static void VerifyQueue(this ServiceEndpoint endpoint)
        {
            if (!(endpoint.Binding is NetMsmqBinding)) return;

            var queue = GetQueueFromUri(endpoint.Address.Uri);

            if (!MessageQueue.Exists(queue))
            {
                MessageQueue.Create(queue, true);
            }

            var binding = endpoint.Binding as NetMsmqBinding;
            if (binding.DeadLetterQueue != DeadLetterQueue.Custom) return;

            Debug.Assert(binding.CustomDeadLetterQueue != null);
            var dlq = GetQueueFromUri(binding.CustomDeadLetterQueue);
            if (!MessageQueue.Exists(dlq))
            {
                MessageQueue.Create(dlq, true);
            }
        }

        /// <summary>
        /// Purge queue for specific endpoint. Clear all waiting messages when queue for endpoint exists.
        /// </summary>
        /// <param name="endpoint">General endpoint, but only for <see cref="NetMsmqBinding"/> types is queue actually cleared</param>
        public static void PurgeQueue(this ServiceEndpoint endpoint)
        {
            if (!(endpoint.Binding is NetMsmqBinding)) return;

            var queueName = GetQueueFromUri(endpoint.Address.Uri);

            if (!MessageQueue.Exists(queueName)) return;
            using (var queue = new MessageQueue(queueName))
            {
                queue.Purge();
            }
        }

        /// <summary>
        /// Delete queue for specific endpoint.
        /// </summary>
        /// <param name="endpoint">General endpoint, but only for <see cref="NetMsmqBinding"/> types is queue actually deleted</param>
        public static void DeleteQueue(this ServiceEndpoint endpoint)
        {
            if (!(endpoint.Binding is NetMsmqBinding)) return;
            DeleteQueue(endpoint.Address.Uri.ToString());
        }


        #endregion

        /// <summary>
        /// Get queue address from specific URI
        /// </summary>
        /// <param name="uri">URI for which to get queue address.</param>
        /// <returns>queue address</returns>
        public static string GetQueueFromUri(Uri uri)
        {
            string queue;

            Debug.Assert(uri.Segments.Length == 3 || uri.Segments.Length == 2);
            if (uri.Segments[1] == @"private/")
            {
                queue = @".\private$\" + uri.Segments[2];
            }
            else
            {
                queue = uri.Host;
                var builder = new System.Text.StringBuilder();
                builder.Append(queue);
                foreach (var segment in uri.Segments)
                {
                    if (segment == "/")
                    {
                        continue;
                    }
                    var localSegment = segment;
                    if (segment[segment.Length - 1] == '/')
                    {
                        localSegment = segment.Remove(segment.Length - 1);
                    }
                    builder.Append(@"\" + localSegment);
                }
                queue = builder.ToString();
            }
            return queue;
        }
    }
}
