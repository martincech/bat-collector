﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Services.Extensions
{
    public static class TypeExtensions
    {
        /// <summary>
        /// Same semantics as <see cref="Type.GetMethods()"/> except when type is interface
        /// get all methods recursively from base interfaces
        /// </summary>
        /// <param name="t">type to be examined</param>
        /// <returns>method info collection</returns>
        public static IEnumerable<MethodInfo> GetMethodsR(this Type t)
        {
            return t.GetMethodsR(BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance);
        }

        public static IEnumerable<MethodInfo> GetMethodsR(this Type t, BindingFlags flags)
        {
            var methods = t.GetMethods(flags).ToList();
            if (!t.IsInterface) return methods;

            foreach (var iface in t.GetInterfaces())
            {
                methods.AddRange(GetMethodsR(iface));
            }
            return methods;
        }

        public static MethodInfo GetMethodR(this Type t, string name)
        {
            return t.GetMethodR(name, BindingFlags.Public | BindingFlags.FlattenHierarchy | BindingFlags.Instance);
        }

        public static MethodInfo GetMethodR(this Type t, string name, BindingFlags flags)
        {
            return t.GetMethodsR(flags).FirstOrDefault(method => method.Name == name);
        }
    }
}
