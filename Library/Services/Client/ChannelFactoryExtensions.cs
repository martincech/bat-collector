﻿using System;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Services.Client
{
    /// <summary>
    /// Extension methods for <see cref="ChannelFactory{TChannel}"/> and <see cref="DuplexChannelFactory{TChannel}"/>
    /// </summary>
    public static class ChannelFactoryExtensions
    {
        /// <summary>
        /// Create client, do action and close client.
        /// </summary>
        /// <typeparam name="I">Service contract to be used for action</typeparam>
        /// <param name="factory">Factory to extend (creates channel)</param>
        /// <param name="action">Action to be executed on channel</param>
        public static void RemoteAction<I>(this IChannelFactory<I> factory, Action<I> action)
        {
            var commander = factory.CreateChannel();
            DoRemoteAction(factory, commander, action);
        }

        /// <summary>
        /// Create client, do action and close client.
        /// </summary>
        /// <typeparam name="I">Service contract to be used for action</typeparam>
        /// <param name="factory">Factory to extend (creates channel)</param>
        /// <param name="context">Object to be used as callback channel</param>
        /// <param name="action">Action to be executed on channel</param>
        public static void RemoteAction<I>(this DuplexChannelFactory<I> factory, object context, Action<I> action)
        {
            var commander = factory.CreateChannel(new InstanceContext(context));
            DoRemoteAction(factory, commander, action);
        }

        #region Helpers

        private static I CreateChannel<I>(this IChannelFactory<I> factory)
        {
            if (factory is ChannelFactory<I>)
            {
                return (factory as ChannelFactory<I>).CreateChannel();
            }
            var m = factory.GetType()
               .GetMethod(nameof(CreateChannel), BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.Instance,
                  null, new Type[] { }, null);
            if (m != null)
            {
                return (I)m.Invoke(factory, new object[] { });
            }
            return default;
        }

        private static void DoRemoteAction<I>(this IChannelFactory<I> factory, I commander, Action<I> action)
        {
            if (commander == null || commander.Equals(default(I)))
            {
                throw new InvalidOperationException($"Factory of {factory.GetType()} must implement method CreateChannel()");
            }
            var communicationObj = (ICommunicationObject)commander;

            try
            {
                action?.Invoke(commander);
                switch(communicationObj.State)
                {
                    case CommunicationState.Faulted:
                        communicationObj.Abort();
                        break;
                    default:
                        communicationObj.Close();
                        break;
                }
            }
            catch (Exception)
            {
                communicationObj.Abort();
                throw;
            }
        }

        #endregion

    }
}
