﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Services.DataContracts
{
    /// <summary>
    /// This class is a bunch of extension method for installing <see cref="GenericResolver"/>.
    /// </summary>
    public static class GenericResolverExtensions
    {
        #region Extension methods for adding the resolver

        /// <summary>
        /// Add resolver to <see cref="ServiceHost"/> for specific types.
        /// </summary>
        /// <param name="host">A <see cref="ServiceHost"/> to which resolver will be installed</param>
        /// <param name="typesToResolve">specific types to be resolved by resolver, when nothing is specified
        /// all classes and structs in the calling assembly plus the public classes and structs in referenced assemblies will be added </param>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void AddGenericResolver(this ServiceHost host, params Type[] typesToResolve)
        {
            Debug.Assert(host.State != CommunicationState.Opened);

            foreach (var endpoint in host.Description.Endpoints)
            {
                AddGenericResolver(endpoint, typesToResolve);
            }
        }

        /// <summary>
        /// Add resolver to <see cref="ClientBase&lt;T&gt;"/> proxy for specific types
        /// </summary>
        /// <typeparam name="T">Service contract interface type</typeparam>
        /// <param name="proxy">client proxy to which resolver will be installed</param>
        /// <param name="typesToResolve">specific types to be resolved by resolver, when nothing is specified
        /// all classes and structs in the calling assembly plus the public classes and structs in referenced assemblies will be added </param>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void AddGenericResolver<T>(this ClientBase<T> proxy, params Type[] typesToResolve) where T : class
        {
            Debug.Assert(proxy.State != CommunicationState.Opened);
            AddGenericResolver(proxy.Endpoint, typesToResolve);
        }

        /// <summary>
        /// Add resolver to <see cref="ChannelFactory&lt;T&gt;"/> factory for specific types
        /// </summary>
        /// <typeparam name="T">Service contract interface type</typeparam>
        /// <param name="factory">client factory to which resolver will be installed</param>
        /// <param name="typesToResolve">specific types to be resolved by resolver, when nothing is specified
        /// all classes and structs in the calling assembly plus the public classes and structs in referenced assemblies will be added </param>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void AddGenericResolver<T>(this ChannelFactory<T> factory, params Type[] typesToResolve)
           where T : class
        {
            Debug.Assert(factory.State != CommunicationState.Opened);
            AddGenericResolver(factory.Endpoint, typesToResolve);
        }

        #endregion

        #region Private stuff

        /// <summary>
        /// Actual adding routine
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="typesToResolve"></param>
        private static void AddGenericResolver(ServiceEndpoint endpoint, Type[] typesToResolve)
        {
            foreach (
               var behavior in
                  endpoint.Contract.Operations.Select(
                     operation => operation.Behaviors.Find<DataContractSerializerOperationBehavior>()))
            {
                GenericResolver newResolver;

                newResolver = typesToResolve == null || !typesToResolve.Any() ? new GenericResolver() : new GenericResolver(typesToResolve);

                var oldResolver = behavior.DataContractResolver as GenericResolver;
                behavior.DataContractResolver = GenericResolver.Merge(oldResolver, newResolver);
            }
        }


        internal static bool IsWebProcess()
        {
            if (Assembly.GetEntryAssembly() != null)
            {
                return false;
            }
            var processName = Process.GetCurrentProcess().ProcessName;

            return processName == "w3wp" || processName == "WebDev.WebServer40";
        }

        internal static Assembly[] GetWebAssemblies()
        {
            Debug.Assert(IsWebProcess());
            var assemblies = new List<Assembly>();

            if (Assembly.GetEntryAssembly() != null)
            {
                throw new InvalidOperationException("Can only call in a web assembly");
            }
            foreach (ProcessModule module in Process.GetCurrentProcess().Modules)
            {
                if ((module.ModuleName.StartsWith("App_Code.") || module.ModuleName.StartsWith("App_Web_")) && module.ModuleName.EndsWith(".dll"))
                {
                    assemblies.Add(Assembly.Load(module.ModuleName));
                }
            }
            if (assemblies.Count == 0)
            {
                throw new InvalidOperationException("Could not find dynamic assembly");
            }
            return assemblies.ToArray();
        }

        #endregion

        public static IEnumerable<Assembly> GetAllAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies();
        }
    }
}
