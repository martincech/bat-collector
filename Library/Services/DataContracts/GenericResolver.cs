﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;

namespace Services.DataContracts
{
    /// <summary>
    ///    Resolver for data types - no need for setting <see cref="KnownTypes" /> attribute when using this resolver.
    /// </summary>
    public class GenericResolver : DataContractResolver
    {
        #region Private fields

        private const string DEFAULT_NAMESPACE = "global";

        private readonly Dictionary<Type, Tuple<string, string>> mTypeToNames;
        private readonly Dictionary<string, Dictionary<string, Type>> mNamesToType;

        #endregion

        #region Public access

        /// <summary>
        /// Get all known types
        /// </summary>
        public Type[] KnownTypes()
        {
            return mTypeToNames.Keys.ToArray();
        }

        /// <summary>
        ///    Default constructor, all classes and structs in the calling assembly plus the public classes and structs in
        ///    referenced assemblies will be added
        /// </summary>
        public GenericResolver()
           : this(ReflectTypes())
        {
        }

        /// <summary>
        ///    Using this constructor only specific types can be resolved.
        /// </summary>
        /// <param name="typesToResolve"><see cref="Type" />s to be resolved</param>
        public GenericResolver(IEnumerable<Type> typesToResolve)
        {
            mTypeToNames = new Dictionary<Type, Tuple<string, string>>();
            mNamesToType = new Dictionary<string, Dictionary<string, Type>>();

            foreach (var type in typesToResolve)
            {
                var typeNamespace = GetNamespace(type);
                var typeName = GetName(type);

                mTypeToNames[type] = new Tuple<string, string>(typeNamespace, typeName);

                if (!mNamesToType.ContainsKey(typeNamespace))
                {
                    mNamesToType[typeNamespace] = new Dictionary<string, Type>();
                }
                mNamesToType[typeNamespace][typeName] = type;
            }
        }

        /// <summary>
        ///    Merges types from two resolver into one common resolver.
        /// </summary>
        /// <param name="resolver1">first resolver to be merged</param>
        /// <param name="resolver2">second resolver to be merged</param>
        /// <returns></returns>
        public static GenericResolver Merge(GenericResolver resolver1, GenericResolver resolver2)
        {
            if (resolver1 == null)
                return resolver2;
            if (resolver2 == null)
                return resolver1;
            var types = new List<Type>();
            types.AddRange(resolver1.KnownTypes());
            types.AddRange(resolver2.KnownTypes());

            return new GenericResolver(types.ToArray());
        }

        #region DataContractResolver overrides

        public override Type ResolveName(string typeName, string typeNamespace, Type declaredType,
           DataContractResolver knownTypeResolver)
        {
            if (mNamesToType.ContainsKey(typeNamespace) && mNamesToType[typeNamespace].ContainsKey(typeName))
            {
                return mNamesToType[typeNamespace][typeName];
            }
            return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, null);
        }

        public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver,
           out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
        {
            if (!mTypeToNames.ContainsKey(type))
                return knownTypeResolver.TryResolveType(type, declaredType, null, out typeName, out typeNamespace);
            var dictionary = new XmlDictionary();
            typeNamespace = dictionary.Add(mTypeToNames[type].Item1);
            typeName = dictionary.Add(mTypeToNames[type].Item2);

            return true;
        }

        #endregion

        #endregion

        #region Private static helpers

        private static string GetNamespace(Type type)
        {
            return type.Namespace ?? DEFAULT_NAMESPACE;
        }

        private static string GetName(Type type)
        {
            return type.Name;
        }

        private static IEnumerable<Type> ReflectTypes()
        {
            var assemblyReferences = GetCustomReferencedAssemblies();

            var types = new List<Type>();

            foreach (var typesInReferencedAssembly in assemblyReferences.Select(assembly => GetTypes(assembly)))
                types.AddRange(typesInReferencedAssembly);
            return types.Distinct().ToArray();
        }

        private static IEnumerable<Assembly> GetCustomReferencedAssemblies()
        {
            return GenericResolverExtensions.GetAllAssemblies().Where(a => !IsSystemAssembly(a.GetName())).Distinct().ToArray();
        }

        private static bool IsSystemAssembly(AssemblyName assemblyName)
        {
            const string dotNetKeyToken1 = "b77a5c561934e089";
            const string dotNetKeyToken2 = "b03f5f7f11d50a3a";
            const string dotNetKeyToken3 = "31bf3856ad364e35";
            var serviceNModelEx = Assembly.GetCallingAssembly();
            var serviceNModelExKeyToken = serviceNModelEx.GetName().FullName.Split('=')[3];
            var keyToken = assemblyName.FullName.Split('=')[3];
            if (keyToken == "null")
                return false;
            if (keyToken == serviceNModelExKeyToken)
                return true;
            switch (keyToken)
            {
                case dotNetKeyToken1:
                case dotNetKeyToken2:
                case dotNetKeyToken3:
                    {
                        return true;
                    }
                default:
                    return false;
            }
        }

        private static Type[] GetTypes(Assembly assembly, bool publicOnly = true)
        {
            Type[] allTypes;
            try
            {
                allTypes = assembly.GetTypes();
            }
            catch (Exception)
            {
                return new Type[] { };
            }

            return GetAllowedTypes(allTypes, publicOnly);
        }

        private static Type[] GetAllowedTypes(Type[] allTypes, bool publicOnly)
        {
            var types = new List<Type>();
            foreach (var type in allTypes)
            {
                if (!IsTypeAllowed(type, publicOnly))
                {
                    continue;
                }
                types.Add(type);
            }

            return types.ToArray();
        }

        private static bool IsTypeAllowed(Type type, bool publicOnly)
        {
            if (!Attribute.IsDefined(type, typeof(DataContractAttribute)) ||
                type.IsInterface ||
                type.IsGenericTypeDefinition)
            {
                return false;
            }

            return !(publicOnly && !type.IsPublic && (!type.IsNested || type.IsNestedPrivate));
        }

        #endregion
    }
}
