﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using Communication.Contract.Wcf.BindingConstants;
using NLog;
using Services.DataContracts;
using Services.PublishSubscribe.Contracts;

namespace Veit.Bat.Collector.Core.Service_References
{
    public class BaseWcfSubscriptionClient<T> : IDisposable
      where T : class, ISubscriptionContract
    {
        private readonly object callbacks;
        private readonly string address;
        private readonly Action<string> errorAction;
        private DuplexChannelFactory<T> factory;
        protected bool reseted;

        protected T Channel;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        protected BaseWcfSubscriptionClient(
           object callbacks,
           string address,
           Action<string> errorAction = null)
        {
            this.callbacks = callbacks;
            this.address = address;
            this.errorAction = errorAction;
            Init();
        }

        protected void Init()
        {
            factory = new DuplexChannelFactory<T>(
              callbacks,
              ServiceNetPipeBindingConstants.NetPipeBinding,
              address);
            factory.AddGenericResolver();
            Channel = factory.CreateChannel();
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                switch (factory.State)
                {
                    case CommunicationState.Faulted:
                        factory.Abort();
                        break;
                    default:
                        factory.Close();
                        break;
                }
            }
            catch (Exception e)
            {
                logger.Warn(e, "Dispose WCF Client exception ");
                factory.Abort();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void TrySend(Action action)
        {
            try
            {
                action?.Invoke();
            }
            catch (Exception e)
            {
                if (reseted)
                {
                    DisplayError(e);
                    reseted = false;
                    return;
                }
                Init();
                Subscribe();
                reseted = true;
                TrySend(action);
            }
        }


        public void Subscribe()
        {
            try
            {
                Channel.Subscribe();
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void Subscribe(string eventOperation)
        {
            try
            {
                Channel.Subscribe(eventOperation);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void Unsubscribe(string eventOperation)
        {
            try
            {
                Channel.Unsubscribe(eventOperation);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void Unsubscribe()
        {
            try
            {
                Channel.Unsubscribe();
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        protected void DisplayError(Exception e)
        {
            var s = $"No listening services! {Channel}";
            logger.Warn(e, s);
            if (errorAction != null)
            {
                errorAction(s);
            }
            else
            {
                Debug.WriteLine(s);
            }
        }
    }
}
