﻿using System;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.DataContracts.Device;
using Communication.Contract.Wcf.WebApi;

namespace Veit.Bat.Collector.Core.Service_References.WebApiConnection
{
    public class WebApiConnectionWcfClient : BaseWcfSubscriptionClient<IWebApiConnection>, IWebApiConnection
    {

        public WebApiConnectionWcfClient(IWebApiConnectionCallbacks callbacks, string address,
           Action<string> errorAction = null)
           : base(callbacks, address, errorAction)
        {

        }

        public void ChangeServerConnection(WebApiConnectionInfo webApiConnectionInfo)
        {
            TrySend(() =>
            {
                Channel.ChangeServerConnection(webApiConnectionInfo);
            });
        }

        public void GetExistingConnectionsRequest()
        {
            try
            {
                Channel.GetExistingConnectionsRequest();
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }


        public void SyncDevice(DeviceNotification device)
        {
            try
            {
                Channel.SyncDevice(device);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }
    }
}
