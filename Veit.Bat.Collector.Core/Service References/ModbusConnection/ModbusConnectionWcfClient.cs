﻿using System;
using System.Collections.Generic;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.DataContracts.Device;
using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Communication.Contract.Wcf.DataSources;

namespace Veit.Bat.Collector.Core.Service_References.ModbusConnection
{
    public class ModbusConnectionWcfClient
      : BaseWcfDataSourceClient<IModbusService>,
      IModbusService
    {

        public ModbusConnectionWcfClient(IModbusServiceCallback callbacks, string address, Action<string> errorAction = null)
           : base(callbacks, address, errorAction)
        {
        }

        #region Implementation of IModbusService

        public void AddSource(string portId)
        {
            try
            {
                Channel.AddSource(portId);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void RemoveSource(string portId)
        {
            try
            {
                Channel.RemoveSource(portId);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void SetPortSettings(string portId, CommunicationParameters communicationSettings)
        {
            try
            {
                Channel.SetPortSettings(portId, communicationSettings);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void ScanPortAddresses(IEnumerable<string> portIds, IEnumerable<byte> addresses)
        {
            try
            {
                Channel.ScanPortAddresses(portIds, addresses);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void ActivateDevices(IEnumerable<string> portIds, IEnumerable<string> addresses)
        {
            try
            {
                Channel.ActivateDevices(portIds, addresses);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void DeactivateDevices(IEnumerable<string> portIds, IEnumerable<string> addresses)
        {
            try
            {
                Channel.DeactivateDevices(portIds, addresses);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        /// <inheritdoc />
        public void GetActive()
        {
            try
            {
                Channel.GetActive();
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void GetConfiguration(string portId, byte address)
        {
            try
            {
                Channel.GetConfiguration(portId, address);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void GetVersion(string portId, byte address)
        {
            try
            {
                Channel.GetVersion(portId, address);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void GetDeviceName(string portId, byte address)
        {
            try
            {
                Channel.GetDeviceName(portId, address);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void SetDeviceName(string portId, byte address, string name)
        {
            try
            {
                Channel.SetDeviceName(portId, address, name);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void SetSavingParameters(string portId, byte address, SavingParameters parameters)
        {
            try
            {
                Channel.SetSavingParameters(portId, address, parameters);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        #endregion

        #region Implementation of IScaleFlock

        public void GetFlock(string portId, byte address, byte flockNumber)
        {
            try
            {
                Channel.GetFlock(portId, address, flockNumber);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        public void SetFlock(string portId, byte address, Flock flock)
        {
            try
            {
                Channel.SetFlock(portId, address, flock);
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        #endregion
    }
}
