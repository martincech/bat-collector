﻿using System;
using Communication.Contract.Wcf.DataSources;

namespace Veit.Bat.Collector.Core.Service_References
{
    public class BaseWcfDataSourceClient<T>
         : BaseWcfSubscriptionClient<T>,
         IDataSource,
         IDiagnosticSetup
     where T : class, IDataSource, IDiagnosticSetup
    {
        private readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public BaseWcfDataSourceClient(object callbacks, string address, Action<string> errorAction = null)
            : base(callbacks, address, errorAction)
        {
        }

        public void GetConnected()
        {
            try
            {
                Channel.GetConnected();
            }
            catch (Exception e)
            {
                DisplayError(e);
            }
        }

        #region Implementation of IDiagnostic

        public void SetDebugMode(bool enable)
        {
            try
            {
                Channel.SetDebugMode(enable);
            }
            catch (Exception e)
            {
                logger.Error(e, "Set debug mode error");
                DisplayError(e);
            }
        }

        public void SendFeedback(string feedbackFileName, string message, bool includeDiagnosticFiles)
        {
            try
            {
                Channel.SendFeedback(feedbackFileName, message, includeDiagnosticFiles);
            }
            catch (Exception e)
            {
                logger.Error(e, "Send feedback exception");
                DisplayError(e);
            }
        }

        #endregion
    }
}
