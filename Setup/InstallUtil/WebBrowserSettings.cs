﻿using System;
using System.IO;
using Microsoft.Win32;

namespace InstallUtil
{
    public class WebBrowserSettings
    {
        private const string BROWSER_EMULATION_SUB_KEY = @"Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION";

        /// <summary>
        /// If you use higher version than user have has installed,
        /// it will be used the highest available version.
        /// <see cref="https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/general-info/ee330730(v=vs.85)#browser-emulation"/>
        /// </summary>
        public enum BrowserEmulation
        {
            IE10Default = 0x02710,
            IE10Standard = 0x02711,
            IE11Default = 0x2af8,
            IE11EdgeMode = 0x2af9
        }

        public static void SetDocumentModeForWebBrowser(string programName, BrowserEmulation emulationVersion = BrowserEmulation.IE11EdgeMode)
        {
            var pricipal = new System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent());
            if (pricipal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator))
            {
                var registrybrowser = Registry.LocalMachine.OpenSubKey(BROWSER_EMULATION_SUB_KEY, true);
                var currentValue = registrybrowser.GetValue(programName);
                if (currentValue == null || (int)currentValue != (int)emulationVersion)
                {
                    registrybrowser.SetValue(programName, emulationVersion, RegistryValueKind.DWord);
                }
            }
            else
            {
                throw new UnauthorizedAccessException("User must have administrator rights for setting document mode in registry!");
            }
        }

        /// <summary>
        /// use current assembly as program name.
        /// </summary>
        public static void SetDocumentModeForWebBrowser()
        {
            var myProgramName = Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            SetDocumentModeForWebBrowser(myProgramName);
        }
    }
}
