﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Threading;
using System.Windows;
using AutoUpdate;

namespace InstallUtil
{
    [RunInstaller(true)]
    public partial class InstallerHelper : Installer
    {
        #region Private fields
        const string PROGRAM_NAME = "Veit.Bat.Collector.exe";

        readonly AutoUpdateInstaller autoUpdateInstaller;
        #endregion

        public InstallerHelper() : base()
        {
            autoUpdateInstaller = new AutoUpdateInstaller();
        }

        public string TargetDir
        {
            get
            {
                try
                {
                    var path = Context.Parameters["targetdir"];
                    return path.TrimEnd(new char[] { '\\', '/' });
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public override void Install(IDictionary stateSaver)
        {
            try
            {
                UninstallServices();
                base.Install(stateSaver);

                InstallServices();
                WebBrowserSettings.SetDocumentModeForWebBrowser(PROGRAM_NAME);
                if (!autoUpdateInstaller.AutoUpdateInProgress)
                {
                    autoUpdateInstaller.Install();
                }
            }
            catch (Exception e)
            {
                ErrorDisplay(e);
            }
        }

        public override void Commit(IDictionary savedState)
        {
            try
            {
                base.Commit(savedState);
                autoUpdateInstaller.RunScheduledTask();
            }
            catch (Exception e)
            {
                ErrorDisplay(e);
            }
        }

        public override void Uninstall(IDictionary savedState)
        {
            try
            {
                UninstallServices();
                if (!autoUpdateInstaller.AutoUpdateInProgress)
                {
                    autoUpdateInstaller.Unistall();
                }

                base.Uninstall(savedState);

                //clean
                ThreadPool.QueueUserWorkItem(o =>
                {
                    Directory.Delete(TargetDir, true);
                });
            }
            catch (Exception e)
            {
                ErrorDisplay(e);
            }
        }

        public override void Rollback(IDictionary savedState)
        {
            try
            {
                UninstallServices();
                if (!autoUpdateInstaller.AutoUpdateInProgress)
                {
                    autoUpdateInstaller.Unistall();
                }

                base.Rollback(savedState);

                //clean
                ThreadPool.QueueUserWorkItem(o =>
                {
                    Directory.Delete(TargetDir, true);
                });
            }
            catch (Exception e)
            {
                ErrorDisplay(e);
            }
        }

        #region Private helpers

        private void InstallServices()
        {
            ChangeServices(true);
        }

        private void UninstallServices()
        {
            ChangeServices(false);
        }

        private void ChangeServices(bool install)
        {
            try
            {
                var services = new List<string>();
                foreach (var s in Services)
                {
                    services.Add(Path.Combine(TargetDir, s));
                }

                if (install)
                {
                    ServiceUtil.ServiceHelper.InstallServices(services);
                }
                else
                {
                    ServiceUtil.ServiceHelper.UninstallServices(services);
                    UninstallNotSupportedServices();
                }
            }
            catch (Exception e)
            {
                ErrorDisplay(e);
            }
        }

        /// <summary>
        /// Old services, not supported in current version
        /// </summary>
        private void UninstallNotSupportedServices()
        {
            var uninstallOnlyServices = new List<string>();
            foreach (var serv in ServicesToUninstallOnly)
            {
                uninstallOnlyServices.Add(Path.Combine(TargetDir, serv));
            }
            ServiceUtil.ServiceHelper.UninstallServices(uninstallOnlyServices);
        }


        private static void ErrorDisplay(Exception e)
        {
            MessageBox.Show("Error: " + e.Message + "\nStackTrace: " + e.StackTrace);
        }

        public string[] Services
        {
            get
            {
                return new string[]
                {
                   "ModbusSensors.Service.exe",
                   "AutoUpdater.Service.exe",
                };
            }
        }

        public string[] ServicesToUninstallOnly
        {
            get
            {
                return new string[]
                {
                   "ModemSensors.Service.exe",
                   "ExternalSensors.Service.exe",
                };
            }
        }

        #endregion
    }
}
