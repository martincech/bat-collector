﻿using System;
using System.IO;
using NLog;

namespace AutoUpdate
{
    public class AutoUpdateInstaller
    {
        const string UPDATE_PROGRAM_NAME = "Veit.Bat.Collector.Update.exe";
        const string UPDATE_PROGRAM_URL = "https://s3.eu-central-1.amazonaws.com/store.veit.cz/download/Update.setup.exe";

        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        readonly string updateFileName;

        readonly IAutoUpdateScheduler scheduler;
        readonly IAutoUpdateRegistry registry;
        readonly IAutoUpdateDownloader downloader;

        public AutoUpdateInstaller(string updateFileName = UPDATE_PROGRAM_NAME)
            : this(new AutoUpdateScheduler(), new AutoUpdateDownloader(), new AutoUpdateRegistry(), updateFileName)
        {
        }

        public AutoUpdateInstaller(
            IAutoUpdateScheduler scheduler,
            IAutoUpdateDownloader downloader,
            IAutoUpdateRegistry registry,
            string updateFileName)
        {
            this.scheduler = scheduler ?? throw new ArgumentNullException($"{nameof(scheduler)} cannot be null");
            this.downloader = downloader ?? throw new ArgumentNullException($"{nameof(downloader)} cannot be null");
            this.registry = registry ?? throw new ArgumentNullException($"{nameof(registry)} cannot be null");
            this.updateFileName = updateFileName ?? UPDATE_PROGRAM_NAME;
        }

        string WindowsFolder => Environment.GetFolderPath(Environment.SpecialFolder.Windows);
        string SharedFolder => Path.Combine(WindowsFolder, @"ServiceProfiles\LocalService\AppData\Local");
        string TempFolder => Path.Combine(SharedFolder, "Temp");
        string SfxFile => Path.Combine(TempFolder, "Update.setup.exe");

        public string UpdateFolder(string eTag) => Path.Combine(SharedFolder, "Veit", $"Update_{eTag?.Replace("\"", "")}");
        public string UpdateFile(string eTag) => Path.Combine(UpdateFolder(eTag), updateFileName);
        public bool AutoUpdateInProgress => registry.GetUpdateState() != UpdateState.None;

        /// <summary>
        /// Download, extract and register collector auto-update application
        /// </summary>
        /// <param name="url">URL of self-extract zip with auto-update application</param>
        public void Install(string url = UPDATE_PROGRAM_URL)
        {
            string eTag = null;
            try
            {
                //try to download latest version of autoupdate
                eTag = downloader.Download(url, SfxFile, registry.Etag);
                //check autoupdate version
                if (!string.IsNullOrEmpty(eTag))
                {
                    //extract autoupdate to folder
                    downloader.Extract(SfxFile, UpdateFolder(eTag));
                    //save etag of autoupdate to Windows registry
                    registry.Update(eTag);
                    //register autoupdate to Windows scheduler
                    scheduler.Register(UpdateFile(eTag));
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                RollbackInstallation(eTag);
            }
        }

        /// <summary>
        /// Update collecotr auto-update application
        /// </summary>
        /// <param name="eTag">ETag of new auto-update</param>
        /// <param name="oldETag">Etag of previous auto-update</param>
        /// <param name="url">URL of self-extract zip with auto-update applicatio</param>
        /// <returns>True - if auto-update application was updated to newer version</returns>
        public bool Update(out string eTag, out string oldETag, string url = UPDATE_PROGRAM_URL)
        {
            oldETag = null;
            eTag = null;

            try
            {
                oldETag = registry.Etag;
                if (!string.IsNullOrEmpty(oldETag))
                {
                    //try to download latest version of autoupdate
                    eTag = downloader.Download(url, SfxFile, registry.Etag);
                    if (!string.IsNullOrEmpty(eTag) && eTag != registry.Etag)
                    {
                        //extract autoupdate to folder
                        downloader.Extract(SfxFile, UpdateFolder(eTag));
                        //remove autoupdate from Windows scheduler
                        scheduler.Unregister();
                        //register new version of autoupdate to Windows scheduler
                        scheduler.Register(UpdateFile(eTag));
                        //update etag of autoupdate in Windows registry
                        registry.Update(eTag);
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                RollbackUpdate(eTag, oldETag);
            }

            return false;
        }

        /// <summary>
        /// Remove collector auto-update application from pc
        /// </summary>
        public void Unistall()
        {
            RollbackInstallation(registry.Etag);
        }

        /// <summary>
        /// Runs the registered task immediately.
        /// </summary>
        public void RunScheduledTask()
        {
            try
            {
                scheduler.RunTask();
            }
            catch (Exception e)
            {
                logger.Error(e, $"Failed to start registered task.");
            }
        }

        void RollbackUpdate(string eTag, string oldETag)
        {
            RollbackInstallation(eTag);
            if (string.IsNullOrEmpty(oldETag)) return;
            try
            {
                scheduler.Register(UpdateFile(oldETag));
                registry.Update(oldETag);
            }
            catch (Exception e)
            {
                logger.Error(e, $"Failed to rollback update of auto-update application. ETag: {oldETag}");
            }

        }

        void RollbackInstallation(string eTag)
        {
            try
            {
                //remove autoupdate from Windows scheduler
                scheduler.Unregister();
                //clear autoupdate from Windows registry
                registry.Remove();

                var updateFolder = UpdateFolder(eTag);
                if (!string.IsNullOrEmpty(eTag) && Directory.Exists(updateFolder))
                {
                    //Delete autoupdate folder
                    Directory.Delete(updateFolder, true);
                }
            }
            catch (Exception e)
            {
                logger.Error(e, $"Failed to rollback installation of auto-update application. ETag: {eTag}");
            }
        }
    }
}
