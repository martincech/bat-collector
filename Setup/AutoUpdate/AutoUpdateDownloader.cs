﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading;
using NLog;

namespace AutoUpdate
{
    public interface IAutoUpdateDownloader
    {
        string Download(string url, string filePath, string etag, int timeout = 1000 * 60 * 5);
        void Extract(string filePath, string updateFolder);
    }

    public sealed class AutoUpdateDownloader : IAutoUpdateDownloader, IDisposable
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();
        readonly SemaphoreSlim semaphore = new SemaphoreSlim(0);

        public string Download(string url, string filePath, string etag, int timeout = 1000 * 60 * 5)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            logger.Debug("Download auto-update application");
            using (var client = new WebClient())
            {
                var ur = new Uri(url);
                client.Headers.Add("If-None-Match", etag);
                client.DownloadFileCompleted += (sender, args) => semaphore.Release();
                client.DownloadFileAsync(ur, filePath);
                semaphore.Wait(timeout);
                var statusCode = GetStatusCode(client);
                logger.Info($"Download auto-update response status code: {statusCode.ToString("D")} - {statusCode.ToString("F")}");
                return client.ResponseHeaders?.Get("ETag");
            }
        }

        public void Extract(string filePath, string updateFolder)
        {
            logger.Debug($"Extract auto-update file '{filePath}' to '{updateFolder}'");
            using (var proc = System.Diagnostics.Process.Start(filePath, $"-o\"{updateFolder}\" -y"))
            {
                proc.WaitForExit(1000 * 60 * 2);
            }
        }

        private static HttpStatusCode GetStatusCode(WebClient client)
        {
            var responseField = client.GetType().GetField("m_WebRequest", BindingFlags.Instance | BindingFlags.NonPublic);
            if (responseField != null && responseField.GetValue(client) is HttpWebRequest request)
            {
                try
                {
                    var response = (HttpWebResponse)request.GetResponse();
                    return response.StatusCode;
                }
                catch (WebException e)
                {
                    return ((HttpWebResponse)e.Response).StatusCode;
                }
            }
            return HttpStatusCode.BadRequest;
        }

        public void Dispose()
        {
            semaphore.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
