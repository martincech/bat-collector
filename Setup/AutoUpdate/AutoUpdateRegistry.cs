﻿using System;
using Microsoft.Win32;

namespace AutoUpdate
{
    public interface IAutoUpdateRegistry
    {
        string Etag { get; }
        void Update(string etag);
        void Remove();
        void SetUpdateState(UpdateState updateState);
        UpdateState GetUpdateState();
    }

    public enum UpdateState
    {
        None,
        BeforeUninstall,
        BeforeInstall,
    }

    public class AutoUpdateRegistry : IAutoUpdateRegistry
    {
        public const string DEFAULT_KEY = @"Software\Veit\BAT Collector";

        readonly string key;
        const string ETAG_KEY = "ETag";
        const string UPDATE_STATE_KEY = "State";

        public AutoUpdateRegistry(string key = DEFAULT_KEY)
        {
            this.key = key;
        }

        public string Etag => ReadValue(ETAG_KEY);

        public void Remove()
        {
            if (SubKeyExist())
            {
                Registry.LocalMachine.DeleteSubKeyTree(key);
            }
        }

        public void Update(string etag)
        {
            using (var subKey = GetSubKey())
            {
                subKey.SetValue(ETAG_KEY, etag, RegistryValueKind.String);
            }
        }

        public void SetUpdateState(UpdateState updateState)
        {
            using (var subKey = GetSubKey())
            {
                subKey.SetValue(UPDATE_STATE_KEY, updateState.ToString(), RegistryValueKind.String);
            }
        }

        public UpdateState GetUpdateState()
        {
            return Enum.TryParse(ReadValue(UPDATE_STATE_KEY), out UpdateState state)
                ? state
                : UpdateState.None;
        }

        bool SubKeyExist()
        {
            using (var subKey = Registry.LocalMachine.OpenSubKey(key))
            {
                return subKey != null;
            }
        }

        RegistryKey GetSubKey()
        {
            return Registry.LocalMachine.OpenSubKey(key, true) ?? Registry.LocalMachine.CreateSubKey(key);
        }

        string ReadValue(string keyName)
        {
            using (var subKey = Registry.LocalMachine.OpenSubKey(key))
            {
                if (subKey != null)
                {
                    var o = subKey.GetValue(keyName);
                    return o == null ? null : o as string;
                }
            }
            return null;
        }
    }
}
