﻿using System;
using Microsoft.Win32.TaskScheduler;
using NLog;

namespace AutoUpdate
{
    public interface IAutoUpdateScheduler
    {
        /// <summary>
        /// Add task to Windows Task Scheduler
        /// </summary>
        /// <param name="taskPath">Path to executable file</param>
        /// <param name="triggerType">Task trigger frequency</param>
        void Register(string taskPath, QuickTriggerType triggerType = QuickTriggerType.Hourly);

        /// <summary>
        /// Remove task from Windows Task Scheduler
        /// </summary>
        void Unregister();

        /// <summary>
        /// Runs the registered task immediately
        /// </summary>
        void RunTask();
    }

    public class AutoUpdateScheduler : IAutoUpdateScheduler
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        readonly string description;
        readonly string name;

        const string TASK_NAME = "VEIT_BAT_Collector_update";
        const string TASK_DESCRIPTION = "This task keeps your BAT Collector applications up to date with the latest enhancements and security fixes";

        public AutoUpdateScheduler(string name = TASK_NAME, string description = TASK_DESCRIPTION)
        {
            this.name = name;
            this.description = description;
        }

        public void Register(string taskPath, QuickTriggerType triggerType = QuickTriggerType.Hourly)
        {
            if (FindTask() == null)
            {
                try
                {
                    logger.Debug($"Add task '{name}' to Windows Task Scheduler.");
                    TaskService.Instance.AddTask(name, triggerType, taskPath, null, @"NT AUTHORITY\SYSTEM", null, TaskLogonType.ServiceAccount, description);
                }
                catch (Exception e)
                {
                    logger.Error(e, $"Failed to add task '{name}' to Windows Task Scheduler.");
                    throw;
                }
            }
        }

        public void Unregister()
        {
            var task = FindTask();
            if (task != null)
            {
                try
                {
                    logger.Debug($"Remove task '{name}' from Windows Task Scheduler.");
                    task.Folder.DeleteTask(name);
                }
                catch (Exception e)
                {
                    logger.Error(e, $"Failed to remove task '{name}' to Windows Task Scheduler.");
                    throw;
                }
            }
        }

        public void RunTask()
        {
            FindTask()?.Run();
        }

        Task FindTask()
        {
            try
            {
                return TaskService.Instance.FindTask(name);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
