﻿using System;
using Moq;
using Xunit;


namespace AutoUpdate.Tests
{
    public class AutoUpdateInstallerTests
    {
        const string FILE_NAME = "Update.File.exe";
        const string UPDATE_FILE_URL = "http://localhost/Update.exe";
        const string ETAG = "123456789";
        const string PREVIOUS_ETAG = "987654321";

        readonly Mock<IAutoUpdateScheduler> mockScheduler;
        readonly Mock<IAutoUpdateDownloader> mockDownloader;
        readonly Mock<IAutoUpdateRegistry> mockRegistry;

        public AutoUpdateInstallerTests()
        {
            mockDownloader = new Mock<IAutoUpdateDownloader>();
            mockScheduler = new Mock<IAutoUpdateScheduler>();
            mockRegistry = new Mock<IAutoUpdateRegistry>();
        }

        [Fact]
        public void CreateInstance()
        {
            var installer = new AutoUpdateInstaller(FILE_NAME);
            Assert.NotNull(installer);
        }

        [Fact]
        public void CreateInstance_ThrowsException_WhenAutoUpdateSchedulerIsNull()
        {
            void act() => new AutoUpdateInstaller(null, mockDownloader.Object, mockRegistry.Object, FILE_NAME);
            Assert.ThrowsAny<ArgumentNullException>((Action)act);
        }

        [Fact]
        public void CreateInstance_ThrowsException_WhenAutoUpdateDownloaderIsNull()
        {
            void act() => new AutoUpdateInstaller(mockScheduler.Object, null, mockRegistry.Object, FILE_NAME);
            Assert.ThrowsAny<ArgumentNullException>((Action)act);
        }

        [Fact]
        public void CreateInstance_ThrowsException_WhenUpdateRegistryIsNull()
        {
            void act() => new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, null, FILE_NAME);
            Assert.ThrowsAny<ArgumentNullException>((Action)act);
        }

        [Fact]
        public void Install_WhenUpdateFileIsAvailable()
        {
            mockDownloader.Setup(x => x.Download(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(ETAG);

            var installer = new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, mockRegistry.Object, FILE_NAME);
            installer.Install(UPDATE_FILE_URL);

            mockDownloader.Verify(v => v.Download(It.Is<string>(url => url == UPDATE_FILE_URL), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), Times.Once);

            mockDownloader.Verify(v => v.Extract(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mockScheduler.Verify(v => v.Register(It.IsAny<string>(), It.IsAny<Microsoft.Win32.TaskScheduler.QuickTriggerType>()), Times.Once);
            mockRegistry.Verify(v => v.Update(It.Is<string>(etag => etag == ETAG)), Times.Once);
        }

        [Fact]
        public void Install_WhenUpdateFileIsNotAvailable()
        {
            mockDownloader.Setup(x => x.Download(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns((string)null);

            var installer = new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, mockRegistry.Object, FILE_NAME);
            installer.Install(UPDATE_FILE_URL);

            mockDownloader.Verify(v => v.Download(It.Is<string>(url => url == UPDATE_FILE_URL), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), Times.Once);
            mockDownloader.Verify(v => v.Extract(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mockScheduler.Verify(v => v.Register(It.IsAny<string>(), It.IsAny<Microsoft.Win32.TaskScheduler.QuickTriggerType>()), Times.Never);
            mockRegistry.Verify(v => v.Update(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void Install_Rollback_WhenUpdateExtractionThrowsException()
        {
            mockDownloader.Setup(x => x.Extract(It.IsAny<string>(), It.IsAny<string>())).Throws(new Exception());

            Install_Rollback();
        }

        [Fact]
        public void Install_Rollback_WhenSchedulerRegistrationThrowsException()
        {
            mockScheduler.Setup(x => x.Register(It.IsAny<string>(), It.IsAny<Microsoft.Win32.TaskScheduler.QuickTriggerType>())).Throws(new Exception());

            Install_Rollback();
        }

        [Fact]
        public void Install_Rollback_WhenRegistryUpdateThrowsException()
        {
            mockRegistry.Setup(x => x.Update(It.IsAny<string>())).Throws(new Exception());

            Install_Rollback();
        }

        private void Install_Rollback()
        {
            mockDownloader.Setup(x => x.Download(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(ETAG);

            var installer = new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, mockRegistry.Object, FILE_NAME);
            installer.Install(UPDATE_FILE_URL);

            mockDownloader.Verify(v => v.Download(It.Is<string>(url => url == UPDATE_FILE_URL), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), Times.Once);
            mockScheduler.Verify(v => v.Unregister(), Times.Once);
            mockRegistry.Verify(v => v.Remove(), Times.Once);
        }

        [Fact]
        public void Update_WhenNewVersionIsAvailable()
        {
            mockDownloader.Setup(x => x.Download(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(ETAG);
            mockRegistry.SetupGet(s => s.Etag).Returns(PREVIOUS_ETAG);

            var installer = new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, mockRegistry.Object, FILE_NAME);

            installer.Update(out var newEtag, out var oldEtag, UPDATE_FILE_URL);

            Assert.Equal(oldEtag, PREVIOUS_ETAG);
            Assert.Equal(newEtag, ETAG);

            Verify_UpdateCalls(Times.Once(), Times.Once());
        }

        [Fact]
        public void Update_WhenNewVersionIsNotAvailable()
        {
            mockDownloader.Setup(x => x.Download(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns((string)null);
            mockRegistry.SetupGet(s => s.Etag).Returns(PREVIOUS_ETAG);

            var installer = new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, mockRegistry.Object, FILE_NAME);

            installer.Update(out var newEtag, out var oldEtag, UPDATE_FILE_URL);

            Assert.Equal(oldEtag, PREVIOUS_ETAG);
            Assert.Null(newEtag);

            Verify_UpdateCalls(Times.Once(), Times.Never());
        }

        [Fact]
        public void Update_WhenPreviousVersionIsNotRegistered()
        {
            mockDownloader.Setup(x => x.Download(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>())).Returns(ETAG);
            mockRegistry.SetupGet(s => s.Etag).Returns((string)null);

            var installer = new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, mockRegistry.Object, FILE_NAME);

            installer.Update(out var newEtag, out var oldEtag, UPDATE_FILE_URL);

            Assert.Null(oldEtag);
            Assert.Null(newEtag);

            Verify_UpdateCalls(Times.Never(), Times.Never());
        }

        void Verify_UpdateCalls(Times donwloadTimes, Times otherTimes)
        {
            mockDownloader.Verify(v => v.Download(It.Is<string>(url => url == UPDATE_FILE_URL), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), donwloadTimes);
            mockDownloader.Verify(v => v.Extract(It.IsAny<string>(), It.IsAny<string>()), otherTimes);
            mockScheduler.Verify(v => v.Unregister(), otherTimes);
            mockScheduler.Verify(v => v.Register(It.IsAny<string>(), It.IsAny<Microsoft.Win32.TaskScheduler.QuickTriggerType>()), otherTimes);
            mockRegistry.Verify(v => v.Update(It.Is<string>(etag => etag == ETAG)), otherTimes);
        }

        [Fact]
        public void Unistall()
        {
            mockRegistry.SetupGet(s => s.Etag).Returns(PREVIOUS_ETAG);

            var installer = new AutoUpdateInstaller(mockScheduler.Object, mockDownloader.Object, mockRegistry.Object, FILE_NAME);
            installer.Unistall();

            mockScheduler.Verify(v => v.Unregister(), Times.Once);
            mockRegistry.Verify(v => v.Remove(), Times.Once);
        }
    }
}
