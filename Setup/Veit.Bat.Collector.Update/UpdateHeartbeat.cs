﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Heartbeat;
using NLog;
using Settings;
using Settings.Core;
using Settings.Core.Repository;
using Veit.Bat.Collector.Update.Update;

namespace Veit.Bat.Collector.Update
{
    public class UpdateHeartbeat
    {
        const string UPDATE_FILE = @"bat_collector_update.msi";
        const string UPDATE_DIRECTORY = @"UpdateFile";

        readonly JsonCoreEngine<HeartbeatSettings> heartbeatStorage;
        readonly IManagerSettings managerSettings;
        readonly IHeartbeatClient heartbeatClient;
        readonly IUpdateProcess updateProcess;
        readonly IUpdateService updateService;

        readonly Uri heartbeatUrl;

        public UpdateHeartbeat(string applicationName, string guiProcessName, string serviceName)
            : this(
                  new HeartbeatStorage(),
                  new ManagerSettings(),
                  new HeartbeatClient(),
                  new UpdateProcess(applicationName, guiProcessName),
                  new UpdateService(serviceName))
        {
        }

        public UpdateHeartbeat(
            JsonCoreEngine<HeartbeatSettings> heartbeatStorage,
            IManagerSettings managerSettings,
            IHeartbeatClient heartbeatClient,
            IUpdateProcess updateProcess,
            IUpdateService updateService)
        {
            this.heartbeatStorage = heartbeatStorage ?? throw new ArgumentNullException($"{nameof(heartbeatStorage)} cannot be null");
            this.managerSettings = managerSettings ?? throw new ArgumentNullException($"{nameof(managerSettings)} cannot be null");
            this.heartbeatClient = heartbeatClient ?? throw new ArgumentNullException($"{nameof(heartbeatClient)} cannot be null");
            this.updateProcess = updateProcess ?? throw new ArgumentNullException($"{nameof(updateProcess)} cannot be null");
            this.updateService = updateService ?? throw new ArgumentNullException($"{nameof(updateService)} cannot be null");

            heartbeatUrl = new Uri(managerSettings.HeartbeatUrl);
        }

        public async Task RunUpdateHeartbeatAsync()
        {
            var heartbeatResult = await heartbeatClient.SendHeartbeatAsync(heartbeatUrl, GetUserInfo());
            if (heartbeatResult != null)
            {
                UpdateEndpoints(heartbeatResult.Heartbeat?.Endpoints);
                await UpdateApplicationAsync(heartbeatResult.Heartbeat?.UpdateInfo);
                heartbeatStorage.Save(heartbeatResult);
            }
            updateService.Start();
        }

        void UpdateEndpoints(Endpoint[] endpoints)
        {
            if (endpoints != null && endpoints.Any() && managerSettings.UpdateEndpoints(endpoints.ToList()))
            {
                updateService.Stop();
            }
        }

        async Task UpdateApplicationAsync(UpdateInfo updateInfo)
        {
            if (!IsValidUpdate(updateInfo)
                || updateProcess.ApplicationIsLatestVersion(updateInfo.Version)
                || (!updateProcess.AutoUpdateInProgress && string.IsNullOrEmpty(updateProcess.ApplicationVersion))) return;

            var file = await GetUpdateFileAsync(updateInfo);

            if (file != null && file.Exists)
            {
                updateService.Stop();
                updateProcess.RunUpdate(file.FullName);
            }
        }

        static bool IsValidUpdate(UpdateInfo updateInfo)
        {
            return updateInfo != null
                && !string.IsNullOrEmpty(updateInfo.Version)
                && !string.IsNullOrEmpty(updateInfo.Address);
        }

        UserInfo GetUserInfo()
        {
            var info = managerSettings.SystemInfo;
            return new UserInfo
            {
                OS = new OSInfo
                {
                    Name = info?.WindowsProductName,
                    Edition = info?.SystemType,
                    Version = info?.WindowsVersionNumber,
                    Localization = info?.SystemLanguage
                },
                App = new AppInfo
                {
                    Id = updateProcess.ApplicationName,
                    Version = updateProcess.ApplicationVersion,
                    Localization = info?.UiLanguage
                }
            };
        }

        async Task<FileInfo> GetUpdateFileAsync(UpdateInfo updateInfo)
        {
            var updateFilePath = GetUpdateFilePath(updateInfo.Address);
            return !updateProcess.UpdateFileIsLatestVersion(updateInfo.Version, updateFilePath)
                 ? await heartbeatClient.FetchUpdateAsync(updateInfo.Address, updateFilePath)
                 : new FileInfo(updateFilePath);
        }

        static string GetUpdateFilePath(string uri)
        {
            var fileName = Uri.TryCreate(uri, UriKind.Absolute, out var fileUri)
                ? Path.GetFileName(fileUri.LocalPath)
                : UPDATE_FILE;

            return Path.Combine(AppFolders.Settings, UPDATE_DIRECTORY, fileName);
        }
    }
}
