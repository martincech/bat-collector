﻿using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using Settings.Core.Logger;

namespace Veit.Bat.Collector.Update
{
    public class UpdateLogger : BaseLoggerConfiguration
    {
        readonly string name;

        public UpdateLogger(string name)
        {
            this.name = name;
            var config = InitLoggerConfiguration();
            LogManager.Configuration = config;
        }

        LoggingConfiguration InitLoggerConfiguration()
        {
            RegisterRenderers();
            var config = new LoggingConfiguration();

            //targets
            var mainTarget = InitMainFileTarget(ref config, name);
            var sentryTarget = InitSentryTarget(ref config);

            //define rules
            var mainRule = new LoggingRule("*", DefaultLevel, mainTarget);
            config.LoggingRules.Add(mainRule);
            DebugRules.Add(mainRule);

            var sentryRule = new LoggingRule("*", LogLevel.Error, sentryTarget);
            config.LoggingRules.Add(sentryRule);

            return config;
        }

        #region Overrides of BaseLoggerConfiguration

        protected override void RegisterRenderers()
        {
            LayoutRenderer.Register("appName", logEvent => name);
            base.RegisterRenderers();
        }

        #endregion
    }
}
