﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoUpdate;
using NLog;

namespace Veit.Bat.Collector.Update
{
    static class Program
    {
        const string APP_NAME = "BAT Collector";
        const string GUI_PROCESS_NAME = "Veit.Bat.Collector";
        const string SERVICE_NAME = "ModbusSensorsService";

        static readonly Logger logger = LogManager.GetCurrentClassLogger();
        static readonly AutoUpdateInstaller autoUpdateInstaller = new AutoUpdateInstaller();

        static void Main(string[] args)
        {
            var serviceLogger = new UpdateLogger("Collector.Update");
            serviceLogger.SetDebugMode(true);

            logger.Info("Start auto-update application.");
            RemovePreviousAutoUpdate(args?.FirstOrDefault());

            if (autoUpdateInstaller.Update(out var eTag, out var oldETag))
            {
                logger.Info("Start new process with updated version of auto-update application.");
                Process.Start(autoUpdateInstaller.UpdateFile(eTag), oldETag);
            }
            else
            {
                logger.Info("Send heartbeat and try to update collector application.");
                RunHeartbeatAsync().GetAwaiter().GetResult();
            }
            logger.Info("Stop auto-update application.");
        }

        static async Task RunHeartbeatAsync()
        {
            try
            {
                var updateHeartbeat = new UpdateHeartbeat(APP_NAME, GUI_PROCESS_NAME, SERVICE_NAME);
                await updateHeartbeat.RunUpdateHeartbeatAsync();
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        /// <summary>
        /// Delete folder with previous version of auto-update application
        /// </summary>
        /// <param name="eTag"></param>
        static void RemovePreviousAutoUpdate(string eTag)
        {
            if (!string.IsNullOrEmpty(eTag))
            {
                var updateFolder = autoUpdateInstaller.UpdateFolder(eTag);
                if (Directory.Exists(updateFolder))
                {
                    Thread.Sleep(2000);
                    logger.Info("Remove previous version of auto-update application.");
                    Directory.Delete(updateFolder, true);
                }
            }
        }
    }
}
