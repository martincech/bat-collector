﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Heartbeat;
using Newtonsoft.Json;
using NLog;

namespace Veit.Bat.Collector.Update
{
    public interface IHeartbeatClient
    {
        Task<FileInfo> FetchUpdateAsync(string url, string fileName);
        Task<HeartbeatSettings> SendHeartbeatAsync(Uri heartbeatUrl, UserInfo userInfo);
    }

    public class HeartbeatClient : IHeartbeatClient
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public async Task<FileInfo> FetchUpdateAsync(string url, string fileName)
        {
            try
            {
                var filename = ValidateTargetFile(fileName);
                logger.Debug($"Download update from {url} to {filename}.");

                using (var httpClient = new HttpClient())
                {
                    using (var request = new HttpRequestMessage(HttpMethod.Get, url))
                    {
                        var reponse = await httpClient.SendAsync(request);
                        using (var contentStream = await reponse.Content.ReadAsStreamAsync())
                        {
                            using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
                            {
                                await contentStream.CopyToAsync(stream);
                            }
                        }
                    }
                }
                return new FileInfo(filename);
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
            return null;
        }

        public async Task<HeartbeatSettings> SendHeartbeatAsync(Uri heartbeatUrl, UserInfo userInfo)
        {
            try
            {
                logger.Debug($"Send heartbeat request to {heartbeatUrl.AbsolutePath}");
                return await SendAsync(heartbeatUrl, userInfo);
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to send periodic heartbeat request.");
            }
            return null;
        }

        static async Task<HeartbeatSettings> SendAsync(Uri heartbeatUrl, UserInfo userInfo)
        {
            using (var client = new HttpClient())
            {
                using (var stringContent = new StringContent(JsonConvert.SerializeObject(userInfo), Encoding.UTF8, "application/json"))
                {
                    var result = await client.PostAsync(heartbeatUrl, stringContent);
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var etag = result.Headers.FirstOrDefault(f => f.Key == "ETag").Value?.FirstOrDefault();
                        var heartbeat = await result.Content.ReadAsStringAsync();
                        return new HeartbeatSettings
                        {
                            ETag = etag,
                            Heartbeat = JsonConvert.DeserializeObject<HeartbeatInfo>(heartbeat)
                        };
                    }
                }
            }
            return null;
        }

        static string ValidateTargetFile(string filename)
        {
            ValidateTargetFolder(Path.GetDirectoryName(filename));

            if (File.Exists(filename))
            {
                return $"{filename}_{DateTime.Now.Ticks}.msi";
            }
            return filename;
        }

        static void ValidateTargetFolder(string directory)
        {
            if (Directory.Exists(directory))
            {
                foreach (var item in Directory.GetFiles(directory).Where(file => Path.GetExtension(file).ToLowerInvariant() == ".msi"))
                {
                    try
                    {
                        File.Delete(item);
                    }
                    catch (Exception e)
                    {
                        logger.Error(e, $"Failed to delete old version of collector update file : {item}.");
                    }
                }
            }
            else
            {
                Directory.CreateDirectory(directory);
            }
        }
    }
}
