﻿using System;
using Heartbeat;
using Settings.Core.Repository;

namespace Veit.Bat.Collector.Update
{
    public class HeartbeatStorage : JsonCoreEngine<HeartbeatSettings>
    {
        public const string HEARTBEAT_FILE = "heartbeat.json";

        public HeartbeatStorage() : base(HEARTBEAT_FILE)
        {
        }
    }


    public class HeartbeatSettings
    {
        public DateTimeOffset? TimeStamp { get; set; }
        public string ETag { get; set; }
        public HeartbeatInfo Heartbeat { get; set; }
    }
}
