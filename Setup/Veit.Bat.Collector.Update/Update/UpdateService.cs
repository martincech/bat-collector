﻿using System;
using System.ServiceProcess;
using NLog;

namespace Veit.Bat.Collector.Update.Update
{
    public interface IUpdateService
    {
        /// <summary>
        /// Start BAT Collector service
        /// </summary>
        void Start();

        /// <summary>
        /// Stop BAT Collector service
        /// </summary>
        void Stop();
    }

    public class UpdateService : IUpdateService
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        readonly string serviceName;

        public UpdateService(string serviceName)
        {
            this.serviceName = serviceName;
        }

        public void Start()
        {
            try
            {
                using (var sc = new ServiceController(serviceName))
                {
                    if (sc.Status == ServiceControllerStatus.Stopped)
                    {
                        logger.Debug($"'{serviceName}' is not running. Starting service.");
                        sc.Start();
                        sc.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromMinutes(5));
                        logger.Debug($"'{serviceName}' started.");
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error($"Error occurred while starting '{serviceName}'. {e.Message}");
            }
        }

        public void Stop()
        {
            try
            {
                using (var sc = new ServiceController(serviceName))
                {
                    if (sc.Status == ServiceControllerStatus.Running)
                    {
                        logger.Debug($"Stopping '{serviceName}'.");
                        sc.Stop();
                        sc.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromMinutes(5));
                        logger.Debug($"'{serviceName}' stopped.");
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error($"Error occurred while stopping '{serviceName}'. {e.Message}");
            }
        }
    }
}
