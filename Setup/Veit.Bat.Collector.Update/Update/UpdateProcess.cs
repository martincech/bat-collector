﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using AutoUpdate;
using NLog;
using Utilities.Process;
using WindowsInstaller;

namespace Veit.Bat.Collector.Update
{
    public interface IUpdateProcess
    {
        string ApplicationName { get; }

        string ApplicationVersion { get; }

        bool AutoUpdateInProgress { get; }

        /// <summary>
        /// Validate installed application version
        /// </summary>
        /// <param name="newVersion">Available update version</param>
        /// <returns>True if installed application version is latest</returns>
        bool ApplicationIsLatestVersion(string newVersion);

        /// <summary>
        /// Validate installer package version
        /// </summary>
        /// <param name="newVersion">Available update version</param>
        /// <param name="msiFile">Path to installer package</param>
        /// <returns>True if file is latest version of installer package</returns>
        bool UpdateFileIsLatestVersion(string newVersion, string msiFile);

        /// <summary>
        /// Run update from file
        /// </summary>
        /// <param name="file">Path to update file</param>
        void RunUpdate(string file);
    }

    public class UpdateProcess : IUpdateProcess
    {
        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        readonly string guiProcessName;

        readonly InstallApplicationProperties appInfo;
        readonly IAutoUpdateRegistry autoUpdateRegistry;

        public string ApplicationName => appInfo?.DisplayName;
        public string ApplicationVersion => appInfo?.DisplayVersion;

        public UpdateProcess(string applicationName, string guiProcessName)
            : this(
                  new AutoUpdateRegistry(),
                  ProcessUtility.GetInstallApplicationProperties(applicationName),
                  guiProcessName)
        {
        }

        public UpdateProcess(
            IAutoUpdateRegistry autoUpdateRegistry,
            InstallApplicationProperties appInfo,
            string guiProcessName)
        {
            this.guiProcessName = guiProcessName ?? throw new ArgumentNullException($"{nameof(guiProcessName)} cannot be null");
            this.autoUpdateRegistry = autoUpdateRegistry ?? throw new ArgumentNullException($"{nameof(autoUpdateRegistry)} cannot be null");

            this.appInfo = appInfo;
        }

        public void RunUpdate(string file)
        {
            var updateFileVersion = GetInstallFileVersion(file);

            if (!string.IsNullOrEmpty(ApplicationVersion) && IsLatestVersion(updateFileVersion, ApplicationVersion))
            {
                logger.Info($"Last version is already installed. Update: {updateFileVersion}, App: {ApplicationVersion}.");
                return;
            }

            logger.Info($"Start auto-update process {ApplicationVersion} => {updateFileVersion}.");
            try
            {
                var restartGui = StopGui();
                Uninstall();
                Install(file, updateFileVersion);
                autoUpdateRegistry.SetUpdateState(UpdateState.None);
                if (restartGui && !string.IsNullOrEmpty(appInfo?.InstallLocation))
                {
                    StartGui();
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to start update process.");
            }

            logger.Info("Auto-update process finished.");
        }

        void Uninstall()
        {
            if (!string.IsNullOrEmpty(appInfo?.UninstallString))
            {
                autoUpdateRegistry.SetUpdateState(UpdateState.BeforeUninstall);
                logger.Debug($"Uninstall previous version of collector : {ApplicationVersion}.");
                ProcessUtility.UninstallMsi(appInfo.UninstallString);
            }
        }

        void Install(string file, string updateFileVersion)
        {
            if (AutoUpdateInProgress)
            {
                autoUpdateRegistry.SetUpdateState(UpdateState.BeforeInstall);
                logger.Debug($"Install new version of collector : {updateFileVersion}");
                ProcessUtility.InstallMsi(file, appInfo?.InstallLocation);
            }
        }

        public bool ApplicationIsLatestVersion(string newVersion) => IsLatestVersion(newVersion, ApplicationVersion);

        public bool UpdateFileIsLatestVersion(string newVersion, string msiFile) => IsLatestVersion(newVersion, GetInstallFileVersion(msiFile));

        public bool AutoUpdateInProgress => autoUpdateRegistry.GetUpdateState() != UpdateState.None;

        static bool IsLatestVersion(string newVersion, string currentVersion)
        {
            if (newVersion == null || newVersion == currentVersion) return true;
            try
            {
                return !string.IsNullOrEmpty(currentVersion) && Version.Parse(newVersion) <= Version.Parse(currentVersion);
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
            return true;
        }

        static string GetInstallFileVersion(string msiFile)
        {
            string retVal = null;

            if (File.Exists(msiFile))
            {
                try
                {
                    var classType = Type.GetTypeFromProgID("WindowsInstaller.Installer");
                    var installerObj = Activator.CreateInstance(classType);
                    var installer = installerObj as Installer;

                    // Open the msi file for reading
                    // 0 - Read, 1 - Read/Write
                    var database = installer.OpenDatabase(msiFile, 0);

                    // Fetch the requested property
                    var view = database.OpenView($"SELECT Value FROM Property WHERE Property='ProductVersion'");
                    view.Execute(null);

                    // Read in the fetched record
                    var record = view.Fetch();
                    if (record != null) retVal = record.get_StringData(1);

                    // Release references
                    view.Close();
                    Marshal.FinalReleaseComObject(database);
                    Marshal.FinalReleaseComObject(installer);
                }
                catch (Exception e)
                {
                    logger.Error(e, $"Failed to load ProductVersion from MSI package.");
                }
            }
            return retVal;
        }

        #region Start/Stop collector GUI application

        bool StopGui()
        {
            if (ProcessUtility.GetSingleProcess(guiProcessName) != null)
            {
                if (ProcessUtility.StopProcess(guiProcessName))
                {
                    logger.Debug($"Stopped GUI process {guiProcessName}.");
                    return true;
                }
                else
                {
                    logger.Error($"Cannot stop process: {guiProcessName}.");
                }
            }
            return false;
        }

        void StartGui()
        {
            try
            {
                logger.Debug("Start UI after auto update process.");
                ProcessUtility.RunAsUser(Path.Combine(appInfo.InstallLocation, $"{guiProcessName}.exe"));
            }
            catch (Exception e)
            {
                logger.Error(e, "Restart application failed.");
            }
        }

        #endregion
    }
}
