﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.IO;
using NLog;

namespace ServiceUtil
{
    public static class ServiceHelper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static void InstallServices(IEnumerable<string> services)
        {
            ChangeServices(services);
        }

        public static void UninstallServices(IEnumerable<string> services)
        {
            ChangeServices(services, "/u");
        }

        private static void ChangeServices(IEnumerable<string> services, string param = null)
        {
            if (services == null) return;
            foreach (var service in services)
            {
                ChangeService(service, param);
            }
        }

        private static void ChangeService(string service, string param = null)
        {
            try
            {
                if (!File.Exists(service))
                {
                    return;
                }
                var args = new List<string> { service };
                if (!string.IsNullOrEmpty(param)) args.Insert(0, param);
                ManagedInstallerClass.InstallHelper(args.ToArray());
            }
            catch (Exception e)
            {
                logger.Warn(e, "change service exception");
            }
        }
    }
}
