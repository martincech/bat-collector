﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;

namespace ServiceUtil
{
    static class Program
    {
        static void Main(string[] args)
        {
            if (args == null || !args.Any()) return;
            if (!IsAdministrator())
            {
                RunAsAdmin(args);
                return;
            }

            try
            {
                //add Path to arguments
                var services = new List<string>();
                foreach (var service in args.Skip(1))
                {
                    services.Add(System.IO.Path.Combine(Path, service));
                }


                switch (args.First())
                {
                    case "-i":
                        ServiceHelper.InstallServices(services);
                        break;
                    case "-u":
                        ServiceHelper.UninstallServices(services);
                        break;
                    default:
                        throw new NotSupportedException($"Unexpected Case: {args.First()}");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static string Path
        {
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }

        private static void RunAsAdmin(string[] args)
        {
            var exeName = Process.GetCurrentProcess().MainModule.FileName;
            var startInfo = new ProcessStartInfo(exeName, string.Join(" ", args)) { Verb = "runas" };
            Process.Start(startInfo);
        }

        private static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
