﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Communication.Contract.Wcf.BindingConstants;
using Communication.Contract.Wcf.DataSources;
using Communication.EventToQueue;
using Communication.Manager;
using Moq;
using Veit.Mqueue;
using Xunit;

namespace ServiceCommon.Tests
{
    public class ServiceBaseImplTests : IDisposable
    {
        private readonly ServiceBaseImpl uut;
        private readonly List<IQueue> processQueues;
        private readonly List<ServiceHost> serviceHosts;
        private readonly Dictionary<ISourceManager, ManagerEventsToQueue> sourceManagers;
        private const string SERVICE_NAME = "Test";

        internal class MockServiceBaseImpl : ServiceBaseImpl
        {
            public MockServiceBaseImpl(string name) : base(name)
            {
            }
        }


        public ServiceBaseImplTests()
        {
            uut = new MockServiceBaseImpl(SERVICE_NAME);
            processQueues = uut.GetType().BaseType.GetField(nameof(processQueues), System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(uut) as List<IQueue>;
            serviceHosts = uut.GetType().BaseType.GetField(nameof(serviceHosts), System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(uut) as List<ServiceHost>;
            sourceManagers = uut.GetType().BaseType.GetField("managers", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(uut) as Dictionary<ISourceManager, ManagerEventsToQueue>;

            Assert.NotNull(processQueues);
            Assert.NotNull(serviceHosts);
            Assert.NotNull(sourceManagers);
        }

        public void Dispose()
        {
            uut.Stop();
            GC.SuppressFinalize(this);
        }

        [Fact]
        public void NameEquals_TheNameGiven()
        {
            Assert.Equal(SERVICE_NAME, uut.Name);
        }


        [Fact]
        public void ModbusManager_Created()
        {
            var manager = uut.CreateModbusManager();
            Assert.True(sourceManagers.ContainsKey(manager));
            Assert.Single(serviceHosts);
            Assert.Single(processQueues); // to process service host queries
            Assert.Equal(1, serviceHosts.Count(sh => sh.SingletonInstance is IModbusService));
        }

        [Fact]
        public void ConsoleRepository_Created()
        {
            uut.CreateConsoleRepository();

            Assert.Single(processQueues);
            Assert.Empty(sourceManagers);
        }

        [Fact]
        public void CsvRepository_Created()
        {
            uut.CreateCsvRepository();
            Assert.Single(processQueues);
            Assert.Empty(sourceManagers);
        }

        [Fact]
        public void WebApiRepository_Created()
        {
            var originCount = processQueues.Count;
            _ = uut.CreateWebApiRepository(ServiceNetPipeBindingConstants.ModbusPipeAddress + ServiceNetPipeBindingConstants.ConnectionSubAddress);

            Assert.Equal(originCount + 1, processQueues.Count);
            Assert.Empty(sourceManagers);
        }

        [Fact]
        public void MultipleManagers_ConnectedTo_MultipleRepositories()
        {
            var managers = new List<ISourceManager>();
            var localRepos = new List<IQueue>();
            managers.Add(uut.CreateModbusManager());

            localRepos.Add(uut.CreateCsvRepository());
            localRepos.Add(uut.CreateConsoleRepository());
            localRepos.AddRange(uut.CreateWebApiRepository(ServiceNetPipeBindingConstants.ModbusPipeAddress + ServiceNetPipeBindingConstants.ConnectionSubAddress));
            localRepos.Add(uut.CreateAdditionalRepository(new Mock<IQueue>().Object));

            foreach (var manager in managers)
            {
                Assert.True(sourceManagers.ContainsKey(manager));

                foreach (var repository in localRepos)
                {
                    Assert.True(sourceManagers[manager].GetRepositories().Contains(repository), $"Manager {manager.GetType()} does not contain repository {repository.GetType()}");
                }
            }
        }
    }
}
