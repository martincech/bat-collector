﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity;
using Communication.Connectivity.Modbus;
using Communication.Connectivity.Modbus.LineRequest;
using Communication.Contract.Wcf.DataSources;
using Communication.Manager.Modbus;
using Moq;
using ServiceCommon.Wcf.Modbus;
using Settings.Core;
using Veit.Bat.Common;
using Xunit;

namespace ServiceCommon.Tests
{
    public class ModbusWcfServiceTests : BaseDataSourceCallbackTests<ModbusWcfService, IModbusService, IModbusServiceCallback, IModbusSourceManager>
    {
        private List<Mock<IModbusConnectivitySource>> mockConnectedSources;
        private Mock<IEthernetSourceCreator> mockEthernetCreator;


        public ModbusWcfServiceTests()
            : base()
        {
        }

        protected override ModbusWcfService SetupUut()
        {
            mockConnectedSources = new List<Mock<IModbusConnectivitySource>>
         {
            new Mock<IModbusConnectivitySource>(),
            new Mock<IModbusConnectivitySource>(),
            new Mock<IModbusConnectivitySource>(),
         };
            mockEthernetCreator = new Mock<IEthernetSourceCreator>();
            var i = 0;
            foreach (var mockConnectedSource in mockConnectedSources)
            {
                mockConnectedSource.SetupAllProperties();
                mockConnectedSource.Setup(s => s.Id).Returns(i.ToString);
                mockConnectedSource.Setup(s => s.Type).Returns(SourceType.Ethernet);
                i++;
            }
            MockManager.Setup(m => m.ConnectedSources).Returns(mockConnectedSources.Select(m => m.Object));
            return new ModbusWcfService(
               MockManager.Object,
               mockEthernetCreator.Object,
               MockActionQueue.Object,
               new Mock<IDiagnostic>().Object);
        }

        [Fact]
        public void ScanModbus_Executed_OnSingleSource()
        {
            //request from client
            var scanAddresses = new byte[] { 1, 2, 3 };
            foreach (var requestedMock in mockConnectedSources)
            {
                ChannelsToService.First().ScanPortAddresses(new[] { requestedMock.Object.Id }, scanAddresses);
                WaitForMessageDelivery();

                //verify
                foreach (var scanAddress in scanAddresses)
                {
                    requestedMock.Verify(s => s.ScanForDevices(It.Is<ModbusScanRequest>(sr => sr.AddressFrom == scanAddress && sr.AddressTo == scanAddress), It.IsAny<Action>()), Times.Once);
                }

                requestedMock.Verify(s => s.ScanForDevices(It.IsAny<ModbusScanRequest>(), It.IsAny<Action>()), Times.Exactly(scanAddresses.Length));
            }
        }

        [Fact]
        public void ScanModbus_Executed_OnMultipleSources()
        {
            //request from client
            var scanAddresses = new byte[] { 1, 2, 3 };
            ChannelsToService.First().ScanPortAddresses(mockConnectedSources.Select(m => m.Object.Id), scanAddresses);
            WaitForMessageDelivery();
            foreach (var requestedMock in mockConnectedSources)
            {
                //verify
                foreach (var scanAddress in scanAddresses)
                {
                    requestedMock.Verify(s => s.ScanForDevices(It.Is<ModbusScanRequest>(sr => sr.AddressFrom == scanAddress && sr.AddressTo == scanAddress), It.IsAny<Action>()), Times.Once);
                }

                requestedMock.Verify(s => s.ScanForDevices(It.IsAny<ModbusScanRequest>(), It.IsAny<Action>()), Times.Exactly(scanAddresses.Length));
            }

        }


        // test others IModbusServiceCallback methods
        protected override void CallAndVerifyCallbackInvocations(IEnumerable<Mock<IModbusServiceCallback>> notSubscribedClients = null)
        {
            Uut.ScanProgress(new Percent(5));
            Uut.Activated("1", "1");
            Uut.Deactivated("2", "2");
            base.CallAndVerifyCallbackInvocations(notSubscribedClients);
        }

        protected override void VerifyCallbackInvocations(
           IEnumerable<Mock<IModbusServiceCallback>> notSubscribedClients = null)
        {
            notSubscribedClients = notSubscribedClients ?? new List<Mock<IModbusServiceCallback>>();

            foreach (var callback in MockCallbacks.Except(notSubscribedClients))
            {
                callback.Verify(c => c.ScanProgress(It.IsAny<Percent>()), Times.Once);
                callback.Verify(c => c.ScanProgress(It.Is<Percent>(p => p == new Percent(5))), Times.Once);

                callback.Verify(c => c.Activated(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
                callback.Verify(c => c.Activated(It.Is<string>(p => p == "1"), It.Is<string>(p => p == "1")), Times.Once);

                callback.Verify(c => c.Deactivated(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
                callback.Verify(c => c.Deactivated(It.Is<string>(p => p == "2"), It.Is<string>(p => p == "2")), Times.Once);
            }
            foreach (var callback in notSubscribedClients)
            {
                callback.Verify(c => c.ScanProgress(It.IsAny<Percent>()), Times.Never);
            }

            base.VerifyCallbackInvocations(notSubscribedClients);
        }
    }
}
