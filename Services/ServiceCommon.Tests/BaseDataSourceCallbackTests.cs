﻿using System.Collections.Generic;
using System.Linq;
using Communication.Contract.Wcf.DataSources;
using Communication.Manager;
using Moq;
using ServiceCommon.Wcf;
using Services.PublishSubscribe.Contracts;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Bat.Common.Sample.Base;

namespace ServiceCommon.Tests
{
    public abstract class BaseDataSourceCallbackTests<S, I> : BaseDataSourceCallbackTests<S, I, IDataSourceCallback, ISourceManager>
       where S : BaseDataSourceCallback<IDataSourceCallback>, I
       where I : class, ISubscriptionContract
    {
    }

    public abstract class BaseDataSourceCallbackTests<S, I, C, M> : BaseSubscriptionTests<S, I, C>
       where S : BaseDataSourceCallback<C>, I, IDataSourceCallback
       where I : class, ISubscriptionContract
       where C : class, IDataSourceCallback
       where M : class, ISourceManager
    {
        protected readonly Mock<M> MockManager;


        protected BaseDataSourceCallbackTests()
             : base()
        {
            MockManager = new Mock<M>();

            SetupMockActionQueue<int>();
            SetupMockActionQueue<TestSample>();
            SetupMockActionQueue<TimeSample>();
            SetupMockActionQueue<SourceConnectivityInfo>();

            Init();
        }


        protected override void SetupCallback(Mock<C> callback)
        {
            callback.Setup(c => c.SampleReaded(It.IsAny<TestSample>()));
            callback.Setup(c => c.Connected(It.IsAny<SourceConnectivityInfo>()));
            callback.Setup(c => c.Disconnected(It.IsAny<SourceConnectivityInfo>()));
        }

        protected override void CallAndVerifyCallbackInvocations(IEnumerable<Mock<C>> notSubscribedClients = null)
        {
            Uut.SampleReaded(new TestSample());
            Uut.Connected(new SourceConnectivityInfo());
            Uut.Disconnected(new SourceConnectivityInfo());
            WaitForMessageDelivery();
            VerifyCallbackInvocations(notSubscribedClients);
        }

        protected override void VerifyCallbackInvocations(IEnumerable<Mock<C>> notSubscribedClients = null)
        {
            notSubscribedClients = notSubscribedClients ?? new List<Mock<C>>();

            foreach (var callback in MockCallbacks.Except(notSubscribedClients))
            {
                callback.Verify(c => c.SampleReaded(It.IsAny<TestSample>()), Times.Once);
                callback.Verify(c => c.Connected(It.IsAny<SourceConnectivityInfo>()), Times.Once);
                callback.Verify(c => c.Disconnected(It.IsAny<SourceConnectivityInfo>()), Times.Once);
            }
            foreach (var callback in notSubscribedClients)
            {
                callback.Verify(c => c.SampleReaded(It.IsAny<TestSample>()), Times.Never);
                callback.Verify(c => c.Connected(It.IsAny<SourceConnectivityInfo>()), Times.Never);
                callback.Verify(c => c.Disconnected(It.IsAny<SourceConnectivityInfo>()), Times.Never);
            }
        }
    }
}
