﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Moq;
using Services.Hosting;
using Services.PublishSubscribe;
using Services.PublishSubscribe.Contracts;
using Veit.Mqueue;
using Xunit;

namespace ServiceCommon.Tests
{
    public abstract class BaseSubscriptionTests<S, I, C> : IDisposable
      where S : SubscriptionManager<C>, I // manager
      where I : class, ISubscriptionContract //client contract
      where C : class //callback
    {
        protected S Uut;
        protected IClientFactory<S, I> WcfFactory;
        protected readonly List<Mock<C>> MockCallbacks;
        protected readonly List<I> ChannelsToService;
        protected readonly Mock<IProcessQueue> MockActionQueue;

        protected BaseSubscriptionTests()
        {
            MockCallbacks = new List<Mock<C>>();
            ChannelsToService = new List<I>();
            MockActionQueue = new Mock<IProcessQueue>();
        }

        protected void Init()
        {
            Uut = SetupUut();
            WcfFactory = InProcFactory.CreateClientFactory<S, I>();
            WcfFactory.SetSingleton(Uut);

            for (var i = 0; i < 5; i++)
            {
                var callback = new Mock<C>();
                callback.SetupAllProperties();
                SetupCallback(callback);
                MockCallbacks.Add(callback);
                ChannelsToService.Add(WcfFactory.CreateInstance(callback.Object));
            }
        }


        public void Dispose()
        {
            foreach (var channelToService in ChannelsToService)
            {
                WcfFactory.CloseProxy(channelToService);
            }
            InProcFactory.StopService<S>();
        }

        [Fact]
        public void Client_IsAmongSubscribers_AfterSubscribe()
        {
            var channelToService = ChannelsToService.First();
            Assert.NotNull(Uut.TransientSubscribers);
            Assert.Empty(Uut.TransientSubscribers);
            channelToService.Subscribe();
            WaitForMessageDelivery();
            Assert.Single(Uut.TransientSubscribers);
        }

        [Fact]
        public void Client_IsNotAmongSubscribers_AfterSubscribe_AndUnsubscribe()
        {
            var channelToService = ChannelsToService.First();
            Assert.NotNull(Uut.TransientSubscribers);
            Assert.Empty(Uut.TransientSubscribers);
            channelToService.Subscribe();
            WaitForMessageDelivery();
            Assert.Single(Uut.TransientSubscribers);
            channelToService.Unsubscribe();
            WaitForMessageDelivery();
            Assert.Empty(Uut.TransientSubscribers);
        }

        [Fact]
        public void Client_MultipleSubscribersRegistered()
        {
            SubscribeAllClients();
            Assert.NotNull(Uut.TransientSubscribers);
            Assert.Equal(ChannelsToService.Count, Uut.TransientSubscribers.Count());
        }


        protected abstract void CallAndVerifyCallbackInvocations(IEnumerable<Mock<C>> mocks = null);
        protected abstract void VerifyCallbackInvocations(IEnumerable<Mock<C>> notSubscribedClients = null);
        protected void SubscribeAllClients()
        {

            foreach (var channelToService in ChannelsToService)
            {
                channelToService.Subscribe();
            }
            WaitForMessageDelivery();
        }

        protected abstract S SetupUut();

        protected abstract void SetupCallback(Mock<C> callback);

        protected static void WaitForMessageDelivery()
        {
            Thread.Sleep(200);
        }


        protected void SetupMockActionQueue<T>()
        {
            var aToInvoke = new List<Func<T, bool>>();
            MockActionQueue
               .Setup(q => q.AddProcessing(It.IsAny<Func<T, bool>>()))
               .Callback<Func<T, bool>>(a =>
               {
                   if (!aToInvoke.Contains(a))
                       aToInvoke.Add(a);
               })
               .Returns(true);
            MockActionQueue
               .Setup(q => q.AddProcessing(It.IsAny<Type>(), It.IsAny<Func<T, bool>>()))
               .Callback<Type, Func<T, bool>>((t, a) =>
               {
                   if (!aToInvoke.Contains(a))
                       aToInvoke.Add(a);
               })
               .Returns(true);
            MockActionQueue
               .Setup(q => q.RemoveProcessing(It.IsAny<Func<T, bool>>()))
               .Callback<Func<T, bool>>(a =>
               {
                   if (aToInvoke.Contains(a))
                   {
                       aToInvoke.Remove(a);
                   }
               })
               .Returns(true);

            MockActionQueue
               .Setup(q => q.RemoveProcessing(It.IsAny<Type>(), It.IsAny<Func<T, bool>>()))
               .Callback<Type, Func<T, bool>>((t, a) =>
               {
                   if (aToInvoke.Contains(a))
                   {
                       aToInvoke.Remove(a);
                   }
               })
               .Returns(true);

            MockActionQueue
               .Setup(q => q.Enqueue(It.IsAny<T>(), Priority.Normal))
               .Callback<object,Priority>((r, p) =>
               {
                   Assert.IsType<T>(r);
                   foreach (var func in aToInvoke)
                   {
                       func((T)r);
                   }
               })
               .Returns(true);
        }
    }
}
