﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.WebApi;
using Moq;
using Repository.Web.WebApi;
using ServiceCommon.Wcf.WebLogin;
using Veit.Mqueue;
using Xunit;

namespace ServiceCommon.Tests
{
    public class WebLoginWcfServiceTests
      : BaseSubscriptionTests<WebLoginWcfService, IWebApiConnection, IWebApiConnectionCallbacks>
    {
        private List<Mock<IWebApi>> mockWebApis;
        private List<WebApiConnectionInfo> connectionInfos;

        #region Overrides of BaseSubscriptionTests<WebLoginWcfService,IWebApiConnection,IWebApiConnectionCallbacks>

        public WebLoginWcfServiceTests()
            : base()
        {
            SetupMockActionQueue<WebApiConnectionInfo>();
            SetupMockActionQueue<IEnumerable<WebApiConnectionInfo>>();
            Init();
        }

        protected override void CallAndVerifyCallbackInvocations(
           IEnumerable<Mock<IWebApiConnectionCallbacks>> notSubscribedClients = null)
        {
            Uut.ConnectionStatusChanged(new WebApiConnectionInfo());
            Uut.ExistingConnections(new List<WebApiConnectionInfo>
             {
                new WebApiConnectionInfo(),
                new WebApiConnectionInfo(),
                new WebApiConnectionInfo(),
             });
            WaitForMessageDelivery();
            VerifyCallbackInvocations(notSubscribedClients);
        }

        protected override void VerifyCallbackInvocations(
           IEnumerable<Mock<IWebApiConnectionCallbacks>> notSubscribedClients = null)
        {
            notSubscribedClients = notSubscribedClients ?? new List<Mock<IWebApiConnectionCallbacks>>();

            foreach (var callback in MockCallbacks.Except(notSubscribedClients))
            {
                callback.Verify(c => c.ConnectionStatusChanged(It.IsAny<WebApiConnectionInfo>()),
                   Times.Once);
                callback.Verify(c => c.ExistingConnections(It.IsAny<IEnumerable<WebApiConnectionInfo>>()), Times.Once);
            }
            foreach (var callback in notSubscribedClients)
            {
                callback.Verify(c => c.ConnectionStatusChanged(It.IsAny<WebApiConnectionInfo>()),
                   Times.Never);
                callback.Verify(c => c.ExistingConnections(It.IsAny<IEnumerable<WebApiConnectionInfo>>()), Times.Never);
            }
        }

        protected override WebLoginWcfService SetupUut()
        {
            mockWebApis = new List<Mock<IWebApi>>();
            connectionInfos = new List<WebApiConnectionInfo>();
            for (var i = 0; i < 4; i++)
            {
                connectionInfos.Add(new WebApiConnectionInfo
                {
                    Login = new Login { UserName = $"u{i}" },
                    BatApi = new Url($"http://{i}.uk")
                });
            }
            foreach (var connectionInfo in connectionInfos)
            {
                var mock = new Mock<IWebApi>();
                mock.Setup(m => m.ConnectionInfo).Returns(connectionInfo);
                mock.Setup(m => m.LoginAsync(It.IsAny<WebApiConnectionInfo>(), It.IsAny<CancellationToken>()))
                   .Callback<WebApiConnectionInfo, CancellationToken>((info, token) =>
                   {
                       mock.Raise(m => m.ConnectivityStatusChanged += null, mock.Object, info);
                   })
                   .Returns(Task.FromResult(true));
                mockWebApis.Add(mock);
            }

            return new WebLoginWcfService(mockWebApis.Select(m => m.Object), new List<IQueue>());
        }

        protected override void SetupCallback(Mock<IWebApiConnectionCallbacks> callback)
        {
            callback.Setup(c => c.ConnectionStatusChanged(It.IsAny<WebApiConnectionInfo>()));
            callback.Setup(c => c.ExistingConnections(It.IsAny<IEnumerable<WebApiConnectionInfo>>()));
        }

        #endregion

        [Fact]
        public void ChangeServerConnection_LoginExecuted_ForFirst_CloudWebApiConnection()
        {
            //request from client
            LoginExecutedOnSingleApi();
        }

        private void LoginExecutedOnSingleApi()
        {
            SubscribeAllClients();
            var info = new WebApiConnectionInfo
            {
                Login = new Login(),
                BatApi = new Url("http://localhost")
            };
            ChannelsToService.First().ChangeServerConnection(info);
            WaitForMessageDelivery();
            var apis = mockWebApis;
            Assert.True(apis.Count() > 1);
            var firstApi = apis.First();

            foreach (var mock in apis.Except(new[] { firstApi }))
            {
                mock.Verify(m => m.LoginAsync(It.IsAny<WebApiConnectionInfo>(), It.IsAny<CancellationToken>()), Times.Never);
            }
            firstApi.Verify(m => m.LoginAsync(It.IsAny<WebApiConnectionInfo>(), It.IsAny<CancellationToken>()), Times.Once);
        }

        [Fact]
        public void ChangeServerConnection_LoginExecuted_ForFirst_NonCloudWebApiConnection()
        {
            //request from client
            LoginExecutedOnSingleApi();
        }
    }
}
