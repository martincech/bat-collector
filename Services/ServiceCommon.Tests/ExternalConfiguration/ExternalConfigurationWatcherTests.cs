﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using Communication.Contract.Wcf.DataContracts;
using ServiceCommon.ExternalConfiguration;
using ServiceCommon.ExternalConfiguration.Models;
using Xunit;

namespace ServiceCommon.Tests.ExternalConfiguration
{
    #region Class fixture

    public class ExternalConfigurationWatcherTestsFixture : IDisposable
    {
        private const string CURVE_NAME = "CurveTest";
        private const string CONFIGURATION_NAME = "ConfigurationTest";
        private const string WEIGHING_NAME = "WeighingTest";

        public ExternalConfigurationWatcherTestsFixture()
        {
            Info = new ExternalWeighingInfo
            {
                Csv = new Csv
                {
                    Path = "TestsCsv",
                    DecimalSeparator = '.',
                    Delimiter = ';',
                    Encoding = "UTF-8"
                },
                CurveFileParams = CURVE_NAME,
                WeighingConfigurationFileParams = CONFIGURATION_NAME,
                WeighingParametersFileParams = WEIGHING_NAME
            };

            CleanUp();

            var w = new ExternalConfigurationWatcher(null);
            var field = w.GetType()
               .GetField("FILE_SUFFIX", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            if (field != null)
            {
                FileSuffix = field.GetValue(w).ToString();
            }
        }

        public ExternalWeighingInfo Info { get; private set; }
        public string FileSuffix { get; private set; }

        public void Dispose()
        {
            CleanUp();
            GC.SuppressFinalize(this);
        }

        private void CleanUp()
        {
            //delete folder with files
            if (Directory.Exists(Info.Csv.Path))
            {
                Directory.Delete(Info.Csv.Path, true);
            }
        }
    }

    #endregion


    public class ExternalConfigurationWatcherTests : IDisposable, IClassFixture<ExternalConfigurationWatcherTestsFixture>
    {
        private ExternalConfigurationWatcher watcher;
        private readonly ExternalConfigurationWatcherTestsFixture fixture;
        private static readonly object lockObj = new object();

        #region Initialization & clean up

        public ExternalConfigurationWatcherTests(ExternalConfigurationWatcherTestsFixture fixture)
        {
            this.fixture = fixture;
            watcher = new ExternalConfigurationWatcher(fixture.Info);
        }

        public void Dispose()
        {
            lock(lockObj)
            {
                File.Delete(GetName(fixture.Info.CurveFileParams));
                File.Delete(GetName(fixture.Info.WeighingConfigurationFileParams));
                File.Delete(GetName(fixture.Info.WeighingParametersFileParams));
            }

            watcher.Dispose();
            watcher = null;
            GC.SuppressFinalize(this);
        }

        #endregion


        [Fact]
        public void Initialization_AfterWatcherStart_NoFiles()
        {
            //No files exist => no initialize event
            ExternalWeighingSettings settings = null;
            var raised = StartWatcherProcess(ref settings);

            Assert.False(raised);
            Assert.Null(settings);
        }

        [Fact]
        public void Initialization_AfterWatcherStart_CurveFileExist()
        {
            CreateFile(fixture.Info.CurveFileParams);
            ExternalWeighingSettings settings = null;
            var raised = StartWatcherProcess(ref settings);

            Assert.True(raised);
            Assert.NotNull(settings);
            Assert.NotNull(settings.Curves);
            Assert.Null(settings.Configuration);
            Assert.Null(settings.Weighing);
        }

        [Fact]
        public void Initialization_AfterWatcherStart_ConfigurationFileExist()
        {
            CreateFile(fixture.Info.WeighingConfigurationFileParams);
            ExternalWeighingSettings settings = null;
            var raised = StartWatcherProcess(ref settings);

            Assert.True(raised);
            Assert.NotNull(settings);
            Assert.Null(settings.Curves);
            Assert.NotNull(settings.Configuration);
            Assert.Null(settings.Weighing);
        }

        [Fact]
        public void Initialization_AfterWatcherStart_WeighingFileExist()
        {
            CreateFile(fixture.Info.WeighingParametersFileParams);
            ExternalWeighingSettings settings = null;
            var raised = StartWatcherProcess(ref settings);

            Assert.True(raised);
            Assert.NotNull(settings);
            Assert.Null(settings.Curves);
            Assert.Null(settings.Configuration);
            Assert.NotNull(settings.Weighing);
        }

        [Fact]
        public void Initialization_AfterWatcherStart_AllFilesExist()
        {
            CreateFile(fixture.Info.WeighingParametersFileParams);
            CreateFile(fixture.Info.WeighingConfigurationFileParams);
            CreateFile(fixture.Info.CurveFileParams);
            ExternalWeighingSettings settings = null;
            var raised = StartWatcherProcess(ref settings);

            Assert.True(raised);
            Assert.NotNull(settings);
            Assert.NotNull(settings.Curves);
            Assert.NotNull(settings.Configuration);
            Assert.NotNull(settings.Weighing);
        }



        [Fact]
        public void RestartWatcher()
        {
            watcher.Start();
            const int waitForRaisedEvents = 2000;

            //create curve
            var curveChanged = false;
            watcher.CurveChanged += (sender, curves) =>
            {
                curveChanged = true;
            };
            CreateFile(fixture.Info.CurveFileParams);

            Thread.Sleep(waitForRaisedEvents);
            Assert.True(curveChanged);
            curveChanged = false;


            watcher.Stop();
            UpdateFile(fixture.Info.CurveFileParams, "text 001");
            Thread.Sleep(waitForRaisedEvents * 2);
            Assert.False(curveChanged);


            watcher.Start();
            UpdateFile(fixture.Info.CurveFileParams, "another text XYZ");
            Thread.Sleep(waitForRaisedEvents);
            Assert.True(curveChanged);
        }

        [Fact]
        public void FileCreated()
        {
            watcher.Start();
            const int waitForRaisedEvents = 2000;

            //create curve
            var curveChanged = false;
            watcher.CurveChanged += (sender, curves) =>
            {
                curveChanged = true;
            };
            CreateFile(fixture.Info.CurveFileParams);

            //create configuration
            var configurationChanged = false;
            watcher.WeighingConfigurationChanged += (sender, curves) =>
            {
                configurationChanged = true;
            };
            CreateFile(fixture.Info.WeighingConfigurationFileParams);

            //create weighing
            var weighingChanged = false;
            watcher.WeighingParametersChanged += (sender, curves) =>
            {
                weighingChanged = true;
            };
            CreateFile(fixture.Info.WeighingParametersFileParams);


            Thread.Sleep(waitForRaisedEvents);
            Assert.True(curveChanged);
            Assert.True(configurationChanged);
            Assert.True(weighingChanged);
        }

        [Fact]
        public void FileChanged()
        {
            watcher.Start();
            CreateFile(fixture.Info.CurveFileParams);
            CreateFile(fixture.Info.WeighingConfigurationFileParams);
            CreateFile(fixture.Info.WeighingParametersFileParams);

            const int waitForRaisedEvents = 2000;
            const string text = "some text";

            //create curve
            var curveChanged = false;
            watcher.CurveChanged += (sender, curves) =>
            {
                curveChanged = true;
            };
            UpdateFile(fixture.Info.CurveFileParams, text);

            //create configuration
            var configurationChanged = false;
            watcher.WeighingConfigurationChanged += (sender, curves) =>
            {
                configurationChanged = true;
            };
            UpdateFile(fixture.Info.WeighingConfigurationFileParams, text);

            //create weighing
            var weighingChanged = false;
            watcher.WeighingParametersChanged += (sender, curves) =>
            {
                weighingChanged = true;
            };
            UpdateFile(fixture.Info.WeighingParametersFileParams, text);


            Thread.Sleep(waitForRaisedEvents);
            Assert.True(curveChanged);
            Assert.True(configurationChanged);
            Assert.True(weighingChanged);
        }


        #region Private helpers

        private bool StartWatcherProcess(ref ExternalWeighingSettings settings)
        {
            var raised = false;
            ExternalWeighingSettings weighingSettings = null;

            watcher.Initialized += (sender, s) =>
            {
                raised = true;
                weighingSettings = s;
            };
            watcher.Start();
            settings = weighingSettings;
            return raised;
        }

        private void CreateFile(string name)
        {
            lock(lockObj)
            {
                if (!Directory.Exists(fixture.Info.Csv.Path))
                {
                    Directory.CreateDirectory(fixture.Info.Csv.Path);
                }
                File.WriteAllText(GetName(name), " ");
            }
        }

        private void UpdateFile(string name, string content)
        {
            lock(lockObj)
            {
                File.WriteAllText(GetName(name), content);
            }
        }

        private string GetName(string file)
        {
            return Path.Combine(fixture.Info.Csv.Path, file + fixture.FileSuffix);
        }

        #endregion
    }
}
