﻿using System;
using Communication.Contract.Wcf.BindingConstants;
using NLog;
using ServiceCommon;

namespace ModbusSensors.Core
{
    public class ModbusSensorsServiceImpl : ServiceBaseImpl
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static string ServiceName { get; } = "ModbusSensorsService";

        public ModbusSensorsServiceImpl()
           : base(ServiceName) { }

        public override void Start(string[] args)
        {
            logger.Info("Starting {0}", Name);
            try
            {
                CreateConsoleRepository();
                CreateWebApiRepository(ServiceNetPipeBindingConstants.ModbusPipeAddress +
                                       ServiceNetPipeBindingConstants.ConnectionSubAddress);
                CreateCsvRepository();

                CreateFileSettingsRepository();
                CreateModbusManager();
            }
            catch (Exception e)
            {
                logger.Fatal(e);
                throw;
            }


            base.Start(args);
            logger.Info("Started {0}", Name);
        }
    }
}
