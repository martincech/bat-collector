﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using NLog;
using ServiceCommon.Properties;

namespace ServiceCommon
{
    public class CommonServiceBase : ServiceBase
    {
        private readonly ServiceBaseImpl serviceImpl;
        private EventLog eventLog;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();


        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
            SERVICE_SHUTDOWN_PENDING = 0x00000008,
            SERVICE_SHUTDOWN = 0x00000009
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long DwServiceType { get; set; }
            public ServiceState DwCurrentState { get; set; }
            public long DwControlsAccepted { get; set; }
            public long DwWin32ExitCode { get; set; }
            public long DwServiceSpecificExitCode { get; set; }
            public long DwCheckPoint { get; set; }
            public long DwWaitHint { get; set; }
        }

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        /// <summary>
        /// Creates a new instance of the <see cref="T:System.ServiceProcess.ServiceBase"/> class.
        /// </summary>
        public CommonServiceBase(string serviceName, Func<ServiceBaseImpl> serviceImplFactory)
        {
            try
            {
                InitializeComponent();
                ServiceName = serviceName;
                if (serviceImplFactory == null)
                {
                    throw new ArgumentNullException(nameof(serviceImplFactory));
                }

                if (!EventLog.SourceExists(ServiceName))
                {
                    EventLog.CreateEventSource(ServiceName, ServiceName);
                }
                eventLog.Source = ServiceName;
                eventLog.Log = ServiceName;
                serviceImpl = serviceImplFactory.Invoke();
            }
            catch (Exception e)
            {
                logger.Fatal(e, "Service: {0} initialize exception", serviceName);
            }
        }
        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command. </param>
        protected override void OnStart(string[] args)
        {
            try
            {
                var currentDomain = AppDomain.CurrentDomain;
                currentDomain.UnhandledException += CurrentDomain_UnhandledException;


                UpdateState(ServiceState.SERVICE_START_PENDING);
                base.OnStart(args);
                serviceImpl.Start(args);
                UpdateState(ServiceState.SERVICE_RUNNING);
            }
            catch (Exception e)
            {
                logger.Error(e, $"{ServiceName} error on start");
            }
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = (Exception)e.ExceptionObject;
            logger.Error(exception, "Service OnStart unhandled exception");
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                UpdateState(ServiceState.SERVICE_STOP_PENDING);
                base.OnStop();
                serviceImpl.Stop();
                UpdateState(ServiceState.SERVICE_STOPPED);
            }
            catch (Exception e)
            {
                logger.Error(e, $"{ServiceName} error on stop");
            }
        }

        #region Overrides of ServiceBase

        /// <summary>
        /// When implemented in a derived class, <see cref="M:System.ServiceProcess.ServiceBase.OnContinue"/> runs when a Continue command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service resumes normal functioning after being paused.
        /// </summary>
        protected override void OnContinue()
        {
            try
            {
                UpdateState(ServiceState.SERVICE_CONTINUE_PENDING);
                base.OnContinue();
                UpdateState(ServiceState.SERVICE_RUNNING);
            }
            catch (Exception e)
            {
                logger.Error(e, $"{ServiceName} error on continue");
            }
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Pause command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service pauses.
        /// </summary>
        protected override void OnPause()
        {
            try
            {
                UpdateState(ServiceState.SERVICE_PAUSE_PENDING);
                base.OnPause();
                UpdateState(ServiceState.SERVICE_PAUSED);
            }
            catch (Exception e)
            {
                logger.Error(e, $"{ServiceName} error on pause");
            }
        }

        /// <summary>
        /// When implemented in a derived class, executes when the system is shutting down. Specifies what should occur immediately prior to the system shutting down.
        /// </summary>
        protected override void OnShutdown()
        {
            try
            {
                UpdateState(ServiceState.SERVICE_SHUTDOWN_PENDING);
                base.OnShutdown();
                UpdateState(ServiceState.SERVICE_SHUTDOWN);
            }
            catch (Exception e)
            {
                logger.Error(e, $"{ServiceName} error on shutdown");
            }
        }

        private void WriteInfo(string message)
        {
            EventLog.WriteEntry(message, EventLogEntryType.Information);
            logger.Info(message);
        }

        private void UpdateState(ServiceState state)
        {
            var serviceStatus = new ServiceStatus
            {
                DwCurrentState = state,
                DwWaitHint = 100000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);
            switch (state)
            {
                case ServiceState.SERVICE_STOPPED:
                    WriteInfo(Resources.Service_stopped);
                    break;
                case ServiceState.SERVICE_START_PENDING:
                    WriteInfo(Resources.Service_start_pending);
                    break;
                case ServiceState.SERVICE_STOP_PENDING:
                    WriteInfo(Resources.Service_stop_pending);
                    break;
                case ServiceState.SERVICE_RUNNING:
                    WriteInfo(Resources.Service_running);
                    break;
                case ServiceState.SERVICE_CONTINUE_PENDING:
                    WriteInfo(Resources.Service_continue_pending);
                    break;
                case ServiceState.SERVICE_PAUSE_PENDING:
                    WriteInfo(Resources.Service_pause_pending);
                    break;
                case ServiceState.SERVICE_PAUSED:
                    WriteInfo(Resources.Service_paused);
                    break;
                case ServiceState.SERVICE_SHUTDOWN_PENDING:
                    WriteInfo(Resources.Service_shutdown_pending);
                    break;
                case ServiceState.SERVICE_SHUTDOWN:
                    WriteInfo(Resources.Service_Shutdown);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        #endregion

        private void InitializeComponent()
        {
            eventLog = new EventLog();
            ((ISupportInitialize)(eventLog)).BeginInit();
            ((ISupportInitialize)(eventLog)).EndInit();
        }
    }
}
