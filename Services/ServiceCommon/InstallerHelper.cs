﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using NLog;

namespace ServiceCommon
{
    public static class InstallerHelper
    {
        private const int RESTART_INTERVAL = 5000;
        private static readonly NLog.Logger LOGGER = LogManager.GetCurrentClassLogger();

        public static void SetRestartBehavior(string serviceName)
        {
            using (var process = new Process())
            {
                var startInfo = process.StartInfo;
                startInfo.FileName = "sc";
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                // tell Windows that the service should restart if it fails
                startInfo.Arguments = $"failure \"{serviceName}\" reset= 0 actions= restart/{RESTART_INTERVAL}";

                process.Start();
                process.WaitForExit();
            }
        }

        public static void StartService(string serviceName)
        {
            try
            {
                using (var sc = new ServiceController(serviceName))
                {
                    sc.Start();
                    sc.WaitForStatus(ServiceControllerStatus.Running);
                }
            }
            catch (Exception e)
            {
                LOGGER.Fatal(e, "Service start action - error:");
            }
        }
    }
}
