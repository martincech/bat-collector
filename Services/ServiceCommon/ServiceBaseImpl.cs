﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using Communication.Connectivity;
using Communication.Contract.Wcf.BindingConstants;
using Communication.Contract.Wcf.DataContracts.Device;
using Communication.Contract.Wcf.DataSources;
using Communication.Contract.Wcf.WebApi;
using Communication.EventToQueue;
using Communication.Manager;
using Communication.Manager.Modbus;
using Communication.Manager.Modbus.Connectivity;
using NLog;
using Repository.Console;
using Repository.Csv;
using Repository.FileSettings;
using Repository.LocalDb;
using Repository.ProcessQueue;
using Repository.Web;
using Repository.Web.WebApi;
using ServiceCommon.ExternalConfiguration;
using ServiceCommon.Logger;
using ServiceCommon.Wcf.Modbus;
using ServiceCommon.Wcf.WebLogin;
using Services.Hosting;
using Settings;
using Settings.Core;
using Settings.DataSourceSettings;
using Settings.DataSourceSettings.Modbus;
using Settings.DataStorageSettings;
using Settings.DeviceSettings;
using Settings.DeviceSettings.SourceSettings;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Mqueue;

namespace ServiceCommon
{
    public abstract class ServiceBaseImpl
    {
        #region Private fields

        private readonly ManagerSettings settingManager;
        private readonly List<ServiceHost> serviceHosts;
        private readonly Dictionary<ISourceManager, ManagerEventsToQueue> managers;
        private readonly List<IQueue> processQueues;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private ServiceLogger serviceLogger;

        private WebRepository webRepository;
        private FileSettingsRepository fileSettingsRepository;
        private ConsoleRepository consoleRepository;
        private CsvRepository csvRepository;
        private ExternalConfiguration.ExternalConfiguration externalConfiguration;

        #endregion

        #region Constructor

        protected ServiceBaseImpl(string name)
        {
            SetCultureToDefault();
            Name = name;
            settingManager = new ManagerSettings();
            processQueues = new List<IQueue>();
            serviceHosts = new List<ServiceHost>();
            managers = new Dictionary<ISourceManager, ManagerEventsToQueue>();
            InitializeLogger(settingManager.Settings.TerminalId);
            logger.Info("APP PATH : {0}", AppDomain.CurrentDomain.BaseDirectory);
            logger.Info("SETTINGS PATH : {0}", AppFolders.Settings);
        }

        private static void SetCultureToDefault()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
        }

        #endregion

        public string Name { get; private set; }

        public Guid TerminalId
        {
            get { return settingManager.Settings.TerminalId; }
        }

        public IEnumerable<IQueue> ProcessQueues
        {
            get { return processQueues; }
        }

        public virtual void Start(string[] args)
        {
            foreach (var serviceHost in serviceHosts)
            {
                try
                {
                    serviceHost.Open();
                }
                catch (Exception e)
                {
                    logger.Error(e, "Service host open exception");
                }
            }
        }


        public ISourceManager CreateModbusManager()
        {
            var ethernetProxy = new EthernetConnectivityProxy();
            var connectivity = new List<IModbusLineConnectivity> { ethernetProxy };
            var settings = settingManager.LoadDevices(true);

            var modbusManagerSettings = GetDataSourceSettings<ModbusManagerSettings>();
            var uidMapper = new UidMapper(modbusManagerSettings.UidMapping);
            var defaultSettings = modbusManagerSettings.DefaultConnectivitySettings;

            var tmpManagers = connectivity.Select(
               modbusLineConnectivity =>
               {
                   IEnumerable<DeviceSourceSettings<ModbusSettings>> deviceSettings = null;
                   switch (modbusLineConnectivity.ModbusLineType)
                   {
                       case ModbusLineType.Ethernet:
                           deviceSettings = settings.EthernetSources;
                           return (IModbusSourceManager)new ModbusEthernetManager((IModbusEthernetConnectivity)modbusLineConnectivity, defaultSettings, deviceSettings, TerminalId, uidMapper);
                       default:
                           throw new InvalidOperationException(nameof(modbusLineConnectivity.ModbusLineType));
                   }
               }).ToList();

            var multiManager = new MultiModbusSourceManager(tmpManagers, uidMapper);
            RegistryNewManagerAndConnectAllToProcessQueues(multiManager);
            IProcessQueue processQueue = new ProcessQueue();
            ConnectProcessQueueToSourceManagers(processQueue);

            var diagnosticManager = new DiagnosticManager(serviceLogger, new Settings.DiagnosticSettings.Collector(settingManager.Settings));
            var wcfService = new ModbusWcfService(multiManager, ethernetProxy, processQueue, diagnosticManager);
            CreateServiceContractRepository<IModbusService>(ServiceNetPipeBindingConstants.ModbusPipeAddress, wcfService);

            ThreadPool.QueueUserWorkItem(o =>
            {
                AddSources(settings?.EthernetSources, wcfService);

                var deviceManager = new DeviceManager(processQueue, multiManager);
                //watch changes in external files and set configuration for connected modbus scales
                var externalConfigWatcher = new ExternalConfigurationWatcher(settingManager.Settings.Weighing);
                externalConfiguration = new ExternalConfiguration.ExternalConfiguration(externalConfigWatcher, deviceManager);
            });

            return multiManager;
        }

        private static void AddSources(IEnumerable<DeviceSourceSettings<ModbusSettings>> sources, ModbusWcfService wcfService)
        {
            if (sources == null) return;
            foreach (var source in sources)
            {
                wcfService.AddSource(source.Uid);
            }
        }

        private void RegistryNewManagerAndConnectAllToProcessQueues(ISourceManager sourceManager)
        {
            managers.Add(sourceManager, null);
            ConnectAllProcessQueuesToAllSourceManagers();
        }

        private void ConnectAllProcessQueuesToAllSourceManagers()
        {
            foreach (var managerEventsToQueue in managers.ToList())
            {
                managerEventsToQueue.Value?.Dispose();
                var manager = managerEventsToQueue.Key;
                managers[manager] = new ManagerEventsToQueue(manager, processQueues);
            }
        }

        /// <summary>
        ///    Create service and start listening
        /// </summary>
        /// <param name="address">WCF service address</param>
        /// <param name="singletonToUse"> instance which will do the service</param>
        private void CreateServiceContractRepository<T>(string address, T singletonToUse)
        {
            var serviceHost = new ServiceHost<T>(singletonToUse);
            serviceHost.AddServiceEndpoint(
               typeof(T),
               ServiceNetPipeBindingConstants.NetPipeBinding,
               address);
            serviceHosts.Add(serviceHost);
        }

        /// <summary>
        ///    Create CSV repository
        /// </summary>
        /// <returns>Instance of CSV repository</returns>
        public IQueue CreateCsvRepository()
        {
            var settings = GetRepositorySettings<LocalCsvRepositorySettings>() ?? new LocalCsvRepositorySettings();
            IProcessQueue processQueue = new ProcessQueue();
            csvRepository = new CsvRepository(new CsvFile(settings), settings, processQueue, settingManager);
            return ConnectProcessQueueToSourceManagers(processQueue);
        }

        private IQueue ConnectProcessQueueToSourceManagers(IQueue processQueue)
        {
            processQueues.Add(processQueue);
            ConnectAllProcessQueuesToAllSourceManagers();
            return processQueue;
        }


        public IQueue CreateConsoleRepository()
        {
            var queue = new ProcessQueue();
            consoleRepository = new ConsoleRepository(queue);
            return ConnectProcessQueueToSourceManagers(queue);
        }

        public IQueue CreateFileSettingsRepository()
        {
            var queue = new ProcessQueue();
            fileSettingsRepository = new FileSettingsRepository(queue, settingManager);
            return ConnectProcessQueueToSourceManagers(queue);
        }


        /// <summary>
        /// Create web API repositories
        /// </summary>
        /// <param name="pipeAddress"></param>
        /// <returns>Instances of web API repository</returns>
        public IEnumerable<IQueue> CreateWebApiRepository(string pipeAddress)
        {
            var settings = GetRepositorySettingsList<WebApiRepositorySettings>().ToList();
            var queues = new List<IQueue>();
            var apis = new List<IWebApi>();
            var deviceSettings = settingManager.LoadDevices(false);

            var deviceMappings = GetDeviceMappings(deviceSettings.EthernetSources);
            foreach (var setting in settings)
            {
                var result = CreateWebApiRepository(setting, deviceMappings);
                queues.Add(result.Key);
                apis.Add(result.Value);
            }

            CreateServiceContractRepository<IWebApiConnection>(pipeAddress, new WebLoginWcfService(apis, queues));
            return queues;
        }

        private static Dictionary<string, Guid?> GetDeviceMappings<T>(List<T> sources)
            where T : DeviceSourceSettings
        {
            var deviceMappings = new Dictionary<string, Guid?>();
            foreach (var source in sources)
            {
                foreach (var device in source.Devices)
                {
                    deviceMappings.Add(device.Uid, device.Guid);
                }
            }
            return deviceMappings;
        }

        private KeyValuePair<IQueue, IWebApi> CreateWebApiRepository(WebApiRepositorySettings setting, Dictionary<string, Guid?> deviceMappings)
        {
            logger.Info("Create CreateWebApiRepository : {0}", setting.BatApi);
            var cache = new DbRepositoryCache(DbStorageSQLite.GetDatabase(setting.Path ?? nameof(WebApi)),
               TimeSpan.FromSeconds(30));
            IProcessQueue queue = new ProcessQueue();

            var permitCachingTypes = new List<Type> { typeof(StatisticSample), typeof(SourceConnectivityInfo), typeof(DeviceNotification) };
            queue = new CachedProcessQueueDecorator(queue, cache, permitCachingTypes);

            ConnectProcessQueueToSourceManagers(queue);
            var api = new WebApi(deviceMappings, settingManager);
            Task.Factory.StartNew(async () => { await api.LoginAsync(setting.ConnectionInfo); });
            webRepository = new WebRepository(api, queue);
            return new KeyValuePair<IQueue, IWebApi>(queue, api);
        }

        public IQueue CreateAdditionalRepository(IQueue repository)
        {
            return ConnectProcessQueueToSourceManagers(repository);
        }

        /// <summary>
        ///    Load settings setting defined by type
        /// </summary>
        /// <typeparam name="T">Storage type</typeparam>
        /// <returns>Settings for storage of type T</returns>
        protected T GetRepositorySettings<T>() where T : RepositorySettings
        {
            return settingManager.Settings.Storages.FirstOrDefault(f => f is T) as T;
        }

        private IEnumerable<T> GetRepositorySettingsList<T>() where T : RepositorySettings
        {
            return settingManager.Settings.Storages.OfType<T>();
        }


        private T GetDataSourceSettings<T>() where T : DataSourceSettings
        {
            return settingManager.Settings.Sources.FirstOrDefault(f => f is T) as T;
        }

        /// <summary>
        ///    Load settings setting defined by type
        /// </summary>
        /// <typeparam name="T">Storage type</typeparam>
        /// <returns>Settings for storage of type T</returns>
        /// <summary>
        ///    Stop and dispose of repositories, WCF service host and data source manager
        /// </summary>
        public void Stop()
        {
            logger.Info("Stopping {0}", Name);
            foreach (var serviceHost in serviceHosts)
            {
                serviceHost.Close();
            }
            webRepository?.Dispose();
            fileSettingsRepository?.Dispose();
            consoleRepository?.Dispose();
            csvRepository?.Dispose();
            externalConfiguration?.Dispose();

            serviceHosts.Clear();
            processQueues.Clear();
            managers.Clear();

            logger.Info("Stopped {0}", Name);
        }

        private void InitializeLogger(Guid terminalId)
        {
            try
            {
                serviceLogger = new ServiceLogger(Name, terminalId);

                //set diagnostic settings from application settings
                var diag = settingManager.LoadDiagnostics();
                serviceLogger.SetDebugMode(diag.UseDebugMode);
            }
            catch (Exception e)
            {
                logger.Error(e, nameof(InitializeLogger));
            }
        }
    }
}
