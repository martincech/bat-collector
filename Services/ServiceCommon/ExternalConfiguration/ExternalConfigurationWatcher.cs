﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Communication.Contract.Wcf.DataContracts;
using NLog;
using ServiceCommon.ExternalConfiguration.Models;

namespace ServiceCommon.ExternalConfiguration
{
    public sealed class ExternalConfigurationWatcher : IDisposable
    {
        #region Private fields

        private readonly ExternalWeighingInfo externalWeighingInfo;
        private FileSystemWatcher fileSystemWatcher;
        private const string FILE_SUFFIX = ".csv";
        private const string FILE_FILTER = "*" + FILE_SUFFIX;
        private readonly ExternalWeighingSettings settings;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructor

        public ExternalConfigurationWatcher(ExternalWeighingInfo externalWeighingInfo)
        {
            if (externalWeighingInfo == null) return;
            this.externalWeighingInfo = externalWeighingInfo;
            settings = new ExternalWeighingSettings();
        }

        #endregion

        public event EventHandler<Curves> CurveChanged;
        public event EventHandler<Configuration> WeighingConfigurationChanged;
        public event EventHandler<Weighing> WeighingParametersChanged;
        public event EventHandler<ExternalWeighingSettings> Initialized;

        public void Start()
        {
            if (externalWeighingInfo == null) return;
            CreateFileSystemWatcher(externalWeighingInfo.Csv.Path);
            LoadFilesIfExist();
        }

        public void Stop()
        {
            Dispose();
        }

        #region Private helpers

        private void CreateFileSystemWatcher(string path)
        {
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            fileSystemWatcher = new FileSystemWatcher(path, FILE_FILTER) { NotifyFilter = NotifyFilters.LastWrite };
            fileSystemWatcher.Created += OnFileChanged;
            fileSystemWatcher.Changed += OnFileChanged;
            fileSystemWatcher.EnableRaisingEvents = true;
        }


        /// <summary>
        /// Try Load all setting files. Don't raised event after load each file,
        /// but send only one common event after finished all operation.
        /// If any setting file doesn't exist, no event will be raised.
        /// </summary>
        private void LoadFilesIfExist()
        {
            var dirPath = externalWeighingInfo.Csv.Path;
            var dirInfo = new DirectoryInfo(dirPath);
            if (!dirInfo.Exists) return;

            var files = dirInfo.GetFiles();
            var curveFile = files.FirstOrDefault(f => f.Name.Equals(externalWeighingInfo.CurveFileParams + FILE_SUFFIX));
            if (curveFile != null)
            {
                LoadCurves(curveFile.FullName, false);
            }
            var configurationFile = files.FirstOrDefault(f => f.Name.Equals(externalWeighingInfo.WeighingConfigurationFileParams + FILE_SUFFIX));
            if (configurationFile != null)
            {
                LoadConfiguration(configurationFile.FullName, false);
            }
            var weighingFile = files.FirstOrDefault(f => f.Name.Equals(externalWeighingInfo.WeighingParametersFileParams + FILE_SUFFIX));
            if (weighingFile != null)
            {
                LoadWeighing(weighingFile.FullName, false);
            }

            if (curveFile == null && configurationFile == null && weighingFile == null)
            {
                return;
            }

            InvokeEvent(Initialized, settings);
        }

        private void OnFileChanged(object sender, FileSystemEventArgs e)
        {
            if (e.Name.Equals(externalWeighingInfo.CurveFileParams + FILE_SUFFIX))
            {
                LoadCurves(e.FullPath);
            }
            else if (e.Name.Equals(externalWeighingInfo.WeighingConfigurationFileParams + FILE_SUFFIX))
            {
                LoadConfiguration(e.FullPath);
            }
            else if (e.Name.Equals(externalWeighingInfo.WeighingParametersFileParams + FILE_SUFFIX))
            {
                LoadWeighing(e.FullPath);
            }
        }

        private void LoadCurves(string fullName, bool raisedEvent = true)
        {
            settings.Curves = ParseCurves(fullName);
            if (!raisedEvent) return;
            InvokeEvent(CurveChanged, settings.Curves);
        }

        private void LoadConfiguration(string fullName, bool raisedEvent = true)
        {
            settings.Configuration = new Configuration();
            ParseSettings(settings.Configuration.Parameters, fullName);
            if (!raisedEvent) return;
            InvokeEvent(WeighingConfigurationChanged, settings.Configuration);
        }

        private void LoadWeighing(string fullName, bool raisedEvent = true)
        {
            settings.Weighing = new Weighing();
            ParseSettings(settings.Weighing.Parameters, fullName);
            if (!raisedEvent) return;
            InvokeEvent(WeighingParametersChanged, settings.Weighing);
        }

        private void ParseSettings(IEnumerable<Parameter> parameters, string path)
        {
            try
            {
                var parser = GetParser(path);
                parser.Parse(parameters);
            }
            catch (Exception e)
            {
                logger.Error(e, "Parse external settings exception");
            }

        }

        private Curves ParseCurves(string path)
        {
            try
            {
                var parser = GetParser(path);
                return parser.ParseCurves();
            }
            catch (Exception e)
            {
                logger.Error(e, "Parse external curves exception");
                return null;
            }
        }

        private ConfigurationCsvParser GetParser(string path)
        {
            return new ConfigurationCsvParser(path, externalWeighingInfo.Csv.Delimiter,
               externalWeighingInfo.Csv.DecimalSeparator.ToString());
        }

        private static void InvokeEvent<T>(EventHandler<T> handler, T obj)
        {
            handler?.Invoke(null, obj);
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (fileSystemWatcher == null) return;
            fileSystemWatcher.Created -= OnFileChanged;
            fileSystemWatcher.Changed -= OnFileChanged;
            fileSystemWatcher.Dispose();
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
