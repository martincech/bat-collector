﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using Communication.Connectivity.Modbus;
using Communication.DataSource.Modbus;
using Communication.EventToQueue;
using NLog;
using ServiceCommon.Extensions;
using ServiceCommon.ExternalConfiguration.Models;
using Veit.Bat.Modbus.Bat2Scale.Registers;
using Veit.Bat.Modbus.Bat2Scale.Registers.Enums;
using Veit.Bat.Modbus.Bat2Scale.Registers.Types;

namespace ServiceCommon.ExternalConfiguration
{
    public sealed class ExternalConfiguration : IDisposable
    {
        #region Private fields

        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private readonly object lockObj = new object();
        private readonly DeviceManager deviceManager;
        private DateTime? commandTimeStampOriginValue;

        private readonly ExternalConfigurationWatcher watcher;
        private Flock flock;
        private CurvePoint[] originFemalePoints;
        private CurvePoint[] originMalePoints;
        private Veit.Bat.Modbus.Bat2Scale.Registers.Types.Configuration configuration;
        private WeighingParameters weighingParamaters;
        private WeighingStatus weighingStatus;
        private bool externalWeighingExist;
        private const byte PredefinedFlockNumber = 0;
        private readonly TimeSpan ModbusResponseTimeout = TimeSpan.FromMinutes(3);

        private const string CmdStart = "START";
        private const string CmdStop = "STOP";
        private System.Timers.Timer weighingWatcher;            //control if scales have set correct weighing
        private readonly int weighingWatcherInterval = 1000 *
#if DEBUG
      60 * 3;   //3min
#else
      60 * 60 * 2;   //2hour
#endif

        #endregion

        #region Constructor

        public ExternalConfiguration(ExternalConfigurationWatcher watcher, DeviceManager deviceManager)
        {
            this.deviceManager = deviceManager;
            this.watcher = watcher;
            externalWeighingExist = false;

            InitData();
            InitFileWatchers();
        }

        #endregion

        private void InitFileWatchers()
        {
            watcher.Initialized += WatcherOnInitialized;
            watcher.Start();
        }


        private void InitData()
        {
            flock = new Flock { Number = 0 };
            configuration = new Veit.Bat.Modbus.Bat2Scale.Registers.Types.Configuration
            {
                SavingParameters = new SavingParameters()
            };
            weighingParamaters = new WeighingParameters();
        }

        private void WatcherOnInitialized(object sender, ExternalWeighingSettings externalWeighingSettings)
        {
            logger.Info("=== External settings was initialization ===");
            if (externalWeighingSettings.Curves != null)
            {
                MapCurves(externalWeighingSettings.Curves);
            }
            if (externalWeighingSettings.Configuration != null)
            {
                MapConfiguration(externalWeighingSettings.Configuration);
            }
            if (externalWeighingSettings.Weighing != null)
            {
                MapWeighing(externalWeighingSettings.Weighing);
            }

            watcher.CurveChanged += WatcherOnCurveChanged;
            watcher.WeighingConfigurationChanged += WatcherOnWeighingConfigurationChanged;
            watcher.WeighingParametersChanged += WatcherOnWeighingParametersChanged;

            deviceManager.ActivatedDevice += DeviceManagerOnActivatedDevice;
            UpdateDevices(true, true);
        }

        private void WatcherOnCurveChanged(object sender, Curves curves)
        {
            MapCurves(curves);
            UpdateDevices(true, false);
        }

        private void MapCurves(Curves curves)
        {
            logger.Info("=== External Curves file was changed ===");
            if (curves == null)
            {
                originFemalePoints = new CurvePoint[0];
                originMalePoints = new CurvePoint[0];
            }
            else
            {
                originFemalePoints = curves.Female.Map();
                originMalePoints = curves.Male.Map();
            }
        }

        private void WatcherOnWeighingConfigurationChanged(object sender, Models.Configuration config)
        {
            MapConfiguration(config);
            UpdateDevices(false, true);
        }

        private void MapConfiguration(Models.Configuration config)
        {
            logger.Info("=== External Configuration file was changed ===");
            MapConfiguration(config, ref configuration);
        }

        private void WatcherOnWeighingParametersChanged(object sender, Models.Weighing weighing)
        {
            MapWeighing(weighing);
            UpdateDevices(true, true);
        }

        private void MapWeighing(Models.Weighing weighing)
        {
            logger.Info("=== External Weighing file was changed ===");
            externalWeighingExist = true;
            if (weighingWatcher == null)
            {
                InitWeighingWatcher();
            }

            MapWeighing(weighing, ref flock, ref configuration, ref weighingParamaters);
        }

        private void InitWeighingWatcher()
        {
            weighingWatcher = new System.Timers.Timer(weighingWatcherInterval);
            weighingWatcher.Elapsed += WeighingWatcherOnElapsed;
            weighingWatcher.Enabled = true;
        }

        private void WeighingWatcherOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            weighingWatcher.Enabled = false;

            lock (lockObj)
            {
                foreach (var device in GetConnectedModbusDevices().ToList())
                {
                    UpdateDeviceWithCheckPreviousSettings(device, true, true);
                }
            }
            weighingWatcher.Enabled = true;
        }

        private WeighingStatus GetExpectedStatus()
        {
            var expectedStatus = weighingStatus;
            if (weighingStatus == WeighingStatus.STARTED && weighingParamaters.DelayedStart.HasValue &&
                DateTimeOffset.Compare(weighingParamaters.DelayedStart.Value, DateTimeOffset.Now) > 0)
            {  //delayed start
                expectedStatus = WeighingStatus.WAITING;
            }
            return expectedStatus;
        }

        private void RestartWeighing(IModbusDataSource device)
        {
            logger.Info("= Restart weighing with status: {0}; device: {1}", weighingStatus, device.Id);
            if (weighingStatus == WeighingStatus.STOPPED) return;

            switch (weighingStatus)
            {
                case WeighingStatus.STARTED:
                    device.StartWeighing(flock, weighingParamaters);
                    break;
                case WeighingStatus.STOPPED:
                    device.StopWeighing();
                    break;
                default:
                    break;
            }
        }

        private void DeviceManagerOnActivatedDevice(object sender, SourceActivityInfo sourceActivityInfo)
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                if (!externalWeighingExist) return;

                lock (lockObj)
                {
                    //external weighing exist -> set new connected scale from external configuration
                    var uid = sourceActivityInfo.Device.DeviceId;
                    if (!(deviceManager.GetAllConnectedDevices().FirstOrDefault(d => d.Id.Equals(uid)) is IModbusDataSource device))
                    {
                        return;
                    }
                    UpdateDeviceWithCheckPreviousSettings(device, true, true);
                }
            });
        }

        private void UpdateDevices(bool setFlock, bool setParameters)
        {
            lock (lockObj)
            {
                var devices = GetConnectedModbusDevices();
                foreach (var device in devices)
                {
                    UpdateDeviceWithCheckPreviousSettings(device, setFlock, setParameters);
                }
            }
        }

        private void UpdateDeviceWithCheckPreviousSettings(IModbusDataSource device, bool setFlock, bool setParameters)
        {
            //check if settings in scale is the same,
            //because restart weighing causes deleting archive (statistics) in scale =>
            //restart weighing with the same parameters would be redundant operation.
            if (!IsDeviceSettingsChanged(device, setFlock, setParameters)) return;
            UpdateDevice(device, setFlock, setParameters);
        }

        private void UpdateDevice(IModbusDataSource device, bool setFlock, bool setParameters)
        {
            //before update flocks and configuration stop weighing
            //it is necessary for weighing with flock -> can't edit flock which is used for weighing -> changes will not be affected
            device.StopWeighing();

            if (setFlock)
            {
                device.SetFlock(flock);
                logger.Info("= Update flock for device: {0}", device.Id);
            }
            if (setParameters)
            {
                device.SetSavingParameters(configuration.SavingParameters);
                logger.Info("= Update configuration for device: {0}", device.Id);
            }
            RestartWeighing(device);
        }

        /// <summary>
        /// Read settings from device and compare with settings from external files
        /// </summary>
        /// <param name="device"></param>
        /// <param name="setFlock"></param>
        /// <param name="setParameters"></param>
        /// <returns>true - settings in external files is different from settings in scale</returns>
        private bool IsDeviceSettingsChanged(IModbusDataSource device, bool setFlock, bool setParameters)
        {
            try
            {
                SetFlockCurves();

                //check weighing status
                var expectedStatus = GetExpectedStatus();
                var status = (WeighingStatus)GetDeferredResponse(device, Bat2Request.WeighingStatus, device.GetWeighingStatus);

                var isStatusChanged = expectedStatus != status;
                var isFlockChanged = IsFlockChanged(setFlock, device);
                var isSavingParameterschanged = IsSavingParameterschanged(setParameters, device);

                logger.Debug($"Is device settings changed: status:{isStatusChanged}, flock:{isFlockChanged}, saving parameters:{isSavingParameterschanged}");
                if (isStatusChanged ||
                    isFlockChanged ||
                    isSavingParameterschanged)
                {
                    return true;
                }
                return IsWeighingChanged(device);
            }
            catch (Exception e)
            {
                logger.Warn(e, "External configuration: Check if settings changed was failed");
                return false;
            }
        }

        private bool IsFlockChanged(bool checkChanges, IModbusDataSource device)
        {
            if (!checkChanges)
            {
                return false;
            }
            var deviceFlock = GetDeferredResponse(device, Bat2Request.Flock,
                    () => device.GetFlock(PredefinedFlockNumber)) as Flock;
            return deviceFlock.IsChanged(flock);
        }

        private bool IsSavingParameterschanged(bool checkChanges, IModbusDataSource device)
        {
            if (!checkChanges)
            {
                return false;
            }
            var deviceParameters = GetDeferredResponse(device, Bat2Request.SavingParameters, device.GetSavingParameters) as SavingParameters;
            return deviceParameters.IsChanged(configuration.SavingParameters);
        }

        private bool IsWeighingChanged(IModbusDataSource device)
        {
            var deviceWeighing = GetDeferredResponse(device, Bat2Request.WeighingParameters, device.GetWeighingParameters)
                    as Veit.Bat.Modbus.Bat2Scale.Registers.Types.Weighing;

            var isChanged = deviceWeighing.WeighingParameters.IsChanged(weighingParamaters) ||
                   deviceWeighing.FlockNumber != PredefinedFlockNumber;

            logger.Debug($"Is weighing changed: {isChanged}");
            return isChanged;
        }

        /// <summary>
        /// Get response from modbus device.
        /// </summary>
        /// <param name="device">Modbus device</param>
        /// <param name="expectedRequest">Expected event</param>
        /// <param name="deviceAction">Device operation</param>
        /// <returns></returns>
        private object GetDeferredResponse(IModbusDataSource device, Bat2Request expectedRequest, Action deviceAction)
        {
            using (var waitHandle = new AutoResetEvent(false))
            {
                object data = null;
                void eventHandler(object sender, ModbusResponse e)
                {
                    if (sender is IModbusDataSource dev && dev.Id == device.Id &&
                        e.Type != expectedRequest)
                    {
                        logger.Debug($"GetDeferredResponse catch response from: {dev.Id}; response type: {e.Type}, expected: {expectedRequest}");
                        return;
                    }
                    data = e.Data;
                    waitHandle.Set();
                }

                device.ResponseRead += eventHandler;
                deviceAction.Invoke();
                var getResponse = waitHandle.WaitOne(ModbusResponseTimeout);
                device.ResponseRead -= eventHandler;
                if (!getResponse)
                {
                    throw new TimeoutException($"Modbus device: '{device.Id}' didn't response in allocated time. Request was: {expectedRequest}");
                }
                return data;
            }
        }

        /// <summary>
        /// Set flock curves depend on use both gender or not settings.
        /// </summary>
        private void SetFlockCurves()
        {
            if (flock.UseBothGenders > 0)
            {
                flock.SetMaleCurve(originMalePoints);
                flock.SetFemaleCurve(originFemalePoints);
            }
            else
            {
                flock.SetFemaleCurve(originMalePoints);
            }
        }

        private IEnumerable<IModbusDataSource> GetConnectedModbusDevices()
        {
            var devices = deviceManager.GetAllConnectedDevices().Where(d => d is IModbusDataSource).Cast<IModbusDataSource>().ToList();
            return devices;
        }

        #region Parse external configuration

        private static void MapConfiguration(Models.Configuration configParams, ref Veit.Bat.Modbus.Bat2Scale.Registers.Types.Configuration config)
        {
            var parameters = configParams.Parameters.ToList();
            var jumpModeValues = Enum.GetValues(typeof(JumpMode)).Cast<JumpMode>().ToList();

            var saving = new SavingParameters
            {
                Filter = GetValue(ExternalConfigurationRegisters.FILTER, parameters, RegisterConstants.DefaultFilter, RegisterConstants.MinFilter, RegisterConstants.MaxFilter),
                StabilizationTime =
                  GetValue(ExternalConfigurationRegisters.STABILIZATIONTIME, parameters, RegisterConstants.DefaultStabilizationTime,
                     RegisterConstants.MinStabilizationTime, RegisterConstants.MaxStabilizationTime),
                StabilizationRange =
                  RegisterConstants.GetStabilizationRangeFromRaw(GetValue(ExternalConfigurationRegisters.STABILIZATIONRANGE, parameters,
                     RegisterConstants.DefaultStabilizationRangeRwaRegisterValue, RegisterConstants.MinPercentageForStabilizationRange,
                     RegisterConstants.MaxValueForStabilizationRangeRegister)),
                JumpMode =
                  RegisterConstants.GetJumpMode(GetValue(ExternalConfigurationRegisters.JUMPMODE, parameters, RegisterConstants.DefaultJumpMode,
                     (ushort)jumpModeValues.Min(), (ushort)jumpModeValues.Max())),
                MarginAboveFemales =
                  GetValue(ExternalConfigurationRegisters.MARGIN_ABOVE_FEMALE, parameters, RegisterConstants.DefaultMargin, RegisterConstants.MinMargin,
                     RegisterConstants.MaxMargin),
                MarginBelowFemales =
                  GetValue(ExternalConfigurationRegisters.MARGIN_BELOW_FEMALE, parameters, RegisterConstants.DefaultMargin, RegisterConstants.MinMargin,
                     RegisterConstants.MaxMargin),
                MarginAboveMales =
                  GetValue(ExternalConfigurationRegisters.MARGIN_ABOVE_MALE, parameters, RegisterConstants.DefaultMargin, RegisterConstants.MinMargin,
                     RegisterConstants.MaxMargin),
                MarginBelowMales =
                  GetValue(ExternalConfigurationRegisters.MARGIN_BELOW_MALE, parameters, RegisterConstants.DefaultMargin, RegisterConstants.MinMargin,
                     RegisterConstants.MaxMargin)
            };
            config.SavingParameters = saving;
        }

        private void MapWeighing(Models.Weighing weighing, ref Flock deviceFlock, ref Veit.Bat.Modbus.Bat2Scale.Registers.Types.Configuration config,
           ref WeighingParameters devWeighing)
        {
            var parameters = weighing.Parameters.ToList();
            var cmd = GetValue(ExternalConfigurationRegisters.WEIGHING_COMMAND, parameters, CmdStart, "", "");
            ParseWeighingCommand(cmd);

            //other parameters
            deviceFlock.UseBothGenders = SwapUseBothGender(GetValue<ushort>(ExternalConfigurationRegisters.USEBOTHGENDERS, parameters, RegisterConstants.DefaultUseBothGenders, 0, 1));
            devWeighing.InitialDayNumber = GetValue(ExternalConfigurationRegisters.INITIALDAYNUMBER, parameters,
               RegisterConstants.DefaultDay, RegisterConstants.DayMinimum, RegisterConstants.DayMaximum);
            CheckCommandTimeStampLimit();
            devWeighing.InitialWeightMalesInGrams = GetValue(ExternalConfigurationRegisters.INITIALWMALES, parameters,
               RegisterConstants.DefaultInitialWeightMales, RegisterConstants.InitialWeightMinimum, RegisterConstants.InitialWeightMaximum);
            devWeighing.InitialWeightFemalesInGrams = GetValue(ExternalConfigurationRegisters.INITIALWFEMALES, parameters,
               RegisterConstants.DefaultInitialWeightFemales, RegisterConstants.InitialWeightMinimum, RegisterConstants.InitialWeightMaximum);
            deviceFlock.WeighFromHour = GetValue(ExternalConfigurationRegisters.WEIGHFROM, parameters, RegisterConstants.WeightHourMaximum,
               RegisterConstants.WeightHourMinimum, RegisterConstants.WeightHourMaximum);
            deviceFlock.WeighTillHour = GetValue(ExternalConfigurationRegisters.WEIGHTILL, parameters, RegisterConstants.WeightHourMaximum,
               RegisterConstants.WeightHourMinimum, RegisterConstants.WeightHourMaximum);
            deviceFlock.UseCurves = GetValue<ushort>(ExternalConfigurationRegisters.USECURVES, parameters, RegisterConstants.DefaultUseCurves, 0, 1);

            CheckWeighingTime(ref deviceFlock);
            var autoModeValues = Enum.GetValues(typeof(AutoMode)).Cast<AutoMode>().ToList();
            config.SavingParameters.AutoMode =
               RegisterConstants.GetAutoMode(GetValue(ExternalConfigurationRegisters.AUTOMODE, parameters, RegisterConstants.DefaultAutoMode,
                  (ushort)autoModeValues.Min(), (ushort)autoModeValues.Max()));
        }

        /// <summary>
        /// UseBothGender variable has swapped meaning than in modbus registers,
        /// therefore it is necessary convert value.
        /// </summary>
        /// <param name="value"></param>
        private static ushort SwapUseBothGender(ushort value)
        {
            return (ushort)(value == 0 ? 1 : 0);
        }

        private static void CheckWeighingTime(ref Flock deviceFlock)
        {
            if (deviceFlock.WeighFromHour != 0 || deviceFlock.WeighTillHour != 0) return;
            // from 0 to 0 means all day
            deviceFlock.WeighFromHour = (byte)RegisterConstants.DisabledValue;
            deviceFlock.WeighTillHour = (byte)RegisterConstants.DisabledValue;
        }

        private static T GetValue<T>(ExternalConfigurationRegisters key, IEnumerable<Parameter> data, T defaultValue, T min,
           T max)
           where T : IComparable
        {
            var item = data.FirstOrDefault(f => f.Name == key);
            if (item == null) return defaultValue;
            try
            {
                var val = (T)Convert.ChangeType(item.Value, typeof(T));
                if (typeof(T) == typeof(string) || val == null) return val;
                if (val.CompareTo(min) < 0)
                {
                    return min;
                }
                if (val.CompareTo(max) > 0)
                {
                    return max;
                }
                return val;
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        private void ParseWeighingCommand(string cmd)
        {
            if (string.IsNullOrEmpty(cmd))
            {
                // not exist
                weighingStatus = WeighingStatus.STOPPED;
                SetWeighingCommandTimeStamp(null);
            }
            else
            {
                //parse command
                var array = cmd.Split(ConfigurationCsvParser.CommandDelimiter);
                var paramCount = array.Count();
                if (paramCount < 2) return;

                switch (array[0])
                {
                    case CmdStart:
                        weighingStatus = WeighingStatus.STARTED;
                        break;
                    case CmdStop:
                        weighingStatus = WeighingStatus.STOPPED;
                        break;
                    default:
                        break;
                }
                logger.Info("== Weighing status was set: {0}", weighingStatus);

                //parse command parameter (time stamp)
                if (paramCount <= 1) return;
                ParseWeighingCommandParameter(array[1]);
            }
        }

        private void ParseWeighingCommandParameter(string value)
        {
            if (DateTime.TryParse(value, out var ts))
            {
                //seconds are truncate - BAT2 can set delayed start only for hours and minutes
                commandTimeStampOriginValue = new DateTime(ts.Year, ts.Month, ts.Day, ts.Hour, ts.Minute, 0);
                SetWeighingCommandTimeStamp(commandTimeStampOriginValue);
            }
            else
            {
                SetWeighingCommandTimeStamp(null);
            }
        }

        private void CheckCommandTimeStampLimit()
        {
            if (!commandTimeStampOriginValue.HasValue) return;
            var days = DateTime.Now.Date.Subtract(commandTimeStampOriginValue.Value.Date).Days +
                       weighingParamaters.InitialDayNumber;
            if (days <= RegisterConstants.DayMaximum) return;
            var dayDiff = days - RegisterConstants.DayMaximum;
            weighingParamaters.DelayedStart = commandTimeStampOriginValue.Value.AddDays(dayDiff);
        }

        private void SetWeighingCommandTimeStamp(DateTime? value)
        {
            weighingParamaters.DelayedStart = value;
            CheckCommandTimeStampLimit();
        }

        public void Dispose()
        {
            watcher.CurveChanged -= WatcherOnCurveChanged;
            watcher.WeighingConfigurationChanged -= WatcherOnWeighingConfigurationChanged;
            watcher.WeighingParametersChanged -= WatcherOnWeighingParametersChanged;
            watcher.Initialized -= WatcherOnInitialized;
            watcher.Stop();

            if (weighingWatcher == null)
            {
                return;
            }
            weighingWatcher.Enabled = false;
            weighingWatcher.Elapsed -= WeighingWatcherOnElapsed;
            weighingWatcher.Dispose();
        }

        #endregion
    }
}
