﻿namespace ServiceCommon.ExternalConfiguration.Models
{
    public class Parameter
    {
        public Parameter(ExternalConfigurationRegisters name, string defaultValue)
        {
            Name = name;
            Value = defaultValue;
        }

        public ExternalConfigurationRegisters Name { get; private set; }
        public string Value { get; set; }
    }
}
