﻿namespace ServiceCommon.ExternalConfiguration.Models
{
    public enum ExternalConfigurationRegisters
    {
        MARGIN_ABOVE_FEMALE,
        MARGIN_ABOVE_MALE,
        MARGIN_BELOW_FEMALE,
        MARGIN_BELOW_MALE,
        FILTER,
        STABILIZATIONRANGE,
        STABILIZATIONTIME,
        JUMPMODE,
        WEIGHING_COMMAND,
        INITIALDAYNUMBER,
        USECURVES,
        USEBOTHGENDERS,
        WEIGHFROM,
        WEIGHTILL,
        AUTOMODE,
        INITIALWFEMALES,
        INITIALWMALES
    }
}
