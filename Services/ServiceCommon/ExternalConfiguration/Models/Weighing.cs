﻿using System.Collections.Generic;
using Veit.Bat.Modbus.Bat2Scale.Registers;
using static ServiceCommon.ExternalConfiguration.Models.Configuration;

namespace ServiceCommon.ExternalConfiguration.Models
{
    public sealed class Weighing : ConfigurationBase
    {
        public Weighing()
        {
            CreateDefaultParameters();
        }

        protected override void CreateDefaultParameters()
        {
            Parameters = new List<Parameter>
            {
                new Parameter(ExternalConfigurationRegisters.WEIGHING_COMMAND, string.Empty),
                new Parameter(ExternalConfigurationRegisters.INITIALDAYNUMBER, RegisterConstants.DefaultDay.ToString()),
                new Parameter(ExternalConfigurationRegisters.USECURVES, RegisterConstants.DefaultUseCurves.ToString()),
                new Parameter(ExternalConfigurationRegisters.USEBOTHGENDERS, RegisterConstants.DefaultUseBothGenders.ToString()),
                new Parameter(ExternalConfigurationRegisters.WEIGHFROM, string.Empty),
                new Parameter(ExternalConfigurationRegisters.WEIGHTILL, string.Empty),
                new Parameter(ExternalConfigurationRegisters.AUTOMODE, RegisterConstants.DefaultAutoMode.ToString()),
                new Parameter(ExternalConfigurationRegisters.INITIALWFEMALES, RegisterConstants.DefaultInitialWeightFemales.ToString()),
                new Parameter(ExternalConfigurationRegisters.INITIALWMALES, RegisterConstants.DefaultInitialWeightMales.ToString())
            };
        }
    }
}
