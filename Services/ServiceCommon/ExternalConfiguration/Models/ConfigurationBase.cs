﻿using System.Collections.Generic;

namespace ServiceCommon.ExternalConfiguration.Models
{
    public abstract class ConfigurationBase
    {
        public ICollection<Parameter> Parameters { get; protected set; }

        protected abstract void CreateDefaultParameters();
    }
}
