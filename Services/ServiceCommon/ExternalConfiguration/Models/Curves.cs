﻿using Veit.Bat.Common;

namespace ServiceCommon.ExternalConfiguration.Models
{
    public class Curves
    {
        public Curves()
        {
            Male = new Curve();
            Female = new Curve();
        }

        public Curve Male { get; set; }
        public Curve Female { get; set; }
    }
}
