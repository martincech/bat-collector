﻿using System.Collections.Generic;
using Veit.Bat.Modbus.Bat2Scale.Registers;

namespace ServiceCommon.ExternalConfiguration.Models
{
    public sealed class Configuration : ConfigurationBase
    {
        public Configuration()
        {
            CreateDefaultParameters();
        }

        protected override void CreateDefaultParameters()
        {
            Parameters = new List<Parameter>
            {
                new Parameter(ExternalConfigurationRegisters.MARGIN_ABOVE_FEMALE, RegisterConstants.DefaultMargin.ToString()),
                new Parameter(ExternalConfigurationRegisters.MARGIN_BELOW_FEMALE, RegisterConstants.DefaultMargin.ToString()),
                new Parameter(ExternalConfigurationRegisters.MARGIN_ABOVE_MALE, RegisterConstants.DefaultMargin.ToString()),
                new Parameter(ExternalConfigurationRegisters.MARGIN_BELOW_MALE, RegisterConstants.DefaultMargin.ToString()),
                new Parameter(ExternalConfigurationRegisters.FILTER, RegisterConstants.DefaultFilter.ToString()),
                new Parameter(ExternalConfigurationRegisters.STABILIZATIONRANGE, RegisterConstants.DefaultStabilizationRangeRwaRegisterValue.ToString()),
                new Parameter(ExternalConfigurationRegisters.STABILIZATIONTIME, RegisterConstants.DefaultStabilizationTime.ToString()),
                new Parameter(ExternalConfigurationRegisters.JUMPMODE, RegisterConstants.DefaultJumpMode.ToString())
            };
        }
    }
}
