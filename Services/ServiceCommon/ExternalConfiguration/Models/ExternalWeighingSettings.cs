﻿namespace ServiceCommon.ExternalConfiguration.Models
{
    public class ExternalWeighingSettings
    {
        public Configuration Configuration { get; set; }
        public Weighing Weighing { get; set; }
        public Curves Curves { get; set; }
    }
}
