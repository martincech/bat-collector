﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Communication.Contract.Wcf.DataContracts.Device;
using NLog;
using ServiceCommon.ExternalConfiguration.Models;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Weight;
using Veit.Bat.Modbus.Bat2Scale.Registers.Enums;

namespace ServiceCommon.ExternalConfiguration
{
    public class ConfigurationCsvParser
    {
        #region Private fields

        private readonly string fileName;
        private readonly char delimiter;
        private readonly string decimalSeparator;
        private readonly Encoding encoding = Encoding.UTF8;
        private const int SettingsColumns = 2;
        private const int BothCurveColumns = 3;
        private const int NameIndex = 0;
        private const int FirstParameterIndex = 1;
        private const int SecondParameterIndex = 2;
        private const int WeightMinimum = 30;
        private const int WeightMaximum = 65535;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public interfaces

        public static char CommandDelimiter => '_';

        public ConfigurationCsvParser(string fileName, char delimiter, string decimalSeparator)
        {
            this.fileName = fileName;
            this.delimiter = delimiter;
            this.decimalSeparator = decimalSeparator;
            if (!File.Exists(fileName)) throw new FileNotFoundException();
        }

        public void Parse(IEnumerable<Parameter> data)
        {
            var parameters = data.ToList();
            foreach (var array in File.ReadLines(fileName, encoding).Select(line => line.Split(delimiter)))
            {
                if (array.Count() < SettingsColumns) continue;

                var name = array[NameIndex];
                var value = array[FirstParameterIndex];

                var newName = Enum.GetNames(typeof(RegistersV153)).FirstOrDefault(f => f.StartsWith(name, StringComparison.InvariantCulture));
                name = newName ?? name;
                if (!Enum.TryParse(name, out ExternalConfigurationRegisters register))
                {
                    //check if parameter is command
                    CheckForCommandParameter(name, value, parameters);
                    continue;
                }

                var match = parameters.FirstOrDefault(f => f.Name == register);
                if (match == null) continue;
                match.Value = value;
            }
        }

        public Curves ParseCurves()
        {
            var curves = new Curves();
            var lineCount = 0;
            foreach (var array in File.ReadLines(fileName, encoding).Select(line => line.Split(delimiter)))
            {
                try
                {
                    lineCount++;
                    ParseCurveLine(array, ref curves);
                }
                catch (Exception e)
                {
                    if (lineCount == 1 && e is FormatException)
                    {   //first line is header
                        continue;
                    }
                    logger.Warn(e, $"Parse curves exception on line: {lineCount}, line value: [{string.Join(delimiter.ToString(), array)}]");
                }
            }
            return curves;
        }

        private void ParseCurveLine(string[] rowRecords, ref Curves curves)
        {
            if (rowRecords.Count() < SettingsColumns)
            {
                return;
            }

            var day = DayParse(rowRecords[NameIndex]);
            var valueMale = WeightParse(rowRecords[FirstParameterIndex]);
            if (!curves.Male.Points.Any() || day > curves.Male.Points.Last().X)
            {
                curves.Male.Points.Add(new CurvePoint(day, valueMale));
            }
            if (rowRecords.Count() < BothCurveColumns || string.IsNullOrEmpty(rowRecords[SecondParameterIndex]))
            {
                return;
            }
            var valueFemale = WeightParse(rowRecords[SecondParameterIndex]);
            if (!curves.Female.Points.Any() || day > curves.Female.Points.Last().X)
            {
                curves.Female.Points.Add(new CurvePoint(day, valueFemale));
            }
        }

        #endregion

        #region Private helpers

        private static void CheckForCommandParameter(string name, string value, IEnumerable<Parameter> data)
        {
            if (!Enum.TryParse(name, out WeighingCommand cmd)) return;

            var record = data.FirstOrDefault(f => f.Name == ExternalConfigurationRegisters.WEIGHING_COMMAND);
            if (record == null) return;
            record.Value = name + CommandDelimiter + value;
        }

        private int DayParse(string value)
        {
            return GetValue<int>(value);
        }

        private Weight WeightParse(string value)
        {
            try
            {
                var convertedValue = GetValue<double>(value);
                CheckWeightLimit(ref convertedValue);
                return new Weight(convertedValue);
            }
            catch (Exception e)
            {
                logger.Info(e, $"Cannot parse weight: {value}; decimal separator: {decimalSeparator}");
                throw;
            }
        }

        private static void CheckWeightLimit(ref double value)
        {
            if (value < WeightMinimum)
            {
                value = WeightMinimum;
            }
            if (value > WeightMaximum)
            {
                value = WeightMaximum;
            }
        }

        private T GetValue<T>(string value)
        {
            return (T)Convert.ChangeType(value, typeof(T), new NumberFormatInfo { NumberDecimalSeparator = decimalSeparator });
        }

        #endregion
    }
}
