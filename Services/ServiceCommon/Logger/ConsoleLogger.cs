﻿using System.Text;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using NLog.Targets;
using NLog.Targets.Wrappers;
using Settings.Core.Logger;

namespace ServiceCommon.Logger
{
    public class ConsoleLogger : BaseLoggerConfiguration
    {
        private readonly string serviceName;

        public ConsoleLogger(string name)
        {
            serviceName = name;
            var config = InitLoggerConfiguration();
            LogManager.Configuration = config;
        }

        private LoggingConfiguration InitLoggerConfiguration()
        {
            RegisterRenderers();
            var config = new LoggingConfiguration();

            //targets
            var mainTarget = InitMainFileTarget(ref config, serviceName);
            var consoleTarget = InitConsoleTarget(ref config);

            //define rules
            var mainRule = new LoggingRule("*", DefaultLevel, mainTarget);
            config.LoggingRules.Add(mainRule);
            DebugRules.Add(mainRule);

            var consoleRule = new LoggingRule("*", LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(consoleRule);

            return config;
        }

        private static Target InitConsoleTarget(ref LoggingConfiguration config)
        {
            const string consoleLogName = "consoleLog";
            var consoleTarget = new ConsoleTarget(consoleLogName);
            config.AddTarget(consoleLogName, consoleTarget);

            const string asyncTargetName = "asyncConsoleTarget";
            var asyncTarget = new AsyncTargetWrapper(asyncTargetName, consoleTarget);
            config.AddTarget(asyncTarget);

            //target properties
            consoleTarget.Encoding = Encoding.UTF8;
            consoleTarget.Layout =
               @"[${date:format=yyyy-MM-ddTHH\:mm\:ss.fffK}]|${uppercase:${level}}|${callsite}:${callsite-linenumber}|${message}";

            return asyncTarget;
        }

        #region Overrides of BaseLoggerConfiguration

        protected override void RegisterRenderers()
        {
            LayoutRenderer.Register("appName", logEvent => serviceName);
            base.RegisterRenderers();
        }

        #endregion
    }
}
