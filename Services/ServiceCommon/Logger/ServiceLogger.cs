﻿using System;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using Settings.Core.Logger;

namespace ServiceCommon.Logger
{
    public class ServiceLogger : BaseLoggerConfiguration
    {
        private readonly string serviceName;
        private readonly Guid terminalId;

        public ServiceLogger(string name, Guid terminalId)
        {
            serviceName = name;
            this.terminalId = terminalId;
            var config = InitLoggerConfiguration();
            LogManager.Configuration = config;
        }

        private LoggingConfiguration InitLoggerConfiguration()
        {
            RegisterRenderers();
            var config = new LoggingConfiguration();

            //targets
            var mainTarget = InitMainFileTarget(ref config, serviceName);
            var sentryTarget = InitSentryTarget(ref config);

            //define rules
            var mainRule = new LoggingRule("*", DefaultLevel, mainTarget);
            config.LoggingRules.Add(mainRule);
            DebugRules.Add(mainRule);

            var sentryRule = new LoggingRule("*", LogLevel.Error, sentryTarget);
            config.LoggingRules.Add(sentryRule);

            return config;
        }

        #region Overrides of BaseLoggerConfiguration

        protected override void RegisterRenderers()
        {
            LayoutRenderer.Register("appName", logEvent => serviceName);
            LayoutRenderer.Register(nameof(terminalId), logEvent => terminalId);
            base.RegisterRenderers();
        }

        #endregion
    }
}
