﻿using System;
using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;

namespace ServiceCommon.Extensions
{
    public static class SavingParamatersExtensions
    {
        public static SavingParameters MapTo(
           this Veit.Bat.Modbus.Bat2Scale.Registers.Types.SavingParameters model)
        {
            if (model == null) return null;
            return new SavingParameters
            {
                MarginFemale = new Margin
                {
                    Above = model.MarginAboveFemales,
                    Below = model.MarginBelowFemales
                },
                MarginMale = new Margin
                {
                    Above = model.MarginAboveMales,
                    Below = model.MarginBelowMales
                },
                Filter = model.Filter,
                StabilizationTime = model.StabilizationTime,
                StabilizationRange = model.StabilizationRange,
                AutoMode = model.AutoMode.MapTo(),
                JumpMode = model.JumpMode.MapTo()
            };
        }

        public static Veit.Bat.Modbus.Bat2Scale.Registers.Types.SavingParameters MapFrom(
           this SavingParameters model)
        {
            if (model == null) return null;
            return new Veit.Bat.Modbus.Bat2Scale.Registers.Types.SavingParameters
            {
                MarginAboveFemales = model.MarginFemale.Above,
                MarginBelowFemales = model.MarginFemale.Below,
                MarginAboveMales = model.MarginMale.Above,
                MarginBelowMales = model.MarginMale.Below,
                Filter = model.Filter,
                StabilizationRange = model.StabilizationRange,
                StabilizationTime = model.StabilizationTime,
                AutoMode = model.AutoMode.MapFrom(),
                JumpMode = model.JumpMode.MapFrom()
            };
        }


        public static AutoMode MapTo(this Veit.Bat.Modbus.Bat2Scale.Registers.Enums.AutoMode model)
        {
            switch (model)
            {
                case Veit.Bat.Modbus.Bat2Scale.Registers.Enums.AutoMode.WithGain:
                    return AutoMode.WithGain;
                case Veit.Bat.Modbus.Bat2Scale.Registers.Enums.AutoMode.WithoutGain:
                    return AutoMode.WithoutGain;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        public static Veit.Bat.Modbus.Bat2Scale.Registers.Enums.AutoMode MapFrom(this AutoMode model)
        {
            switch (model)
            {
                case AutoMode.WithGain:
                    return Veit.Bat.Modbus.Bat2Scale.Registers.Enums.AutoMode.WithGain;
                case AutoMode.WithoutGain:
                    return Veit.Bat.Modbus.Bat2Scale.Registers.Enums.AutoMode.WithoutGain;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        public static JumpMode MapTo(this Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode model)
        {
            switch (model)
            {
                case Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode.Both:
                    return JumpMode.Both;
                case Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode.Enter:
                    return JumpMode.Enter;
                case Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode.Leave:
                    return JumpMode.Leave;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        public static Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode MapFrom(this JumpMode model)
        {
            switch (model)
            {
                case JumpMode.Enter:
                    return Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode.Enter;
                case JumpMode.Leave:
                    return Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode.Leave;
                case JumpMode.Both:
                    return Veit.Bat.Modbus.Bat2Scale.Registers.Enums.JumpMode.Both;
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }
    }
}
