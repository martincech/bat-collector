﻿using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Veit.Bat.Modbus.Bat2Scale.Registers.Types;

namespace ServiceCommon.Extensions
{
    public static class DeviceConfigurationExtensions
    {
        public static DeviceConfiguration Map(this Configuration config)
        {
            if (config == null) return null;
            return new DeviceConfiguration
            {
                ScaleName = config.ScaleName,
                Version = config.Version.Map(),
                SavingParameters = config.SavingParameters.MapTo()
            };
        }

    }
}
