﻿using System;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;
using Version = Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version;

namespace ServiceCommon.Extensions
{
    public static class DeviceVersionExtensions
    {
        public static DeviceVersion Map(this Version version)
        {
            switch (version)
            {
                case Version.V111:
                    return DeviceVersion.Bat2V111;
                case Version.V150:
                    return DeviceVersion.Bat2V150;
                case Version.V153:
                    return DeviceVersion.Bat2V153;
                default:
                    throw new ArgumentOutOfRangeException(nameof(version));
            }
        }
    }
}
