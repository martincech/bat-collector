﻿using System.Collections.Generic;
using System.Linq;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Weight;

namespace ServiceCommon.Extensions
{
    public static class DeviceFlockExtensions
    {
        private const byte INVALID_TIME_LIMIT = 0xFF;

        public static Communication.Contract.Wcf.DataContracts.Device.Flock MapTo(this Veit.Bat.Modbus.Bat2Scale.Registers.Types.Flock model)
        {
            if (model == null) return null;

            byte? from = null;
            if (model.WeighFromHour != INVALID_TIME_LIMIT)
            {
                from = model.WeighFromHour;
            }
            byte? till = null;
            if (model.WeighTillHour != INVALID_TIME_LIMIT)
            {
                till = model.WeighTillHour;
            }

            return new Communication.Contract.Wcf.DataContracts.Device.Flock
            {
                Number = model.Number,
                UseBothGenders = model.UseBothGenders == 1,
                UseCurves = model.UseCurves == 1,
                WeighFromHour = from,
                WeighTillHour = till,
                FemaleCurve = model.GetFemaleCurve().Map(),
                MaleCurve = model.GetMaleCurve().Map()
            };
        }

        public static Veit.Bat.Modbus.Bat2Scale.Registers.Types.Flock MapFrom(this Communication.Contract.Wcf.DataContracts.Device.Flock model)
        {
            if (model == null) return null;

            var flock = new Veit.Bat.Modbus.Bat2Scale.Registers.Types.Flock
            {
                Number = model.Number,
                UseBothGenders = (ushort)(model.UseBothGenders ? 1 : 0),
                UseCurves = (ushort)(model.UseCurves ? 1 : 0),
                WeighFromHour = model.WeighFromHour ?? INVALID_TIME_LIMIT,
                WeighTillHour = model.WeighTillHour ?? INVALID_TIME_LIMIT
            };
            flock.SetFemaleCurve(model.FemaleCurve.Map());
            flock.SetMaleCurve(model.MaleCurve.Map());
            return flock;
        }


        private static Curve Map(this IEnumerable<Veit.Bat.Modbus.Bat2Scale.Registers.Types.CurvePoint> model)
        {
            var curve = new Curve();
            foreach (var rawPoint in model)
            {
                curve.Points.Add(new CurvePoint(rawPoint.Day, new Weight(rawPoint.WeightInGrams)));
            }
            return curve;
        }

        public static Veit.Bat.Modbus.Bat2Scale.Registers.Types.CurvePoint[] Map(this Curve model)
        {
            return model.Points.Select(rawPoint => new Veit.Bat.Modbus.Bat2Scale.Registers.Types.CurvePoint
            {
                Day = (ushort)rawPoint.X,
                WeightInGrams = (ushort)rawPoint.Y.AsG
            }).ToArray();
        }
    }
}
