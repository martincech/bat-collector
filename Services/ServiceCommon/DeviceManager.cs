﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.DataSource;
using Communication.EventToQueue;
using Communication.Manager;
using Veit.Mqueue;

namespace ServiceCommon
{
    public class DeviceManager
    {
        private readonly ISourceManager sourceManager;

        public DeviceManager(IProcessQueue processQueue, ISourceManager sourceManager)
        {
            this.sourceManager = sourceManager;
            processQueue.AddProcessing<SourceActivityInfo>(OnActivationChanged);
        }

        public event EventHandler<SourceActivityInfo> ActivatedDevice;

        public IEnumerable<IDataSource> GetAllConnectedDevices()
        {
            var dataSources = new List<IDataSource>();
            var sources = sourceManager.ConnectedSources.ToList();
            foreach (var source in sources)
            {
                dataSources.AddRange(source.ConnectedSources);
            }
            return dataSources;
        }

        #region Private helpers

        private bool OnActivationChanged(SourceActivityInfo arg)
        {
            if (!arg.Device.Active) return false;
            Invoke(arg);
            return true;
        }

        private void Invoke(SourceActivityInfo info)
        {
            ActivatedDevice?.Invoke(this, info);
        }

        #endregion
    }
}
