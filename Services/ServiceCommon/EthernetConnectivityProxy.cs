﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity;
using Communication.Manager.Modbus.Connectivity;
using ServiceCommon.Wcf.Modbus;
using Veit.Bat.Modbus.Common.Connectivity;

namespace ServiceCommon
{
    public class EthernetConnectivityProxy : IModbusEthernetConnectivity, IEthernetSourceCreator
    {
        private readonly List<IEthernet> connectedSources;

        public EthernetConnectivityProxy()
        {
            connectedSources = new List<IEthernet>();
        }


        public ModbusLineType ModbusLineType { get { return ModbusLineType.Ethernet; } }

        public IEnumerable<IEthernet> ConnectedSources
        {
            get { return connectedSources; }
        }

        public event EventHandler<IEthernet> SourceConnected;
        public event EventHandler<IEthernet> SourceDisconnected;

        private void OnSourceConnected(IEthernet e)
        {
            if (SourceConnected == null) return;
            SourceConnected(this, e);
        }

        private void OnSourceDisconnected(IEthernet e)
        {
            if (SourceDisconnected == null) return;
            SourceDisconnected(this, e);
        }

        public bool AddSource(string portName)
        {
            try
            {
                var eth = new EthernetSource();
                eth.ParseEndPoint(portName);
                if (ConnectedSources.Any(d => d.Address.Equals(eth.Address) && d.PortNumber == eth.PortNumber))
                {
                    return true;
                }

                connectedSources.Add(eth);
                OnSourceConnected(eth);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RemoveSource(string portName)
        {
            try
            {
                var ethernetSource = connectedSources.FirstOrDefault(f => f.ToString().Equals(portName));
                if (ethernetSource == null)
                {
                    return false;
                }
                connectedSources.Remove(ethernetSource);
                OnSourceDisconnected(ethernetSource);
                ethernetSource.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
