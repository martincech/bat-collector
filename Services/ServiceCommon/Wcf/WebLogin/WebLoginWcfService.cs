﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.DataContracts.Device;
using Communication.Contract.Wcf.WebApi;
using Repository.Web.WebApi;
using Services.PublishSubscribe;
using Veit.Mqueue;

namespace ServiceCommon.Wcf.WebLogin
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class WebLoginWcfService : SubscriptionManager<IWebApiConnectionCallbacks>, IWebApiConnection, IWebApiConnectionCallbacks
    {
        private readonly IEnumerable<IWebApi> webApiConnections;
        private readonly IEnumerable<IQueue> queues;

        public WebLoginWcfService(IEnumerable<IWebApi> connections, IEnumerable<IQueue> queues)
        {
            connections = connections ?? new List<IWebApi>();
            this.queues = queues ?? new List<IQueue>();

            webApiConnections = connections.ToList();
            foreach (var webApiConnection in webApiConnections)
            {
                webApiConnection.ConnectivityStatusChanged += WebApiConnection_ConnectivityStatusChanged;
            }
        }

        private void WebApiConnection_ConnectivityStatusChanged(object sender, WebApiConnectionInfo e)
        {
            if (!(sender is IWebApi))
            {
                return;
            }
            ConnectionStatusChanged(e);
        }

        #region Implementation of ISynchronizerClient

        public async void ChangeServerConnection(WebApiConnectionInfo webApiConnectionInfo)
        {
            if (webApiConnectionInfo == null)
            {
                return;
            }
            var connection = webApiConnections.FirstOrDefault();
            if (connection == null)
            {
                return;
            }
            await connection.LoginAsync(webApiConnectionInfo);
        }

        public void GetExistingConnectionsRequest()
        {
            ExistingConnections(webApiConnections.Select(s => s.ConnectionInfo));
        }

        #endregion


        #region Implementation of ISynchronizerClientCallbacks

        public void ExistingConnections(IEnumerable<WebApiConnectionInfo> connections)
        {
            Publish(nameof(ExistingConnections), connections);
        }

        public void ConnectionStatusChanged(WebApiConnectionInfo webApiConnectionInfo)
        {
            Publish(nameof(ConnectionStatusChanged), webApiConnectionInfo);
        }

        #endregion

        public void SyncDevice(DeviceNotification device)
        {
            foreach (var queue in queues)
            {
                queue.Enqueue(device, Priority.High);
            }
        }
    }
}
