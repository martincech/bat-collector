﻿using System.ServiceModel;
using Communication.Contract.Wcf.DataSources;
using Communication.EventToQueue;
using Communication.Manager;
using Services.PublishSubscribe;
using Settings.Core;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Bat.Common.Sample.Base;
using Veit.Mqueue;

namespace ServiceCommon.Wcf
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class BaseDataSourceCallback<C> : SubscriptionManager<C>, IDataSource, IDataSourceCallback, IDiagnosticSetup
      where C : class, IDataSourceCallback
    {
        private readonly IProcessQueue processQueue;
        private readonly ISourceManager sourceManager;
        private readonly IDiagnostic diagnosticSetup;

        public BaseDataSourceCallback(IProcessQueue processQueue, ISourceManager sourceManager, IDiagnostic diagnosticSetup)
        {
            this.processQueue = processQueue;
            this.sourceManager = sourceManager;
            this.diagnosticSetup = diagnosticSetup;
            processQueue.AddProcessing<TimeSample>(NewSample);
            processQueue.AddProcessing<SourceConnectivityInfo>(OnConnectionChanged);
        }


        private bool OnConnectionChanged(SourceConnectivityInfo arg)
        {
            if (arg.Device.Connected)
            {
                Connected(arg);
            }
            else
            {
                Disconnected(arg);
            }
            return true;
        }

        private bool NewSample(TimeSample arg)
        {
            SampleReaded(arg);
            return true;
        }

        #region Implementation of IDataSourceCallback

        public void SampleReaded(TimeSample sample)
        {

            Publish(nameof(SampleReaded), sample);
        }

        public void Connected(SourceConnectivityInfo device)
        {
            Publish(nameof(Connected), device);
        }

        public void Disconnected(SourceConnectivityInfo device)
        {
            Publish(nameof(Disconnected), device);
        }

        #endregion

        #region Implementation of IDataSource

        public void GetConnected()
        {
            foreach (var connectivitySource in sourceManager.ConnectedSources)
            {
                processQueue.Enqueue(connectivitySource.CreateSourceConnectivityInfo(null, true));
                foreach (var dataSource in connectivitySource.ConnectedSources)
                {
                    processQueue.Enqueue(connectivitySource.CreateSourceConnectivityInfo(dataSource, true));
                }
            }
        }

        #endregion

        #region Implementation of IDiagnosticSetup

        public void SetDebugMode(bool enable)
        {
            diagnosticSetup.SetDebugMode(enable);
        }

        public void SendFeedback(string feedbackFileName, string message, bool includeDiagnosticFiles)
        {
            diagnosticSetup.SendFeedback(feedbackFileName, message, includeDiagnosticFiles);
        }

        #endregion
    }
}
