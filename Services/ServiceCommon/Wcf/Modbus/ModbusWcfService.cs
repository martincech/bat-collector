﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using Communication.Connectivity.Modbus;
using Communication.Connectivity.Modbus.LineRequest;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;
using Communication.Contract.Wcf.DataSources;
using Communication.DataSource;
using Communication.DataSource.Modbus;
using Communication.EventToQueue;
using Communication.Manager.Modbus;
using NLog;
using ServiceCommon.Extensions;
using Settings.Core;
using Veit.Bat.Common;
using Veit.Bat.Modbus.Bat2Scale.Registers.Types;
using Veit.Mqueue;

namespace ServiceCommon.Wcf.Modbus
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class ModbusWcfService : BaseDataSourceCallback<IModbusServiceCallback>, IModbusService, IModbusServiceCallback
    {
        #region Private fields

        private readonly IModbusSourceManager sourceManager;
        private readonly IEthernetSourceCreator ethCreator;
        private readonly IProcessQueue processQueue;
        private double progress;
        private double progressStep;
        private const double FULL_PROGRESS_VALUE = 99.999999;
        private const double EXPSILON = 0.0000001;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private readonly object lockObj = new object();

        #endregion

        #region Constructor

        public ModbusWcfService(
           IModbusSourceManager sourceManager,
           IEthernetSourceCreator ethCreator,
           IProcessQueue processQueue,
           IDiagnostic diagnosticSetup)
           : base(processQueue, sourceManager, diagnosticSetup)
        {
            processQueue.AddProcessing<SourceActivityInfo>(OnActivationChanged);
            this.sourceManager = sourceManager;
            this.ethCreator = ethCreator;
            this.processQueue = processQueue;

            ThreadPool.QueueUserWorkItem(o =>
            {
                sourceManager.SourceConnected += SourceManager_SourceConnected;
                foreach (var source in sourceManager.ConnectedSources.ToList())
                {
                    SourceManager_SourceConnected(null, source);
                }
            });
        }

        private void SourceManager_SourceConnected(object sender, Communication.Connectivity.IConnectivitySource source)
        {
            source.SourceConnected += Source_SourceConnected;
            source.SourceDisconnected += Source_SourceDisconnected;

            foreach (var device in source.ConnectedSources.ToList())
            {
                Source_SourceConnected(source, device);
            }
        }

        private void Source_SourceDisconnected(object sender, Communication.DataSource.IDataSource e)
        {
            if (e is IModbusDataSource device)
            {
                device.ResponseRead -= Device_ResponseRead;
            }
        }

        private void Source_SourceConnected(object sender, Communication.DataSource.IDataSource e)
        {
            if (e is IModbusDataSource device)
            {
                device.ResponseRead += Device_ResponseRead;
            }
        }

        /// <summary>
        /// Central point for catching devices responses
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Device_ResponseRead(object sender, ModbusResponse e)
        {
            try
            {
                if (!(sender is IModbusDataSource scale))
                {
                    logger.Warn("Device response for not IModbusDataSource object!");
                    return;
                }
                if (!DataSourceExtensions.ParseSourceFromUid(scale.Id, out var source))
                {
                    logger.Warn($"Device response: failed to parse source device from UID: {scale.Id}");
                    return;
                }

                switch(e.Type)
                {
                    case Bat2Request.Configuration:
                        ResponseConfiguration(source, scale.Id, e.Data as Configuration);
                        break;
                    case Bat2Request.Version:
                        ResponseVersion(source, scale.Id, (Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version)e.Data);
                        break;
                    case Bat2Request.ScaleName:
                        ResponseScaleName(source, scale.Id, e.Data.ToString());
                        break;
                    case Bat2Request.Flock:
                        ResponseFlock(source, scale.Id, e.Data as Flock);
                        break;
                    default:
                        logger.Info($"Unsupported modbus device response: {e.Type}, device: {scale.Id}");
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex, "Modbus device response exception");
            }
        }

        private void ResponseConfiguration(string source, string uid, Configuration data)
        {
            var configuration = data.Map();
            if (configuration == null)
            {
                logger.Info($"GetConfiguration: '{uid}' ERROR - configuration is null");
                return;
            }
            DeviceConfiguration(source, uid, configuration);
            logger.Info($"GetConfiguration: '{uid}' version: {configuration.Version}, scaleName {configuration.ScaleName}");
        }

        private void ResponseVersion(string source, string uid, Veit.Bat.Modbus.Bat2Scale.Registers.Enums.Version data)
        {
            var version = data.Map();
            DeviceVersion(source, uid, version);
            logger.Info($"GetVersion: '{uid}'; '{version}'");
        }

        private void ResponseScaleName(string source, string uid, string name)
        {
            DeviceNameRead(source, uid, name);
            logger.Info($"GetDeviceName: '{uid}'; '{name}'");
        }

        private void ResponseFlock(string source, string uid, Flock data)
        {
            var flock = data.MapTo();
            logger.Info(flock == null
               ? $"GetFlock: Flock doesn't exist '{uid}', flock number: {flock?.Number}"
               : $"GetFlock: '{uid}', flock number: {flock.Number}");
            DeviceFlock(source, uid, flock);
        }

        #endregion

        #region Implementation of IModbusService

        public void AddSource(string portId)
        {
            CheckArgument(portId);
            if (IsComPort(ref portId))
            {
                throw new ArgumentException("COM port is not supported!", nameof(portId));
            }
            else
            {
                AddEthernetSource(portId);
            }
        }

        public void RemoveSource(string portId)
        {
            CheckArgument(portId);
            if (IsComPort(ref portId))
            {
                throw new ArgumentException("COM port is not supported!", nameof(portId));
            }
            else
            {
                RemoveEthernetSource(portId);
            }
        }

        private static void CheckArgument(string portId)
        {
            if (string.IsNullOrWhiteSpace(portId))
            {
                throw new ArgumentException($"Invalid argument: {nameof(portId)}");
            }
        }

        private bool IsComPort(ref string id)
        {
            if (sourceManager is MultiModbusSourceManager manager &&
               manager.UidMapper.TryGetMappingKeyOrDefault(id, out var uid))
            {
                id = uid;
            }
            return id.Contains("COM");
        }

        private void AddEthernetSource(string id)
        {
            CheckArgument(id);
            ethCreator.AddSource(id);
            logger.Info("AddEthernetSource: {0} ", id);
        }

        private void RemoveEthernetSource(string id)
        {
            CheckArgument(id);
            ethCreator.RemoveSource(id);
            logger.Info("RemoveEthernetSource: {0} ", id);
        }

        public void SetPortSettings(string portId, CommunicationParameters communicationSettings)
        {
            CheckArgument(portId);
            var modbusManager = sourceManager;
            if (modbusManager == null)
            {
                return;
            }

            StopScanning(portId);
            ScanProgress(0);

            modbusManager.ChangeCommunicationParameters(portId, communicationSettings);
            logger.Info($"SetPortSettings: '{portId}', {communicationSettings}");
        }

        public void ScanPortAddresses(IEnumerable<string> portIds, IEnumerable<byte> addresses)
        {
            var ports = portIds.ToList();
            StopScanning(ports);

            var addrs = addresses.ToList();
            progressStep = 100.0 / (ports.Count * addrs.Count);
            progress = 0;
            foreach (var portId in ports)
            {
                ScanPortAddresses(portId, addrs);
            }
        }

        private void ScanPortAddresses(string portId, IEnumerable<byte> addresses)
        {
            logger.Info($"ScanPortAddresses: '{portId}'");
            if (!(sourceManager.ConnectedSources.FirstOrDefault(s => s.Id == portId) is IModbusConnectivitySource port)) return;
            foreach (var address in addresses)
            {
                port.ScanForDevices(new ModbusScanRequest { AddressFrom = address, AddressTo = address }, ScanProgress);
            }
        }

        private void ScanProgress()
        {
            lock (lockObj)
            {
                progress += progressStep;
                if (IsDoubleGreaterThan(progress, FULL_PROGRESS_VALUE))
                {
                    progress = FULL_PROGRESS_VALUE;
                }
                ScanProgress(progress);
            }
        }

        private static bool IsDoubleGreaterThan(double value, double limit)
        {
            return (value - limit) > EXPSILON;
        }


        public void StopScanning(IEnumerable<string> ports)
        {
            foreach (var port in ports)
            {
                StopScanning(port);
            }
            ScanProgress(0);
        }

        private void StopScanning(string portId)
        {
            if (!(sourceManager.ConnectedSources.FirstOrDefault(s => s.Id == portId) is IModbusConnectivitySource port)) return;
            port.StopScanForDevices();
        }

        public void ActivateDevices(IEnumerable<string> portIds, IEnumerable<string> addresses)
        {
            var addrs = addresses.ToList();
            foreach (var portId in portIds)
            {
                ActivateDevices(portId, addrs);
            }
        }

        public void DeactivateDevices(IEnumerable<string> portIds, IEnumerable<string> addresses)
        {
            var addrs = addresses.ToList();
            foreach (var portId in portIds)
            {
                DeactivateDevices(portId, addrs);
            }
        }

        /// <inheritdoc />
        public void GetActive()
        {
            foreach (var modbusConnectivitySource in sourceManager.ConnectedSources.Where(s => s is IModbusConnectivitySource).Cast<IModbusConnectivitySource>())
            {
                foreach (var modbusDataSource in modbusConnectivitySource.ActiveSources)
                {
                    processQueue.Enqueue(modbusConnectivitySource.CreateSourceActivityInfo(modbusDataSource, true));
                }
            }
        }


        #region Implementation of Device Configuration

        public void GetConfiguration(string portId, byte address)
        {
            var device = GetDevice(portId, address);
            device.GetConfiguration();
        }

        public void GetVersion(string portId, byte address)
        {
            var device = GetDevice(portId, address);
            device?.GetVersion();
        }

        public void GetDeviceName(string portId, byte address)
        {
            var device = GetDevice(portId, address);
            device?.GetScaleName();
        }

        public void SetDeviceName(string portId, byte address, string name)
        {
            CheckArgument(name);
            var device = GetDevice(portId, address);
            if (device == null)
            {
                return;
            }
            device.SetScaleName(name);
            logger.Info($"SetDeviceName: '{device.Id}', {name}");
        }

        public void SetSavingParameters(string portId, byte address, Communication.Contract.Wcf.DataContracts.Device.Configuration.SavingParameters parameters)
        {
            var device = GetDevice(portId, address);
            if (device == null) return;

            device.SetSavingParameters(parameters.MapFrom());
            logger.Info($"SetSavingParameters: '{device.Id}'");
        }

        private IModbusDataSource FindDevice(string portId, string address)
        {
            if (!(sourceManager.ConnectedSources.FirstOrDefault(s => s.Id == portId) is IModbusConnectivitySource port)) return null;

            var device =
               port.ConnectedSources.Where(d => d is IModbusDataSource)
                  .Cast<IModbusDataSource>()
                  .FirstOrDefault(d => d.Id == address);
            return device;
        }

        private IModbusDataSource GetDevice(string portId, byte address)
        {
            var uid = DataSourceExtensions.FormatIdOfDataSource(address, portId);
            return FindDevice(portId, uid);
        }

        #endregion


        private void ActivateDevices(string portId, IEnumerable<string> addresses)
        {
            if (!(sourceManager.ConnectedSources.FirstOrDefault(s => s.Id == portId) is IModbusConnectivitySource port))
            {
                return;
            }
            foreach (var address in addresses)
            {
                var device = port.ConnectedSources.Where(d => d is IModbusDataSource).Cast<IModbusDataSource>().FirstOrDefault(d => d.Id == address);
                if (device != null)
                {
                    port.ActivateSource(device);
                }
            }
        }

        private void DeactivateDevices(string portId, IEnumerable<string> addresses)
        {
            if (!(sourceManager.ConnectedSources.FirstOrDefault(s => s.Id == portId) is IModbusConnectivitySource port))
            {
                return;
            }
            foreach (var address in addresses)
            {
                var device = port.ConnectedSources.Where(d => d is IModbusDataSource).Cast<IModbusDataSource>().FirstOrDefault(d => d.Id == address);
                if (device != null)
                {
                    port.DeactivateSource(device);
                }
            }
        }

        #endregion

        #region Implementation of IModbusServiceCallback

        public void ScanProgress(Percent percent)
        {
            Publish(nameof(ScanProgress), percent);
        }

        public void Activated(string portId, string deviceAddress)
        {
            Publish(nameof(Activated), portId, deviceAddress);
        }

        public void Deactivated(string portId, string deviceAddress)
        {
            Publish(nameof(Deactivated), portId, deviceAddress);
        }

        public void DeviceNameRead(string portId, string deviceAddress, string name)
        {
            Publish(nameof(DeviceNameRead), portId, deviceAddress, name);
        }

        public void DeviceVersion(string portId, string deviceAddress, DeviceVersion version)
        {
            Publish(nameof(DeviceVersion), portId, deviceAddress, version);
        }

        public void DeviceConfiguration(string portId, string deviceAddress, DeviceConfiguration configuration)
        {
            Publish(nameof(DeviceConfiguration), portId, deviceAddress, configuration);
        }


        private bool OnActivationChanged(SourceActivityInfo arg)
        {
            var deviceAddress = string.IsNullOrEmpty(arg.Device.DeviceId) ? string.Empty : arg.Device.DeviceId;
            if (arg.Device.Active)
            {
                Activated(arg.Port, deviceAddress);
            }
            else
            {
                Deactivated(arg.Port, deviceAddress);
            }
            return true;
        }

        #endregion

        #region Implementation of IScaleFlock

        public void GetFlock(string portId, byte address, byte flockNumber)
        {
            var device = GetDevice(portId, address);
            device?.GetFlock(flockNumber);
        }

        public void SetFlock(string portId, byte address, Communication.Contract.Wcf.DataContracts.Device.Flock flock)
        {
            var device = GetDevice(portId, address);
            if (device == null)
            {
                return;
            }
            device.SetFlock(flock.MapFrom());
            logger.Info($"SetFlock: '{device.Id}', flock number: '{flock.Number}'");
        }

        #endregion

        #region Implementation of IScaleFlockCallback

        public void DeviceFlock(string portId, string deviceAddress, Communication.Contract.Wcf.DataContracts.Device.Flock flock)
        {
            Publish(nameof(DeviceFlock), portId, deviceAddress, flock);
        }

        #endregion
    }
}
