﻿namespace ServiceCommon.Wcf.Modbus
{
    public interface IEthernetSourceCreator
    {
        bool AddSource(string portName);
        bool RemoveSource(string portName);
    }
}
