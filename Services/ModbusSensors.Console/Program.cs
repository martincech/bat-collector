﻿using ModbusSensors.Core;
using NLog;
using ServiceCommon.Logger;

namespace ModbusSensors.Console
{
    static class Program
    {
        private static readonly Logger LOGGER = LogManager.GetCurrentClassLogger();

        static void Main()
        {
            _ = new ConsoleLogger(ModbusSensorsServiceImpl.ServiceName);
            var serviceImpl = new ModbusSensorsServiceImpl();
            serviceImpl.Start(null);

            System.Console.ReadKey();
            serviceImpl.Stop();
            LOGGER.Debug("Program exiting ...");
        }
    }
}
