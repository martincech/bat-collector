﻿using ModbusSensors.Core;
using ServiceCommon;

namespace ModbusSensors.Service
{
    public partial class ModbusSensorsService : CommonServiceBase
    {
        public static string Name { get; } = ModbusSensorsServiceImpl.ServiceName;

        /// <inheritdoc />
        public ModbusSensorsService() : base(Name, ServiceImplFactory)
        {
        }

        #region Overrides of CommonServiceBase

        private static ServiceBaseImpl ServiceImplFactory()
        {
            return new ModbusSensorsServiceImpl();
        }

        #endregion
    }
}
