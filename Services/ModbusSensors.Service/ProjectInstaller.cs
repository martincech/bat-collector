﻿using System.ComponentModel;
using System.Configuration.Install;
using ServiceCommon;

namespace ModbusSensors.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();

            ModbusSensorsServiceInstaller.DisplayName = ModbusSensorsService.Name;
            ModbusSensorsServiceInstaller.ServiceName = ModbusSensorsService.Name;
            Committed += OnCommitted;
            AfterInstall += OnAfterInstall;
        }

        private static void OnAfterInstall(object sender, InstallEventArgs installEventArgs)
        {
            InstallerHelper.StartService(ModbusSensorsService.Name);
        }

        private static void OnCommitted(object sender, InstallEventArgs installEventArgs)
        {
            InstallerHelper.SetRestartBehavior(ModbusSensorsService.Name);
        }
    }
}
