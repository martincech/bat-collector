﻿using System.ComponentModel;
using System.ServiceProcess;

namespace ModbusSensors.Service
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ModbusSensorsServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ModbusSensorsServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ModbusSensorsServiceProcessInstaller
            // 
            this.ModbusSensorsServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ModbusSensorsServiceProcessInstaller.Password = null;
            this.ModbusSensorsServiceProcessInstaller.Username = null;
            // 
            // ModbusSensorsServiceInstaller
            // 
            this.ModbusSensorsServiceInstaller.Description = "Synchronization of cable connected scales";
            this.ModbusSensorsServiceInstaller.DisplayName = "Modbus Sensors Synchronizer";
            this.ModbusSensorsServiceInstaller.ServiceName = "ModbusSensorsService";
            this.ModbusSensorsServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ModbusSensorsServiceProcessInstaller,
            this.ModbusSensorsServiceInstaller});

        }

        #endregion

        private ServiceInstaller ModbusSensorsServiceInstaller;
        public ServiceProcessInstaller ModbusSensorsServiceProcessInstaller;
    }
}
