﻿using Communication.Connectivity;

namespace Communication.Manager
{
    public interface ISourceManager : IConnectivitySourcePattern<IConnectivitySource>
    {
    }
}
