﻿using System;
using System.Collections.Generic;
using Communication.Connectivity;

namespace Communication.Manager
{
    public abstract class SourceManager : ISourceManager
    {
        protected SourceManager()
        {
            ConnectedSources = new List<IConnectivitySource>();
        }

        #region Implementation of ISourceManager

        public event EventHandler<IConnectivitySource> SourceConnected;
        public event EventHandler<IConnectivitySource> SourceDisconnected;
        public IEnumerable<IConnectivitySource> ConnectedSources { get; private set; }

        #endregion

        protected void OnSourceConnected(IConnectivitySource e)
        {
            if (!(ConnectedSources is IList<IConnectivitySource> connectedSources) || connectedSources.Contains(e)) return;

            connectedSources.Add(e);
            SourceConnected?.Invoke(this, e);
        }

        protected void OnSourceDisconnected(IConnectivitySource e)
        {
            if (!(ConnectedSources is IList<IConnectivitySource> connectedSources) || !connectedSources.Contains(e)) return;
            connectedSources.Remove(e);
            SourceDisconnected?.Invoke(this, e);
        }
    }
}
