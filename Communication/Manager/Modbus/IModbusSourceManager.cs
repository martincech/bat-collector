﻿using Communication.Contract.Wcf.DataContracts;

namespace Communication.Manager.Modbus
{
    public interface IModbusSourceManager : ISourceManager
    {
        void ChangeCommunicationParameters(string portId, CommunicationParameters communicationSettings);
    }
}
