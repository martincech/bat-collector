﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity;
using Communication.Contract.Wcf.DataContracts;

namespace Communication.Manager.Modbus
{
    public class MultiModbusSourceManager : MultiSourceManager<IModbusSourceManager>, IModbusSourceManager
    {
        public UidMapper UidMapper { get; private set; }

        /// <inheritdoc />
        public MultiModbusSourceManager(IEnumerable<IModbusSourceManager> managers,
           UidMapper uidMapper)
           : base(managers)
        {
            UidMapper = uidMapper;
        }

        /// <inheritdoc />
        public void ChangeCommunicationParameters(string portId, CommunicationParameters communicationSettings)
        {
            foreach (var modbusSourceManager in Managers.Where(m => m.ConnectedSources.Any(s => s.Id == portId)).ToList())
            {
                modbusSourceManager.ChangeCommunicationParameters(portId, communicationSettings);
            }
        }
    }

    public class MultiSourceManager<T> : ISourceManager
       where T : ISourceManager
    {
        protected readonly IEnumerable<T> Managers;

        public MultiSourceManager(IEnumerable<T> managers)
        {
            Managers = managers;
        }

        #region Implementation of IConnectivitySourcePattern<IConnectivitySource>

        public event EventHandler<IConnectivitySource> SourceConnected
        {
            add
            {
                foreach (var sourceManager in Managers)
                {
                    sourceManager.SourceConnected += value;
                }
            }
            remove
            {
                foreach (var sourceManager in Managers)
                {
                    sourceManager.SourceConnected -= value;
                }
            }
        }

        public event EventHandler<IConnectivitySource> SourceDisconnected
        {
            add
            {
                foreach (var sourceManager in Managers)
                {
                    sourceManager.SourceDisconnected += value;
                }
            }
            remove
            {
                foreach (var sourceManager in Managers)
                {
                    sourceManager.SourceDisconnected -= value;
                }
            }
        }

        public IEnumerable<IConnectivitySource> ConnectedSources
        {
            get { return Managers.SelectMany(m => m.ConnectedSources); }
        }

        #endregion
    }
}
