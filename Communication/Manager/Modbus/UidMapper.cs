﻿using System;
using System.Collections.Generic;

namespace Communication.Manager.Modbus
{
    public class UidMapper
    {
        private readonly Dictionary<string, string> uidMappings;

        public UidMapper(Dictionary<string, string> uidMappings)
        {
            this.uidMappings = uidMappings ?? throw new ArgumentNullException(nameof(uidMappings));
        }

        /// <summary>
        /// Try get value from uid mappings. If no match
        /// found return key as default value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetMappingValueOrDefault(string key, out string value)
        {
            var result = uidMappings.TryGetValue(key, out value);
            if (!result)
            {
                value = key;
            }
            return result;
        }

        /// <summary>
        /// Try get key from uid mapping by
        /// its value. If not match found return value.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool TryGetMappingKeyOrDefault(string value, out string key)
        {
            key = value;
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            foreach (var record in uidMappings)
            {
                if (value.Equals(record.Value))
                {
                    key = record.Key;
                    return true;
                }
            }
            return false;
        }
    }
}
