﻿using Communication.Connectivity;
using Veit.Bat.Modbus.Common.Connectivity;

namespace Communication.Manager.Modbus.Connectivity
{
    public interface IModbusEthernetConnectivity : IModbusLineConnectivity, IConnectivitySourcePattern<IEthernet>
    {
    }
}
