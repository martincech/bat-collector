﻿using Communication.Connectivity;

namespace Communication.Manager.Modbus.Connectivity
{
    public interface IModbusLineConnectivity
    {
        ModbusLineType ModbusLineType { get; }
    }
}
