﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Communication.Connectivity.Modbus;
using Communication.Manager.Modbus.Connectivity;
using Settings.DeviceSettings;
using Settings.DeviceSettings.SourceSettings;
using Veit.Bat.Modbus.Bat2Scale;
using Veit.Bat.Modbus.Common.Connectivity;

namespace Communication.Manager.Modbus
{
    public class ModbusEthernetManager : ModbusManager
    {
        private const int SourceReconnectTime = 60 * 1000 * 5;
        private readonly object lockObj = new object();
        private const int AttemptsForAlert = 100;


        public ModbusEthernetManager(IModbusEthernetConnectivity modbusLine,
                    ModbusSettings defaultSettings,
                    IEnumerable<DeviceSourceSettings<ModbusSettings>> deviceSettings,
                    Guid terminalId,
                    UidMapper uidMapper)
            : base (modbusLine, defaultSettings, deviceSettings, terminalId, uidMapper)
        {
            modbusLine.SourceConnected += ModbusLineSourceConnected;
            modbusLine.SourceDisconnected += ModbusLineSourceDisconnected;
            TryConnectSources(modbusLine.ConnectedSources.ToList());
        }

        public override void ChangeCommunicationParameters(string portId, Contract.Wcf.DataContracts.CommunicationParameters communicationSettings)
        {
            logger.Debug($"ChangeCommunicationParameters for: {portId}");
            UidMapper.TryGetMappingKeyOrDefault(portId, out var id);
            foreach (var ethernetManager in connectivitySources.Where(pair => ((IEthernet)pair.Key).ToString().Equals(id)).ToList())
            {
                if (!(ethernetManager.Key is IEthernet device)) continue;

                ModbusLineSourceDisconnected(this, device);
                ChangeSettings<EthernetSourceSettings>(id, communicationSettings, ethernetManager.Value);
                ModbusSourceConnect(device);
            }
        }

        private void ModbusSourceConnect(IEthernet device)
        {
            CommonSourceConnected(device, () => CreateModbusSource(device));
        }

        private ModbusConnectivitySource CreateModbusSource(IEthernet device)
        {
            UidMapper.TryGetMappingValueOrDefault(device.ToString(), out var deviceIdentifier);
            var settingsForDevice = deviceSettings.FirstOrDefault(s => s.Uid == deviceIdentifier);
            var master = new Bat2ModbusMaster(device, CommunicationSettingsOrDefault(settingsForDevice));
            modbusMasters.TryAdd(device, master);
            master.CommunicationCrashed += Master_CommunicationCrashed;

            return GetConnectivitySource(deviceIdentifier, master, settingsForDevice);
        }

        private void Master_CommunicationCrashed(object sender, System.EventArgs e)
        {
            if (!(sender is Bat2ModbusMaster master))
            {
                logger.Warn("Cannot reconnect modbus master, because sender is not required type!");
                return;
            }
            ReconnectClient(master);
        }

        private void TryConnectSources(ICollection<IEthernet> sources)
        {
            if (!sources.Any()) return;

            logger.Info("Try connect sources");
            foreach (var device in sources)
            {
                ThreadPool.QueueUserWorkItem(o =>
                {
                    TrySourceConnectRepeatly(device);
                });
            }
        }

        private void ModbusLineSourceConnected(object sender, IEthernet e)
        {
            var deviceInfo = e.ToString();
            if (IsSourceStillUsing(deviceInfo))
            {  //existing source
                TrySourceConnectRepeatly(e);
            }
            else
            {  //new source - if attempt to add failed, not repeat process
                ModbusSourceConnect(e);
            }
        }

        /// <summary>
        /// Socket can be blocked for a while, therefore there is more attempts to reconnect.
        /// </summary>
        /// <param name="device"></param>
        private void TrySourceConnectRepeatly(IEthernet device)
        {
            if (device == null)
            {
                logger.Warn("TrySourceConnectRepeatly for null source device!");
                return;
            }

            var deviceInfo = device.ToString();
            var attempt = 1;
            while (true)
            {
                logger.Info($"Trying connect ethernet source: {deviceInfo}, attempt: {attempt}");
                if (attempt % AttemptsForAlert == 0)
                {
                    logger.Error($"Can't reconnect to ethernet source: '{deviceInfo}' after '{AttemptsForAlert}' attempts!");
                }
                if (!IsSourceStillUsing(deviceInfo))
                {
                    logger.Info($"Trying connect ethernet source: {deviceInfo}, source was removed from settings, it is not using now. Stop reconnecting process!");
                    return;
                }

                ModbusSourceConnect(device);
                if (connectivitySources.ContainsKey(device))
                {  //reconnect success
                    logger.Info($"Source was connect SUCCESSFULLY: {deviceInfo}");
                    return;
                }
                Thread.Sleep(SourceReconnectTime);
                attempt++;
            }
        }

        private void ModbusLineSourceDisconnected(object sender, IEthernet device)
        {
            CommonSourceDisconnected(device);
        }

        /// <summary>
        /// Check if source is still using. Source is saved in settings.
        /// If is not in settings, was deleted.
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        private bool IsSourceStillUsing(string deviceId)
        {
            if (!UidMapper.TryGetMappingValueOrDefault(deviceId, out var uid))
            {
                uid = deviceId;
            }
            return deviceSettings.FirstOrDefault(f => f.Uid.Equals(uid)) != null;
        }

        protected override void ReconnectClient(Bat2ModbusMaster master)
        {
            IEthernet device = null;
            lock (lockObj)
            {
                device = modbusMasters.Keys.FirstOrDefault(p => modbusMasters[p] == master) as IEthernet;
                if (device == null) return;   //device is already disconnected

                logger.Debug($"Ethernet source reconnecting: {device.ToString()}");
                ModbusLineSourceDisconnected(this, device);
            }

            ThreadPool.QueueUserWorkItem(o =>
            {
                TrySourceConnectRepeatly(device);
            });
        }
    }
}
