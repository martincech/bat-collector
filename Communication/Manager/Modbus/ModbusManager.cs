﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Communication.Connectivity;
using Communication.Connectivity.Modbus;
using Communication.Contract.Wcf.DataContracts;
using Communication.DataSource.Modbus;
using Communication.Manager.Modbus.Connectivity;
using NLog;
using Settings.DeviceSettings;
using Settings.DeviceSettings.SourceSettings;
using Veit.Bat.Modbus.Bat2Scale;
using Veit.Bat.Modbus.Bat2Scale.Registers;
using Veit.Mqueue;

namespace Communication.Manager.Modbus
{
    public abstract class ModbusManager : SourceManager, IModbusSourceManager
    {
        #region Private fields

        protected readonly ConcurrentDictionary<object, Bat2ModbusMaster> modbusMasters;
        protected readonly ConcurrentDictionary<object, ModbusConnectivitySource> connectivitySources;
        protected readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly ModbusSettings defaultSettings;
        private readonly Guid terminalId;
        protected readonly UidMapper UidMapper;
        protected readonly List<DeviceSourceSettings<ModbusSettings>> deviceSettings;
        protected readonly SourceType ConnectivitySourceType;

        private Timer checkSourceDevicesAliveTimer;
        private const int CheckDeviceAliveTimerInterval = 1000 * 30;

        #endregion


        protected ModbusManager(IModbusLineConnectivity modbusLine,
                             ModbusSettings defaultSettings,
                             IEnumerable<DeviceSourceSettings<ModbusSettings>> deviceSettings,
                             Guid terminalId,
                             UidMapper uidMapper)
        {
            modbusMasters = new ConcurrentDictionary<object, Bat2ModbusMaster>();
            connectivitySources = new ConcurrentDictionary<object, ModbusConnectivitySource>();

            this.defaultSettings = defaultSettings;
            this.terminalId = terminalId;
            UidMapper = uidMapper;
            if (deviceSettings == null)
            {
                deviceSettings = new List<DeviceSourceSettings<ModbusSettings>>();
            }
            this.deviceSettings = deviceSettings.ToList();

            ConnectivitySourceType = (SourceType)modbusLine.ModbusLineType;
            InitTimerForCheckSourceDevices();
        }


        protected void CommonSourceConnected(object device, Func<ModbusConnectivitySource> getSourceFunc)
        {
            try
            {
                if (device == null || connectivitySources.ContainsKey(device)) return;
                var connectivitySource = getSourceFunc.Invoke();

                if (connectivitySource == null)
                {
                    return;
                }
                connectivitySources.GetOrAdd(device, connectivitySource);
                OnSourceConnected(connectivitySource);
            }
            catch (Exception e)
            {
                logger.Warn(e);
            }
        }

        protected void CommonSourceDisconnected(object device)
        {
            if (device == null || !connectivitySources.ContainsKey(device)) return;
            var source = connectivitySources[device];
            connectivitySources.TryRemove(device, out _);
            OnSourceDisconnected(source);
            source.Dispose();

            if (!modbusMasters.TryRemove(device, out _))
            {
                logger.Warn($"CommonSourceDisconnected: removing of modbus master {device} FAILED!");
            }
        }


        protected Func<byte, Bat2ModbusDataSource> Bat2DataSourceFactory(Bat2ModbusMaster master, IProcessQueue queue, string devicePortName)
        {
            return address =>
            {
                IBat2ModbusRegisterRw scale = null;
                logger.Debug($"Check if modbus scale is connected. Source: {master.ToString()}, address: {address}");
                if (!master.IsScaleConnected(address, ref scale))   //Note: source factory is called from modbus scan request => this modbus action happens synchronously
                {   //No scale on address
                    logger.Info($"No scale is connected on: {master.ToString()}, address: {address}");
                    return null;
                }
                var lastRead = GetLastReadDate(devicePortName, address);
                return new Bat2ModbusDataSource(scale, queue, address, terminalId, devicePortName, lastRead);
            };
        }

        private DateTimeOffset? GetLastReadDate(string portName, byte address)
        {
            var device = deviceSettings.SelectMany(s => s.Devices).FirstOrDefault(w => w.Uid == portName + "-" + address);
            if (device == null) return null;
            return device.LastSampleTimeStamp;
        }


        private void InitTimerForCheckSourceDevices()
        {
            checkSourceDevicesAliveTimer = new Timer(CheckDeviceAliveTimerInterval);
            checkSourceDevicesAliveTimer.Elapsed += CheckSourceDevicesAliveTimer_Elapsed;
            checkSourceDevicesAliveTimer.Enabled = true;
        }

        private void CheckSourceDevicesAliveTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            checkSourceDevicesAliveTimer.Enabled = false;
            foreach (var source in connectivitySources.Values)
            {
                var sourceDevices = GetNotActivatedSourceDevices(source);
                if (sourceDevices == null)
                {
                    continue;
                }
                source.AddActiveDevices(sourceDevices, true);
            }
            checkSourceDevicesAliveTimer.Enabled = true;
        }

        protected ModbusConnectivitySource GetConnectivitySource(string deviceIdentifier, Bat2ModbusMaster master, DeviceSourceSettings<ModbusSettings> settingsForDevice)
        {
            var queue = new ProcessQueue();
            var modbusSource = new ModbusConnectivitySource(queue, deviceIdentifier, ConnectivitySourceType);
            modbusSource.AddSupportedDataSource(Bat2DataSourceFactory(master, queue, deviceIdentifier));
            modbusSource.AddActiveDevices(settingsForDevice?.Devices);
            return modbusSource;
        }

        protected void ChangeSettings<T>(string portId, CommunicationParameters communicationSettings, ModbusConnectivitySource source)
           where T : DeviceSourceSettings<ModbusSettings>, new()
        {
            var conSettings = deviceSettings.FirstOrDefault(d => d.Uid.Equals(portId));
            if (conSettings == null)
            {
                conSettings = new T
                {
                    Description = portId,
                    Name = portId,
                    Uid = portId,
                    Devices = new List<DeviceSettings>()
                };
                foreach (var dev in source.ActiveSources)
                {
                    conSettings.Devices.Add(new DeviceSettings
                    {
                        Name = dev.Id,
                        Uid = dev.Id
                    });
                }
            }
            conSettings.Settings = ToModbusSettings(communicationSettings);
        }

        protected static ModbusSettings ToModbusSettings(CommunicationParameters s)
        {
            return new ModbusSettings
            {
                BaudRate = s.Speed,
                ReplyDelay = s.ReplyDelay,
                SilentInterval = s.SilentInterval,
                Repetitions = s.Repetitions,
                Protocol = s.Protocol,
                Parity = s.Parity
            };
        }

        private List<DeviceSettings> GetNotActivatedSourceDevices(ModbusConnectivitySource source)
        {
            var sourceDevices = new List<DeviceSettings>();
            var settings = deviceSettings.FirstOrDefault(d => d.Uid.Equals(source.Id));
            if (settings == null)
            {
                return sourceDevices;
            }

            foreach (var dev in settings.Devices)
            {
                if (source.ConnectedSources.FirstOrDefault(f => dev.Uid.Equals(f.Id)) == null)
                {
                    sourceDevices.Add(dev);
                    logger.Debug($"Not activated device yet: {dev.Uid}");
                }
            }
            return sourceDevices;
        }

        protected Veit.Bat.Modbus.Common.Parameters.CommunicationParameters CommunicationSettingsOrDefault(DeviceSourceSettings<ModbusSettings> sourceSettings)
        {
            return sourceSettings != null ? ToCommunicationParams(sourceSettings.Settings) : ToCommunicationParams(defaultSettings);
        }

        private static Veit.Bat.Modbus.Common.Parameters.CommunicationParameters ToCommunicationParams(ModbusSettings s)
        {
            return new Veit.Bat.Modbus.Common.Parameters.CommunicationParameters
            {
                Speed = s.BaudRate,
                ReplyDelay = s.ReplyDelay,
                SilentInterval = s.SilentInterval,
                Repetitions = s.Repetitions,
                Protocol = PacketModeMapping(s.Protocol),
                Parity = ParityMapping(s.Parity)
            };
        }

        private static Veit.Bat.Modbus.Common.Parameters.PacketMode PacketModeMapping(PacketMode mode)
        {
            switch (mode)
            {
                case PacketMode.ASCII:
                    return Veit.Bat.Modbus.Common.Parameters.PacketMode.ASCII;
                case PacketMode.RTU:
                    return Veit.Bat.Modbus.Common.Parameters.PacketMode.RTU;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mode));
            }
        }

        private static Veit.Bat.Modbus.Common.Parameters.Parity ParityMapping(Parity mode)
        {
            switch (mode)
            {
                case Parity.Even:
                    return Veit.Bat.Modbus.Common.Parameters.Parity.Even;
                case Parity.None:
                    return Veit.Bat.Modbus.Common.Parameters.Parity.None;
                case Parity.Odd:
                    return Veit.Bat.Modbus.Common.Parameters.Parity.Odd;
                default:
                    throw new ArgumentOutOfRangeException(nameof(mode));
            }
        }

        protected abstract void ReconnectClient(Bat2ModbusMaster master);

        public abstract void ChangeCommunicationParameters(string portId, CommunicationParameters communicationSettings);
    }
}
