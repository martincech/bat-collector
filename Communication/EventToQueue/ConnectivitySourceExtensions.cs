﻿using System;
using Communication.Connectivity;
using Communication.DataSource;
using Communication.DataSource.Modbus;
using Veit.Bat.Common.DeviceStatusInfo;

namespace Communication.EventToQueue
{
    public static class ConnectivitySourceExtensions
    {
        public static SourceConnectivityInfo CreateSourceConnectivityInfo(this IConnectivitySource source, IDataSource dataSource, bool connected)
        {
            return new SourceConnectivityInfo
            {
                Source = source.Id,
                Device = new DeviceConnectivityInfo
                {
                    Connected = connected,
                    TimeStamp = DateTime.Now,
                    Type = dataSource == null ? source.Type.ToString() : dataSource.Type.ToString(),
                    SensorUid = dataSource?.Id
                }
            };
        }

        public static SourceActivityInfo CreateSourceActivityInfo(this IConnectivitySource source, IModbusDataSource dataSource, bool activated)
        {
            var info = new SourceActivityInfo
            {
                Port = source.Id,
                Device = new DeviceActivityInfo
                {
                    Active = activated,
                    DeviceId = dataSource.Id
                }
            };
            return info;
        }
    }
}
