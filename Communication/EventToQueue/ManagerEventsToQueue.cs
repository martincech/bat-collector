﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Communication.Connectivity;
using Communication.Connectivity.Modbus;
using Communication.DataSource;
using Communication.DataSource.Modbus;
using Communication.Manager;
using Communication.Manager.Modbus;
using Veit.Mqueue;

namespace Communication.EventToQueue
{
    /// <summary>
    ///    Register to events from source manager and share data from events to all registered repositories
    /// </summary>
    public sealed class ManagerEventsToQueue : IDisposable
    {
#if DEBUG
#pragma warning disable CC0052 // Make field readonly
        private static int _instances;
#pragma warning restore CC0052 // Make field readonly
        private static readonly object _synchro;
        private static readonly Dictionary<ManagerEventsToQueue, List<IQueue>> _queues;
        private readonly int instanceNumber;

        static ManagerEventsToQueue()
        {
            _instances = 0;
            _synchro = new object();
            _queues = new Dictionary<ManagerEventsToQueue, List<IQueue>>();
        }

        private static void LineDelim()
        {
            Debug.WriteLine(new string('-', 50));
        }
        private static void PrintQueues()
        {
            Debug.WriteLine($"Active ManagerEventsToQueue: {_queues.Keys.Count}\n");
            foreach (var item in _queues)
            {
                var manager = item.Key;
                var queueList = item.Value;
                LineDelim();
                LineDelim();
                Debug.WriteLine($"ManagerEventsToQueue instance {manager.instanceNumber}");
                var qnum = 1;
                foreach (var queue in queueList)
                {
                    var sb = new StringBuilder();
                    sb.Append($"Q{qnum} ({queue.GetType().ToString()}): | ");
                    foreach (var o in queue.Queue())
                    {
                        sb.Append($"| {o.GetType().Name}({o.ToString()}) ");
                    }
                    sb.Replace('\n', ' ');
                    sb.Append($"||\n ");
                    Debug.Write(sb.ToString());
                    qnum++;
                    LineDelim();
                }
                LineDelim();

            }
        }


#endif

        private readonly List<IQueue> repositories;
        private readonly Dictionary<IConnectivitySource, List<IDataSource>> connectedSources;

        public ManagerEventsToQueue(ISourceManager sourceManager, IEnumerable<IQueue> repositories = null)
        {
            SourceManager = sourceManager ?? throw new ArgumentNullException(nameof(sourceManager));
            this.repositories = repositories != null ? new List<IQueue>(repositories) : new List<IQueue>();
#if DEBUG
            lock (_synchro)
            {
                instanceNumber = _instances++;
                _queues.Add(this, this.repositories);
            }
#endif
            connectedSources = new Dictionary<IConnectivitySource, List<IDataSource>>();
            sourceManager.SourceConnected += SourceConnected;
            sourceManager.SourceDisconnected += SourceDisconnected;
            foreach (var sources in sourceManager.ConnectedSources)
            {
                SourceConnected(sourceManager, sources);
            }
        }

        private void SourceConnected(object sender, IConnectivitySource source)
        {
            if (connectedSources.ContainsKey(source))
            {
                return;
            }

            source.SourceConnected += DataSourceConnected;
            source.SourceDisconnected += DataSourceDisconnected;
            connectedSources.Add(source, new List<IDataSource>());
            ConnectionChanged(source, null, true);

            foreach (var connectedDataSource in source.ConnectedSources)
            {
                DataSourceConnected(source, connectedDataSource);
            }

            if (!(sender is IModbusSourceManager))
            {
                return;
            }
            if (!(source is IModbusConnectivitySource modbusCSource))
            {
                return;
            }
            modbusCSource.SourceActivated += DataSourceActivated;
            modbusCSource.SourceDeactivated += DataSourceDeactivated;
            foreach (var connectedDataSource in modbusCSource.ActiveSources)
            {
                DataSourceActivated(modbusCSource, connectedDataSource);
            }
        }

        private void SourceDisconnected(object sender, IConnectivitySource source)
        {
            if (!connectedSources.ContainsKey(source))
            {
                return;
            }
            source.SourceConnected -= DataSourceConnected;
            source.SourceDisconnected -= DataSourceDisconnected;

            foreach (var connectedDataSource in connectedSources[source].ToList())
            {
                DataSourceDisconnected(source, connectedDataSource);
            }
            connectedSources.Remove(source);
            ConnectionChanged(source, null, false);

            if (!(sender is IModbusSourceManager))
            {
                return;
            }
            if (!(source is IModbusConnectivitySource modbusCSource))
            {
                return;
            }
            modbusCSource.SourceActivated -= DataSourceActivated;
            modbusCSource.SourceDeactivated -= DataSourceDeactivated;
            foreach (var connectedDataSource in modbusCSource.ActiveSources)
            {
                DataSourceDeactivated(modbusCSource, connectedDataSource);
            }
        }

        private void DataSourceConnected(object sender, IDataSource e)
        {
            if (!(sender is IConnectivitySource connectedSource) || !connectedSources.ContainsKey(connectedSource))
            {
                return;
            }
            connectedSources[connectedSource].Add(e);
            e.NewSourceData += Action;
            ConnectionChanged(connectedSource, e, true);
        }

        private void DataSourceDisconnected(object sender, IDataSource e)
        {
            if (!(sender is IConnectivitySource connectedSource) || !connectedSources.ContainsKey(connectedSource))
            {
                return;
            }
            e.NewSourceData -= Action;
            connectedSources[connectedSource].Remove(e);
            ConnectionChanged(connectedSource, e, false);
        }

        private void ConnectionChanged(IConnectivitySource source, IDataSource dataSource, bool connected)
        {
            if (source == null)
            {
                return;
            }
            var connectivityInfo = source.CreateSourceConnectivityInfo(dataSource, connected);
            Action(source, connectivityInfo);
        }


        private void DataSourceActivated(object sender, IModbusDataSource dataSource)
        {
            if (!(sender is IModbusConnectivitySource connectedSource) || !connectedSources.ContainsKey(connectedSource))
            {
                return;
            }
            ActivationChanged(connectedSource, dataSource, true);
        }

        private void DataSourceDeactivated(object sender, IModbusDataSource e)
        {
            if (!(sender is IModbusConnectivitySource connectedSource) || !connectedSources.ContainsKey(connectedSource))
            {
                return;
            }
            ActivationChanged(connectedSource, e, false);
        }

        private void ActivationChanged(IConnectivitySource source, IModbusDataSource dataSource, bool activated)
        {
            if (source == null || dataSource == null)
            {
                return;
            }
            var activityInfo = source.CreateSourceActivityInfo(dataSource, activated);
            Action(source, activityInfo);
        }

        private void Action(object sender, object e)
        {
            foreach (var repository in repositories)
            {
                repository.Enqueue(e);
            }
#if DEBUG
            PrintQueues();
#endif
        }

        /// <summary>
        ///    Register repository to receive data from source manager
        /// </summary>
        /// <param name="repository"></param>
        public void Add(IQueue repository)
        {
            if (repository == null || repositories.Contains(repository))
            {
                return;
            }
            repositories.Add(repository);
#if DEBUG
            PrintQueues();
#endif
        }

        /// <summary>
        ///    Unregister repository
        /// </summary>
        /// <param name="repository"></param>
        public void Remove(IQueue repository)
        {
            if (repository == null || !repositories.Contains(repository))
            {
                return;
            }
            repositories.Remove(repository);
#if DEBUG
            PrintQueues();
#endif
        }

        #region Implementation of IDisposable

        /// <summary>
        ///    Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            foreach (var connectedSource in connectedSources.Keys.ToList())
            {
                SourceDisconnected(SourceManager, connectedSource);
            }
            SourceManager.SourceConnected -= SourceConnected;
            SourceManager.SourceDisconnected -= SourceDisconnected;

#if DEBUG
            Debug.Write($"ManagerEventsToQueue disposed instance {instanceNumber}");
            lock (_synchro)
            {
                _queues.Remove(this);
            }
            PrintQueues();
            GC.SuppressFinalize(this);
#endif
        }

        #endregion


        /// <summary>
        ///    Data source manager
        /// </summary>
        public ISourceManager SourceManager { get; }

        /// <summary>
        /// Return list of all registered repositories
        /// </summary>
        public IEnumerable<IQueue> GetRepositories()
        {
            return repositories.ToList();
        }
    }
}
