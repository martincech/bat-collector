﻿namespace Communication.EventToQueue
{
    public class SourceActivityInfo
    {
        public string Port { get; set; }
        public DeviceActivityInfo Device { get; set; }
    }
}
