﻿namespace Communication.EventToQueue
{
    public class DeviceActivityInfo
    {
        public string DeviceId { get; set; }
        public bool Active { get; set; }
    }
}
