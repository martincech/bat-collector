﻿using Communication.DataSource;

namespace Communication.Connectivity
{
    public interface IConnectivitySource : IConnectivitySourcePattern<IDataSource>
    {
        string Id { get; }
        SourceType Type { get; }
    }
}
