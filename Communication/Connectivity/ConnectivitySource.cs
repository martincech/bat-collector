﻿using System;
using System.Collections.Generic;
using Communication.DataSource;

namespace Communication.Connectivity
{
    public abstract class ConnectivitySource : IConnectivitySource
    {
        private readonly SourceType type;
        private readonly List<IDataSource> connectedDataSources;

        public SourceType Type { get { return type; } }

        public string Id { get; private set; }

        protected ConnectivitySource(string id, SourceType type)
        {
            this.type = type;
            connectedDataSources = new List<IDataSource>();
            Id = id;
        }

        #region Implementation of IDataSourceConnectivity

        public event EventHandler<IDataSource> SourceConnected;
        public event EventHandler<IDataSource> SourceDisconnected;

        public IEnumerable<IDataSource> ConnectedSources
        {
            get { return connectedDataSources; }
        }



        #endregion

        protected virtual void OnSourceConnected(IDataSource e)
        {
            if (connectedDataSources.Contains(e))
            {
                return;
            }

            connectedDataSources.Add(e);
            SourceConnected?.Invoke(this, e);
        }

        protected virtual void OnSourceDisconnected(IDataSource e)
        {
            if (!connectedDataSources.Contains(e))
            {
                return;
            }
            connectedDataSources.Remove(e);
            SourceDisconnected?.Invoke(this, e);
        }


    }
}
