﻿namespace Communication.Connectivity.Modbus.LineRequest
{
    public abstract class ModbusRequest
    {
        public byte Address { get; set; }
    }
}
