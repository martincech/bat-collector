﻿namespace Communication.Connectivity.Modbus.LineRequest
{
    public class ModbusScanRequest
    {
        public byte AddressFrom { get; set; }
        public byte AddressTo { get; set; }
        public bool ActivateFoundDevices { get; set; }
    }
}
