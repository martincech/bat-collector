﻿using System.Collections.Generic;

namespace Communication.Connectivity.Modbus.LineRequest
{
    public class ModbusDeviceRequest : ModbusRequest
    {
        public ModbusDeviceRequest(object parameter = null)
            : this(parameter != null ? new List<object> { parameter } : null)
        {
        }

        public ModbusDeviceRequest(IEnumerable<object> parameters)
        {
            Parameters = parameters;
        }

        public IEnumerable<object> Parameters { get; }

        public Bat2Request Type { get; set; }
        public Bat2RequestMethod Method { get; set; }
    }
}
