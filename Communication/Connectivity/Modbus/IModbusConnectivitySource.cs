﻿using System;
using System.Collections.Generic;
using Communication.Connectivity.Modbus.LineRequest;
using Communication.DataSource.Modbus;

namespace Communication.Connectivity.Modbus
{
    public interface IModbusConnectivitySource : IConnectivitySource
    {
        void ScanForDevices(ModbusScanRequest modbusScanRequest, Action progressCallback);
        void StopScanForDevices();

        event EventHandler<IModbusDataSource> SourceActivated;
        event EventHandler<IModbusDataSource> SourceDeactivated;
        IEnumerable<IModbusDataSource> ActiveSources { get; }

        void ActivateSource(IModbusDataSource device);
        void DeactivateSource(IModbusDataSource device);
    }
}
