﻿namespace Communication.Connectivity.Modbus
{
    public class ModbusResponse
    {
        public ModbusResponse(Bat2Request type, object data)
        {
            Type = type;
            Data = data;
        }

        public Bat2Request Type { get; }
        public object Data { get; }
    }
}
