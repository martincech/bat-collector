﻿namespace Communication.Connectivity.Modbus
{
    public enum Bat2Request
    {
        Nop = 0,
        Version,
        ScaleName,
        Configuration,
        SavingParameters,
        Flock,
        StartWeighing,
        StopWeighing,
        PauseWeighing,
        WeighingStatus,
        WeighingParameters
    }
}
