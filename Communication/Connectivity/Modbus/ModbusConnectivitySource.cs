﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity.Modbus.LineRequest;
using Communication.DataSource;
using Communication.DataSource.Modbus;
using NLog;
using Settings.DeviceSettings;
using Veit.Mqueue;

namespace Communication.Connectivity.Modbus
{
    public sealed class ModbusConnectivitySource
      : ConnectivitySource,
         IModbusConnectivitySource, IDisposable
    {
        #region Private fields

        private readonly IProcessQueue modbusProcessQueue;
        private readonly List<Func<byte, IModbusDataSource>> supportedDeviceFactories;

        public event EventHandler<IModbusDataSource> SourceActivated;
        public event EventHandler<IModbusDataSource> SourceDeactivated;
        private readonly List<IModbusDataSource> activeDataSources;
        private Action progressCallback;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly object lockObj = new object();

        #endregion

        public ModbusConnectivitySource(IProcessQueue modbusProcessQueue,
           string descriptor,
           SourceType sourceType = SourceType.Ethernet)
           : base(descriptor, sourceType)
        {
            activeDataSources = new List<IModbusDataSource>();
            this.modbusProcessQueue = modbusProcessQueue;
            modbusProcessQueue.AddProcessing<ModbusScanRequest>(ScanForDevicesReq);
            supportedDeviceFactories = new List<Func<byte, IModbusDataSource>>();
            SupportedDataSources = new List<Type>();
        }

        public void ScanForDevices(ModbusScanRequest modbusScanRequest, Action progressCallback)
        {
            lock (lockObj)
            {
                this.progressCallback = progressCallback;
            }
            modbusProcessQueue.Enqueue(modbusScanRequest, Priority.High);
        }

        public void StopScanForDevices()
        {
            ((ProcessQueue)modbusProcessQueue).RemoveObjectsOfTypeFromQueue<ModbusScanRequest>();
        }

        public void AddSupportedDataSource<T>(
           Func<byte, T> deviceFactory) where T : IDataSource
        {
            if (typeof(T) == typeof(IDataSource))
            {
                throw new ArgumentException("T has to be specified as concrete type of IDataSource");
            }
            if (SupportedDataSources.Contains(typeof(T)))
            {
                throw new ArgumentException("Source already registered");
            }

            supportedDeviceFactories.Add(deviceFactory as Func<byte, IModbusDataSource>);
            SupportedDataSources.Add(typeof(T));
        }

        public List<Type> SupportedDataSources { get; private set; }

        private bool ScanForDevicesReq(ModbusScanRequest scanParams)
        {
            try
            {
                var activeSources = ActiveSources.ToList();
                // "i" has to be int, not byte otherwise incrementing of "i" will cause overflow back to zero
                for (int i = scanParams.AddressFrom; i <= scanParams.AddressTo; i++)
                {
                    ScanAddress(scanParams, i);
                }
                ActivateSources(activeSources);
                return true;
            }
            catch (Exception e)
            {
                logger.Warn(e, "ScanForDevicesReq Exception");
                return false;
            }
        }

        private void ScanAddress(ModbusScanRequest scanParams, int address)
        {
            logger.Debug($"Scan address: {address} for: {Id}");
            foreach (var supportedDeviceFactory in supportedDeviceFactories)
            {
                if (ConnectedSources.Cast<IModbusDataSource>().Any(d => d.Address == address))
                {
                    OnSourceDisconnected(ConnectedSources.Cast<IModbusDataSource>().First(d => d.Address == address));
                }
                var newDevice = supportedDeviceFactory((byte)address);
                if (newDevice == null)
                {
                    continue;
                }
                OnSourceConnected(newDevice);
                if (scanParams.ActivateFoundDevices)
                {
                    ActivateSource(newDevice);
                }
                break;
            }
            lock (lockObj)
            {
                progressCallback?.Invoke();
            }
        }

        private void ActivateSources(IEnumerable<IModbusDataSource> activeSources)
        {
            foreach (var activeSource in activeSources)
            {
                var cs = ConnectedSources.Cast<IModbusDataSource>().FirstOrDefault(s => Equals(s, activeSource));
                if (cs != null)
                {
                    ActivateSource(activeSource);
                }
            }
        }


        public IEnumerable<IModbusDataSource> ActiveSources
        {
            get { return activeDataSources; }
        }

        public void ActivateSource(IModbusDataSource device)
        {
            if (activeDataSources == null || activeDataSources.Contains(device) || !ConnectedSources.Contains(device))
            {
                return;
            }
            activeDataSources.Add(device);
            device.Activate();
            device.CommunicationOkStatusChanged += DeviceOnCommunicationOkStatusChanged;
            OnSourceActivated(device);
        }

        private void DeviceOnCommunicationOkStatusChanged(object sender, bool b)
        {
            if (!(sender is IModbusDataSource))
            {
                return;
            }
            if (b)
            {
                OnSourceActivated((IModbusDataSource)sender);
            }
            else
            {
                OnSourceDeactivated((IModbusDataSource)sender);
            }
        }

        public void DeactivateSource(IModbusDataSource device)
        {
            if (!activeDataSources.Contains(device))
            {
                return;
            }
            device.Deactivate();
            device.CommunicationOkStatusChanged -= DeviceOnCommunicationOkStatusChanged;
            activeDataSources.Remove(device);
            OnSourceDeactivated(device);
        }

        private void OnSourceActivated(IModbusDataSource e)
        {
            SourceActivated?.Invoke(this, e);
        }

        private void OnSourceDeactivated(IModbusDataSource e)
        {
            SourceDeactivated?.Invoke(this, e);
        }

        protected override void OnSourceDisconnected(IDataSource e)
        {
            if (e is IModbusDataSource && activeDataSources.Contains(e))
            {
                DeactivateSource((IModbusDataSource)e);
            }
            base.OnSourceDisconnected(e);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            foreach (var connectedSource in ConnectedSources.Where(s => s is IModbusDataSource).Cast<IModbusDataSource>())
            {
                connectedSource.Dispose();
            }

            GC.SuppressFinalize(this);
        }

        public void AddActiveDevices(List<DeviceSettings> activeDevices, bool checkIfRequestalreadyExist = false)
        {
            if (activeDevices != null)
            {
                foreach (var activeDevice in activeDevices)
                {
                    if (activeDevice.Uid.ParseAddressFromUid(out var addr))
                    {
                        var request = new ModbusScanRequest
                        {
                            AddressFrom = addr,
                            AddressTo = addr,
                            ActivateFoundDevices = true
                        };

                        if (checkIfRequestalreadyExist && IsRequestAlreadyInProcessQueue(request))
                        {
                            continue;
                        }
                        ScanForDevices(request, null);
                    }
                }
            }
        }


        /// <summary>
        /// Check if the same request is already in queue
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool IsRequestAlreadyInProcessQueue(ModbusScanRequest request)
        {
            var queue = modbusProcessQueue.Queue();
            var existReq = queue.Where(w => w is ModbusScanRequest)
               .FirstOrDefault(f => ((ModbusScanRequest)f).AddressFrom == request.AddressFrom &&
                                    ((ModbusScanRequest)f).AddressTo == request.AddressTo);

            return existReq != null;
        }
    }
}
