﻿using System;

namespace Communication.Connectivity
{
    public enum ModbusLineType
    {
        [Obsolete("ELO modbus line is no longer supported in Collector. Use Ethernet instead.")]
        Elo = SourceType.Elo,
        [Obsolete("COM modbus line is no longer supported in Collector. Use Ethernet instead.")]
        Com = SourceType.Com,
        Ethernet = SourceType.Ethernet
    }
}
