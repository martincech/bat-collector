﻿using System;

namespace Communication.Connectivity
{
    public enum SourceType
    {
        None,
        External,
        [Obsolete("Modem device is no longer supported in Collector. Instead of modem will be used Veit Gateway")]
        Modem,
        [Obsolete("ELO device is no longer supported in Collector. Use Ethernet source instead.")]
        Elo,
        [Obsolete("COM device is no longer supported in Collector. Use Ethernet source instead.")]
        Com,
        Ethernet
    }
}
