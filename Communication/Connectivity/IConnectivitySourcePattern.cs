﻿using System;
using System.Collections.Generic;

namespace Communication.Connectivity
{
    public interface IConnectivitySourcePattern<T>
    {
        event EventHandler<T> SourceConnected;
        event EventHandler<T> SourceDisconnected;

        IEnumerable<T> ConnectedSources { get; }
    }
}
