﻿using System;
using System.Text.RegularExpressions;

namespace Communication.DataSource
{
    public static class DataSourceExtensions
    {
        private static readonly Regex uidRegex = new Regex(@"([^\s]+)-(\d{1,3})");
        private static readonly Regex oldTerminalUidRegex = new Regex(@"([^\s]+)-T-([^\s]+)");


        public static string FormatIdOfDataSource(byte address, string basePort)
        {
            return $"{basePort}-{address}";
        }

        public static bool ParseAddressFromUid(this string uid, out byte address)
        {
            var match = uidRegex.Match(uid);
            return byte.TryParse(match.Groups[2].ToString(), out address);
        }

        public static bool ParseSourceFromUid(this string uid, out string source)
        {
            var match = uidRegex.Match(uid);
            source = match.Groups[1].ToString();
            return !string.IsNullOrEmpty(source);
        }

        public static string CreateTerminalUid(string sensorUid, Guid terminalId)
        {
            return $"{sensorUid}-T-{terminalId}";
        }

        public static bool TryParseSensorUidFromTerminalUid(string terminalUid, ref string sensorUid)
        {
            var match = oldTerminalUidRegex.Match(terminalUid);

            if (match.Groups.Count < 2)
            {
                return false;
            }
            sensorUid = match.Groups[1].ToString();
            return true;
        }
    }
}
