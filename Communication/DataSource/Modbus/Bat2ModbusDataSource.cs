﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity.Modbus;
using Communication.Connectivity.Modbus.LineRequest;
using NLog;
using Settings.DeviceSettings;
using Utilities.Timer;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Bat.Common.Sample.Weight;
using Veit.Bat.Common.Units;
using Veit.Bat.Modbus.Bat2Scale.Registers;
using Veit.Bat.Modbus.Bat2Scale.Registers.Enums;
using Veit.Bat.Modbus.Bat2Scale.Registers.Types;
using Veit.Mqueue;

namespace Communication.DataSource.Modbus
{
    public sealed class Bat2ModbusDataSource : DataSource, IModbusDataSource
    {
        private readonly IBat2ModbusRegisterRw registerDecoder;
        private readonly IProcessQueue queue;
        private readonly Guid terminalId;
        private DateTimeOffset? lastRead;
        private readonly Func<ITimer> timerFactory;
        private readonly Dictionary<Type, ITimer> repeatableRequests;
        private readonly TimeSpan firstShortTimerMs = TimeSpan.FromSeconds(10);
        private readonly TimeSpan readStatsTimerMs =
#if DEBUG
        TimeSpan.FromSeconds(15);
#else
        TimeSpan.FromMinutes(15);
#endif
        private bool communicationOk = true;
        private readonly TimeSpan firstShortReadAllStatsTimerMs =
#if DEBUG
        TimeSpan.FromMinutes(1);
#else
        TimeSpan.FromMinutes(5);
#endif

        private readonly TimeSpan timeForReadingAllStatistics = new TimeSpan(0, 2, 0, 0);
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public Bat2ModbusDataSource(
           IBat2ModbusRegisterRw registerDecoder,
           IProcessQueue queue,
           byte address,
           Guid terminalId,
           string connectionPortName,
           DateTimeOffset? lastRead,
           Func<ITimer> timerFactory = null)
           : base(
                 DataSourceExtensions.FormatIdOfDataSource(address, connectionPortName),
                 DeviceType.Bat2Cable)
        {
            this.registerDecoder = registerDecoder;
            this.queue = queue;
            Address = address;
            this.terminalId = terminalId;
            SetLastRead(lastRead);
            this.timerFactory = timerFactory;
            if (timerFactory == null)
            {
                this.timerFactory = TimerAdapter.Factory;
            }
            repeatableRequests = new Dictionary<Type, ITimer>();
            queue.AddProcessing<ModbusReadSamplesRequest>(ProcessRequest);
            queue.AddProcessing<ModbusReadStatisticsRequest>(ProcessRequest);
            queue.AddProcessing<ModbusReadAllStatisticsRequest>(ProcessRequest);
            queue.AddProcessing<ModbusDeviceRequest>(ProcessDeviceRequest);
        }

        public event EventHandler<ModbusResponse> ResponseRead;

        private void SetLastRead(DateTimeOffset? date)
        {
            if (!date.HasValue) return;
            lastRead = new DateTimeOffset(date.Value.Year, date.Value.Month, date.Value.Day, date.Value.Hour, date.Value.Minute, 1, date.Value.Offset);
            lastRead = lastRead.Value.AddMinutes(1);
        }

        public byte Address { get; }

        public void GetVersion()
        {
            CreateRequest(Bat2RequestMethod.Get, Bat2Request.Version, Priority.High);
        }

        public void GetScaleName()
        {
            CreateRequest(Bat2RequestMethod.Get, Bat2Request.ScaleName, Priority.High);
        }

        public void SetScaleName(string name)
        {
            CreateRequest(Bat2RequestMethod.Set, Bat2Request.ScaleName, Priority.High, new List<object> { name });
        }

        public void GetConfiguration()
        {
            CreateRequest(Bat2RequestMethod.Get, Bat2Request.Configuration, Priority.High);
        }

        public void GetSavingParameters()
        {
            CreateRequest(Bat2RequestMethod.Get, Bat2Request.SavingParameters);
        }

        public void SetSavingParameters(SavingParameters parameters)
        {
            CreateRequest(Bat2RequestMethod.Set, Bat2Request.SavingParameters, Priority.Normal, new List<object> { parameters });
        }

        public void GetFlock(byte flockNumber)
        {
            CreateRequest(Bat2RequestMethod.Get, Bat2Request.Flock, Priority.Normal, new List<object> { flockNumber });
        }

        public void SetFlock(Flock flock)
        {
            CreateRequest(Bat2RequestMethod.Set, Bat2Request.Flock, Priority.Normal, new List<object> { flock });
        }

        public void StartWeighing(Flock flock, WeighingParameters parameters)
        {
            CreateRequest(Bat2RequestMethod.Set, Bat2Request.StartWeighing, Priority.Normal, new List<object> { flock, parameters });
        }

        public void StopWeighing()
        {
            CreateRequest(Bat2RequestMethod.Set, Bat2Request.StopWeighing);
        }

        public void PauseWeighing(bool pause)
        {
            CreateRequest(Bat2RequestMethod.Set, Bat2Request.PauseWeighing, Priority.Normal, new List<object> { pause });
        }

        public void GetWeighingStatus()
        {
            CreateRequest(Bat2RequestMethod.Get, Bat2Request.WeighingStatus);
        }

        public void GetWeighingParameters()
        {
            CreateRequest(Bat2RequestMethod.Get, Bat2Request.WeighingParameters);
        }

        private void CreateRequest(Bat2RequestMethod method,
                                   Bat2Request requestType,
                                   Priority priority = Priority.Normal,
                                   IEnumerable<object> parameters = null)
        {
            queue.Enqueue(new ModbusDeviceRequest(parameters)
            {
                Address = Address,
                Method = method,
                Type = requestType
            },
            priority);
        }

        private bool ProcessRequest(ModbusRequest request)
        {
            try
            {
                if (request.Address != Address)
                {
                    return true;
                }
                if (request is ModbusReadSamplesRequest)
                {
                    return ReadWeights();
                }
                if (request is ModbusReadStatisticsRequest)
                {
                    return ReadTodayStatistics();
                }
                if (request is ModbusReadAllStatisticsRequest)
                {
                    return ReadAllStatistics();
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Warn(e, $"Perform of modbus request failed! Device: {Id}");
                return false;
            }
        }

        private bool ProcessDeviceRequest(ModbusDeviceRequest request)
        {
            try
            {
                if (request.Address != Address)
                {
                    return true;
                }

                object data = null;
                switch (request.Type)
                {
                    case Bat2Request.Version:
                        data = RequestVersion(request.Method);
                        break;
                    case Bat2Request.ScaleName:
                        data = RequestScaleName(request.Method, request.Parameters);
                        break;
                    case Bat2Request.Configuration:
                        data = RequestConfiguration(request.Method);
                        break;
                    case Bat2Request.SavingParameters:
                        data = RequestSavingParameters(request.Method, request.Parameters);
                        break;
                    case Bat2Request.Flock:
                        data = RequestFlock(request.Method, request.Parameters);
                        break;
                    case Bat2Request.StartWeighing:
                        data = RequestStartWeighing(request.Method, request.Parameters);
                        break;
                    case Bat2Request.StopWeighing:
                        data = RequestStopWeighing(request.Method);
                        break;
                    case Bat2Request.PauseWeighing:
                        data = RequestPauseWeighing(request.Method, request.Parameters);
                        break;
                    case Bat2Request.WeighingStatus:
                        data = RequestWeighingStatus(request.Method);
                        break;
                    case Bat2Request.WeighingParameters:
                        data = RequestWeighingParameters(request.Method);
                        break;
                    default:
                        break;
                }

                InvokeResponseRead(request.Type, data);
                return true;
            }
            catch (Exception e)
            {
                logger.Warn(e, $"Perform of modbus DEVICE request failed! Device: {Id}");
                return false;
            }
        }

        private object RequestVersion(Bat2RequestMethod method)
        {
            if (method != Bat2RequestMethod.Get)
            {
                return null;
            }
            return registerDecoder.Version;
        }

        private object RequestScaleName(Bat2RequestMethod method, IEnumerable<object> parameters)
        {
            if (method == Bat2RequestMethod.Get)
            {
                var name = string.Empty;
                if (registerDecoder.ReadDeviceName())
                {
                    name = registerDecoder.ScaleName;
                }
                return name;
            }
            else if (method == Bat2RequestMethod.Set)
            {
                var name = parameters.FirstOrDefault() as string;
                if (string.IsNullOrEmpty(name))
                {
                    throw new ArgumentException("Write device name. Invalid parameter name");
                }
                registerDecoder.WriteDeviceName(name);
            }
            return null;
        }

        private object RequestConfiguration(Bat2RequestMethod method)
        {
            if (method != Bat2RequestMethod.Get)
            {
                return null;
            }
            return !registerDecoder.ReadConfig() ? null : registerDecoder.Configuration;
        }

        private object RequestSavingParameters(Bat2RequestMethod method, IEnumerable<object> parameters)
        {
            if (method == Bat2RequestMethod.Get)
            {
                return registerDecoder.ReadSavingParameters();
            }
            else if (method == Bat2RequestMethod.Set)
            {
                if (!(parameters.FirstOrDefault() is SavingParameters savingParams))
                {
                    throw new ArgumentException("Write saving parameters. Invalid parameter.");
                }
                registerDecoder.WriteSavingParameters(savingParams);
            }
            return null;
        }

        private object RequestFlock(Bat2RequestMethod method, IEnumerable<object> parameters)
        {
            if (method == Bat2RequestMethod.Get)
            {
                if (!(parameters.FirstOrDefault() is byte flockNumber))
                {
                    throw new ArgumentException("Get flock. Invalid flock number.");
                }
                return registerDecoder.ReadFlock(flockNumber) ? registerDecoder.GetFlocks()[flockNumber] : null;
            }
            else if (method == Bat2RequestMethod.Set)
            {
                if (!(parameters.FirstOrDefault() is Flock flock))
                {
                    throw new ArgumentException("Write flock. Invalid parameter.");
                }
                registerDecoder.WriteFlock(flock);
            }
            return null;
        }

        private object RequestStartWeighing(Bat2RequestMethod method, IEnumerable<object> parameters)
        {
            if (method != Bat2RequestMethod.Set)
            {
                return null;
            }

            if (parameters.Count() != 2)
            {
                throw new ArgumentException($"Start weighing. Invalid parameters count. Device: {Id}");
            }
            if (!(parameters.ElementAt(0) is Flock flock))
            {
                throw new ArgumentException($"Start weighing. Invalid parameter flock. Device: {Id}");
            }
            if (!(parameters.ElementAt(1) is WeighingParameters weighingParameters))
            {
                throw new ArgumentException($"Start weighing. Invalid weighing parameters. Device: {Id}");
            }

            WriteCurrentDateTimeToScale();
            if (!registerDecoder.WriteWeighingParams(flock, weighingParameters) || !registerDecoder.Start())
            {
                logger.Info($"Start weighing FAILED! Device: {Id}");
            }
            return null;
        }

        private object RequestStopWeighing(Bat2RequestMethod method)
        {
            if (method != Bat2RequestMethod.Set)
            {
                return null;
            }
            if (!registerDecoder.Stop())
            {
                logger.Info($"Stop weighing FAILED! Device: {Id}");
            }
            return null;
        }

        private object RequestPauseWeighing(Bat2RequestMethod method, IEnumerable<object> parameters)
        {
            if (method != Bat2RequestMethod.Set)
            {
                return null;
            }

            if (!(parameters.FirstOrDefault() is bool pause))
            {
                throw new ArgumentException($"Pause weighing. Invalid parameter pause. Device: {Id}");
            }
            if (pause)
            {
                if (!registerDecoder.Pause())
                {
                    logger.Info($"Pause weighing FAILED! Device: {Id}");
                }
            }
            else
            {
                if (!registerDecoder.Unpause())
                {
                    logger.Info($"Unpause weighing FAILED! Device: {Id}");
                }
            }
            return null;
        }

        private object RequestWeighingStatus(Bat2RequestMethod method)
        {
            if (method != Bat2RequestMethod.Get)
            {
                return null;
            }
            return registerDecoder.GetWeighingStatus();
        }

        private object RequestWeighingParameters(Bat2RequestMethod method)
        {
            if (method != Bat2RequestMethod.Get)
            {
                return null;
            }
            return registerDecoder.ReadWeighingParameters();
        }

        private void InvokeResponseRead(Bat2Request request, object data)
        {
            if (data == null)
            {
                return;
            }
            ResponseRead?.Invoke(this, new ModbusResponse(request, data));
        }

        private void WriteCurrentDateTimeToScale()
        {
            var currnetDateTime = DateTime.Now;
            registerDecoder.WriteDateTime((ushort)currnetDateTime.Day, (ushort)currnetDateTime.Month, (ushort)currnetDateTime.Year,
                                            (ushort)currnetDateTime.Hour, (ushort)currnetDateTime.Minute);
        }

        private bool ReadTodayStatistics()
        {
            if (!registerDecoder.ReadTodayStatistics())
            {
                OnCommunicationOkStatusChanged(false);
                return false;
            }
            OnCommunicationOkStatusChanged(true);
            InvokeDataRead(registerDecoder.Statistics);
            return true;
        }

        private void InvokeDataRead(StatisticSample sample)
        {
            if (sample == null)
            {
                return;
            }

            sample.SensorUid = Id;
            sample.ScaleNumber = Address;
            sample.PhoneNumber = DataSourceExtensions.CreateTerminalUid(sample.SensorUid, terminalId);
            DataRead(sample);
        }

        private bool ReadAllStatistics()
        {
            logger.Info($"Read all statistics START for: {Id}");

            registerDecoder.ReadConfig();
            var statistics = registerDecoder.ReadAllStatistics();
            if (statistics == null)
            {
                OnCommunicationOkStatusChanged(false);
                logger.Info($"Read all statistics FAILED for: {Id}");
                return false;
            }
            OnCommunicationOkStatusChanged(true);
            if (!statistics.Any())
            {
                logger.Info($"Read all statistics FINISHED for: {Id}, NO STATISTICS at scale!");
                return true;
            }

            SendWeighingInfo(statistics.FirstOrDefault(), ConvertUnits(registerDecoder.Configuration.WeightUnit));
            foreach (var stat in statistics)
            {
                InvokeDataRead(stat);
            }
            logger.Info($"Read all statistics FINISHED for: {Id}");
            return true;
        }

        private static WeightUnits ConvertUnits(WeightUnit model)
        {
            switch (model)
            {
                case WeightUnit.Lb:
                    return WeightUnits.LB;
                case WeightUnit.Kg:
                default:
                    return WeightUnits.KG;
            }
        }

        private void SendWeighingInfo(StatisticSample sample, WeightUnits unit)
        {
            DataRead(new WeighingInfoSample
            {
                Start = sample.TimeStamp,
                SensorUid = Id,
                TimeStamp = DateTimeOffset.Now,
                Unit = unit
            });
        }

        private bool ReadWeights()
        {
            if (!registerDecoder.ReadLastBirdWeights())
            {
                OnCommunicationOkStatusChanged(false);
                return false;
            }
            OnCommunicationOkStatusChanged(true);
            foreach (var birdWeight in registerDecoder.BirdWeights)
            {
                birdWeight.SensorUid = Id;
                if (IsNewWeight(birdWeight))
                {
                    DataRead(birdWeight);
                }
                else
                {
                    logger.Debug($"Discard old sample : {birdWeight}");
                }
            }
            return true;
        }

        private bool IsNewWeight(BirdWeight weight)
        {
            return lastRead == null || lastRead < weight.TimeStamp;
        }

        public static bool IsConnected(IBat2ModbusRegisterRw registerDecoder)
        {
            return registerDecoder.ReadConfig() && registerDecoder.VersionValid;
        }


        /// <inheritdoc />
        public event EventHandler<bool> CommunicationOkStatusChanged;

        /// <inheritdoc />
        public void Activate()
        {
            CreateRepeatableRequest<ModbusReadStatisticsRequest>(firstShortTimerMs, () => readStatsTimerMs, Priority.Low);
            CreateRepeatableRequest<ModbusReadAllStatisticsRequest>(firstShortReadAllStatsTimerMs, GetTimeSpanForNextReadingOfAllStatistics, Priority.Low);
        }

        private TimeSpan GetTimeSpanForNextReadingOfAllStatistics()
        {
            var nextEventDate = DateTimeOffset.Now.Date.AddDays(1).Add(timeForReadingAllStatistics);
            var nextTimeSpan = nextEventDate - DateTimeOffset.Now;
            return nextTimeSpan;
        }

        private void CreateRepeatableRequest<T>(TimeSpan firstInterval, Func<TimeSpan> getCommonInterval, Priority priority)
           where T : ModbusRequest, new()
        {
            if (repeatableRequests.ContainsKey(typeof(T)))
            {
                return;
            }
            var timer = timerFactory();
            timer.Elapsed += (sender, args) =>
            {
                timer.Interval = getCommonInterval.Invoke().TotalMilliseconds;
                queue.Enqueue(new T { Address = Address }, priority);
            };
            timer.AutoReset = true;
            timer.Interval = firstInterval.TotalMilliseconds;
            timer.Start();
            repeatableRequests.Add(typeof(T), timer);
        }



        public void Deactivate()
        {
            StopRepeatableRequest<ModbusReadSamplesRequest>();
            StopRepeatableRequest<ModbusReadStatisticsRequest>();
            StopRepeatableRequest<ModbusReadAllStatisticsRequest>();

            queue.RemoveProcessing<ModbusDeviceRequest>(ProcessDeviceRequest);
        }

        private void StopRepeatableRequest<T>()
           where T : ModbusRequest
        {
            var type = typeof(T);
            if (!repeatableRequests.ContainsKey(type))
            {
                return;
            }
            repeatableRequests[type].Stop();
            repeatableRequests.Remove(type);
        }


        public void Dispose()
        {
            Deactivate();
        }

        private void OnCommunicationOkStatusChanged(bool e)
        {
            if (communicationOk == e)
            {
                return;
            }
            logger.Debug($"OnCommunicationOkStatusChanged: {e} for: '{Id}'");
            CommunicationOkStatusChanged?.Invoke(this, e);
            communicationOk = e;
        }
    }
}
