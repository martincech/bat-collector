﻿using System;
using Communication.Connectivity.Modbus;
using Veit.Bat.Modbus.Bat2Scale.Registers.Types;

namespace Communication.DataSource.Modbus
{
    public interface IModbusDataSource : IDataSource, IDisposable
    {
        event EventHandler<bool> CommunicationOkStatusChanged;
        void Activate();
        void Deactivate();

        byte Address { get; }


        event EventHandler<ModbusResponse> ResponseRead;

        //configuration
        void GetVersion();
        void GetScaleName();
        void SetScaleName(string name);
        void GetConfiguration();
        void SetSavingParameters(SavingParameters parameters);
        void GetSavingParameters();

        //flocks
        void GetFlock(byte flockNumber);
        void SetFlock(Flock flock);

        //weighing
        void StartWeighing(Flock flock, WeighingParameters parameters);
        void StopWeighing();
        void PauseWeighing(bool pause);
        void GetWeighingStatus();
        void GetWeighingParameters();
    }
}
