﻿using System;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Base;

namespace Communication.DataSource
{
    public interface IDataSource
    {
        event EventHandler<TimeSample> NewSourceData;

        string Id { get; }

        DeviceType Type { get; }
    }
}
