﻿using System;
using System.Collections.Generic;
using NLog;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Base;

namespace Communication.DataSource
{
    public class DataSource : IDataSource, IEquatable<DataSource>
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        public DeviceType Type { get; }
        public event EventHandler<TimeSample> NewSourceData;
        public string Id { get; private set; }

        public DataSource(string id, DeviceType type)
        {
            Type = type;
            Id = id;
        }

        public void DataRead(TimeSample data)
        {
            InvokeSampleRead(data);
        }

        public void DataRead(IEnumerable<TimeSample> data)
        {
            foreach (var d in data)
            {
                DataRead(d);
            }
        }

        #region Overrides of Object

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DataSource)obj);
        }

        #region Equality members

        public virtual bool Equals(DataSource other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return
                 (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id, StringComparison.Ordinal)
                ) &&
                (
                    Type == other.Type ||
                    Type.Equals(other.Type)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)Type * 397) ^ (Id != null ? Id.GetHashCode() : 0);
            }
        }

        public static bool operator ==(DataSource left, DataSource right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DataSource left, DataSource right)
        {
            return !Equals(left, right);
        }

        #endregion

        #endregion

        private void InvokeSampleRead(TimeSample value)
        {
            logger.Info("Value read: {1} / {0} ", value, GetType().Name);
            OnNewSourceData(value);
        }

        private void OnNewSourceData(TimeSample e)
        {
            NewSourceData?.Invoke(this, e);
        }
    }
}
