﻿using System;
using System.Collections.Generic;
using Communication.DataSource;
using Settings;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Mqueue;

namespace Repository.FileSettings
{
    public sealed class FileSettingsRepository : IDisposable
    {
        private readonly IProcessQueue processQueue;
        private readonly IManagerSettings settingManager;
        private readonly Dictionary<string, DateTimeOffset> lastReadDates;

        public FileSettingsRepository(IProcessQueue processQueue, IManagerSettings settingManager)
        {
            this.processQueue = processQueue;
            this.settingManager = settingManager;
            lastReadDates = new Dictionary<string, DateTimeOffset>();
            AddAllProcessing();
        }

        public void Dispose()
        {
            RemoveAllProcessing();
        }

        private void AddAllProcessing()
        {
            processQueue.AddProcessing<StatisticSample>(SampleRead);
            processQueue.AddProcessing<TimeStampedSample>(SampleRead);
        }

        private void RemoveAllProcessing()
        {
            processQueue.RemoveProcessing<StatisticSample>(SampleRead);
            processQueue.RemoveProcessing<TimeStampedSample>(SampleRead);
        }


        private bool SampleRead(StatisticSample sample)
        {
            //for StatisticSample is SensorUid null -> use PhoneNumber (modbus scales which send statistics have in PhoneNumber uid with terminal id - need to parse)
            var uid = sample.PhoneNumber;
            DataSourceExtensions.TryParseSensorUidFromTerminalUid(sample.PhoneNumber, ref uid);
            settingManager.UpdateDevice(uid, sample.ScaleName, sample.TimeStamp);
            return true;
        }

        private bool SampleRead(TimeStampedSample sample)
        {
            if (UpdateLastRead(sample))
            {
                settingManager.UpdateDevice(sample.SensorUid, null, sample.TimeStamp);
            }
            return true;
        }

        private bool UpdateLastRead(TimeStampedSample sample)
        {
            var hasDevice = lastReadDates.ContainsKey(sample.SensorUid);
            if (!hasDevice)
            {
                lastReadDates.Add(sample.SensorUid, sample.TimeStamp);
                return true;
            }
            var lastRead = lastReadDates[sample.SensorUid];
            if (lastRead > sample.TimeStamp) return false;
            lastReadDates[sample.SensorUid] = sample.TimeStamp;
            return true;
        }
    }
}
