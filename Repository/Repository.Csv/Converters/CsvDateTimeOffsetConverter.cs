﻿using System;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;

namespace Repository.Csv.Converters
{
    public class CsvDateTimeOffsetConverter : DateTimeOffsetConverter
    {
        public override string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            if (value is DateTimeOffset dateTime)
            {
                return dateTime.ToString("s");
            }

            return base.ConvertToString(value, row, memberMapData);
        }
    }
}
