﻿using System;
using System.Collections.Concurrent;
using System.IO;
using NLog;
using Settings.DataStorageSettings;
using Settings.DeviceSettings;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Mqueue;

namespace Repository.Csv
{
    /// <summary>
    /// Store data to CSV file (persistent storage)
    /// </summary>
    public sealed class CsvRepository : IDisposable
    {
        #region Private field

        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly LocalCsvRepositorySettings configuration;
        private const string FILE_SUFFIX = ".csv";
        private readonly IDeviceSettingsManager settingsManager;
        private static readonly object lockStatisticObj = new object();
        private readonly IProcessQueue processQueue;
        private readonly ConcurrentDictionary<string, WeighingInfoSample> weighingInfos;
        private readonly CsvFile file;

        #endregion

        #region Constructor

        public CsvRepository(CsvFile file, LocalCsvRepositorySettings configuration, IProcessQueue processQueue, IDeviceSettingsManager settingsManager)
        {
            this.configuration = configuration;
            this.settingsManager = settingsManager;
            this.processQueue = processQueue;
            this.file = file;

            if (!Directory.Exists(configuration.Csv.Path))
            {
                Directory.CreateDirectory(configuration.Csv.Path);
            }
            weighingInfos = new ConcurrentDictionary<string, WeighingInfoSample>();
            AddAllProcessing();
        }

        #endregion

        private void AddAllProcessing()
        {
            processQueue.AddProcessing<StatisticSample>(AddStatistic);
            processQueue.AddProcessing<WeighingInfoSample>(WeighingInfo);
        }

        private void RemoveAllProcessing()
        {
            processQueue.RemoveProcessing<StatisticSample>(AddStatistic);
            processQueue.RemoveProcessing<WeighingInfoSample>(WeighingInfo);
        }

        private bool WeighingInfo(WeighingInfoSample info)
        {
            weighingInfos.AddOrUpdate(info.SensorUid, info, (k, v) => info);
            logger.Debug($"CSV repository read weighing info: start: {info.Start}, unit: {info.Unit}");
            return true;
        }

        private bool AddStatistic(StatisticSample statistic)
        {
            try
            {
                var fileName = GetFileName(statistic);
                if (string.IsNullOrEmpty(fileName) || statistic == null) return true;

                var fileId = settingsManager.GetDeviceRelativeId(statistic.SensorUid);
                var fileFullName = Path.Combine(configuration.Csv.Path, fileName + fileId + FILE_SUFFIX);

                lock(lockStatisticObj)
                {
                    weighingInfos.TryGetValue(statistic.SensorUid, out var weighingInfo);
                    file.UpdateFile(fileFullName, statistic, weighingInfo);
                    logger.Debug($"CSV repository add statistic: device: '{statistic.SensorUid}', time stamp: {statistic.TimeStamp}");
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Warn(e, "Add statistic sample error");
                return false;
            }
        }


        #region Private helpers


        private string GetFileName(TimeSample type)
        {
            var dataType = type.GetDataType();
            if (configuration.SensorsWithItsFileNames.ContainsKey(dataType))
            {
                return configuration.SensorsWithItsFileNames[dataType];
            }
            return string.Empty;
        }

        public void Dispose()
        {
            RemoveAllProcessing();
        }

        #endregion
    }
}
