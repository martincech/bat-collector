﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Bat.Common.Sample.Weight;
using Veit.Bat.Common.Units;

namespace Repository.Csv.Extensions
{
    public static class MapStatistics
    {
        public static IEnumerable<Statistics> MapTo(this IEnumerable<StatisticSample> samples, Sex sex, WeightUnits unit)
        {
            return samples?.Select(s => s.MapTo(sex, unit)).Where(w => w != null);
        }

        public static Statistics MapTo(this StatisticSample sample, Sex sex, WeightUnits unit)
        {
            if (sample == null)
            {
                return null;
            }

            // time stamp and day number use direct from StatisticSample, not from sex specific StatData (there is not data for SMS or old version of scales)
            return new Statistics
            {
                DateTime = sample.TimeStamp,
                Day = sample.DayNumber,
                Sex = sex.MapTo(),
                Count = MapInteger(sex == Sex.Female ? sample.FemaleData?.Count : sample.MaleData?.Count),
                Avg = MapNumber(sex == Sex.Female ? sample.FemaleData?.Average : sample.MaleData?.Average, unit),
                Gain = MapNumber(sex == Sex.Female ? sample.FemaleData?.Gain : sample.MaleData?.Gain, unit),
                Sig = MapNumber(sex == Sex.Female ? sample.FemaleData?.Sigma : sample.MaleData?.Sigma, unit),
                Cv = MapPercent(sex == Sex.Female ? sample.FemaleData?.Cv : sample.MaleData?.Cv),
                Uni = MapPercent(sex == Sex.Female ? sample.FemaleData?.Uniformity : sample.MaleData?.Uniformity)
            };
        }


        private static int MapTo(this Sex sex)
        {
            switch (sex)
            {
                case Sex.Female:
                    return 0;
                case Sex.Male:
                    return 1;
                default:
                    throw new ArgumentOutOfRangeException(nameof(sex));
            }
        }

        private static int MapInteger(int? number)
        {
            if (!number.HasValue)
            {
                return 0;
            }
            return number.Value;
        }

        private static int MapNumber(float? value, WeightUnits unit)
        {
            if (!value.HasValue)
            {
                return 0;
            }

            var weight = new Weight(value.Value, unit);
            return (int)weight.AsG;
        }

        private static int MapPercent(float? value)
        {
            if (!value.HasValue)
            {
                return 0;
            }
            return (int)Math.Round(value.Value, 0);
        }
    }
}
