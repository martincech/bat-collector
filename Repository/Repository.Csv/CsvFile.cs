﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Repository.Csv.Extensions;
using Settings.DataStorageSettings;
using Settings.DeviceSettings;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Bat.Common.Units;

namespace Repository.Csv
{
    public class CsvFile
    {
        private readonly LocalCsvRepositorySettings configuration;
        private const WeightUnits DefaultUnit = WeightUnits.KG;


        public CsvFile(LocalCsvRepositorySettings configuration)
        {
            this.configuration = configuration;
        }


        public void UpdateFile(string path, StatisticSample statistic, WeighingInfoSample info)
        {
            var unit = info == null ? DefaultUnit : info.Unit;
            if (!File.Exists(path))
            {
                Write(path, statistic, unit);
                return;
            }

            //file existed, check last record, update or insert new record
            var tempName = path + "_tmp";
            CheckTempFile(tempName);

            var timeFilterFrom = info?.Start;
            // We didn't count with sex differentiation, already use female data
            UpdateTempFile(path, tempName, statistic.MapTo(Sex.Female, unit), timeFilterFrom);

            if (File.Exists(tempName))
            {
                File.Replace(tempName, path, null);
            }
        }


        #region Private helpers

        private void Write(string path, StatisticSample statistic, WeightUnits unit)
        {
            Write(path, new List<StatisticSample> { statistic }, unit);
        }

        private void Write(string path, IEnumerable<StatisticSample> samples, WeightUnits unit)
        {
            Write(path, samples.MapTo(Sex.Female, unit));
        }

        private void Write(string path, IEnumerable<Statistics> samples)
        {
            using (var writer = new StreamWriter(path))
            using (var csv = new CsvWriter(writer))
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = configuration.Csv.Delimiter.ToString();
                csv.WriteRecords(samples);
            }
        }

        private IEnumerable<Statistics> ReadAll(string path)
        {
            using (var reader = new StreamReader(path))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = configuration.Csv.Delimiter.ToString();
                var records = csv.GetRecords<Statistics>().ToList();
                if (records == null)
                {
                    return new List<Statistics>();
                }
                return records.OrderBy(o => o.DateTime);
            }
        }

        private void UpdateTempFile(string originFilePath,
                                    string tempName,
                                    Statistics newStatistic,
                                    DateTimeOffset? timeFilterFrom)
        {
            var wasUpdate = false;
            var statsToWrite = new List<Statistics>();
            newStatistic = AddTimeToDatetimeSample(newStatistic);

            foreach (var stat in ReadAll(originFilePath))
            {
                if (timeFilterFrom.HasValue && DateTimeOffset.Compare(stat.DateTime.Date, timeFilterFrom.Value.Date) < 0)
                {   //record is earlier than beginning of actual flock
                    continue;
                }

                if (DateTimeOffset.Compare(newStatistic.DateTime.Date, stat.DateTime.Date) == 0)
                {
                    if (wasUpdate)
                    {   //record with the same date has already exist
                        continue;
                    }
                    wasUpdate = true;
                    statsToWrite.Add(newStatistic);
                }
                else
                {   //write origin record
                    statsToWrite.Add(AddTimeToDatetimeSample(stat));
                }
            }

            if (!wasUpdate)
            {
                statsToWrite.Add(newStatistic);
            }

            Write(tempName, statsToWrite);
        }

        /// <summary>
        /// Data from scale are without time (only with date). This method
        /// add current time to today statistic and end time of day
        /// to old days statistics.
        /// </summary>
        /// <param name="sample"></param>
        /// <returns></returns>
        private static Statistics AddTimeToDatetimeSample(Statistics sample)
        {
            var now = DateTimeOffset.Now;
            var span = new TimeSpan(0, 23, 59, 0);
            var dateTimeCompare = DateTimeOffset.Compare(sample.DateTime.Date, now.Date);

            if (dateTimeCompare == 0)
            {
                span = new TimeSpan(0, now.Hour, now.Minute, 0);
            }
            else if (dateTimeCompare > 0)
            {
                return sample;
            }

            sample.DateTime = sample.DateTime.Date.Add(span);
            return sample;
        }


        private static void CheckTempFile(string name)
        {
            if (File.Exists(name))
            {
                File.Delete(name);
            }
        }

        #endregion
    }
}
