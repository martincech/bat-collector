﻿using System;
using CsvHelper.Configuration.Attributes;
using Repository.Csv.Converters;

namespace Repository.Csv
{
    public class Statistics
    {
        [Index(0)]
        [TypeConverter(typeof(CsvDateTimeOffsetConverter))]
        public DateTimeOffset DateTime { get; set; }

        [Index(1)]
        public int Day { get; set; }

        [Index(2)]
        public int Sex { get; set; }

        [Index(3)]
        public int Count { get; set; }

        [Index(4)]
        public int Avg { get; set; }

        [Index(5)]
        public int Gain { get; set; }

        [Index(6)]
        public int Sig { get; set; }

        [Index(7)]
        public int Cv { get; set; }

        [Index(8)]
        public int Uni { get; set; }
    }
}
