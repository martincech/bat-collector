﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NLog;
using Utilities.Timer;

namespace Repository.LocalDb
{
    public class DbRepositoryCache : ICache
    {
        #region Private fields

        private const int MINIMAL_FLUSH_PERIOD_MS = 50;

        private readonly IDbStorage storage;
        private readonly ITimer timer;
        private readonly HashSet<object> cache;
        private readonly HashSet<object> cacheDelete;

        private readonly object Lock = new object();
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Constructor

        public DbRepositoryCache(IDbStorage storage, TimeSpan flushPeriod, Func<ITimer> timerFactory = null)
        {
            if (timerFactory == null)
            {
                timerFactory = TimerAdapter.Factory;
            }

            Debug.Assert(flushPeriod.TotalMilliseconds >= MINIMAL_FLUSH_PERIOD_MS,
                $"Flush period ({flushPeriod.TotalMilliseconds} ms) is lower than MINIMAL_FLUSH_PERIOD_MS ({MINIMAL_FLUSH_PERIOD_MS} ms)");
            if (flushPeriod.TotalMilliseconds < MINIMAL_FLUSH_PERIOD_MS)
                flushPeriod = TimeSpan.FromMilliseconds(MINIMAL_FLUSH_PERIOD_MS);

            cache = new HashSet<object>();
            cacheDelete = new HashSet<object>();

            this.storage = storage ?? throw new ArgumentNullException(nameof(storage));
            timer = timerFactory?.Invoke();
            timer.Interval = flushPeriod.TotalMilliseconds;
            timer.Elapsed += FlushAndRestartTimer;
            timer.Start();
        }

        #endregion


        private void FlushAndRestartTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timer.Stop();
                Flush();
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                timer.Start();
            }
        }

        private void Flush()
        {
            lock (Lock)
            {
                FlushDelete();
                FlushCache();
            }
        }

        private void FlushDelete()
        {
            var count = cacheDelete.Count;
            if (count == 0) return;
            logger.Debug($"LocalDb cache DELETE count: {count}");
            if (storage.Delete(cacheDelete)) cacheDelete.Clear();
        }

        private void FlushCache()
        {
            var count = cache.Count;
            if (count == 0) return;
            logger.Debug($"LocalDb cache INSERT count: {count}");
            if (storage.Insert(cache)) cache.Clear();
        }

        #region Implementation of ICache

        public void Save(object item)
        {
            lock (Lock)
            {
                cache.Add(item);
            }
        }

        public void Delete(object item)
        {
            lock (Lock)
            {
                if (!cache.Remove(item)) cacheDelete.Add(item);
            }
        }

        public IEnumerable<T> Load<T>()
        {
            FlushAndRestartTimer(null, null);
            return storage.SelectAll().OfType<T>();
        }

        #endregion
    }
}
