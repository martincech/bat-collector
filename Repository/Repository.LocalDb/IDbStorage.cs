﻿using System.Collections.Generic;

namespace Repository.LocalDb
{
    public interface IDbStorage
    {
        bool Insert(IEnumerable<object> items);
        bool Delete(IEnumerable<object> items);

        bool Clear();

        IEnumerable<object> SelectAll();
    }
}
