﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using NLog;

namespace Repository.LocalDb
{
    public class DbStorageSQLite : IDbStorage
    {
        //script tables
        private const string CreateTableQuery = @"CREATE TABLE [Queue] (
                                               [Id] INTEGER PRIMARY KEY AUTOINCREMENT
                                             , [Hash] INTEGER NOT NULL
                                             , [Type] TEXT NOT NULL
                                             , [Value] TEXT NOT NULL
                                             , [Date] TEXT NOT NULL                                         
                                             );
                                            CREATE INDEX IF NOT EXISTS hash_index ON [Queue] ([Hash]);";

        private const string CountQuery = "SELECT COUNT(Id) FROM [Queue]";
        private const string DeleteQuery = "DELETE FROM [Queue] WHERE Hash = @Hash";

        private const string InsertQuery = "INSERT INTO [Queue] (Hash, Type, Value, Date) VALUES(@Hash, @Type, @Value, @Date)";
        private const string SelectQuery = "SELECT Type, Value FROM [Queue] ORDER BY Id LIMIT @Count OFFSET @From";

        private readonly object lockObj = new object();
        private readonly string connection;

        private static int MaxRecordsLoadedCount = 10000;
        private readonly string file;
        private readonly int maxRecordsLoaded;
        private readonly MD5 md5Factory;
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private DbStorageSQLite(string file, int maxRecordsLoaded)
        {
            this.file = file;
            this.maxRecordsLoaded = maxRecordsLoaded;
            connection = $@"Data Source={file};Version=3;Locking Mode=EXCLUSIVE";
            md5Factory = MD5.Create();
        }

        #region Manage instances of DbStorageSQLite

        private static readonly Dictionary<string, DbStorageSQLite> Databases = new Dictionary<string, DbStorageSQLite>();
        private const string DbSuffix = ".sqlite";

        internal static DbStorageSQLite GetDatabase(string name, int maxRecordsLoaded)
        {
            MaxRecordsLoadedCount = maxRecordsLoaded;
            return GetDatabase(name);
        }

        public static DbStorageSQLite GetDatabase(string name)
        {
            if (string.IsNullOrEmpty(name)) return null;
            if (!name.EndsWith(DbSuffix))
            {
                name += DbSuffix;
            }
            if (Databases.ContainsKey(name)) return Databases[name];
            var dbStorage = new DbStorageSQLite(name, MaxRecordsLoadedCount);
            dbStorage.CreateDatabaseIfNotExist();
            if (!dbStorage.TryConnect()) return null;
            Databases.Add(name, dbStorage);
            return dbStorage;
        }

        #endregion

        #region Create database

        private void CreateDatabaseIfNotExist()
        {
            if (!File.Exists(file))
            {
                lock (lockObj)
                {
                    CreateDatabaseTable();
                }
            }
        }

        private void CreateDatabaseTable()
        {
            using (var conn = new SQLiteConnection(connection))
            {
                conn.Open();
                using (var cmd = new SQLiteCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = CreateTableQuery;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Implementation of IDbStorage

        public bool Insert(IEnumerable<object> items)
        {
            return ExecuteNonQuery((cmd, item) =>
            {
                var value = JsonConvert.SerializeObject(item);
                cmd.Parameters.Add("@Hash", DbType.Binary, 16).Value = CreateMd5Hash(value);
                cmd.Parameters.Add("@Type", DbType.String, 500).Value = item.GetType().AssemblyQualifiedName;
                cmd.Parameters.Add("@Value", DbType.String, 4000).Value = value;
                cmd.Parameters.Add("@Date", DbType.DateTime, 255).Value = DateTime.Now;
            }, items, InsertQuery);
        }

        public bool Delete(IEnumerable<object> items)
        {
            return ExecuteNonQuery((cmd, item) =>
            {
                cmd.Parameters.Add("@Hash", DbType.Binary).Value = CreateMd5Hash(JsonConvert.SerializeObject(item));
            }, items, DeleteQuery);
        }


        public bool Clear()
        {
            lock (lockObj)
            {
                if (File.Exists(file))
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public IEnumerable<object> SelectAll()
        {
            CreateDatabaseIfNotExist();
            var count = GetCacheSize();
            var start = 0;
            while (start < count)
            {
                var items = SelectRows(start, maxRecordsLoaded);
                start += maxRecordsLoaded;
                foreach (var item in items)
                {
                    yield return item;
                }
            }
        }

        #endregion

        #region

        private bool ExecuteNonQuery(Action<SQLiteCommand, object> action, IEnumerable<object> items, string sqlCommand)
        {
            lock (lockObj)
            {
                CreateDatabaseIfNotExist();
                using (var conn = new SQLiteConnection(connection))
                {
                    conn.Open();
                    using (var transaction = conn.BeginTransaction())
                    {
                        try
                        {
                            //SQLiteCommand is not reused -> it's faster, but it leads to a memory overhead
                            foreach (var item in items)
                            {
                                try
                                {
                                    if (item == null) continue;
                                    using (var cmd = new SQLiteCommand(sqlCommand, conn))
                                    {
                                        action?.Invoke(cmd, item);
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                                catch (Exception e)
                                {
                                    logger.Debug(e, "ExecuteNonQuery exception");
                                }
                            }
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        private IEnumerable<object> SelectRows(int from, int count)
        {
            var items = new List<object>();
            if (from < 0 || count <= 0) return items;

            try
            {
                using (var conn = new SQLiteConnection(connection))
                {
                    conn.Open();
                    using (var cmd = new SQLiteCommand(SelectQuery, conn))
                    {
                        cmd.Parameters.Add("@From", DbType.Int32).Value = from;
                        cmd.Parameters.Add("@Count", DbType.Int32).Value = count;
                        using (var reader = cmd.ExecuteReader(CommandBehavior.Default))
                        {
                            while (reader.Read())
                            {
                                var typeName = reader.GetString(0);
                                var type = Type.GetType(typeName);
                                if (type == null) continue;
                                var value = reader.GetString(1);
                                items.Add(JsonConvert.DeserializeObject(value, type));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Warn(e, "Select rows exception");
            }
            return items;
        }

        private int GetCacheSize()
        {
            var count = 0;
            try
            {
                using (var conn = new SQLiteConnection(connection))
                {
                    conn.Open();
                    using (var cmd = new SQLiteCommand(CountQuery, conn))
                    {
                        using (var reader = cmd.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            if (reader.Read())
                            {
                                count = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Warn(e, "Get cache size exception");
            }
            return count;
        }

        private byte[] CreateMd5Hash(string json)
        {
            return md5Factory.ComputeHash(Encoding.UTF8.GetBytes(json));
        }

        private bool TryConnect()
        {
            using (var testConn = new SQLiteConnection(connection))
            {
                try
                {
                    testConn.Open();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        #endregion
    }
}
