﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using NLog;
using Repository.ProcessQueue;
using Veit.Mqueue;

namespace Repository
{
    public class FileRepositoryCache : ProcessQueueDecorator, ICache
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly string file;
        private string LastFile
        {
            get { return file + ".last"; }
        }

        public FileRepositoryCache(string file) : this(file, new Veit.Mqueue.ProcessQueue()) { }
        public FileRepositoryCache(string file, IProcessQueue processQueue)
           : base(processQueue)
        {
            this.file = file;
            var filder = file.Substring(0, file.LastIndexOf("\\", StringComparison.InvariantCulture) + 1);
            if (!Directory.Exists(filder)) Directory.CreateDirectory(filder);
            processQueue.AddProcessing<object>(SaveToFile);
        }

        private bool SaveToFile(object sample)
        {
            try
            {
                File.AppendAllLines(file, new[] { CreateRow(sample) });
                return true;
            }
            catch (Exception e)
            {
                logger.Info(e, "File repository cache failed on save action");
                return false;
            }
        }

        #region Implementation of ICache

        public void Save(object item)
        {
            SaveToFile(item);
        }

        public void Delete(object item)
        {
            try
            {
                File.WriteAllText(LastFile, CreateRow(item));
            }
            catch (Exception e)
            {
                logger.Info(e, "File repository cache failed on delete action");
            }
        }

        public IEnumerable<T> Load<T>()
        {
            var values = LoadLast();
            var data = new List<T>();
            foreach (var value in values)
            {
                try
                {
                    var type = Type.GetType(value.Key);
                    if (type == null) continue;
                    if (!typeof(T).IsAssignableFrom(type)) continue;
                    data.AddRange(from row in value.Value select JsonConvert.DeserializeObject(row, type) into obj where obj != null select (T)obj);
                }
                catch (Exception e)
                {
                    logger.Warn(e, "File repository Load exception");
                }
            }
            return data;
        }

        #endregion


        private static string CreateRow(object item)
        {
            return item.GetType().AssemblyQualifiedName + ";" + JsonConvert.SerializeObject(item);
        }

        private Dictionary<string, List<string>> LoadLast()
        {
            var lines = File.Exists(file) ? File.ReadAllLines(file) : new string[] { };
            var lastLine = File.Exists(LastFile) ? File.ReadAllText(LastFile) : null;
            var index = Array.FindIndex(lines, f => f == lastLine);
            if (index != -1)
            {
                lines = lines.Skip(index + 1).ToArray();
                File.WriteAllLines(file, lines);
            }

            var values = new Dictionary<string, List<string>>();

            foreach (var line in lines)
            {
                var row = line.Split(';');
                if (row.Length != 2) continue;
                if (!values.ContainsKey(row[0])) values.Add(row[0], new List<string>());
                values[row[0]].Add(row[1]);
            }
            return values;
        }

    }
}
