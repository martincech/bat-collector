﻿using System;
using System.Collections.Generic;
using Veit.Mqueue;

namespace Repository.ProcessQueue
{
    public class CachedProcessQueueDecorator : ProcessQueueDecorator
    {
        #region Private fields

        private readonly ICache cache;
        private readonly ICollection<Type> permitChachingTypes;  //contains types which will be cached - if null - all incoming types are permitted

        #endregion

        #region Constructors

        public CachedProcessQueueDecorator(IProcessQueue processQueue, ICache cache, ICollection<Type> permitChachingTypes)
           : this(processQueue, cache)
        {
            this.permitChachingTypes = permitChachingTypes;
        }

        public CachedProcessQueueDecorator(IProcessQueue processQueue, ICache cache)
           : base(processQueue)
        {
            this.cache = cache;
        }

        #endregion

        #region Overrides of RepositoryBase

        public override bool Enqueue(object item, Priority priority = Priority.Normal)
        {
            return Enqueue(item, true, priority);
        }

        private bool Enqueue(object item, bool useCache, Priority priority = Priority.Normal)
        {
            if (useCache && CanCaching(item))
            {
                cache.Save(item);
            }
            if (!base.Enqueue(item, priority))
            {
                if (useCache && !CanCaching(item))
                {
                    cache.Delete(item);
                }
                return false;
            }
            return true;
        }

        public override bool AddProcessing<T>(Type type, Func<T, bool> action)
        {
            if (!base.AddProcessing(type, action))
                return false;
            var cached = cache.Load<T>();
            foreach (var item in cached)
            {
                Enqueue(item, false, Priority.Normal);
            }
            return true;
        }



        protected override Func<T, bool> WrapperForAction<T>(Func<T, bool> action)
        {
            return item =>
            {
                var result = action?.Invoke(item) ?? default;
                if (result)
                {
                    cache.Delete(item);
                }
                return result;
            };
        }

        #endregion

        #region Private helpers

        private bool CanCaching(object item)
        {
            if (permitChachingTypes == null) return true; //no restrictions
            return permitChachingTypes.Contains(item.GetType());
        }

        #endregion
    }
}
