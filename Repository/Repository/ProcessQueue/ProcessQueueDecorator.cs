﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Mqueue;

namespace Repository.ProcessQueue
{
    public abstract class ProcessQueueDecorator : IProcessQueue
    {
        private readonly IProcessQueue processQueue;
        private readonly Dictionary<object, object> WrappedActions;
        private readonly Dictionary<Type, List<object>> typeActions;
        private const int DefaultDelayBetweenOperations = 50;

        protected ProcessQueueDecorator(IProcessQueue processQueue)
        {
            this.processQueue = processQueue;
            WrappedActions = new Dictionary<object, object>();
            typeActions = new Dictionary<Type, List<object>>();
            DelayBetweenOperations = DefaultDelayBetweenOperations;
        }

        #region Implementation of IRepository

        /// <summary>
        /// Add item to digest
        /// </summary>
        /// <param name="item">Object to digest</param>
        /// <param name="priority">Queue priority</param>
        /// <returns>Success of adding item to digest</returns>
        public virtual bool Enqueue(object item, Priority priority = Priority.Normal)
        {
            return processQueue.Enqueue(item, priority);
        }

        public bool EnqueueRange(IEnumerable<object> elements, Priority priority = Priority.Normal)
        {
            return elements == null || elements.Aggregate(false, (current, element) => current || Enqueue(element, priority));
        }

        /// <summary>
        /// List of undigested items
        /// </summary>
        public IEnumerable<object> Queue()
        {
            return processQueue.Queue();
        }

        #endregion

        #region Implementation of IProcessQueue

        public int DelayBetweenOperations { get { return processQueue.DelayBetweenOperations; } set { processQueue.DelayBetweenOperations = value; } }

        public bool AddProcessing<T>(Func<T, bool> action)
        {
            return AddProcessing(typeof(T), action);
        }

        public virtual bool AddProcessing<T>(Type type, Func<T, bool> action)
        {
            if (action == null || WrappedActions.ContainsKey(action))
            {
                return false;
            }
            var wrapper = WrapperForAction(action);

            if (!processQueue.AddProcessing(type, wrapper))
            {
                return false;
            }
            if (!typeActions.ContainsKey(type))
            {
                typeActions.Add(type, new List<object>());
            }
            typeActions[type].Add(action);
            WrappedActions.Add(action, wrapper);
            return true;
        }

        public bool RemoveProcessing(Type type)
        {
            if (!processQueue.RemoveProcessing(type))
            {
                return false;
            }
            if (!typeActions.ContainsKey(type))
            {
                return true;
            }
            foreach (var action in typeActions[type])
            {
                WrappedActions.Remove(action);
            }
            typeActions[type].Clear();
            return true;
        }

        public bool RemoveProcessing<T>()
        {
            return RemoveProcessing(typeof(T));
        }

        public bool RemoveProcessing<T>(Func<T, bool> action)
        {
            return RemoveProcessing(typeof(T), action);
        }

        public virtual bool RemoveProcessing<T>(Type type, Func<T, bool> action)
        {
            if (!WrappedActions.ContainsKey(action))
            {
                return false;
            }
            if (!processQueue.RemoveProcessing(type, (Func<T, bool>)WrappedActions[action]))
            {
                return false;
            }
            WrappedActions.Remove(action);
            typeActions[type].Remove(action);
            return true;
        }

        protected virtual Func<T, bool> WrapperForAction<T>(Func<T, bool> action)
        {
            return action;
        }

        #endregion
    }
}
