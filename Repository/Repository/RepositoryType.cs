﻿using System;

namespace Repository
{
    public enum RepositoryType
    {
        Unknown,
        [Obsolete("Not in use anymore", true)]
        Kafka,
        Database,
        Csv,
        WebApi
    }
}
