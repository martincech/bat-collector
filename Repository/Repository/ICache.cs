﻿using System.Collections.Generic;

namespace Repository
{
    public interface ICache
    {
        void Save(object item);
        void Delete(object item);
        IEnumerable<T> Load<T>();
    }
}
