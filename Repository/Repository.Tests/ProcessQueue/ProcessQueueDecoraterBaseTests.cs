﻿using System;
using System.Collections.Generic;
using Moq;
using Repository.ProcessQueue;
using Veit.Mqueue;
using Xunit;

namespace Repository.Tests.ProcessQueue
{
    public abstract class ProcessQueueDecoraterBaseTests
    {
        protected readonly Mock<IProcessQueue> MockActionQueue;
        private ProcessQueueDecorator uut;

        protected ProcessQueueDecoraterBaseTests()
        {
            MockActionQueue = new Mock<IProcessQueue>();
        }

        public void Init()
        {
            SetupMockActionQueue<int>();
            uut = SetupUut();
        }

        protected abstract ProcessQueueDecorator SetupUut();

        protected void SetupMockActionQueue<T>()
        {
            var aToInvoke = new List<Func<T, bool>>();
            MockActionQueue
               .Setup(q => q.AddProcessing(It.IsAny<Func<T, bool>>()))
               .Callback<Func<T, bool>>(a =>
               {
                   if (!aToInvoke.Contains(a))
                       aToInvoke.Add(a);
               })
               .Returns(true);
            MockActionQueue
               .Setup(q => q.AddProcessing(It.IsAny<Type>(), It.IsAny<Func<T, bool>>()))
               .Callback<Type, Func<T, bool>>((t, a) =>
               {
                   if (!aToInvoke.Contains(a))
                       aToInvoke.Add(a);
               })
               .Returns(true);
            MockActionQueue
               .Setup(q => q.RemoveProcessing(It.IsAny<Func<T, bool>>()))
               .Callback<Func<T, bool>>(a =>
               {
                   if (aToInvoke.Contains(a))
                   {
                       aToInvoke.Remove(a);
                   }
               })
               .Returns(true);

            MockActionQueue
               .Setup(q => q.RemoveProcessing(It.IsAny<Type>(), It.IsAny<Func<T, bool>>()))
               .Callback<Type, Func<T, bool>>((t, a) =>
               {
                   if (aToInvoke.Contains(a))
                   {
                       aToInvoke.Remove(a);
                   }
               })
               .Returns(true);

            MockActionQueue
               .Setup(q => q.Enqueue(It.IsAny<T>(), Priority.Normal))
               .Callback<object,Priority>((r,p) =>
               {
                   Assert.IsType<T>(r);
                   foreach (var func in aToInvoke)
                   {
                       func((T)r);
                   }
               })
               .Returns(true);
        }

        [Fact]
        public void Action_InvokedAfterRegistration()
        {
            const int value = 5;
            var raised = false;

            uut.AddProcessing((Func<int, bool>)(i =>
                 {
                     raised = true;
                     Assert.Equal(value, i);
                     return true;
                 }));
            uut.Enqueue(value);
            Assert.True(raised);
        }

        [Fact]
        public void NullActionCantRegister()
        {
            Assert.False(uut.AddProcessing<int>(null));
            Assert.False(uut.AddProcessing<double>(typeof(int), null));
        }

        [Fact]
        public void SameAction_CanBeRegisteredOnlyOnce()
        {
            var action = new Func<int, bool>(i => true);
            Assert.True(uut.AddProcessing(action));
            Assert.False(uut.AddProcessing(action));
        }

        [Fact]
        public void ActionDeregistered()
        {
            const int value = 5;
            var raisedFirst = false;
            var raisedSecond = false;

            uut.AddProcessing((Func<int, bool>)(i =>
                 {
                     raisedFirst = true;
                     return true;
                 }));

#pragma warning disable IDE0039 // Use local function
            Func<int, bool> action2 = i =>

            {
                raisedSecond = true;
                return true;
            };
#pragma warning restore IDE0039 // Use local function

            uut.AddProcessing(action2);
            uut.RemoveProcessing(action2);
            uut.Enqueue(value);
            Assert.True(raisedFirst);
            Assert.False(raisedSecond);
        }

        [Fact]
        public void RegisterDeregisterTests()
        {
            const int value = 5;
            var raisedFirstCount = 0;
            var raisedSecondCount = 0;

            uut.AddProcessing((Func<int, bool>)(i =>
             {
                 raisedFirstCount++;
                 return true;
             }));

#pragma warning disable IDE0039 // Use local function
            Func<int, bool> action2 = i =>

            {
                raisedSecondCount++;
                return true;
            };
#pragma warning restore IDE0039 // Use local function

            uut.AddProcessing(action2);
            uut.Enqueue(value);
            Assert.Equal(1, raisedFirstCount);
            Assert.Equal(1, raisedSecondCount);

            uut.RemoveProcessing(action2);
            uut.Enqueue(value);
            Assert.Equal(2, raisedFirstCount);
            Assert.Equal(1, raisedSecondCount);

            uut.AddProcessing(action2);
            uut.Enqueue(value);
            Assert.Equal(3, raisedFirstCount);
            Assert.Equal(2, raisedSecondCount);

        }
    }
}
