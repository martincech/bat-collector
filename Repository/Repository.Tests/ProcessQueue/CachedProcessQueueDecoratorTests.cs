﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Repository.ProcessQueue;
using Veit.Mqueue;
using Xunit;

namespace Repository.Tests.ProcessQueue
{
    public class CachedProcessQueueDecoratorTests : ProcessQueueDecoraterBaseTests
    {
        private readonly Mock<ICache> mockCache;
        private CachedProcessQueueDecorator unitUnderTest;

        public CachedProcessQueueDecoratorTests()
            : base()
        {
            mockCache = new Mock<ICache>();
            mockCache.SetupAllProperties();
            Init();
        }

        protected override ProcessQueueDecorator SetupUut()
        {

            unitUnderTest = new CachedProcessQueueDecorator(MockActionQueue.Object, mockCache.Object);
            return unitUnderTest;
        }


        [Fact]
        public void Action_CachedAndDeleted_AfterEnqeuingAndValidProcessing()
        {
            unitUnderTest.AddProcessing((Func<int, bool>)(i => true));

            mockCache.Verify(c => c.Save(It.IsAny<object>()), Times.Never);
            mockCache.Verify(c => c.Delete(It.IsAny<object>()), Times.Never);
            const int value = 5;
            unitUnderTest.Enqueue(value, Priority.Normal);
            mockCache.Verify(c => c.Save(It.Is<int>(o => o == value)), Times.Once);
            mockCache.Verify(c => c.Delete(It.IsAny<object>()), Times.Once);
        }


        [Fact]
        public void InvalidActionProcessing_LeavesItemInCache()
        {
            Assert.True(unitUnderTest.AddProcessing((Func<int, bool>)(i => false)));

            mockCache.Verify(c => c.Save(It.IsAny<object>()), Times.Never);
            mockCache.Verify(c => c.Delete(It.IsAny<object>()), Times.Never);
            const int value = 5;
            unitUnderTest.Enqueue(value, Priority.Normal);
            mockCache.Verify(c => c.Save(It.Is<int>(o => o == value)), Times.Once);
            mockCache.Verify(c => c.Delete(It.IsAny<object>()), Times.Never);
        }


        [Fact]
        public void CachedItems_Processed()
        {
            var cache = new[]
            {
                5,2,3
            };
            var raisedValues = new List<int>();
            mockCache.Setup(c => c.Load<int>()).Returns(cache);
            //re-initialization to create again the uut with cached data (simulate program crash
            Init();

            Assert.True(unitUnderTest.AddProcessing((Func<int, bool>)(i =>
                 {
                     raisedValues.Add(i);
                     return true;
                 })));
            Assert.Equal(cache, raisedValues);
        }

        [Fact]
        public void RegisterDeregisterTestsWithCache()
        {
            const int value = 5;
            var raisedFirstCount = 0;

#pragma warning disable IDE0039 // Use local function
            Func<int, bool> action1 = i =>

            {
                raisedFirstCount++;
                return true;
            };
#pragma warning restore IDE0039 // Use local function

            var cache = new List<object>();
            mockCache.Setup(c => c.Save(It.IsAny<object>())).Callback<object>(o => cache.Add(o));
            mockCache.Setup(c => c.Delete(It.IsAny<object>())).Callback<object>(o => cache.Remove(o));
            mockCache.Setup(c => c.Load<int>()).Returns(cache.Where(o => o is int).Cast<int>().ToList);

            unitUnderTest.AddProcessing(action1);
            unitUnderTest.Enqueue(value, Priority.Normal);
            Assert.Equal(1, raisedFirstCount);
            mockCache.Verify(c => c.Save(It.IsAny<object>()), Times.Exactly(1));
            mockCache.Verify(c => c.Delete(It.IsAny<object>()), Times.Exactly(1));

            unitUnderTest.RemoveProcessing(action1);
            unitUnderTest.Enqueue(value, Priority.Normal);
            Assert.Equal(1, raisedFirstCount);
            mockCache.Verify(c => c.Save(It.IsAny<object>()), Times.Exactly(2));
            mockCache.Verify(c => c.Delete(It.IsAny<object>()), Times.Exactly(1));

            unitUnderTest.AddProcessing(action1);
            Assert.Equal(2, raisedFirstCount);
            unitUnderTest.Enqueue(value, Priority.Normal);
            Assert.Equal(3, raisedFirstCount);
            mockCache.Verify(c => c.Save(It.IsAny<object>()), Times.Exactly(3));
            mockCache.Verify(c => c.Delete(It.IsAny<object>()), Times.Exactly(3));
        }
    }
}
