﻿using System;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Mqueue;

namespace Repository.Console
{
    public sealed class ConsoleRepository : IDisposable
    {
        private readonly IProcessQueue processQueue;

        public ConsoleRepository(IProcessQueue processQueue)
        {
            this.processQueue = processQueue;
            AddAllProcessing();
        }

        private void AddAllProcessing()
        {
            processQueue.AddProcessing<StatisticSample>(SampleReaded);
            processQueue.AddProcessing<TimeStampedSample>(SampleReaded);
            processQueue.AddProcessing<SourceConnectivityInfo>(ConnectionChanged);
        }

        private void RemoveAllProcessing()
        {
            processQueue.RemoveProcessing<StatisticSample>(SampleReaded);
            processQueue.RemoveProcessing<TimeStampedSample>(SampleReaded);
            processQueue.RemoveProcessing<SourceConnectivityInfo>(ConnectionChanged);
        }

        private static bool SampleReaded(StatisticSample sample)
        {
            System.Console.WriteLine($"ConsoleRepository - {(sample.SensorUid ?? "")} - {sample.GetType().Name} read : {sample}");
            return true;
        }


        private static bool ConnectionChanged(SourceConnectivityInfo conInfo)
        {

            System.Console.WriteLine("ConsoleRepository - {0} {1} ({2} - {3}): {4} at {5}",
               conInfo.GetType().Name,
               conInfo.Source, conInfo.Device.Type,
               conInfo.Device.SensorUid,
               conInfo.Device.Connected ? "CONNECTED" : "DISCONNECTED",
               conInfo.Device.TimeStamp);
            return true;
        }

        private static bool SampleReaded(TimeStampedSample sample)
        {
            System.Console.WriteLine("ConsoleRepository - {0} - {1} read : {2} - {3}",
               sample.SensorUid,
               sample.GetType().Name,
               sample.TimeStamp,
               sample.Value);
            return true;
        }

        public void Dispose()
        {
            RemoveAllProcessing();
        }
    }
}
