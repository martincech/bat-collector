﻿using System.Runtime.Serialization;

namespace Repository.Web.Model
{
    [DataContract]
    public class ApiResponse
    {
        /// <summary>
        /// Gets or Sets Code
        /// </summary>
        [DataMember(Name = "code")]
        public int Code { get; set; }

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; set; }

        /// <summary>
        /// Concrete type as action result
        /// </summary>
        /// <value>Concrete type as result on create/update action</value>
        [DataMember(Name = "data", EmitDefaultValue = false)]
        public object Data { get; set; }
    }
}
