﻿using System;
using System.Runtime.Serialization;

namespace Repository.Web.Model
{
    [DataContract]
    public class Device
    {
        [DataMember(Name = "uid")]
        public string Uid { get; set; }

        [DataMember(Name = "id")]
        public Guid? Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "type")]
        public DeviceType Type { get; set; }

        [DataMember(Name = "parentId")]
        public Guid? ParentId { get; set; }
    }
}
