﻿using System;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.DataContracts.Device;
using HardwareId;
using NLog;
using Repository.Web.WebApi;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Mqueue;

namespace Repository.Web
{
    public sealed class WebRepository : IDisposable
    {
        private readonly IWebApi webApi;
        private readonly IProcessQueue processQueue;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public WebRepository(
           IWebApi webApi,
           IProcessQueue processQueue)
        {
            this.webApi = webApi;
            this.processQueue = processQueue;
            webApi.ConnectivityStatusChanged += WebApi_ConnectivityStatusChanged;
        }

        private void WebApi_ConnectivityStatusChanged(object sender, WebApiConnectionInfo e)
        {
            if (e.Logged.IsLogged())
            {
                AddAllProcessing();
            }
            else
            {
                RemoveAllProcessing();
            }
        }

        private void RemoveAllProcessing()
        {
            processQueue.RemoveProcessing<StatisticSample>(StatRead);
            processQueue.RemoveProcessing<SourceConnectivityInfo>(ConnectionChanged);
            processQueue.RemoveProcessing<DeviceNotification>(SyncDevice);
        }

        private void AddAllProcessing()
        {
            processQueue.AddProcessing<StatisticSample>(StatRead);
            processQueue.AddProcessing<SourceConnectivityInfo>(ConnectionChanged);
            processQueue.AddProcessing<DeviceNotification>(SyncDevice);
        }

        private static bool ConnectionChanged(SourceConnectivityInfo info)
        {
            try
            {
                //TODO: update device connectivity event (Future improvement)
                return true;
            }
            catch (Exception e)
            {
                logger.Info(e, $"Connection changed exception for: '{info.Device.SensorUid}'");
                return false;
            }
        }

        private bool StatRead(StatisticSample stat)
        {
            bool result;
            try
            {
                logger.Debug($"Web repository - stat received for: '{stat.SensorUid}'");
                result = webApi.SyncStatisticAsync(stat).GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                logger.Info(e, $"Stat read exception: '{stat.SensorUid}'");
                result = false;
            }
            return result;
        }

        public void Dispose()
        {
            webApi.ConnectivityStatusChanged -= WebApi_ConnectivityStatusChanged;
            RemoveAllProcessing();
        }


        private bool SyncDevice(DeviceNotification device)
        {
            try
            {
                if (device == null)
                {
                    throw new ArgumentNullException(nameof(device));
                }

                switch (device.Action)
                {
                    case DeviceAction.Add:
                        return webApi.AddDeviceAsync(MapDevice(device)).GetAwaiter().GetResult();
                    case DeviceAction.Update:
                        return webApi.UpdateDeviceAsync(MapDevice(device)).GetAwaiter().GetResult();
                    case DeviceAction.Delete:
                        return webApi.DeleteDeviceAsync(device.Uid).GetAwaiter().GetResult();
                    default:
                    case DeviceAction.Unknown:
                        throw new InvalidOperationException("Not selected action for device synchronization");
                }
            }
            catch (Exception e)
            {
                logger.Info(e, $"Sync device exception: '{device.Uid}'");
                return false;
            }
        }

        private static Model.Device MapDevice(DeviceNotification device)
        {
            return new Model.Device
            {
                Uid = device.Uid,
                Name = device.Name,
                Type = Model.DeviceType.Bat2CableScale,
                ParentId = HwId.SystemId
            };
        }
    }
}
