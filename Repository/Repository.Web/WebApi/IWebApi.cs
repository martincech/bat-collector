﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Communication.Contract.Wcf.DataContracts;
using Repository.Web.Model;
using Veit.Bat.Common.Sample.Statistics;

namespace Repository.Web.WebApi
{
    public interface IWebApi
    {
        WebApiConnectionInfo ConnectionInfo { get; }
        bool IsCommunicating { get; }

        event EventHandler<WebApiConnectionInfo> ConnectivityStatusChanged;

        Task<bool> LoginAsync(WebApiConnectionInfo info, CancellationToken cancelToken = default);
        Task<bool> SyncStatisticAsync(StatisticSample sample, CancellationToken cancelToken = default);

        Task<bool> AddDeviceAsync(Device device, CancellationToken cancelToken = default);
        Task<bool> DeleteDeviceAsync(string deviceUid, CancellationToken cancelToken = default);
        Task<bool> UpdateDeviceAsync(Device device, CancellationToken cancelToken = default);
    }
}
