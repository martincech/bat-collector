﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using Communication.Contract.Wcf.DataContracts;
using HardwareId;
using HardwareId.Legacy;
using Newtonsoft.Json;
using NLog;
using Repository.Web.Extensions;
using Repository.Web.Model;
using Repository.Web.Model.Migration.v07;
using Settings.Core;
using Settings.DeviceSettings;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Cognito.Service;

namespace Repository.Web.WebApi
{
    public class WebApi : IWebApi
    {
        #region Private fields

        const string AuthBearer = "Bearer";
        const string MediaJson = "application/json";
        const string DefaultScheme = "https://";
        const string StatsPath = "v1/stats";
        const string DevicePath = "v1/device";
        const string SyncDevicePath = DevicePath + "/sync";
        const string MigrationV07Base = "v1/migration/0.7/";
        const string MigrationV07Terminal = MigrationV07Base + "terminal";
        const string MigrationV07Device = MigrationV07Base + "device";
        const string addressSeparator = "/";

        static readonly Logger logger = LogManager.GetCurrentClassLogger();
        static readonly SemaphoreSlim semaphore = new SemaphoreSlim(1);
        readonly ConcurrentDictionary<string, Guid?> deviceMapping;
        readonly IDeviceSettingsManager deviceSettingsManager;

        int communications;
        Url batApi;
        Url sensorsApi;
        string accessToken;
        string refreshToken;
        string idToken;
        string userName;
        CognitoSettings cognitoSettings;
        readonly Guid hwId;
        readonly string legacyHwId;

        #endregion

        public WebApi(Dictionary<string, Guid?> deviceMapping, IDeviceSettingsManager deviceSettingsManager)
        {
            this.deviceSettingsManager = deviceSettingsManager;
            this.deviceMapping = new ConcurrentDictionary<string, Guid?>(deviceMapping);

            hwId = HwId.SystemId;
            legacyHwId = GetLegacyHwId();
        }

        #region Public interfaces

        public bool IsCommunicating { get; private set; }

        public event EventHandler<WebApiConnectionInfo> ConnectivityStatusChanged;

        public WebApiConnectionInfo ConnectionInfo
        {
            get => new WebApiConnectionInfo
            {
                Login = new Login
                {
                    AccessToken = accessToken,
                    RefreshToken = refreshToken,
                    IdToken = idToken,
                    CognitoSettings = cognitoSettings,
                    UserName = userName
                },
                BatApi = batApi,
                SensorsApi = sensorsApi,
                Logged = accessToken == null ? LoggedStatus.UnAuthorized : LoggedStatus.Logged,
            };
        }

        /// <summary>
        /// Login into cloud
        /// </summary>
        /// <param name="info"></param>
        /// <param name="cancelToken"></param>
        /// <returns>True if login has been successful</returns>
        public async Task<bool> LoginAsync(WebApiConnectionInfo info,
            CancellationToken cancelToken = default)
        {
            if (info == null) return false;
            accessToken = info.Login?.AccessToken;
            refreshToken = info.Login?.RefreshToken;
            idToken = info.Login?.IdToken;
            userName = info.Login?.UserName;
            cognitoSettings = info.Login?.CognitoSettings;
            sensorsApi = info.SensorsApi;
            batApi = info.BatApi;
            var result = await ConcurentAuthorizedFuncAsync(SyncTerminalAsync, cancelToken);
            OnConnectivityStatusChanged();
            return result;
        }

        /// <summary>
        /// Send statistic data to cloud
        /// </summary>
        /// <param name="sample"></param>
        /// <param name="cancelToken"></param>
        /// <returns>True if statistic data have been successfully sent</returns>
        public async Task<bool> SyncStatisticAsync(StatisticSample sample, CancellationToken cancelToken = default)
        {
            return await ConcurentAuthorizedFuncAsync(async () =>
            {
                if (sample == null) throw new ArgumentNullException($"{nameof(StatisticSample)} cannot be null");
                if (!deviceMapping.TryGetValue(sample.SensorUid, out var guid) || !guid.HasValue)
                {
                    logger.Warn($"Data from not synchronized device: {sample.SensorUid}");
                    guid = await TrySyncDeviceAsync(sample.SensorUid);
                    if (!guid.HasValue)
                    {
                        return false;
                    }
                }
                return await SyncStatisticInternalAsync(guid.Value, sample, cancelToken);
            }, cancelToken);
        }

        private async Task<Guid?> TrySyncDeviceAsync(string uid)
        {
            logger.Info($"Try sync unknown device: '{uid}'");
            var device = new Device
            {
                Id = null,
                Uid = uid,
                Type = DeviceType.Bat2CableScale,
                ParentId = hwId
            };
            device.Id = await GetMigrationDeviceIdAsync(device.Uid);
            if (device.Id.HasValue && await ExistDeviceAsync(device.Id.Value))
            {
                return device.Id;
            }
            if (!await AddDeviceInternalAsync(device))
            {
                return null;
            }
            UpdateSettings(device);
            return device.Id;
        }


        private async Task<bool> SyncStatisticInternalAsync(Guid guid, StatisticSample sample, CancellationToken cancelToken = default)
        {
            logger.Debug("Web API: Start sync statistics");
            var result = await PostAsJsonAsync(sample.Map(guid), StatsPath, sensorsApi.Value, cancelToken);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                logger.Info($"Sync statistics for: '{sample.SensorUid}' done.");
                return true;
            }
            if (result.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            throw new InvalidOperationException($"Sync statistics for: '{sample.SensorUid}' was failed. Status code : {result.StatusCode} {result.ReasonPhrase}");
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Check if terminal is already created in server,
        /// if not, create it.
        /// </summary>
        /// <returns></returns>
        async Task<bool> SyncTerminalAsync()
        {
            try
            {
                logger.Debug("Start Sync Terminal routine");
                await MigrateTerminalAsync(hwId, legacyHwId);
                await MigrateExistedDevicesAsync();

                var getResponse = await GetAsync(DevicePath + addressSeparator + hwId, batApi.Value);
                if (getResponse.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
                if (getResponse.StatusCode == HttpStatusCode.OK)
                {
                    return true;
                }

                await CreateTerminalAsync(hwId);
                return true;
            }
            catch (UnauthorizedAccessException)
            {   //re-thrown exception for refresh token and repeat query
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Sync Terminal exception");
                return false;
            }
        }

        private async Task MigrateExistedDevicesAsync()
        {
            logger.Debug("Start migrate existed devices");
            if (deviceMapping == null)
            {   //no devices for migration
                return;
            }

            var notSyncDevices = deviceMapping.Where(w => !w.Value.HasValue).ToList();  //creates list, because source dictionary will be updated
            foreach (var map in notSyncDevices)
            {
                var device = new Device
                {
                    Id = null,
                    Uid = map.Key
                };
                device.Id = await GetMigrationDeviceIdAsync(device.Uid);
                if (!device.Id.HasValue)
                {
                    continue;
                }
                UpdateSettings(device);
            }
        }

        private async Task MigrateTerminalAsync(Guid terminalId, string oldId)
        {
            if (string.IsNullOrEmpty(oldId))
            {
                logger.Debug("Terminal has no old HWID, migration is not necessary");
                return;
            }

            var terminal = new Terminal
            {
                Id = terminalId,
                OldId = oldId
            };
            var migrationResult = await PutAsync(terminal, MigrationV07Terminal + addressSeparator + oldId, batApi.Value);
            if (migrationResult.Content == null)
            {
                throw new InvalidOperationException($"Migrate terminal failed. Response content is empty. Code: {migrationResult.StatusCode}");
            }

            var apiResponse = await migrationResult.Content.ReadAsAsync<ApiResponse>();
            if (migrationResult.StatusCode == HttpStatusCode.OK ||
                migrationResult.StatusCode == HttpStatusCode.PreconditionFailed ||
                migrationResult.StatusCode == HttpStatusCode.NotFound)
            {   //migration was success or nothing to migrate
                logger.Info($"Web API: Terminal migration finished with code: {apiResponse.Code}; {apiResponse.Message}");
                return;
            }
            if (migrationResult.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            throw new InvalidOperationException($"Migrate terminal failed: {apiResponse.Code}; {apiResponse.Message}");
        }

        private async Task CreateTerminalAsync(Guid id)
        {
            var terminalDevice = new Device
            {
                Id = id,
                Type = DeviceType.BatCollectorTerminal,
                Name = GetPcNameOrDefault(),
                ParentId = null
            };
            var result = await PostAsJsonAsync(terminalDevice, DevicePath, batApi.Value);
            if (result.StatusCode == HttpStatusCode.Created)
            {
                logger.Info("Web API: Terminal was created");
                return;
            }
            if (result.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            if (result.Content == null)
            {
                throw new InvalidOperationException($"Create terminal failed. Response content is empty. Code: {result.StatusCode}");
            }

            var apiResponse = await result.Content.ReadAsAsync<ApiResponse>();
            throw new InvalidOperationException($"Status code : {apiResponse.Code} {apiResponse.Message}");
        }

        private string GetPcNameOrDefault()
        {
            try
            {
                var name = Environment.MachineName;
                logger.Debug($"PC name was read: '{name}'");
                if (string.IsNullOrEmpty(name))
                {
                    name = hwId.ToString();
                }
                return name;
            }
            catch (Exception e)
            {
                logger.Info(e, "Cannot get PC name");
                return hwId.ToString();
            }
        }

        private static string GetLegacyHwId()
        {
            try
            {
                var hwIdLegacy = new HwIdLegacy(AppFolders.Settings);
                return hwIdLegacy.Generate();
            }
            catch (Exception e)
            {
                logger.Info(e, "Load old HWID exception");
                return string.Empty;
            }
        }


        async Task RefreshTokensAsync()
        {
            logger.Debug("Trying to refresh access token");
            var tokens = await cognitoSettings.Oauth2RefreshTokensAsync(refreshToken);
            if (!tokens.Any())
            {
                logger.Warn("Failed to refresh access token");
                return;
            }
            accessToken = TokenHelper.ParseToken(tokens, TokenHelper.AccessTokenKey);
            idToken = TokenHelper.ParseToken(tokens, TokenHelper.IdTokenKey);
            logger.Debug("Refreshing access token was successful");
            OnConnectivityStatusChanged();
        }

        async Task<HttpResponseMessage> PostAsJsonAsync<T>(T body, string apiPath, string server,
          CancellationToken cancelToken = default)
        {
            return await HttpClientRequestAsync(c => c.PostAsJsonAsync(apiPath, body, cancelToken), server);
        }

        async Task<HttpResponseMessage> GetAsync(string apiPath, string server, CancellationToken cancelToken = default)
        {
            return await HttpClientRequestAsync(c => c.GetAsync(apiPath, cancelToken), server);
        }

        async Task<HttpResponseMessage> DeleteAsync(string apiPath, string server, CancellationToken cancelToken = default)
        {
            return await HttpClientRequestAsync(c => c.DeleteAsync(apiPath, cancelToken), server);
        }

        async Task<HttpResponseMessage> PutAsync<T>(T body, string apiPath, string server,
          CancellationToken cancelToken = default)
        {
            return await HttpClientRequestAsync(c => c.PutAsJsonAsync(apiPath, body, cancelToken), server);
        }


        private async Task<HttpResponseMessage> HttpClientRequestAsync(Func<HttpClient, Task<HttpResponseMessage>> func, string server)
        {
            var uri = CheckServerAddress(server);
            if (uri == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            using (var client = new HttpClient())
            {
                CreateHeader(client, uri);
                try
                {
                    AddCommunication();
                    var res = await func?.Invoke(client);
                    return res;
                }
                catch (Exception ex)
                {
                    logger.Warn(ex, "HttpClientRequestAsync error!");
                    return new HttpResponseMessage(HttpStatusCode.ServiceUnavailable);
                }
                finally
                {
                    FinishCommunication();
                }
            }
        }

        private void AddCommunication()
        {
            Interlocked.Increment(ref communications);
            IsCommunicating = communications == 0;
        }

        private void FinishCommunication()
        {
            IsCommunicating = communications != 1;
            Interlocked.Decrement(ref communications);
        }

        void CreateHeader(HttpClient client, Uri uri)
        {
            client.BaseAddress = uri;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaJson));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(AuthBearer, accessToken);
        }

        private static Uri CheckServerAddress(string address)
        {
            address = address.EndsWith(addressSeparator) ? address : address + addressSeparator;
            if (!Uri.IsWellFormedUriString(address, UriKind.RelativeOrAbsolute))
            {
                return null;
            }
            Uri uri;
            try
            {
                uri = new Uri(address);
            }
            catch (Exception)
            {
                uri = null;
            }

            if (uri != null && Uri.CheckSchemeName(uri.Scheme) &&
                (uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps))
            {
                return uri;
            }

            // try to add HTTP in front of address
            address = DefaultScheme + address;
            try
            {
                uri = new Uri(address);
            }
            catch (Exception)
            {
                if (uri == null || !Uri.CheckSchemeName(uri.Scheme) || uri.Scheme != Uri.UriSchemeHttps ||
                    uri.Scheme != Uri.UriSchemeHttp)
                {
                    return null;
                }
            }

            return uri;
        }

        async Task<T> ConcurentAuthorizedFuncAsync<T>(Func<Task<T>> func, CancellationToken cancelToken)
        {
            try
            {
                await semaphore.WaitAsync(cancelToken);
                return await AuthorizedActionAsync(func);
            }
            catch (UnauthorizedAccessException e)
            {
                logger.Debug(e);
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
            finally
            {
                semaphore.Release();
            }
            return default;
        }

        async Task<T> AuthorizedActionAsync<T>(Func<Task<T>> function)
        {
            if (string.IsNullOrEmpty(refreshToken)) return default;
            try
            {
                if (string.IsNullOrEmpty(accessToken)) await RefreshTokensAsync();
                try
                {
                    return await function?.Invoke();
                }
                catch (UnauthorizedAccessException)
                {
                    await RefreshTokensAsync();
                    return await function?.Invoke();
                }
            }
            catch (UnauthorizedAccessException e)
            {
                logger.Warn(e, "Unauthorized exception, tokens are deleted");
                accessToken = null;
                refreshToken = null;
                throw;
            }
        }

        protected virtual void OnConnectivityStatusChanged()
        {
            ConnectivityStatusChanged?.Invoke(this, ConnectionInfo);
        }

        public async Task<bool> AddDeviceAsync(Device device, CancellationToken cancelToken = default)
        {
            return await ConcurentAuthorizedFuncAsync(async () =>
            {
                if (deviceMapping.TryGetValue(device.Uid, out var guid) && guid.HasValue)
                {   //device was use in history, use the same ID
                    device.Id = guid;
                }
                if (!device.Id.HasValue)
                {
                    device.Id = await GetMigrationDeviceIdAsync(device.Uid);
                }
                if (device.Id.HasValue && await ExistDeviceAsync(device.Id.Value))
                {
                    return true;
                }
                if (!await AddDeviceInternalAsync(device))
                {
                    return false;
                }

                UpdateSettings(device);
                return true;
            }, cancelToken);
        }

        private void UpdateSettings(Device device)
        {
            if (!deviceSettingsManager.UpdateDevice(device.Uid, null, DateTime.Now, device.Id))
            {
                logger.Debug($"Update device settings for: '{device.Uid}' failed.");
            }
            deviceMapping.AddOrUpdate(device.Uid, device.Id, (k, v) => device.Id);
        }

        private async Task<bool> ExistDeviceAsync(Guid id)
        {
            var getResponse = await GetAsync(DevicePath + addressSeparator + id, batApi.Value);
            if (getResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            return getResponse.StatusCode == HttpStatusCode.OK;
        }

        private async Task<Guid?> GetMigrationDeviceIdAsync(string deviceUid)
        {
            if (string.IsNullOrEmpty(legacyHwId))
            {
                logger.Debug("No old HWID, device can't be migrate, because it is use new format of ID.");
                return null;
            }

            var fullDeviceOldAddress = $"{deviceUid}-T-{legacyHwId}";
            var migration = await GetAsync(MigrationV07Device + addressSeparator + fullDeviceOldAddress, batApi.Value);
            if (migration.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            if (migration.StatusCode == HttpStatusCode.NotFound || migration.StatusCode == HttpStatusCode.BadRequest || migration.Content == null)
            {
                logger.Debug($"Device: '{deviceUid}' migration failed. Status code: '{migration.StatusCode}'");
                return null;
            }

            var apiResponse = await migration.Content.ReadAsAsync<ApiResponse>();
            if (migration.StatusCode == HttpStatusCode.OK)
            {
                if (apiResponse.Data == null)
                {
                    throw new InvalidOperationException($"Response on device '{deviceUid}' migration contains no data!");
                }
                var id = GetGuidFromResponse(apiResponse.Data);
                logger.Debug($"Web API: '{deviceUid}' Device migration success, returned ID: '{id}'");
                return id;
            }
            throw new InvalidOperationException($"Device '{deviceUid}' migration failed: {apiResponse.Code}; {apiResponse.Message}");
        }

        private static Guid? GetGuidFromResponse(object data)
        {
            try
            {
                return Guid.Parse(data.ToString());
            }
            catch (Exception e)
            {
                logger.Debug(e, $"Get GUID from API response failed for input: {data}");
                return null;
            }
        }


        private async Task<bool> AddDeviceInternalAsync(Device device)
        {
            var response = await PostAsJsonAsync(device, DevicePath, batApi.Value);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException("Update device");
            }
            if (response.Content == null)
            {
                logger.Warn($"Add device failed. Response content is empty. Code: {response.StatusCode}");
                return false;
            }

            var apiResponse = await response.Content.ReadAsAsync<ApiResponse>();
            if (response.StatusCode == HttpStatusCode.Created)
            {   //get created object ID
                if (apiResponse.Data == null)
                {
                    throw new InvalidOperationException("Response on create new device contains no data!");
                }
                var data = JsonConvert.DeserializeObject<Device>(apiResponse.Data.ToString());
                device.Id = data.Id;
                logger.Info($"Web API: Device: '{device.Uid}' was added. Received GUID: {data.Id}");
                return true;
            }

            logger.Warn($"Add device unexpected response. Status code : {apiResponse.Code}, {apiResponse.Message}");
            return false;
        }

        public async Task<bool> DeleteDeviceAsync(string deviceUid, CancellationToken cancelToken = default)
        {
            if (!deviceMapping.TryGetValue(deviceUid, out var guid) || !guid.HasValue)
            {
                logger.Warn($"Deleting of unknown device: '{deviceUid}'");
                return false;
            }
            return await ConcurentAuthorizedFuncAsync(() => DeleteDeviceInternalAsync(guid.Value), cancelToken);
        }

        private async Task<bool> DeleteDeviceInternalAsync(Guid id)
        {
            if (!IsDeviceDeleted(id))
            {
                logger.Warn($"Delete device - device ID:{id} is not deleted from DeviceSettings.");
                return true;
            }

            var response = await DeleteAsync(DevicePath + addressSeparator + id, batApi.Value);
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                logger.Warn($"Delete device - device not found.");
                return true;
            }
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException("Delete device");
            }
            if (response.Content == null)
            {
                logger.Warn($"Delete device failed. Response content is empty. Code: {response.StatusCode}");
                return false;
            }
            if (response.StatusCode == HttpStatusCode.OK)
            {
                logger.Info($"Web API: Device: '{id}' was deleted");
                return true;
            }

            var apiResponse = await response.Content.ReadAsAsync<ApiResponse>();
            logger.Warn($"Delete device unexpected response. Status code: {apiResponse.Code}, {apiResponse.Message}");
            return false;
        }

        private bool IsDeviceDeleted(Guid id)
        {
            return deviceSettingsManager
                .LoadDevices(true)
                .EthernetSources?
                .SelectMany(s => s.Devices)
                .FirstOrDefault(f => f.Guid == id) == null;
        }


        public async Task<bool> UpdateDeviceAsync(Device device, CancellationToken cancelToken = default)
        {
            if (!deviceMapping.TryGetValue(device.Uid, out var guid) || !guid.HasValue)
            {
                logger.Warn($"Updating of unknown device: '{device.Uid}'");
                return false;
            }

            device.Id = guid;
            return await ConcurentAuthorizedFuncAsync(() => UpdateDeviceInternalAsync(device), cancelToken);
        }

        private async Task<bool> UpdateDeviceInternalAsync(Device device)
        {
            var response = await PutAsync(device, SyncDevicePath + addressSeparator + device.Id.Value, batApi.Value);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException("Update device");
            }
            if (response.Content == null)
            {
                logger.Warn($"Update device failed. Response content is empty. Code: {response.StatusCode}");
                return false;
            }
            if (response.StatusCode == HttpStatusCode.OK)
            {
                logger.Info($"Web API: Device: '{device.Uid}' was updated");
                return true;
            }

            var apiResponse = await response.Content.ReadAsAsync<ApiResponse>();
            logger.Warn($"Update device unexpected response. Status code : {apiResponse.Code}, {apiResponse.Message}");
            return false;
        }

        #endregion
    }
}
