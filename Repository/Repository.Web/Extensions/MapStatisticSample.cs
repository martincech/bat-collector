﻿using System;
using System.Collections.Generic;
using Veit.Bat.Common.Sample.Statistics;
using Veit.Bat.Common.Schema;
using Veit.Bat.Common.Units;
using Veit.Bat.Common.Units.Conversion;

namespace Repository.Web.Extensions
{
    public static class MapStatisticSample
    {
        public static StatsBody Map(this StatisticSample sample, Guid guid)
        {
            if (sample is null) return null;
            var statsBody = new StatsBody
            {
                DateTime = DateTime.Now,
                Version = 1,
                Scales = new List<Scale>
                {
                   new Scale {
                       Day = sample.DayNumber,
                       DisplayName = sample.ScaleName ?? sample.ScaleNumber.ToString(),
                       Id = guid,
                       Flock = new Flock {
                          Females = sample.FemaleData.Map(),
                          Males = sample.MaleData.Map(),
                       }
                   }
                }
            };
            return statsBody;
        }

        public static SexStats Map(this StatData stats)
        {
            if (stats is null) return null;
            return new SexStats
            {
                Stats = new List<DailyStats> {
                    new DailyStats {
                        Day = stats.DayNumber,
                        Avg = (int)Math.Round(ConvertWeight.Convert(stats.Average, WeightUnits.KG, WeightUnits.G)),
                        Count = stats.Count,
                        Cv = stats.Cv,
                        DateTime = stats.DateOfTheDay,
                        Gain = (int)Math.Round(ConvertWeight.Convert(stats.Gain, WeightUnits.KG, WeightUnits.G)),
                        Sig = (int)Math.Round(ConvertWeight.Convert(stats.Sigma, WeightUnits.KG, WeightUnits.G)),
                        Uniformity = stats.Uniformity,
                    }
                }
            };
        }

    }
}
