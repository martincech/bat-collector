﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Settings.DataStorageSettings;
using Settings.DeviceSettings;
using Veit.Bat.Common.Sample.Statistics;
using Xunit;

namespace Repository.Csv.Tests
{
    public class CsvFileTests : IDisposable
    {
        #region Private fields

        private readonly LocalCsvRepositorySettings csvSettings;
        private readonly CsvFile engine;
        private const string TestFolder = "test_logs";
        private readonly string TestFile = Path.Combine(TestFolder, "testStatFile.csv");

        private const string DemoScaleName = "1101";
        private const string DemoScaleAddress = "192.168.1.250:10001-1";
        private const string DemoScaleUid = DemoScaleAddress + "-T-1234-5678-90ab-0def";

        private const string CsvDelimiter = ";";

        #endregion

        public CsvFileTests()
        {
            csvSettings = new LocalCsvRepositorySettings
            {
                Csv = { Path = TestFolder },
            };
            engine = new CsvFile(csvSettings);

            if (!Directory.Exists(TestFolder))
            {
                Directory.CreateDirectory(TestFolder);
            }
        }

        public void Dispose()
        {
            if (Directory.Exists(TestFolder))
            {
                Directory.Delete(TestFolder, true);
            }
            GC.SuppressFinalize(this);
        }


        [Fact]
        public void AddStatistics_FromOneSameDay()
        {
            // Arrange
            var data = GetDemoStatsForSameDay();

            // Act
            foreach (var stat in data)
            {
                engine.UpdateFile(TestFile, stat, null);
            }

            // Assert
            CheckFile(TestFile, new List<StatisticSample> { data.Last() });
        }

        [Fact]
        public void AddStatistics_FromDifferentDays()
        {
            // Arrange
            var data = GetDemoStatsForDifferentDays();

            // Act
            foreach (var stat in data)
            {
                engine.UpdateFile(TestFile, stat, null);
            }

            // Assert
            CheckFile(TestFile, data);
        }

        [Fact]
        public void AddStatistics_FromDifferentAndSameDaysTogether()
        {
            // Arrange
            var expectedData = GetDemoStatsForDifferentDays();
            var mixedData = expectedData.ToList();
            var day1Stat = CopyStatistic(mixedData[1]);
            day1Stat.FemaleData.Count += 100;
            mixedData.Add(day1Stat);
            var day2Stat = CopyStatistic(mixedData[2]);
            day2Stat.FemaleData.Count += 222;
            mixedData.Add(day2Stat);

            // Act
            foreach (var stat in mixedData)
            {
                engine.UpdateFile(TestFile, stat, null);
            }

            // Assert
            CheckFile(TestFile, expectedData);
        }

        [Fact]
        public void UseWeighingInfo_SaveOnlydataFromActualFlock()
        {
            // Arrange
            var demoData = GetDemoStatsForDifferentDays();
            var day1TimeStamp = demoData.ElementAt(1).TimeStamp;
            var day2Stat = CopyStatistic(demoData.ElementAt(2));
            day2Stat.FemaleData.Count += 222;
            var expectedData = new List<StatisticSample>
            {
                demoData.ElementAt(1),
                day2Stat
            };
            var info = new WeighingInfoSample
            {
                SensorUid = DemoScaleUid,
                Unit = Veit.Bat.Common.Units.WeightUnits.KG,
                Start = day1TimeStamp
            };

            //insert demo data without weighing info
            foreach (var item in demoData)
            {
                engine.UpdateFile(TestFile, item, null);
            }

            // Act
            //add new statistic with weighing info => data from out actual flock will be removed
            engine.UpdateFile(TestFile, day2Stat, info);

            // Assert
            CheckFile(TestFile, expectedData);
        }

        [Fact]
        public void UseWeighingInfo_StartFlockOnOldData()
        {
            // Arrange
            var demoData = GetDemoStatsForDifferentDays();
            var newDayStat = CopyStatistic(demoData.ElementAt(2));
            newDayStat.FemaleData.Count += 222;
            newDayStat.TimeStamp = newDayStat.TimeStamp.AddDays(1);

            var info = new WeighingInfoSample
            {
                SensorUid = DemoScaleUid,
                Unit = Veit.Bat.Common.Units.WeightUnits.KG,
                Start = newDayStat.TimeStamp
            };

            //insert demo data without weighing info
            foreach (var item in demoData)
            {
                engine.UpdateFile(TestFile, item, null);
            }

            // Act
            engine.UpdateFile(TestFile, newDayStat, info);  //new data from new flock

            // Assert
            CheckFile(TestFile, new List<StatisticSample> { newDayStat });
        }


        private static StatisticSample CopyStatistic(StatisticSample data)
        {
            if (data == null)
            {
                return null;
            }

            return new StatisticSample
            {
                PhoneNumber = data.PhoneNumber,
                ScaleNumber = data.ScaleNumber,
                ScaleName = data.ScaleName,
                DayNumber = data.DayNumber,
                SensorUid = data.SensorUid,
                TimeStamp = data.TimeStamp,
                MaleData = CopyStatisticData(data.MaleData),
                FemaleData = CopyStatisticData(data.FemaleData)
            };
        }

        private static StatData CopyStatisticData(StatData data)
        {
            if (data == null)
            {
                return null;
            }
            return new StatData
            {
                Gain = data.Gain,
                DateOfTheDay = data.DateOfTheDay,
                DayNumber = data.DayNumber,
                Target = data.Target,
                Count = data.Count,
                Average = data.Average,
                LastAverage = data.LastAverage,
                Sigma = data.Sigma,
                Uniformity = data.Uniformity,
                Cv = data.Cv
            };
        }

        private static void CheckFile(string testFile, IEnumerable<StatisticSample> data)
        {
            var actual = ParseStatistics(testFile);
            Assert.NotNull(actual);
            Assert.Equal(data.Count(), actual.Count());

            data = data.OrderBy(o => o.TimeStamp);
            actual = actual.OrderBy(o => o.DateTime);

            for (var i = 0; i < data.Count(); i++)
            {   //Check only time stamps not whole statistic
                Assert.Equal(data.ElementAt(i).TimeStamp, actual.ElementAt(i).DateTime);
            }
        }


        private static IEnumerable<Statistics> ParseStatistics(string file)
        {
            using (var reader = new StreamReader(file))
            using (var csv = new CsvReader(reader))
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = CsvDelimiter;
                var records = csv.GetRecords<Statistics>().ToList();
                if (records == null)
                {
                    return new List<Statistics>();
                }
                return records;
            }
        }


        #region Demo data

        private static IEnumerable<StatisticSample> GetDemoStatsForSameDay()
        {
            var now = DateTimeOffset.Now;
            var actualDate = new DateTimeOffset(new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0));
            return new List<StatisticSample>
            {
                new StatisticSample
                {
                    DayNumber = 1,
                    ScaleName = DemoScaleName,
                    PhoneNumber = DemoScaleAddress,
                    SensorUid = DemoScaleUid,
                    TimeStamp = actualDate.AddHours(-4),
                    FemaleData = new StatData
                    {
                        DayNumber = 1,
                        DateOfTheDay = actualDate.AddHours(-4),
                        Average = 0.45f,
                        Count = 54,
                        Cv = 2,
                        Gain = -0.010f,
                        Target = 0.5f,
                        Sigma = 0.0f,
                        Uniformity = 98
                    }
                },
                new StatisticSample
                {
                    DayNumber = 1,
                    ScaleName = DemoScaleName,
                    PhoneNumber = DemoScaleAddress,
                    SensorUid = DemoScaleUid,
                    TimeStamp = actualDate.AddHours(-2),
                    FemaleData = new StatData
                    {
                        DayNumber = 1,
                        DateOfTheDay = actualDate.AddHours(-2),
                        Average = 0.492f,
                        Count = 201,
                        Cv = 2,
                        Gain = -0.010f,
                        Target = 0.5f,
                        Sigma = 0.01f,
                        Uniformity = 99
                    }
                },
                new StatisticSample
                {
                    DayNumber = 1,
                    ScaleName = DemoScaleName,
                    PhoneNumber = DemoScaleAddress,
                    SensorUid = DemoScaleUid,
                    TimeStamp = actualDate,
                    FemaleData = new StatData
                    {
                        DayNumber = 1,
                        DateOfTheDay = actualDate,
                        Average = 0.543f,
                        Count = 400,
                        Cv = 2,
                        Gain = -0.010f,
                        Target = 0.5f,
                        Sigma = 0.053f,
                        Uniformity = 100
                    }
                }
            };
        }


        private static IEnumerable<StatisticSample> GetDemoStatsForDifferentDays()
        {
            var now = DateTimeOffset.Now;
            var actualDate = new DateTimeOffset(new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0));
            var endOfActualDateOfTheDay = new DateTimeOffset(new DateTime(actualDate.Year, actualDate.Month, actualDate.Day, 23, 59, 0));

            return new List<StatisticSample>
            {
                new StatisticSample
                {
                    DayNumber = 1,
                    ScaleName = DemoScaleName,
                    PhoneNumber = DemoScaleAddress,
                    SensorUid = DemoScaleUid,
                    TimeStamp = endOfActualDateOfTheDay.AddDays(-2),
                    FemaleData = new StatData
                    {
                        DayNumber = 1,
                        DateOfTheDay = endOfActualDateOfTheDay.AddDays(-2),
                        Average = 0.45f,
                        Count = 1054,
                        Cv = 2,
                        Gain = -0.010f,
                        Target = 0.5f,
                        Uniformity = 98,
                        Sigma = 0.00707f
                    }
                },
                new StatisticSample
                {
                    DayNumber = 2,
                    ScaleName = DemoScaleName,
                    PhoneNumber = DemoScaleAddress,
                    SensorUid = DemoScaleUid,
                    TimeStamp = endOfActualDateOfTheDay.AddDays(-1),
                    FemaleData = new StatData
                    {
                        DayNumber = 2,
                        DateOfTheDay = endOfActualDateOfTheDay.AddDays(-1),
                        Average = 0.57f,
                        Count = 994,
                        Cv = 2,
                        Gain = 0.018f,
                        Target = 0.55f,
                        Uniformity = 100,
                        Sigma = 0.025f
                    }
                },
                new StatisticSample
                {
                    DayNumber = 3,
                    ScaleName = DemoScaleName,
                    PhoneNumber = DemoScaleAddress,
                    SensorUid = DemoScaleUid,
                    TimeStamp = actualDate,
                    FemaleData = new StatData
                    {
                        DayNumber = 3,
                        DateOfTheDay = actualDate,
                        Average = 0.599f,
                        Count = 600,
                        Cv = 2,
                        Gain = 0.024f,
                        Target = 0.6f,
                        Uniformity = 99,
                        Sigma = 0.005f
                    }
                }
            };
        }

        #endregion
    }
}
