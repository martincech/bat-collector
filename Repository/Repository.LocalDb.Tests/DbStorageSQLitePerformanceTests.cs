﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Xunit;

namespace Repository.LocalDb.Tests
{
    /// <summary>
    /// Simple performance test. Because test is time consuming it is disabled by default (To enable test remove [Ignore] attribute)
    /// </summary>
    public class DbStorageSQLitePerformanceTests : IDisposable
    {
        private readonly List<MockItem> items;
        private readonly string databaseFile;
        private readonly DbStorageSQLite dbStorage;
        private readonly Stopwatch watch;

        public DbStorageSQLitePerformanceTests()
        {
            const int itemsCount = 50000;
            items = MockItemFactory.Create(itemsCount).ToList();
            databaseFile = Guid.NewGuid() + ".sqlite";
            dbStorage = DbStorageSQLite.GetDatabase(databaseFile);
            watch = new Stopwatch();
        }

        #region Additional test attributes

        public void Dispose()
        {
            if (File.Exists(databaseFile)) File.Delete(databaseFile);
            GC.SuppressFinalize(this);
        }

        #endregion

        [Fact]
        public void TestSequence()
        {
            Insert();
            Select();
            Delete();
        }


        private void Insert()
        {
            Start();

            var result = dbStorage.Insert(items);

            Stop("INSERT");
            Assert.True(result);
        }


        private void Select()
        {
            Start();

            var loadedItems = dbStorage.SelectAll();

            Stop("SELECT");
            Assert.Equal(items.Count, loadedItems.Count());
        }


        private void Delete()
        {
            Start();

            var result = dbStorage.Delete(items);

            Stop("DELETE");
            Assert.True(result);
        }

        private void Start()
        {
            watch.Start();
        }

        private void Stop(string testName)
        {
            watch.Stop();
            Trace.WriteLine(testName + " - " + items.Count + " records : " + watch.Elapsed.TotalMilliseconds + " ms");
        }
    }
}
