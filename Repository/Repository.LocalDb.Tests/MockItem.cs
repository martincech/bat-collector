﻿using System.Collections.Generic;
using System.Linq;

namespace Repository.LocalDb.Tests
{
    public class MockItem
    {
        public int Id { get; set; }
    }

    public class MockItemFactory
    {
        public static IEnumerable<MockItem> Create(int count)
        {
            if (count < 0) return new List<MockItem>();
            return Enumerable.Range(0, count).Select(s => new MockItem { Id = s });
        }
    }
}
