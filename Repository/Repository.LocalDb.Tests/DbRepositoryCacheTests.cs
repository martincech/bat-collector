﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Utilities.Timer;
using Xunit;

namespace Repository.LocalDb.Tests
{
    /// <summary>
    /// Summary description for DbRepositoryCacheTests
    /// </summary>
    public class DbRepositoryCacheTests
    {
        private readonly TimeSpan flushPeriod = TimeSpan.FromMilliseconds(50);
        private readonly List<MockItem> items;
        private readonly Mock<ITimer> mockTimer;

        private readonly IDbStorage mockDbStorageObject;
        private DbRepositoryCache dbRepositoryCache;
        private readonly Mock<IDbStorage> mockDbStorage;


        public DbRepositoryCacheTests()
        {
            var collection = new List<object>();

            mockDbStorage = new Mock<IDbStorage>();
            mockDbStorage.Setup(s => s.Delete(It.IsAny<IEnumerable<object>>())).Callback<IEnumerable<object>>(toDelete => { collection.RemoveAll(toDelete.Contains); }).Returns(true);
            mockDbStorage.Setup(s => s.Insert(It.IsAny<IEnumerable<object>>())).Callback<IEnumerable<object>>(collection.AddRange).Returns(true);
            mockDbStorage.Setup(s => s.SelectAll()).Returns(collection);
            mockDbStorage.Setup(s => s.Clear()).Callback(collection.Clear).Returns(true);

            mockTimer = new Mock<ITimer>();
            mockDbStorageObject = mockDbStorage.Object;
            items = MockItemFactory.Create(5).ToList();
        }

        private void CreaDbRepositoryCache()
        {
            dbRepositoryCache = new DbRepositoryCache(mockDbStorageObject, flushPeriod, () => mockTimer.Object);
        }

        [Fact]
        public void ThrowExeption_WhenCreateInstanceWithNullIDdbStorage()
        {
            void act() => dbRepositoryCache = new DbRepositoryCache(null, flushPeriod, () => mockTimer.Object);
            Assert.Throws<ArgumentNullException>((Action)act);
        }

        [Fact]
        public void Timer_IsTimed_ForCreatedObject()
        {
            double setPeriod = 0;
            mockTimer.SetupSet<double>(s => s.Interval = It.IsAny<double>())
               .Callback(d => setPeriod = d);
            dbRepositoryCache = new DbRepositoryCache(mockDbStorageObject, flushPeriod, () => mockTimer.Object);

            mockTimer.Verify(s => s.Start(), Times.Once);
            mockTimer.VerifySet(s => s.Interval = setPeriod, Times.Once());
            Assert.Equal(flushPeriod.TotalMilliseconds, setPeriod);
        }

        [Fact]
        public void TimerRestarted_WhenElapsed()
        {

            mockTimer.Verify(s => s.Stop(), Times.Never);
            dbRepositoryCache = new DbRepositoryCache(mockDbStorageObject, flushPeriod, () => mockTimer.Object);
            mockTimer.Raise(t => t.Elapsed += null, mockTimer.Object, null);
            mockTimer.Verify(s => s.Stop(), Times.Once);
            mockTimer.Verify(s => s.Start(), Times.Exactly(2));
        }

        [Fact]
        public void Saved_WhenFlushInvoked()
        {
            CreaDbRepositoryCache();
            var item = items.First();

            dbRepositoryCache.Save(item);
            InvokeElapsed();

            Assert.Single(mockDbStorageObject.SelectAll());
            Assert.Equal(item, mockDbStorageObject.SelectAll().FirstOrDefault());
        }

        private void InvokeElapsed()
        {
            mockTimer.Raise(t => t.Elapsed += null, mockTimer.Object, null);
        }

        [Fact]
        public void SavedCollection_WhenFlushInvoked()
        {
            CreaDbRepositoryCache();

            foreach (var item in items)
            {
                dbRepositoryCache.Save(item);
            }
            InvokeElapsed();

            mockDbStorage.Verify(v => v.Insert(It.IsAny<IEnumerable<object>>()), Times.Once);
            Assert.Equal(items.Count, mockDbStorageObject.SelectAll().Count());
            Assert.Equal(items, mockDbStorageObject.SelectAll().ToList());
        }

        [Fact]
        public void Deleted_WhenFlushInvoked()
        {
            CreaDbRepositoryCache();
            var item = items.First();
            mockDbStorageObject.Insert(new object[] { item });
            Assert.Equal(item, mockDbStorageObject.SelectAll().FirstOrDefault());

            dbRepositoryCache.Delete(item);
            InvokeElapsed();

            mockDbStorage.Verify(v => v.Delete(It.IsAny<IEnumerable<object>>()), Times.Once);
            Assert.Empty(mockDbStorageObject.SelectAll());
        }

        [Fact]
        public void DeletedCollection_WhenFlushInvoked()
        {
            CreaDbRepositoryCache();
            mockDbStorageObject.Insert(items);
            Assert.Equal(items, mockDbStorageObject.SelectAll().ToList());

            foreach (var item in items)
            {
                dbRepositoryCache.Delete(item);
            }
            InvokeElapsed();

            mockDbStorage.Verify(v => v.Insert(It.IsAny<IEnumerable<object>>()), Times.Once);
            Assert.Empty(mockDbStorageObject.SelectAll());
        }

        [Fact]
        public void Loaded_AfterFlushInvoked()
        {
            CreaDbRepositoryCache();
            SaveToCache(new[] { items[0], items[1], items[2] });
            DeleteFromCache(new[] { items[0] });
            InvokeElapsed();
            SaveToCache(new[] { items[3], items[4] });
            DeleteFromCache(new[] { items[1], items[3] });
            InvokeElapsed();

            var loaded = dbRepositoryCache.Load<object>().ToList();


            mockDbStorage.Verify(v => v.Insert(It.IsAny<IEnumerable<object>>()), Times.Exactly(2));
            mockDbStorage.Verify(v => v.Delete(It.IsAny<IEnumerable<object>>()), Times.Once);
            mockDbStorage.Verify(v => v.SelectAll(), Times.Once);
            Assert.Equal(2, loaded.Count());
            Assert.Equal(new[] { items[2], items[4] }, loaded);
        }

        [Fact]
        public void Loaded_BeforeFlushInvoked()
        {
            CreaDbRepositoryCache();
            SaveToCache(new[] { items[0], items[1], items[2] });
            DeleteFromCache(new[] { items[0] });
            InvokeElapsed();
            SaveToCache(new[] { items[3], items[4] });
            DeleteFromCache(new[] { items[1], items[3] });

            var loaded = dbRepositoryCache.Load<object>().ToList();

            mockDbStorage.Verify(v => v.Insert(It.IsAny<IEnumerable<object>>()), Times.Exactly(2));
            mockDbStorage.Verify(v => v.Delete(It.IsAny<IEnumerable<object>>()), Times.Once);
            mockDbStorage.Verify(v => v.SelectAll(), Times.Once);
            Assert.Equal(2, loaded.Count);
            Assert.Equal(new[] { items[2], items[4] }, loaded);
        }

        private void SaveToCache(IEnumerable<object> objects)
        {
            foreach (var o in objects)
            {
                dbRepositoryCache.Save(o);
            }
        }

        private void DeleteFromCache(IEnumerable<object> objects)
        {
            foreach (var o in objects)
            {
                dbRepositoryCache.Delete(o);
            }
        }
    }
}
