﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xunit;

namespace Repository.LocalDb.Tests
{
    public class DbStorageSQLiteTests : IDisposable
    {
        private const string DB_SUFFIX = ".sqlite";
        private readonly string databaseFile;
        private readonly List<MockItem> items;
        private DbStorageSQLite dbStorage;

        public DbStorageSQLiteTests()
        {
            items = MockItemFactory.Create(5).ToList();
            databaseFile = Guid.NewGuid() + DB_SUFFIX;
        }

        public void Dispose()
        {
            DeleteDatabase();
            GC.SuppressFinalize(this);
        }

        [Fact]
        public void GetDatatabase_IfNotExist()
        {
            Assert.False(DabaseFileExist());
            var db = DbStorageSQLite.GetDatabase(databaseFile);
            Assert.NotNull(db);
            Assert.True(DabaseFileExist());
        }

        [Fact]
        public void GetDatatabase_IfExist()
        {
            CreateDatabase();
            Assert.True(DabaseFileExist());
            var db = DbStorageSQLite.GetDatabase(databaseFile);
            Assert.NotNull(db);
            Assert.Equal(dbStorage, db);
            Assert.True(DabaseFileExist());
        }

        [Fact]
        public void Insert_WhenDatabaseExist()
        {
            CreateDatabase();
            dbStorage.Insert(items.Take(2));
            var loadedItems = dbStorage.SelectAll().ToList();
            DataInCollectionsEqual(items.Take(2).ToList(), loadedItems);
        }

        [Fact]
        public void Insert_WhenDatabaseNotExist()
        {
            CreateDatabase();
            DeleteDatabase();
            dbStorage.Insert(items.Take(2));
            var loadedItems = dbStorage.SelectAll().ToList();
            DataInCollectionsEqual(items.Take(2).ToList(), loadedItems);
        }

        [Fact]
        public void Delete_WhenDatabaseExist()
        {
            CreateDatabaseWithData(items);
            dbStorage.Delete(items.Skip(2));
            var loadedItems = dbStorage.SelectAll().ToList();
            DataInCollectionsEqual(items.Take(2).ToList(), loadedItems);
        }


        [Fact]
        public void Delete_WhenDatabaseNotExist()
        {
            CreateDatabaseWithData(items);
            DeleteDatabase();
            dbStorage.Delete(items.Skip(2));
            var loadedItems = dbStorage.SelectAll().ToList();
            DataInCollectionsEqual(new List<MockItem>(), loadedItems);
        }

        [Fact]
        public void SelectAll_WhenNumberOfStoredRecords_Exceed_MaxRecordsLoaded_Settings()
        {
            dbStorage = DbStorageSQLite.GetDatabase(databaseFile, 2);
            dbStorage.Insert(items);
            var loadedItems = dbStorage.SelectAll().ToList();
            DataInCollectionsEqual(items, loadedItems);
        }

        [Fact]
        public void Insert_Parallel()
        {
            CreateDatabase();

            Parallel.ForEach(items, item => dbStorage.Insert(new[] { item }));

            var loadedItems = dbStorage.SelectAll().ToList();
            DataInCollectionsEqual(items, loadedItems);
        }

        [Fact]
        public void Delete_Parallel()
        {
            CreateDatabaseWithData(items);

            Parallel.ForEach(items, item => dbStorage.Delete(new[] { item }));

            var loadedItems = dbStorage.SelectAll().ToList();
            Assert.Empty(loadedItems);
        }

        [Fact]
        public void Clear_WhenDatabaseExist()
        {
            CreateDatabaseWithData(items);

            var result = dbStorage.Clear();

            var loadedItems = dbStorage.SelectAll().ToList();
            Assert.True(result);
            Assert.Empty(loadedItems);
        }


        [Fact]
        public void Clear_WhenDatabaseNotExist()
        {
            CreateDatabase();
            File.Delete(databaseFile);
            var result = dbStorage.Clear();

            var loadedItems = dbStorage.SelectAll().ToList();
            Assert.True(result);
            Assert.Empty(loadedItems);
        }

        [Fact]
        public void Clear_WhenDatabaseFileIsLocked()
        {
            CreateDatabaseWithData(items);

            using (File.Open(databaseFile, FileMode.Open))
            {
                var result = dbStorage.Clear();
                Assert.False(result);
            }
        }

        private void CreateDatabaseWithData(List<MockItem> insertData)
        {
            CreateDatabase();
            InsertData(insertData);
        }

        private void CreateDatabase()
        {
            dbStorage = DbStorageSQLite.GetDatabase(databaseFile);
        }

        private void InsertData(List<MockItem> insertData)
        {
            dbStorage.Insert(insertData);
        }

        private static void DataInCollectionsEqual(IEnumerable<object> expected, IEnumerable<object> actual)
        {
            var jsonExpexted = expected.Select(JsonConvert.SerializeObject).ToList();
            var jsonActual = actual.Select(JsonConvert.SerializeObject).ToList();
            Assert.Empty(jsonExpexted.Except(jsonActual));
        }

        private bool DabaseFileExist()
        {
            return File.Exists(databaseFile);
        }

        private void DeleteDatabase()
        {
            if (DabaseFileExist()) File.Delete(databaseFile);
        }
    }
}
