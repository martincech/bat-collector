﻿using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts.Device;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract]
    public interface IScaleFlockCallback
    {
        [OperationContract(IsOneWay = true)]
        void DeviceFlock(string portId, string deviceAddress, Flock flock);
    }
}
