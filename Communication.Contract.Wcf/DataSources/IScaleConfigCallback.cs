﻿using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract]
    public interface IScaleConfigCallback
    {
        [OperationContract(IsOneWay = true)]
        void DeviceNameRead(string portId, string deviceAddress, string name);

        [OperationContract(IsOneWay = true)]
        void DeviceVersion(string portId, string deviceAddress, DeviceVersion version);

        [OperationContract(IsOneWay = true)]
        void DeviceConfiguration(string portId, string deviceAddress, DeviceConfiguration configuration);
    }
}
