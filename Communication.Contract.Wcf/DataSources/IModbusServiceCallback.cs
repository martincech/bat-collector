﻿using System.ServiceModel;
using Veit.Bat.Common;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract]
    public interface IModbusServiceCallback : IDataSourceCallback, IScaleConfigCallback, IScaleFlockCallback
    {
        [OperationContract(IsOneWay = true)]
        void ScanProgress(Percent percent);

        [OperationContract(IsOneWay = true)]
        void Activated(string portId, string deviceAddress);

        [OperationContract(IsOneWay = true)]
        void Deactivated(string portId, string deviceAddress);
    }
}
