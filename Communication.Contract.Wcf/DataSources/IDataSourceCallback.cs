﻿using System.ServiceModel;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Bat.Common.Sample.Base;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract]
    public interface IDataSourceCallback
    {
        [OperationContract(IsOneWay = true)]
        void SampleReaded(TimeSample sample);

        [OperationContract(IsOneWay = true)]
        void Connected(SourceConnectivityInfo device);

        [OperationContract(IsOneWay = true)]
        void Disconnected(SourceConnectivityInfo device);
    }
}
