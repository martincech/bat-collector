﻿using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts.Device;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract(CallbackContract = typeof(IScaleFlockCallback))]
    public interface IScaleFlock
    {
        [OperationContract(IsOneWay = true)]
        void GetFlock(string portId, byte address, byte flockNumber);

        [OperationContract(IsOneWay = true)]
        void SetFlock(string portId, byte address, Flock flock);
    }
}
