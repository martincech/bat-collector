﻿using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts.Device.Configuration;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract(CallbackContract = typeof(IScaleConfigCallback))]
    public interface IScaleConfig
    {
        [OperationContract(IsOneWay = true)]
        void GetConfiguration(string portId, byte address);

        [OperationContract(IsOneWay = true)]
        void GetVersion(string portId, byte address);

        [OperationContract(IsOneWay = true)]
        void GetDeviceName(string portId, byte address);

        [OperationContract(IsOneWay = true)]
        void SetDeviceName(string portId, byte address, string name);

        [OperationContract(IsOneWay = true)]
        void SetSavingParameters(string portId, byte address, SavingParameters parameters);
    }
}
