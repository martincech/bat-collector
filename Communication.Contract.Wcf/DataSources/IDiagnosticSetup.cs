﻿using System.ServiceModel;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract]
    public interface IDiagnosticSetup
    {
        [OperationContract(IsOneWay = true)]
        void SetDebugMode(bool enable);

        [OperationContract(IsOneWay = true)]
        void SendFeedback(string feedbackFileName, string message, bool includeDiagnosticFiles);
    }
}
