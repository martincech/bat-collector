﻿using System.ServiceModel;
using Services.PublishSubscribe.Contracts;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract(CallbackContract = typeof(IDataSourceCallback))]
    public interface IDataSource : ISubscriptionContract
    {
        [OperationContract(IsOneWay = true)]
        void GetConnected();
    }
}
