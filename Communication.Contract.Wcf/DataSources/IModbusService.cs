﻿using System.Collections.Generic;
using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts;

namespace Communication.Contract.Wcf.DataSources
{
    [ServiceContract(CallbackContract = typeof(IModbusServiceCallback))]
    public interface IModbusService : IDataSource, IDiagnosticSetup, IScaleConfig, IScaleFlock
    {
        [OperationContract(IsOneWay = true)]
        void AddSource(string portId);
        [OperationContract(IsOneWay = true)]
        void RemoveSource(string portId);

        [OperationContract(IsOneWay = true)]
        void SetPortSettings(string portId, CommunicationParameters communicationSettings);

        [OperationContract(IsOneWay = true)]
        void ScanPortAddresses(IEnumerable<string> portIds, IEnumerable<byte> addresses);

        [OperationContract(IsOneWay = true)]
        void ActivateDevices(IEnumerable<string> portIds, IEnumerable<string> addresses);
        [OperationContract(IsOneWay = true)]
        void DeactivateDevices(IEnumerable<string> portIds, IEnumerable<string> addresses);

        [OperationContract(IsOneWay = true)]
        void GetActive();
    }
}
