﻿using System.Collections.Generic;
using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts;

namespace Communication.Contract.Wcf.WebApi
{
    [ServiceContract]
    public interface IWebApiConnectionCallbacks
    {
        [OperationContract(IsOneWay = true)]
        void ExistingConnections(IEnumerable<WebApiConnectionInfo> connections);

        [OperationContract(IsOneWay = true)]
        void ConnectionStatusChanged(WebApiConnectionInfo webApiConnectionInfo);
    }
}
