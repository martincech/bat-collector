﻿using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts.Device;

namespace Communication.Contract.Wcf.WebApi
{
    [ServiceContract]
    public interface IApiDevice
    {
        [OperationContract(IsOneWay = true)]
        void SyncDevice(DeviceNotification device);
    }
}
