﻿using System.ServiceModel;
using Communication.Contract.Wcf.DataContracts;
using Services.PublishSubscribe.Contracts;

namespace Communication.Contract.Wcf.WebApi
{
    [ServiceContract(CallbackContract = typeof(IWebApiConnectionCallbacks))]
    public interface IWebApiConnection : ISubscriptionContract, IApiDevice
    {
        [OperationContract(IsOneWay = true)]
        void ChangeServerConnection(WebApiConnectionInfo webApiConnectionInfo);

        [OperationContract(IsOneWay = true)]
        void GetExistingConnectionsRequest();
    }
}
