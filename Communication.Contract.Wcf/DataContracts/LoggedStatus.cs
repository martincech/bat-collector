﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public enum LoggedStatus
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Logged = 200,        //OK logged
        [EnumMember]
        BadRequest = 400,    //login OK, but error occurred when processing request
        [EnumMember]
        UnAuthorized = 401,  //bad login
        [EnumMember]
        Forbidden = 403,    //login OK, but something is wrong with user, company or terminal
    }

    public static class LoggedStatusExtension
    {
        public static bool IsLogged(this LoggedStatus status)
        {
            return status == LoggedStatus.Logged;
        }
    }
}
