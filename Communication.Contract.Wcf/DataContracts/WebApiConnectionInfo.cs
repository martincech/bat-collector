﻿using System.Runtime.Serialization;
using System.Security.Policy;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public class WebApiConnectionInfo
    {
        [DataMember(IsRequired = true)]
        public Url BatApi { get; set; }

        [DataMember(IsRequired = true)]
        public Url SensorsApi { get; set; }

        [DataMember(IsRequired = true)]
        public Login Login { get; set; }

        [DataMember(IsRequired = true)]
        public LoggedStatus Logged { get; set; }

        public override string ToString()
        {
            return $"{(Login == null ? "NO LOGIN INFO" : Login.ToString())}@{(BatApi == null ? "NONE" : BatApi.Value)}";
        }
    }
}
