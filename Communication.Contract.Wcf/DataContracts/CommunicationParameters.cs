﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public class CommunicationParameters
    {
        [DataMember]
        public int Speed { get; set; }
        [DataMember]
        public int ReplyDelay { get; set; }
        [DataMember]
        public int SilentInterval { get; set; }
        [DataMember]
        public Parity Parity { get; set; }
        [DataMember]
        public int Repetitions { get; set; }
        [DataMember]
        public PacketMode Protocol { get; set; }
    }
}
