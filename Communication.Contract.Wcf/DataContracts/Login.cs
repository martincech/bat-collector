﻿using System.Runtime.Serialization;
using Veit.Cognito.Service;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public class Login
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public Login()
        {
            UserName = "";
        }

        public Login(string userName)
        {
            UserName = userName;
        }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string IdToken { get; set; }
        [DataMember]
        public string AccessToken { get; set; }
        [DataMember]
        public string RefreshToken { get; set; }

        [DataMember]
        public CognitoSettings CognitoSettings { get; set; }


        public override string ToString()
        {
            return $"{UserName}";
        }
    }
}
