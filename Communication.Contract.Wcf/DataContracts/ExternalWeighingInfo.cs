﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public class ExternalWeighingInfo
    {
        public ExternalWeighingInfo()
        {
            Csv = new Csv();
        }

        [DataMember]
        public Csv Csv { get; set; }
        [DataMember]
        public string CurveFileParams { get; set; }
        [DataMember]
        public string WeighingConfigurationFileParams { get; set; }
        [DataMember]
        public string WeighingParametersFileParams { get; set; }
    }
}
