﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public enum PacketMode
    {
        [EnumMember]
        RTU,                      // RTU mode
        [EnumMember]
        ASCII                    // ASCII mode
    }
}
