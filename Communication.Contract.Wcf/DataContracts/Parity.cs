﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public enum Parity
    {
        [EnumMember]
        None,
        [EnumMember]
        Odd,
        [EnumMember]
        Even,
    }
}
