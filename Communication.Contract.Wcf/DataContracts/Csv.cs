﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts
{
    [DataContract]
    public class Csv
    {
        public Csv()
        {
            Path = @"Logs";
            DecimalSeparator = ',';
            Delimiter = ';';
            Encoding = "UTF-8";
        }

        #region Implementation of ICsv
        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public char DecimalSeparator { get; set; }
        [DataMember]
        public char Delimiter { get; set; }
        [DataMember]
        public string Encoding { get; set; }

        #endregion
    }
}
