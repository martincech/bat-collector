﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Weight;

namespace Communication.Contract.Wcf.DataContracts.Device
{
    [DataContract]
    public class DeviceWeighing
    {
        public WeighingCommand Command { get; set; }
        public DateTimeOffset? CommandTimeStamp { get; set; }

        public int InitialDay { get; set; }
        public Weight InitialWeightMales { get; set; }
        public Weight InitialWeightFemales { get; set; }
    }
}
