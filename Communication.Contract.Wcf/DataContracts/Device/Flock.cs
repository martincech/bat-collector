﻿using System.Runtime.Serialization;
using Veit.Bat.Common;

namespace Communication.Contract.Wcf.DataContracts.Device
{
    [DataContract]
    public class Flock
    {
        public byte Number { get; set; }
        public bool UseCurves { get; set; }
        public bool UseBothGenders { get; set; }
        public byte? WeighFromHour { get; set; }
        public byte? WeighTillHour { get; set; }

        public Curve FemaleCurve { get; set; }
        public Curve MaleCurve { get; set; }
    }
}
