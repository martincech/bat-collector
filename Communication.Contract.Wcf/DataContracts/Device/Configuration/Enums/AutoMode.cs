﻿namespace Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums
{
    public enum AutoMode
    {
        WithoutGain,
        WithGain
    }
}
