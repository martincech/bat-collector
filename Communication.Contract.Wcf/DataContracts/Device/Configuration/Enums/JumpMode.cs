﻿namespace Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums
{
    public enum JumpMode
    {
        Enter,
        Leave,
        Both
    }
}
