﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums
{
    [DataContract]
    public enum DeviceVersion
    {
        [EnumMember]
        Unknown = 0,
        [EnumMember]
        Bat2Gsm = 0x0001,
        [EnumMember]
        Bat2V111 = 0x0111,
        [EnumMember]
        Bat2V150 = 0x0150,
        [EnumMember]
        Bat2V153 = 0x0153,
    }
}
