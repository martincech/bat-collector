﻿using Veit.Bat.Common;

namespace Communication.Contract.Wcf.DataContracts.Device.Configuration
{
    public class Margin
    {
        public Percent Above { get; set; }
        public Percent Below { get; set; }
    }
}
