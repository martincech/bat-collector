﻿using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;
using Veit.Bat.Common;

namespace Communication.Contract.Wcf.DataContracts.Device.Configuration
{
    public class SavingParameters
    {
        public SavingParameters()
        {
            MarginFemale = new Margin();
            MarginMale = new Margin();
        }

        public Margin MarginFemale { get; set; }
        public Margin MarginMale { get; set; }

        public ushort Filter { get; set; }
        public Percent StabilizationRange { get; set; }
        public ushort StabilizationTime { get; set; }
        public AutoMode AutoMode { get; set; }
        public JumpMode JumpMode { get; set; }
    }
}
