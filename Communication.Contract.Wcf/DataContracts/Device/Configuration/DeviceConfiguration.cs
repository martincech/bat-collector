﻿using System.Runtime.Serialization;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;

namespace Communication.Contract.Wcf.DataContracts.Device.Configuration
{
    [DataContract]
    public class DeviceConfiguration
    {
        [DataMember]
        public DeviceVersion Version { get; set; }
        [DataMember]
        public string ScaleName { get; set; }
        [DataMember]
        public SavingParameters SavingParameters { get; set; }
    }
}
