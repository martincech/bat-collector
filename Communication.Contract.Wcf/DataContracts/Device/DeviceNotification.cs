﻿using System.Runtime.Serialization;

namespace Communication.Contract.Wcf.DataContracts.Device
{
    [DataContract]
    public class DeviceNotification
    {
        [DataMember]
        public string Uid { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DeviceAction Action { get; set; }
    }

    [DataContract]
    public enum DeviceAction
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Add,
        [EnumMember]
        Update,
        [EnumMember]
        Delete
    }
}
