﻿namespace Communication.Contract.Wcf.DataContracts.Device
{
    public enum WeighingCommand
    {
        START,
        STOP,
        PAUSE
    }
}
