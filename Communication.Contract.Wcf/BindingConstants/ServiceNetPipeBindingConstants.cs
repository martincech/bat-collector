﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Communication.Contract.Wcf.BindingConstants
{
    public static class ServiceNetPipeBindingConstants
    {
        private const string PIPE_ROOT_NAME = "net.pipe://localhost/";

        public static string ModbusPipeAddress => PIPE_ROOT_NAME + "ModbusSensorService";
        public static string ConnectionSubAddress => "/ConnectionInfo";

        public static readonly Binding NetPipeBinding = new NetNamedPipeBinding
        {
            //without setting timeouts - for subscribers we need permanent connection

            Security = { Mode = NetNamedPipeSecurityMode.None },
            //CloseTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
            //OpenTimeout = TimeSpan.FromSeconds(OPEN_CLOSE_TIMEOUT),
            //ReceiveTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT),
            //SendTimeout = TimeSpan.FromSeconds(RECEIVE_SEND_TIMEOUT),
            TransactionFlow = true,
            ReceiveTimeout = TimeSpan.MaxValue
        };
    }
}
