﻿using Veit.Bat.Collector.ViewModels.Home;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleHomeBarMenuViewModel : HomeBarMenuViewModel
    {
        public SampleHomeBarMenuViewModel()
            : base (null, new SampleSensorsViewModel())
        {
            Title = "Everything is OK:-)";
        }
    }
}
