﻿using System.Collections.Generic;
using Veit.Bat.Collector.ViewModels.Cloud;
using Veit.Bat.Collector.ViewModels.Services;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleCloudConnectionViewModel : CloudConnectionViewModel
    {
        public SampleCloudConnectionViewModel()
            : base(null, new Settings.Core.SystemInfo
                    {
                        LoggedUserName = "user01@comp.cz"
                    },
                  null,
                  new ServicesManagerViewModel(new List<ServiceBaseViewModel>()))
        {
        }
    }
}
