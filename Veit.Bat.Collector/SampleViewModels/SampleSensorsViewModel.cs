﻿using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleSensorsViewModel : SensorsViewModel
    {
        public SampleSensorsViewModel()
           : base(null, null, null, new SampleSensorFilterViewModel(), null, null, null)
        {
        }
    }
}
