﻿using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleSensorDetailBarMenuViewModel : SensorDetailBarMenuViewModel
    {
        public SampleSensorDetailBarMenuViewModel()
           : base(new TemperatureSensorViewModel
           {
               Name = "Temperature #2"
           }, null)
        {
        }
    }
}
