﻿using System.Collections.Generic;
using Settings;
using Settings.Core;
using Veit.Bat.Collector.ViewModels.Cloud.Settings;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleApplicationSettingsViewModel : ApplicationSettingsViewModel
    {

        public SampleApplicationSettingsViewModel()
            : base(new DiagnosticSettingsManagerMock(), new List<IDiagnostic>())
        {
        }
    }

    #region Mock of IDiagnosticSettingsManager

    internal class DiagnosticSettingsManagerMock : IDiagnosticSettingsManager
    {
        public Settings.DiagnosticSettings.Diagnostics LoadDiagnostics()
        {
            return new Settings.DiagnosticSettings.Diagnostics
            {
                UseDebugMode = true
            };
        }

        public bool SaveDiagnostic(Settings.DiagnosticSettings.Diagnostics diagnostic)
        {
            return true;
        }
    }

    #endregion
}
