﻿using System.Collections.Generic;
using Veit.Bat.Collector.ViewModels.Sensor.Add;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleAddSensorViewModel : AddSensorViewModel
    {
        public SampleAddSensorViewModel()
           : base(new List<string>
                  {
                   "House 01",
                   "House 02"
                  },
                  null,
                  null,
                  null)
        {
            DeviceType = Models.Enums.DeviceType.Scale;
            ScaleType = Models.Enums.ScaleType.Bat2Modbus;
        }
    }
}
