﻿using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleSensorsBarMenuViewModel : SensorsBarMenuViewModel
    {
        public SampleSensorsBarMenuViewModel()
           : base(null, null, new SampleSensorsViewModel())
        {
            Bat2Count = 9;
            ThermometerCount = 3;
            HydrometerCount = 2;
            Co2Count = 4;
        }
    }
}
