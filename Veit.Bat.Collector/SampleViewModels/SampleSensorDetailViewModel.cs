﻿using System;
using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleSensorDetailViewModel : SensorDetailViewModel
    {
        public SampleSensorDetailViewModel()
           : base(null,
                  new Bat2ModbusSensorViewModel("123.456.789.123", 1)
                  {
                     Name = "Bat2 scale #3",
                     State = Models.Enums.SensorState.Active,
                     Connection = Models.Enums.SensorConnection.Connected
                  },
                  new SensorDetailFilterViewModel(DateTimeOffset.Now, DateTimeOffset.Now))
        {
        }
    }
}
