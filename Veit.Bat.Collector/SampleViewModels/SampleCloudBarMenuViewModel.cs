﻿using Veit.Bat.Collector.ViewModels.Cloud;

namespace Veit.Bat.Collector.SampleViewModels
{
    public class SampleCloudBarMenuViewModel : CloudBarMenuViewModel
    {
        public SampleCloudBarMenuViewModel()
           : base(null, null)
        {
        }
    }
}
