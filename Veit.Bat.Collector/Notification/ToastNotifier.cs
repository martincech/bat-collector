﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;
using Veit.Bat.Collector.IoC;
using Veit.Bat.Collector.Notification.Custom;
using Veit.Bat.Collector.Views;

namespace Veit.Bat.Collector.Notification
{
    public class ToastNotifier
    {
        private static ToastNotifier instance;

        private readonly Notifier notifier;
        private static readonly Window parentWindow = ContainerLocator.Resolve<MainWindow>();

        private ToastNotifier()
        {
            notifier = new Notifier(cfg =>
            {
                cfg.PositionProvider = new WindowPositionProvider(
                    parentWindow: parentWindow,
                    corner: Corner.TopRight,
                    offsetX: 10,
                    offsetY: 50);

                cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                    notificationLifetime: TimeSpan.FromSeconds(3),
                    maximumNotificationCount: MaximumNotificationCount.FromCount(5));

                cfg.Dispatcher = Application.Current.Dispatcher;
            });
        }

        private static Notifier Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ToastNotifier();
                }
                return instance.notifier;
            }
        }

        public static void ShowWarning(string message)
        {
            Show(Instance.ShowWarning, message);
        }

        public static void ShowError(string message)
        {
            Show(Instance.ShowError, message);
        }

        public static void ShowSuccess(string message)
        {
            Show(Instance.ShowSuccess, message);
        }

        public static void ShowInformation(string message)
        {
            Show(Instance.ShowInformation, message);
        }


        private static readonly object lockObj = new object();
        public static Dictionary<string, List<StringBuilder>> Messages { get; set; } = new Dictionary<string, List<StringBuilder>>();


        public static void ShowOnce(string id, string message, Action<string> action, int delay = 5000)
        {
            lock (lockObj)
            {
                if (!Messages.ContainsKey(id)) Messages.Add(id, new List<StringBuilder>());
                var mInstance = new StringBuilder(message);
                Messages[id].Add(mInstance);

                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(delay);
                    lock (lockObj)
                    {
                        if (!Messages.ContainsKey(id)) return;
                        if (Messages[id].LastOrDefault() != mInstance) return;
                        action?.Invoke(mInstance.ToString());
                        Messages.Remove(id);
                    }
                });
            }
        }

        private static void Show(Action<string> send, string message)
        {
            if (send == null)
            {
                throw new ArgumentNullException(nameof(send));
            }

            if (parentWindow == null || !parentWindow.IsVisible) return;

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate { Instance.Notify<CustomNotification>(() => new CustomNotification(message)); });
        }

    }
}
