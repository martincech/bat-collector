﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using ToastNotifications.Core;

namespace Veit.Bat.Collector.Notification.Custom
{
    public class CustomNotification : NotificationBase, INotifyPropertyChanged
    {
        private CustomDisplayPart _displayPart;


        public override NotificationDisplayPart DisplayPart
        {
            get
            {
                return _displayPart ?? (_displayPart = new CustomDisplayPart(this));
            }
        }

        public CustomNotification(string message)
        {
            Message = message;
        }

        private string _message;
        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
