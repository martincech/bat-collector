﻿using ToastNotifications.Core;

namespace Veit.Bat.Collector.Notification.Custom
{
    /// <summary>
    /// Interaction logic for CustomDisplayPart.xaml
    /// </summary>
    public partial class CustomDisplayPart : NotificationDisplayPart
    {
        public CustomDisplayPart(CustomNotification customNotification)
        {
            DataContext = customNotification; // this allows to bind UI with data in notification
            InitializeComponent();
        }
    }
}
