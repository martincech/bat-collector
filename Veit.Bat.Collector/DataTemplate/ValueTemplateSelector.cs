﻿using System.Windows;
using System.Windows.Controls;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Sample.Co2;
using Veit.Bat.Common.Sample.Humidity;
using Veit.Bat.Common.Sample.Temperature;
using Veit.Bat.Common.Sample.Weight;

namespace Veit.Bat.Collector.DataTemplate
{
    public class ValueTemplateSelector : DataTemplateSelector
    {
        public override System.Windows.DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null && item is TimeStampedSample sample)
            {
                if (sample is TemperatureSample)
                {
                    return ((FrameworkElement)container).TryFindResource("TemplateTemperatureSample") as System.Windows.DataTemplate;
                }
                if (sample is HumiditySample)
                {
                    return ((FrameworkElement)container).TryFindResource("TemplateHumiditySample") as System.Windows.DataTemplate;
                }
                if (sample is Co2Sample)
                {
                    return ((FrameworkElement)container).TryFindResource("TemplateCo2Sample") as System.Windows.DataTemplate;
                }
                if (sample is Models.StatisticSample)
                {
                    return ((FrameworkElement)container).TryFindResource("TemplateStatisticSample") as System.Windows.DataTemplate;
                }
                if (sample is BirdWeight)
                {
                    return ((FrameworkElement)container).TryFindResource("TemplateBirdWeightSample") as System.Windows.DataTemplate;
                }
                return null;
            }
            else
            {
                return base.SelectTemplate(item, container);
            }
        }
    }
}
