﻿using System.Windows;
using System.Windows.Controls;
using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.DataTemplate
{
    public class EditSensorTemplateSelector : DataTemplateSelector
    {
        public override System.Windows.DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null && item is SensorViewModel sensor)
            {
                if (sensor.Type == Models.Enums.SensorType.Thermometer)
                {
                    return ((FrameworkElement)container).TryFindResource("EditTemperatureSensorTemplate") as System.Windows.DataTemplate;
                }
                else if (sensor.Type == Models.Enums.SensorType.Hydrometer)
                {
                    return ((FrameworkElement)container).TryFindResource("EditHumiditySensorTemplate") as System.Windows.DataTemplate;
                }
                else if (sensor.Type == Models.Enums.SensorType.Co2)
                {
                    return ((FrameworkElement)container).TryFindResource("EditCo2SensorTemplate") as System.Windows.DataTemplate;
                }
                else if (sensor.Type == Models.Enums.SensorType.Bat2 && sensor is Bat2ModbusSensorViewModel)
                {
                    return ((FrameworkElement)container).TryFindResource("EditBat2ModbusSensorTemplate") as System.Windows.DataTemplate;
                }
                return null;
            }
            else
            {
                return base.SelectTemplate(item, container);
            }
        }
    }
}
