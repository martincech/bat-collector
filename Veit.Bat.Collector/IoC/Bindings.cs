﻿using Communication.Contract.Wcf.DataSources;
using Communication.Contract.Wcf.WebApi;
using Ninject.Modules;
using Settings;
using Settings.Core;
using Settings.Core.Logger;
using Veit.Bat.Collector.Logger;
using Veit.Bat.Collector.Models;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Bat.Collector.Repository;
using Veit.Bat.Collector.Repository.House;
using Veit.Bat.Collector.Repository.Sensor;
using Veit.Bat.Collector.Service_References;
using Veit.Bat.Collector.Service_References.Modbus;
using Veit.Bat.Collector.Service_References.Watchers;
using Veit.Bat.Collector.Service_References.WebApi;
using Veit.Bat.Collector.ViewModels;
using Veit.Bat.Collector.ViewModels.Cloud;
using Veit.Bat.Collector.ViewModels.Feedback;
using Veit.Bat.Collector.ViewModels.Home;
using Veit.Bat.Collector.ViewModels.Sensor;
using Veit.Bat.Collector.ViewModels.Sensor.Add;
using Veit.Bat.Collector.ViewModels.Services;
using Veit.Bat.Collector.Views;
using Veit.Bat.Collector.Views.Cloud;
using Veit.Bat.Collector.Views.Cloud.Settings;
using Veit.Bat.Collector.Views.Feedback;
using Veit.Bat.Collector.Views.Home;
using Veit.Bat.Collector.Views.Sensor;
using Veit.Wpf.Controls.Models;
using Veit.Wpf.Controls.Models.Navigation;

namespace Veit.Bat.Collector.IoC
{
    internal class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IManagerSettings, IModbusSettings, Settings.DeviceSettings.IDeviceSettingsManager, ManagerSettings>().To<ManagerSettings>().InSingletonScope();
            Bind<ViewModelLocator>().ToSelf().InTransientScope();
            Bind<INavigationService>().To<NavigationService>().InSingletonScope();

            Bind<DataManager,
                ISensorDataRepository,
                IHouseDataRepository,
                IDiagnosticSettingsManager>().To<DataManager>().InSingletonScope();

            InitViews();
            InitViewModels();
            InitWcfServices();

            //bind service watchers
            Bind<ISystemServiceController>().To<SystemServiceController>().InTransientScope();

            Bind<ModbusSystemServiceWatcher>().ToSelf().InSingletonScope();

            Bind<BaseLoggerConfiguration, LoggerConfiguration>().To<LoggerConfiguration>().InSingletonScope();
            Bind<IDiagnostic, DiagnosticManager>().To<DiagnosticManager>().InSingletonScope();
            Bind<Settings.DiagnosticSettings.Collector>().ToSelf().InTransientScope();
            Bind<SystemInfo>().ToSelf().InSingletonScope();
        }

        #region Private helpers

        private void InitViews()
        {
            Bind<MainWindow, IErrorView>().To<MainWindow>().InSingletonScope();
            Bind<HomeView>().ToSelf().InTransientScope();

            Bind<SensorsView>().ToSelf().InTransientScope();
            Bind<CloudView>().ToSelf().InTransientScope();
            Bind<ApplicationSettingsView>().ToSelf().InSingletonScope();

            Bind<FeedbackView>().ToSelf().InTransientScope();
        }

        private void InitViewModels()
        {
            Bind<MainWindowViewModel>().ToSelf().InSingletonScope();

            //services
            Bind<ModbusServiceViewModel>().ToSelf().InSingletonScope();


            // Home
            Bind<HomeViewModel>().ToSelf().InSingletonScope();
            Bind<HomeBarMenuViewModel>().ToSelf().InSingletonScope();

            // sensors
            Bind<IHardwareDevices, ISensorsWatcher, SensorsViewModel>().To<SensorsViewModel>().InSingletonScope();
            Bind<SensorsBarMenuViewModel>().ToSelf().InSingletonScope();
            Bind<SensorDetailViewModel>().ToSelf().InTransientScope();
            Bind<SensorDetailBarMenuViewModel>().ToSelf().InTransientScope();
            Bind<SensorFilterViewModel>().ToSelf().InSingletonScope();
            Bind<SensorDetailFilterViewModel>().ToSelf().InSingletonScope();

            Bind<IProgressBar, AddSensorViewModel>().To<AddSensorViewModel>().InTransientScope();

            // connection
            Bind<CloudViewModel>().ToSelf().InSingletonScope();
            Bind<CloudBarMenuViewModel>().ToSelf().InSingletonScope();
            Bind<IConnectionWatcher, CloudConnectionViewModel>().To<CloudConnectionViewModel>().InSingletonScope();

            //feedback
            Bind<FeedbackViewModel>().ToSelf().InTransientScope();
            Bind<FeedbackBarMenuViewModel>().ToSelf().InTransientScope();
        }

        private void InitWcfServices()
        {
            Bind<ModbusConnectionCommands,
                 IModbusServiceCallback,
                 IModbusCommands,
                 IDeviceConfigurationCommands>().To<ModbusConnectionCommands>().InSingletonScope();
            Bind<WebApiConnectionCommands,
                 IWebApiConnectionCallbacks,
                 IWebApiCommands,
                 IApiDevice>().To<WebApiConnectionCommands>().InSingletonScope();
        }

        #endregion
    }
}
