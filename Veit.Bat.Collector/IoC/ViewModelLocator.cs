﻿using System;
using System.Collections.Generic;
using System.Linq;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Settings;
using Settings.Core;
using Settings.DataSourceSettings.Modbus;
using Veit.Bat.Collector.Helpers;
using Veit.Bat.Collector.Models;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.Models.Pages;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Bat.Collector.Repository.House;
using Veit.Bat.Collector.Repository.Sensor;
using Veit.Bat.Collector.Service_References.Modbus;
using Veit.Bat.Collector.Service_References.WebApi;
using Veit.Bat.Collector.ViewModels;
using Veit.Bat.Collector.ViewModels.Cloud;
using Veit.Bat.Collector.ViewModels.Cloud.Settings;
using Veit.Bat.Collector.ViewModels.Dialogs;
using Veit.Bat.Collector.ViewModels.Feedback;
using Veit.Bat.Collector.ViewModels.Home;
using Veit.Bat.Collector.ViewModels.Sensor;
using Veit.Bat.Collector.ViewModels.Sensor.Add;
using Veit.Bat.Collector.ViewModels.Services;
using Veit.Bat.Collector.Views;
using Veit.Bat.Collector.Views.Dialogs;
using Veit.Wpf.Controls.Models.Navigation;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;
using Veit.Wpf.Controls.ViewModels.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.IoC
{
    public class ViewModelLocator
    {
        //menu models
        private readonly HomeModel homeModel;
        private readonly SensorsModel sensorsModel;
        private readonly CloudModel cloudModel;
        private readonly FeedbackModel feedbackModel;

        private Uri EmptyView => ModelHelper.GetUriFromType(typeof(EmptyView));

        public ViewModelLocator()
        {
            if (DesignerHelper.IsInDesignMode())
            {
                return;
            }

            homeModel = new HomeModel();
            sensorsModel = new SensorsModel();
            cloudModel = new CloudModel();
            feedbackModel = new FeedbackModel();

            SetupViewModels();
        }




        public MainWindowViewModel MainWindowViewModel
        {
            get
            {
                var cloudConnectionVm = ContainerLocator.Resolve<CloudConnectionViewModel>();
                var loginDialog = new ViewModels.Helper.LoginDialog(DialogCoordinator.Instance,
                                        ContainerLocator.Resolve<TryLoginDialog>(),
                                        () => { cloudConnectionVm.LogoutCommand.Execute(null); });


                return ContainerLocator.Resolve<MainWindowViewModel>(new Dictionary<string, object>
                {
                   { "dialog", loginDialog },
                   { "navService", ContainerLocator.Resolve<INavigationService>() },
                   { "pages", new List<PageViewModel>
                      {
                         HomeViewModel,
                         SensorsViewModel,
                         CloudViewModel,
                         FeedbackViewModel
                      } },
                   { "connectionVm", cloudConnectionVm }
                });
            }
        }

        public AboutDialogViewModel AboutDialogViewModel
        {
            get => ContainerLocator.Resolve<AboutDialogViewModel>();
        }


        #region Home

        public HomeViewModel HomeViewModel
        {
            get
            {
                return ContainerLocator.Resolve<HomeViewModel>(new Dictionary<string, object>
                {
                   {"model", new PageModel
                   {
                      MainMenuItems = GetMainMenuItems(),
                      OptionalMenuItems = GetOptionalMenuItems(),
                      BarMenu = homeModel.BarMenu,
                      DefaultDetail = homeModel
                   } },
                   {"sensorCard", SensorsViewModel.Overview },
                   {"cloudCard", CloudViewModel.Overview },
                });
            }
        }

        public HomeBarMenuViewModel HomeBarMenuViewModel
        {
            get
            {
                return ContainerLocator.Resolve<HomeBarMenuViewModel>(new Dictionary<string, object>
                {
                   {"connectionWatcher", CloudConnectionViewModel },
                   {"sensorWatcher", SensorsViewModel }
                });
            }
        }

        #endregion

        #region Sensors

        public SensorsViewModel SensorsViewModel
        {
            get
            {
                return ContainerLocator.Resolve<SensorsViewModel>(new Dictionary<string, object>
                {
                   {"model", GetSensorsContent() },
                   {"repository", ContainerLocator.Resolve<ISensorDataRepository>() },
                   {"showDetailAction", CreateActionForDisplaySubContent(sensorsModel.DetailContent, sensorsModel.DetailBarMenu) },
                   {"filterVm", SensorFilterViewModel }
                });
            }
        }

        public SensorFilterViewModel SensorFilterViewModel
        {
            get
            {
                return ContainerLocator.Resolve<SensorFilterViewModel>();
            }
        }

        public SensorsBarMenuViewModel SensorsBarMenuViewModel
        {
            get
            {
                return ContainerLocator.Resolve<SensorsBarMenuViewModel>(new Dictionary<string, object>
                {
                   {"switchToDetailAction", CreateActionForDisplayMenu(sensorsModel) },
                   {"addSensorAction", CreateActionForDisplaySubContent(sensorsModel.AddSensor, EmptyView) },
                   {"watcher", SensorsViewModel },
                });
            }
        }

        public SensorDetailViewModel SensorDetailViewModel
        {
            get
            {
                //if selected sensor is null -> go back to previous page (it means you are trying display sensor detail of deleted sensor)
                var selectedSensor = GetSelectedSensor();
                if (selectedSensor == null)
                {
                    var goBack = CreateActionForGoBack();
                    goBack?.Invoke();
                    return null;
                }

                return ContainerLocator.Resolve<SensorDetailViewModel>(new Dictionary<string, object>
                {
                   {"model", GetSensorsContent() },
                   {"detail", selectedSensor },
                   {"filterVm", SensorDetailFilterViewModel },
                });
            }
        }

        public SensorDetailFilterViewModel SensorDetailFilterViewModel
        {
            get
            {
                var selectedSensor = GetSelectedSensor();
                return ContainerLocator.Resolve<SensorDetailFilterViewModel>(new Dictionary<string, object>
                {
                   {"from",  GetSampleTimeStamp(selectedSensor?.HourSamples.LastOrDefault()) },
                   {"to", GetSampleTimeStamp(selectedSensor?.HourSamples.FirstOrDefault()) }
                });
            }
        }

        private SensorViewModel GetSelectedSensor()
        {
            if (SensorsViewModel.SelectedSensor != null)
            {
                return SensorsViewModel.SelectedSensor;
            }
            return SensorsViewModel.PreviousSelectedSensor;
        }

        private static DateTimeOffset GetSampleTimeStamp(SensorDataViewModel model)
        {
            if (model == null)
            {
                return DateTimeOffset.Now;
            }
            return model.Sample.TimeStamp;
        }

        public SensorDetailBarMenuViewModel SensorDetailBarMenuViewModel
        {
            get
            {
                return ContainerLocator.Resolve<SensorDetailBarMenuViewModel>(new Dictionary<string, object>
                {
                   {"detail",  GetSelectedSensor() },
                   {"editSensorAction", CreateActionForDisplaySubContent(sensorsModel.EditSensor, EmptyView) }
                });
            }
        }

        public SensorAddWizardViewModel SensorAddWizardViewModel
        {
            get
            {
                return ContainerLocator.Resolve<SensorAddWizardViewModel>(new Dictionary<string, object>
                {
                   {"model", GetSensorsContent() },
                   {"closeWizardAction", CreateActionForGoBack() },
                   {"contentVm", AddSensorViewModel },
                   {"sensorsVm", SensorsViewModel },
                   {"validator", new SensorValidator(SensorsViewModel) }
                });
            }
        }

        private AddSensorViewModel AddSensorViewModel
        {
            get
            {
                var manager = ContainerLocator.Resolve<ManagerSettings>();
                var uidMappings = !(manager.Settings.Sources.FirstOrDefault(f => f is ModbusManagerSettings) is ModbusManagerSettings source)
                    ? new Dictionary<string, string>() : source.UidMapping;

                var rep = ContainerLocator.Resolve<IHouseDataRepository>();
                return ContainerLocator.Resolve<AddSensorViewModel>(new Dictionary<string, object>
                {
                   {"houses", rep.GetHouses() },
                   {"modbusCommands", ContainerLocator.Resolve<IModbusCommands>() },
                   {"sensors", ContainerLocator.Resolve<ISensorsWatcher>() },
                   {nameof(uidMappings), uidMappings }
                });
            }
        }

        public SensorEditViewModel SensorEditViewModel
        {
            get
            {
                return ContainerLocator.Resolve<SensorEditViewModel>(new Dictionary<string, object>
                {
                   {"closeAction", CreateActionForGoBack() },
                   {"goToSectionMenuAction",CreateActionForGoBack(2) }, // when a sensor will be deleted, we want to go back to previous sensors main page (2 steps back). And we want to remove these steps from navigation history
                   {"sensorsVm", SensorsViewModel },
                   {"validator", new SensorValidator(SensorsViewModel) },
                   {"deviceCommands", ContainerLocator.Resolve<IModbusCommands>() },
                   {"repository", ContainerLocator.Resolve<ISensorDataRepository>() },
                });
            }
        }

        private PageModel GetSensorsContent()
        {
            return new PageModel
            {
                MainMenuItems = GetMainMenuItems(),
                OptionalMenuItems = GetOptionalMenuItems(),
                BarMenu = sensorsModel.BarMenu,
                DefaultDetail = sensorsModel
            };
        }

        #endregion

        #region Connection (Cloud)

        public CloudViewModel CloudViewModel
        {
            get
            {
                return ContainerLocator.Resolve<CloudViewModel>(new Dictionary<string, object>
                {
                   {"model", new PageModel
                         {
                            MainMenuItems = GetMainMenuItems(),
                            OptionalMenuItems = GetOptionalMenuItems(),
                            BarMenu = null,
                            DefaultDetail = cloudModel
                         }},
                   {"batCloudCard", BatCloudCardViewModel },
                   {"connectionCard", ConnectionCardViewModel },
                   {"sensorsCard", SensorsCardViewModel },
                   {"settingsCard", SettingsCardViewModel }
                });
            }
        }

        public CloudBarMenuViewModel CloudBarMenuViewModel
        {
            get
            {
                return ContainerLocator.Resolve<CloudBarMenuViewModel>(new Dictionary<string, object>
                {
                   {"switchToDetailAction", CreateActionForDisplayMenu(cloudModel) },
                   {"watcher", CloudConnectionViewModel }
                });
            }
        }

        public CloudConnectionViewModel CloudConnectionViewModel
        {
            get
            {
                return ContainerLocator.Resolve<CloudConnectionViewModel>(new Dictionary<string, object>
                {
                   {"commands", ContainerLocator.Resolve<IWebApiCommands>() },
                   {"systemInfo", ContainerLocator.Resolve<SystemInfo>() },
                   {"successLoginAction", GetActionForSuccessLoginToCloud() },
                   { "servicesManager", ServicesManagerViewModel }
                });
            }
        }

        public ApplicationSettingsViewModel ApplicationSettingsViewModel
        {
            get
            {
                var diagObjects = new List<IDiagnostic>
                {
                    ContainerLocator.Resolve<IDiagnostic>(),
                    ContainerLocator.Resolve<ModbusConnectionCommands>(),
                };

                return ContainerLocator.Resolve<ApplicationSettingsViewModel>(new Dictionary<string, object>
                {
                   {"settingsRepository", ContainerLocator.Resolve<IDiagnosticSettingsManager>() },
                   {nameof(diagObjects), diagObjects },
                });
            }
        }


        public ServicesManagerViewModel ServicesManagerViewModel
        {
            get
            {
                return ContainerLocator.Resolve<ServicesManagerViewModel>("services", SystemServices);
            }
        }

        private IEnumerable<ServiceBaseViewModel> SystemServices
        {
            get
            {
                return new List<ServiceBaseViewModel>
                {
                    ContainerLocator.Resolve<ModbusServiceViewModel>(),
                };
            }
        }


        /// <summary>
        /// If the current displayed screen is CloudConnectionViewModel when successful log in,
        /// go to previous page.
        /// </summary>
        /// <returns></returns>
        private Action GetActionForSuccessLoginToCloud()
        {
            return new Action(() =>
            {
                var vm = ContainerLocator.Resolve<MainWindowViewModel>();
                if (vm.CurrentPageViewModel.DetailPage.Source == cloudModel.LoginContent && vm.CanGoBack)
                {
                    vm.GoBackCommand.Execute(null);
                }
            });
        }

        public CardSmallViewModel BatCloudCardViewModel
        {
            get
            {
                var vm = ContainerLocator.Resolve<CardSmallViewModel>(new Dictionary<string, object>
                {
                   {"icon", -1 },
                   {"title", "BAT Cloud" },
                   {"switchToDetailAction", CreateActionForDisplaySubContent(cloudModel.LoginContent, EmptyView) },
                });
                vm.Status = "You are not logged in";
                vm.StateIcon = StockIcon.CloudUpload;
                vm.Flag = CardState.Error;
                return vm;
            }
        }

        public CardSmallViewModel ConnectionCardViewModel
        {
            get
            {
                var vm = ContainerLocator.Resolve<CardSmallViewModel>(new Dictionary<string, object>
                {
                   {"icon", -1 },
                   {"title", "Data availability" },
                   //{"switchToDetailAction", CreateActionForDisplaySubContent(cloudModel.DataLogContent, cloudModel.DataLogBarMenu) },
                   {"switchToDetailAction", null },
                });
                vm.StateIcon = (StockIcon)(-1);
                vm.Flag = CardState.Neutral;
                vm.Status = Properties.Resources.NotImplementedFunction;
                return vm;
            }
        }

        public CardSmallViewModel SensorsCardViewModel
        {
            get
            {
                var vm = ContainerLocator.Resolve<CardSmallViewModel>(new Dictionary<string, object>
                {
                   {"icon", -1 },
                   {"title", "Activity" },
                   //{"switchToDetailAction", CreateActionForDisplaySubContent(cloudModel.SensorsContent, cloudModel.SensorsLogBarMenu) },
                   {"switchToDetailAction", null },
                });
                vm.StateIcon = (StockIcon)(-1);
                vm.Flag = CardState.Neutral;
                vm.Status = Properties.Resources.NotImplementedFunction;
                return vm;
            }
        }

        public CardSmallViewModel SettingsCardViewModel
        {
            get
            {
                var vm = ContainerLocator.Resolve<CardSmallViewModel>(new Dictionary<string, object>
                {
                   {"icon", -1 },
                   {"title", nameof(Settings) },
                   {"switchToDetailAction", CreateActionForDisplaySubContent(cloudModel.ApplicationSettingsContent, cloudModel.ApplicationSettingsBarMenu) },
                });
                vm.StateIcon = StockIcon.CheckMark;
                vm.Flag = CardState.Ok;
                vm.Status = "Everything is set properly";
                return vm;
            }
        }

        #endregion

        #region Feedback

        public FeedbackViewModel FeedbackViewModel
        {
            get
            {
                return ContainerLocator.Resolve<FeedbackViewModel>(new Dictionary<string, object>
                {
                   {"model", GetFeedbackContent() },
                   {"positiveFeedbackCard", PositiveFeedbackViewModel },
                   {"negativeFeedbackCard", NegativeFeedbackViewModel },
                });
            }
        }

        public CardSmallViewModel PositiveFeedbackViewModel
        {
            get
            {
                var vm = ContainerLocator.Resolve<CardSmallViewModel>(new Dictionary<string, object>
            {
               {"icon", -1 },
               {"title", string.Empty },
               {"switchToDetailAction", CreateActionForDisplaySubContent(feedbackModel.DetailFeedbackContent, feedbackModel.DetailPositiveFeedbackBarMenu) },
            });
                vm.Status = string.Empty;
                vm.Icon = StockIcon.Feedback;
                vm.Flag = CardState.Ok;
                return vm;
            }
        }

        public CardSmallViewModel NegativeFeedbackViewModel
        {
            get
            {
                var vm = ContainerLocator.Resolve<CardSmallViewModel>(new Dictionary<string, object>
                {
                   {"icon", -1 },
                   {"title", string.Empty },
                   {"switchToDetailAction", CreateActionForDisplaySubContent(feedbackModel.DetailFeedbackContent, feedbackModel.DetailNegativeFeedbackBarMenu) },
                });
                vm.Status = string.Empty;
                vm.Icon = StockIcon.SadFace;
                vm.Flag = CardState.Error;
                return vm;
            }
        }

        public FeedbackBarMenuViewModel FeedbackBarMenuViewModel
        {
            get
            {
                return ContainerLocator.Resolve<FeedbackBarMenuViewModel>("icon", feedbackModel.Icon);
            }
        }

        public FeedbackDetailViewModel FeedbackDetailViewModel
        {
            get
            {
                Action<string, string> displayAction = async (string title, string message) =>
                {
                    var win = ContainerLocator.Resolve<MainWindow>() as MetroWindow;
                    await win?.ShowMessageAsync(title, message);
                };

                return ContainerLocator.Resolve<FeedbackDetailViewModel>(new Dictionary<string, object>
                {
                    { "model", GetFeedbackContent() },
                    { "type",  GetFeedbackType() },
                    { "systemInfo", ContainerLocator.Resolve<SystemInfo>() },
                    { "diagnostic", ContainerLocator.Resolve<ModbusConnectionCommands>() },
                    { "serviceVm", ContainerLocator.Resolve<ModbusServiceViewModel>() },
                    { nameof(displayAction), displayAction }
                });
            }
        }

        private FeedbackType GetFeedbackType()
        {
            var mainVm = MainWindowViewModel;
            var currentBarMenu = mainVm.CurrentPageViewModel.BarMenu;
            if (currentBarMenu == feedbackModel.DetailPositiveFeedbackBarMenu)
            {
                return FeedbackType.Positive;
            }
            if (currentBarMenu == feedbackModel.DetailNegativeFeedbackBarMenu)
            {
                return FeedbackType.Negative;
            }
            return FeedbackType.Neutral;
        }

        private PageModel GetFeedbackContent()
        {
            return new PageModel
            {
                MainMenuItems = GetMainMenuItems(),
                OptionalMenuItems = GetOptionalMenuItems(),
                BarMenu = null,
                DefaultDetail = feedbackModel
            };
        }

        #endregion


        #region Private helpers

        private List<IMenuPage> GetMainMenuItems()
        {
            return new List<IMenuPage>
         {
            homeModel,
            sensorsModel,
            cloudModel,
         };
        }

        private List<IMenuPage> GetOptionalMenuItems()
        {
            return new List<IMenuPage>
            {
                feedbackModel,
            };
        }

        private void SetupViewModels()
        {
            //set overviews
            SensorsViewModel.Overview = SensorsBarMenuViewModel;
            CloudViewModel.Overview = CloudBarMenuViewModel;
        }

        //display as sub page -> doesn't switch menu item
        private static Action CreateActionForDisplaySubContent(Uri content, Uri barMenu)
        {
            return new Action(() =>
            {
                var vm = ContainerLocator.Resolve<MainWindowViewModel>();
                vm.ShowSubContent(content, barMenu);
            });
        }

        private Action CreateActionForDisplayMenu(IMenuPage page, bool isOptional = false)
        {
            return new Action(() =>
            {
                var vm = ContainerLocator.Resolve<MainWindowViewModel>();
                var newMenuItemIndex = GetMainMenuItems().IndexOf(page);
                vm.SelectionChanged(newMenuItemIndex, isOptional);
            });
        }


        /// <param name="repetition">Number of steps for executing GoBackCommand</param>
        /// <returns></returns>
        private static Action CreateActionForGoBack(int repetition = 1)
        {
            return new Action(() =>
            {
                var vm = ContainerLocator.Resolve<MainWindowViewModel>();

                while (repetition > 0)
                {
                    if (vm.CanGoBack)
                    {
                        vm.GoBackCommand.Execute(null);
                    }
                    repetition--;
                }
            });
        }

        #endregion
    }
}
