﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Ninject.Parameters;

namespace Veit.Bat.Collector.IoC
{
    public static class ContainerLocator
    {
        private static IKernel container;

        public static void Init(IKernel container)
        {
            container.Settings.AllowNullInjection = true;
            ContainerLocator.container = container;
        }

        public static T Resolve<T>()
        {
            CheckContainer();
            return container.Get<T>();
        }

        public static T Resolve<T>(string parameterName, object parameterValue)
        {
            return Resolve<T>(new Dictionary<string, object>
                 {
                    {parameterName, parameterValue}
                 });
        }

        public static T Resolve<T>(Dictionary<string, object> parameters)
        {
            CheckContainer();
            IParameter[] args = parameters.Select(param => new ConstructorArgument(param.Key, param.Value)).ToArray();
            return container.Get<T>(args);
        }

        /// <summary>
        /// Register type to concrete instance.
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <param name="instance">instance of type</param>
        public static void Register<T>(T instance)
        {
            container.Unbind<T>();
            container.Bind<T>().ToConstant(instance).InSingletonScope();
        }


        private static void CheckContainer()
        {
            if (container == null)
            {
                throw new FieldAccessException("IoC container is not initialized");
            }
        }
    }
}
