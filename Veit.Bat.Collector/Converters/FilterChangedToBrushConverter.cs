﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Bat.Collector.Converters
{
    public class FilterChangedToBrushConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var selectBrush = Brush.Dark;
            if (values == null || values.Count() != 3 ||
               !bool.TryParse(values[1].ToString(), out var isMouseOver) ||
               !bool.TryParse(values[2].ToString(), out var isFilterChanged))
            {
                return selectBrush;
            }

            if (isFilterChanged)
            {
                selectBrush = Brush.AccentBrush;
            }
            else if (isMouseOver)
            {
                selectBrush = Brush.White;
            }
            return BrushResolver.GetBrushFromResources(selectBrush);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
