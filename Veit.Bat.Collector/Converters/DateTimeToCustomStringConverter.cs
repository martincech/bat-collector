﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Veit.Bat.Collector.Converters
{
    public class DateTimeToCustomStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !DateTimeOffset.TryParse(value.ToString(), out var timeStamp))
            {
                return string.Empty;
            }

            return timeStamp.DayOfYear == DateTimeOffset.Now.DayOfYear ? timeStamp.ToString("T") + " Today" : timeStamp.LocalDateTime.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
