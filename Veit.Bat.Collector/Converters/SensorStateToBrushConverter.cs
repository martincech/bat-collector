﻿using System;
using System.Globalization;
using System.Windows.Data;
using Veit.Bat.Collector.Models.Enums;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Bat.Collector.Converters
{
    public class SensorStateToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var selectBrush = Brush.AccentBrush;
            if (value == null || !Enum.TryParse(value.ToString(), out SensorState state))
            {
                return BrushResolver.GetBrushFromResources(selectBrush);
            }

            switch (state)
            {
                case SensorState.Active:
                    selectBrush = Brush.AccentBrush;
                    break;
                case SensorState.Inactive:
                    selectBrush = Brush.Red;
                    break;
                default:
                    selectBrush = Brush.AccentBrush;
                    break;
            }
            return BrushResolver.GetBrushFromResources(selectBrush);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
