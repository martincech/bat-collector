﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Veit.Bat.Collector.Models.Enums;

namespace Veit.Bat.Collector.Converters
{
    public class SensorStatusInfoConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Count() != 2 ||
               !Enum.TryParse(values[0].ToString(), out SensorState state) ||
               !Enum.TryParse(values[1].ToString(), out SensorConnection connection))
            {
                return string.Empty;
            }

            var message = Properties.Resources.StateInactive;
            if (state == SensorState.Active)
            {
                message = Properties.Resources.StateActive;
            }

            switch (connection)
            {
                case SensorConnection.Inactive:
                    return message + " " + Properties.Resources.AndDisconnected;
                case SensorConnection.Connected:
                    return message + " " + Properties.Resources.AndConnected;
                default:
                    return string.Empty;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
