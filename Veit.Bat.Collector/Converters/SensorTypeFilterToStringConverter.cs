﻿using System;
using System.Globalization;
using System.Windows.Data;
using Veit.Bat.Collector.Models.Enums;

namespace Veit.Bat.Collector.Converters
{
    public class SensorTypeFilterToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !Enum.TryParse(value.ToString(), out SensorType type))
            {
                return string.Empty;
            }

            switch (type)
            {
                case SensorType.Bat2:
                    return Properties.Resources.DevicesBat2;
                case SensorType.Thermometer:
                    return Properties.Resources.DevicesThermometer;
                case SensorType.Hydrometer:
                    return Properties.Resources.DevicesHydrometers;
                case SensorType.Co2:
                    return Properties.Resources.DevicesCo2;
                case SensorType.All:
                    return Properties.Resources.DevicesAll;
                default:
                    return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
