﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using Veit.Bat.Common.Sample.Base;
using Veit.Wpf.Resources.Dictionaries;

namespace Veit.Bat.Collector.Converters
{
    public class CheckSampleInRangeToBrushConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var selectBrush = Brush.Dark;
            if (values == null || values.Count() != 3)
            {
                return BrushResolver.GetBrushFromResources(selectBrush);
            }

            if (values[0] is Models.StatisticSample sample &&
                values[1] is Models.StatisticSample lowLimit &&
                values[2] is Models.StatisticSample highLimit)
            {
                selectBrush = GetBrushforStatisticSample(sample, lowLimit, highLimit);
            }
            else if (values[0] is TimeStampedSample current &&
                     values[1] is TimeStampedSample low &&
                     values[2] is TimeStampedSample high)
            {
                selectBrush = GetBrushforSimpleSample(current, low, high);
            }
            return BrushResolver.GetBrushFromResources(selectBrush);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }


        private static Brush GetBrushforSimpleSample(TimeStampedSample sample, TimeStampedSample low, TimeStampedSample high)
        {
            if (sample.Value >= low.Value &&
                sample.Value <= high.Value)
            {
                return Brush.AccentBrush;
            }
            return Brush.Red;
        }

        private static Brush GetBrushforStatisticSample(Models.StatisticSample sample, Models.StatisticSample low, Models.StatisticSample high)
        {
            try
            {
                if (sample.FemaleValue != null &&
                    !IsValueInRange(sample.FemaleValue.Value, low.FemaleValue.Value, high.FemaleValue.Value))
                {
                    return Brush.Red;
                }
                if (sample.MaleValue != null &&
                    !IsValueInRange(sample.MaleValue.Value, low.MaleValue.Value, high.MaleValue.Value))
                {
                    return Brush.Red;
                }
                if (sample.UndefinedValue != null &&
                    !IsValueInRange(sample.UndefinedValue.Value, low.UndefinedValue.Value, high.UndefinedValue.Value))
                {
                    return Brush.Red;
                }
                return Brush.AccentBrush;
            }
            catch (Exception)
            {
                return Brush.Dark;
            }
        }

        private static bool IsValueInRange(int value, int low, int high)
        {
            return value >= low && value <= high;
        }
    }
}
