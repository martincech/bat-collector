﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Veit.Bat.Collector.Converters
{
    /// <summary>
    /// If bool property is binds to 2 radio buttons as [on, off] state
    /// use this converter to prevent incorrect binding (from inverted state).
    /// </summary>
    public class BoolToRadioButtonStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !bool.TryParse(value.ToString(), out var val) ||
                parameter == null || !bool.TryParse(parameter.ToString(), out var param))
            {
                return null;
            }
            return val == param;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null && value.Equals(true) ? parameter : Binding.DoNothing;
        }
    }
}
