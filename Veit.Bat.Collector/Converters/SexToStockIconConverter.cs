﻿using System;
using System.Globalization;
using System.Windows.Data;
using Veit.Bat.Common;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.Converters
{
    public class SexToStockIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !Enum.TryParse(value.ToString(), out Sex sex))
            {
                return StockIcon.MaleFemaleSign;
            }

            switch (sex)
            {
                case Sex.Female:
                    return StockIcon.FemaleSign;
                case Sex.Male:
                    return StockIcon.MaleSign;
                case Sex.Undefined:
                default:
                    return StockIcon.MaleFemaleSign;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
