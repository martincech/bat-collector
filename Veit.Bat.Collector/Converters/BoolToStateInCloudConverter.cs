﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Veit.Bat.Collector.Converters
{
    public class BoolToStateInCloudConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !bool.TryParse(value.ToString(), out var flag))
            {
                return string.Empty;
            }

            if (flag)
            {
                return Properties.Resources.CloudValueSaved;
            }
            return Properties.Resources.CloudValueNotSaved;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
