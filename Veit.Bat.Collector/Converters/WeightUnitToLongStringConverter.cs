﻿using System;
using System.Globalization;
using System.Windows.Data;
using Veit.Bat.Common.Units;

namespace Veit.Bat.Collector.Converters
{
    public class WeightUnitToLongStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || !Enum.TryParse<WeightUnits>(value.ToString(), out var unit))
            {
                return string.Empty;
            }

            switch (unit)
            {
                case WeightUnits.G:
                    return Properties.Resources.Grams;
                case WeightUnits.LB:
                    return Properties.Resources.Pounds;
                case WeightUnits.KG:
                    return Properties.Resources.kilograms;
                default:
                    return string.Empty;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
