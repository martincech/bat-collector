﻿using ModbusSensors.Service;

namespace Veit.Bat.Collector.ViewModels.Services
{
    public class ModbusServiceViewModel : ServiceBaseViewModel
    {
        public ModbusServiceViewModel()
            : base(ModbusSensorsService.Name)
        {
        }
    }
}
