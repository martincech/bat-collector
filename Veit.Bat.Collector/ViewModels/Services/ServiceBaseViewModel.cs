﻿using NLog;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Services
{
    public abstract class ServiceBaseViewModel : ObservableObject, IServiceBaseViewModel
    {
        private bool isRunning;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();

        protected ServiceBaseViewModel(string serviceName)
        {
            ServiceName = serviceName;
        }


        public string ServiceName { get; }

        public bool IsRunning
        {
            get { return isRunning; }
            set
            {
                if (SetProperty(ref isRunning, value))
                {
                    LogIsRunningChanged();
                }
            }
        }

        private void LogIsRunningChanged()
        {
            var msg = ServiceName + " was STOPPED";
            var level = LogLevel.Warn;
            if (isRunning)
            {
                msg = ServiceName + " is running";
                level = LogLevel.Info;
            }

            logger.Log(level, msg);
        }
    }
}
