﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Services
{
    public class ServicesManagerViewModel : ObservableObject
    {
        private readonly IEnumerable<ServiceBaseViewModel> services;

        public ServicesManagerViewModel(IEnumerable<ServiceBaseViewModel> services)
        {
            this.services = services;

            if (services == null)
            {
                return;
            }
            foreach (var service in services)
            {
                service.PropertyChanged += Service_PropertyChanged;
            }
        }

        public event EventHandler<IServiceBaseViewModel> ServiceRunningChanged;


        private void Service_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!(sender is ServiceBaseViewModel service))
            {
                return;
            }

            if (e.PropertyName.Equals(nameof(service.IsRunning)))
            {
                ServiceRunningChanged?.Invoke(this, service);
            }
        }

        public bool IsAnyServiceRunning
        {
            get { return services.Any(a => a.IsRunning); }
        }
    }
}
