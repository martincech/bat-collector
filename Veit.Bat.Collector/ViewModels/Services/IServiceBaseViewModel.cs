﻿namespace Veit.Bat.Collector.ViewModels.Services
{
    public interface IServiceBaseViewModel
    {
        string ServiceName { get; }
        bool IsRunning { get; set; }
    }
}
