﻿using System.Windows.Input;
using Veit.Wpf.Commands;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Helper
{
    public abstract class FilterViewModel : ObservableObject
    {
        private ICommand resetFilterCommand;
        private bool filterIsChanged;


        public bool FilterIsChanged { get { return filterIsChanged; } set { SetProperty(ref filterIsChanged, value); } }

        public ICommand ResetFilterCommand
        {
            get
            {
                return resetFilterCommand ?? (resetFilterCommand = new RelayCommand(
                () =>
                {
                    ResetFilter();
                    FilterIsChanged = false;
                },
                () => true));
            }
        }

        protected abstract void ResetFilter();
    }
}
