﻿using System.Threading.Tasks;
using MahApps.Metro.Controls.Dialogs;

namespace Veit.Bat.Collector.ViewModels.Helper
{
    public class BaseDialog
    {
        private readonly IDialogCoordinator dialogCoordinator;
        private readonly CustomDialog dialog;

        public BaseDialog(IDialogCoordinator dialogCoordinator, CustomDialog dialog)
        {
            this.dialogCoordinator = dialogCoordinator;
            this.dialog = dialog;

            this.dialog.DialogSettings.OwnerCanCloseWithDialog = true;
        }

        public bool IsVisible { get; private set; }

        public async Task ShowDialogAsync()
        {
            IsVisible = true;
            await dialogCoordinator.ShowMetroDialogAsync(this, dialog);
            await dialog.WaitUntilUnloadedAsync();
        }

        public async Task HideDialogAsync()
        {
            IsVisible = false;
            await dialogCoordinator.HideMetroDialogAsync(this, dialog);
        }
    }
}
