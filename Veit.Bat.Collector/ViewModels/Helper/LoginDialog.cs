﻿using System;
using System.Threading.Tasks;
using MahApps.Metro.Controls.Dialogs;
using Veit.Bat.Collector.Views.Dialogs;

namespace Veit.Bat.Collector.ViewModels.Helper
{
    public class LoginDialog : BaseDialog
    {
        private readonly Action closeAction;

        public LoginDialog(IDialogCoordinator dialogCoordinator, TryLoginDialog dialog, Action closeAction)
            : base(dialogCoordinator, dialog)
        {
            this.closeAction = closeAction;

            dialog.CloseRequest += Dialog_CloseRequestAsync;
            dialog.CloseButton.Click += CloseButton_Click;
        }


        private void CloseButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            closeAction?.Invoke();
        }


        private async void Dialog_CloseRequestAsync(object sender, EventArgs e)
        {
            await HideDialogAsync();
        }
    }
}
