﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Communication.Contract.Wcf.WebApi;
using NLog;
using Veit.Bat.Collector.IoC;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.Notification;
using Veit.Bat.Collector.Repository.Sensor;
using Veit.Bat.Collector.Service_References.Modbus;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.ViewModels;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorEditViewModel : ErrorValidationViewModel
    {
        #region Private fields

        private readonly Action closeAction;
        private readonly Action goToSectionMenuAction;
        private readonly SensorsViewModel sensorsVm;
        private readonly SensorValidator validator;
        private ICommand closeCommand;
        private ICommand saveCommand;
        private ICommand deleteSensorCommand;
        private SensorViewModel sensor;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IModbusCommands deviceCommands;
        private readonly ISensorDataRepository repository;
        private readonly IApiDevice apiDevice;

        #endregion


        public SensorEditViewModel(Action closeAction,
                                   Action goToSectionMenuAction,
                                   SensorsViewModel sensorsVm,
                                   SensorValidator validator,
                                   IModbusCommands deviceCommands,
                                   ISensorDataRepository repository,
                                   IApiDevice apiDevice)
        {
            this.closeAction = closeAction;
            this.sensorsVm = sensorsVm;
            this.goToSectionMenuAction = goToSectionMenuAction;
            this.validator = validator;
            this.apiDevice = apiDevice;
            this.deviceCommands = deviceCommands ?? throw new ArgumentNullException(nameof(deviceCommands));
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));

            Sensor = sensorsVm.SelectedSensor.Clone();
        }

        public SensorViewModel Sensor { get { return sensor; } set { SetProperty(ref sensor, value); } }

        public ICommand CloseCommand
        {
            get
            {
                return closeCommand ?? (closeCommand = new RelayCommand(
                () =>
                {
                    closeAction.Invoke();
                },
                () => true));
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                return saveCommand ?? (saveCommand = new RelayCommand(
                () =>
                {
                    OnValidate();
                    if (IsValidationError)
                    {
                        return;
                    }

                    Sensor.Copy(sensorsVm.SelectedSensor);
                    repository.AddOrUpdateSensor(sensorsVm.SelectedSensor);
                    closeAction.Invoke();
                },
                () => true));
            }
        }

        public ICommand DeleteSensorCommand
        {
            get
            {
                return deleteSensorCommand ?? (deleteSensorCommand = new RelayCommand(
                () =>
                {
                    TryDeactivateScale();
                    sensorsVm.RemoveSensor(sensorsVm.SelectedSensor);
                    goToSectionMenuAction.Invoke();
                },
                () => true));
            }
        }


        protected override void OnValidate()
        {
            base.OnValidate();

            switch (Sensor.Type)
            {
                case SensorType.Thermometer:
                case SensorType.Hydrometer:
                case SensorType.Co2:
                    ValidateSensor();
                    break;
                case SensorType.Bat2:
                    ValidateScale();
                    break;
                default:
                    break;
            }
        }

        #region Private Helpers

        /// <summary>
        /// If device is modbus scale, it will be deactivated.
        /// </summary>
        private void TryDeactivateScale()
        {
            if (!(Sensor is Bat2ModbusSensorViewModel bat2))
            {
                return;
            }

            var removeSuccess = deviceCommands.DeactivateDeviceCommand().Invoke(bat2.Gateway, new List<string> { bat2.Uid });
            if (!removeSuccess)
            {
                var errMsg = $"Cannot remove device: {bat2.Uid}";
                logger.Warn(errMsg);
                ToastNotifier.ShowWarning(errMsg);
            }
        }

        /// <summary>
        /// For modbus scale check only name (gateway and device are read only).
        /// </summary>
        private void ValidateScale()
        {
            if (!(Sensor is Bat2SensorViewModel bat2))
            {
                return;
            }

            var errs = ValidateName();
            if (bat2.ScaleType == ScaleType.Bat2Modbus)
            {
                ChangeNameForModbusScale(ref errs, (Bat2ModbusSensorViewModel)bat2);
            }

            NotifyError(errs);
        }

        private void ChangeNameForModbusScale(ref ICollection<string> errs, Bat2ModbusSensorViewModel bat2)
        {
            if (bat2.Connection == SensorConnection.Inactive)
            {
                errs.Add("Name cannot be changed for disconnected scale!");
                return;
            }

            if (bat2.Version == Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums.DeviceVersion.Bat2V150)
            {
                if (!bat2.Name.Equals(sensorsVm.SelectedSensor.Name))
                {
                    errs.Add($"This version of scale: '{bat2.Version}' has no editable name!");
                }
                return;
            }

            if (!bat2.IsBat2ScaleNameValid(bat2.Name))
            {
                errs.Add(Bat2SensorViewModel.GetErrorMessageForBadBat2CableName(bat2.Version));
            }
            else
            {
                if (!deviceCommands.ChangeDeviceNameCommand().Invoke(bat2.Gateway, (byte)bat2.Address, bat2.Name))
                {
                    errs.Add(Properties.Resources.ModbusScaleWriteNameFailed);
                }
                else
                {   //update device in API
                    apiDevice.SyncDevice(new Communication.Contract.Wcf.DataContracts.Device.DeviceNotification
                    {
                        Action = Communication.Contract.Wcf.DataContracts.Device.DeviceAction.Update,
                        Uid = bat2.Uid,
                        Name = bat2.Name
                    });
                }
            }
        }


        private void ValidateSensor()
        {
            var errs = ValidateName();
            if (((RepositorySensorViewModel)Sensor).DataSource.Equals(string.Empty))
            {
                errs.Add(string.Format(Properties.Resources.ErrItemMissing, Properties.Resources.DataSourceFile));
            }

            NotifyError(errs);
        }

        private void NotifyError(IEnumerable<string> errors)
        {
            if (errors.Any())
            {
                SetError(errors);
                logger.Info($"Sensor edit: validation errors: {string.Join(";", errors)}");
            }
        }

        private ICollection<string> ValidateName()
        {
            var errs = new List<string>();
            if (Sensor.Name.Equals(string.Empty))
            {
                errs.Add(string.Format(Properties.Resources.ErrItemMissing, Properties.Resources.Name));
            }
            else
            {  //check if name is unique
                if (!validator.IsNameUnique(Sensor.Type, Sensor.Name, true))
                {
                    errs.Add(Properties.Resources.ErrSensorNameExist);
                }
            }
            return errs;
        }

        #endregion
    }
}
