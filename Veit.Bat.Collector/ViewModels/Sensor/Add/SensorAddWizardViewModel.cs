﻿using System;
using System.Collections.Generic;
using System.Linq;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.Service_References.Modbus;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels.Wizard;

namespace Veit.Bat.Collector.ViewModels.Sensor.Add
{
    public class SensorAddWizardViewModel : WizardViewModel
    {
        private readonly AddSensorViewModel contentVm;
        private readonly SensorsViewModel sensorsVm;
        private readonly SensorValidator validator;
        private readonly IModbusCommands deviceCommands;

        public SensorAddWizardViewModel(PageModel model,
                                        Action closeWizardAction,
                                        AddSensorViewModel contentVm,
                                        SensorsViewModel sensorsVm,
                                        SensorValidator validator,
                                        IModbusCommands deviceCommands)
           : base(model, closeWizardAction, contentVm, "Add Device")
        {
            this.contentVm = contentVm;
            this.sensorsVm = sensorsVm;
            this.validator = validator;
            this.deviceCommands = deviceCommands;
            WizardFinished += SensorAddWizardViewModel_WizardFinished;
        }


        private void SensorAddWizardViewModel_WizardFinished(object sender, EventArgs e)
        {
            if (!contentVm.Houses.Contains(contentVm.House))
            {
                //TODO: house - if is new, raise some action for create a new house
            }
            sensorsVm.AddSensor(contentVm.CreateSensor());
        }


        protected override void OnValidate()
        {
            base.OnValidate();

            switch (Step)
            {
                case 0:
                    ValidateStepSensorType();
                    break;
                case 1:
                    ValidateLocation();
                    break;
                default:
                    break;
            }
        }

        private void ValidateStepSensorType()
        {
            if (contentVm.DeviceType == DeviceType.Scale)
            {
                ValidateScale();
            }
        }

        private void ValidateScale()
        {
            if (contentVm.ScaleType == ScaleType.Bat2Modbus)
            {
                ValidateScaleModbus();
            }
        }

        private void ValidateScaleModbus()
        {
            //check gateway and device
            var errs = new List<string>();
            if (string.IsNullOrEmpty(contentVm.SelectedGateway))
            {
                errs.Add(string.Format(Properties.Resources.ErrItemMissing, Properties.Resources.Gateway));
            }
            if (contentVm.SelectedDevice == null)
            {
                errs.Add(string.Format(Properties.Resources.ErrItemMissing, Properties.Resources.Device));
            }

            if (errs.Any())
            {
                SetError(errs);
            }
        }

        private void ValidateLocation()
        {
            var errs = new List<string>();

            //for sensors check name (scales can set name from other settings - GSM from tel., modbus from gateway and device, ...)
            if (contentVm.DeviceType == DeviceType.Sensor && string.IsNullOrEmpty(contentVm.Name))
            {
                errs.Add(string.Format(Properties.Resources.ErrItemMissing, Properties.Resources.Name));
            }

            //check name if is unique for all sensor types
            if (!string.IsNullOrEmpty(contentVm.Name) && !validator.IsNameUnique(contentVm.SensorType, contentVm.Name))
            {
                errs.Add(Properties.Resources.ErrSensorNameExist);
            }

            //check house
            //if (string.IsNullOrEmpty(contentVm.House))
            //{
            //    errs.Add(Properties.Resources.ErrHouseMissing);
            //}


            var msg = string.Empty;
            if (!CheckDeviceName(ref msg))
            {
                errs.Add(msg);
            }


            if (errs.Any())
            {
                SetError(errs);
            }
        }


        private bool CheckDeviceName(ref string errorMessage)
        {
            //if name of modbus scale was changed manually in form -> need to be checked and update in scale, if it is correct
            if (contentVm.SensorType == SensorType.Bat2 &&
                contentVm.ScaleType == ScaleType.Bat2Modbus &&
                contentVm.Name != contentVm.SelectedDevice.Name &&
                !ChangeModbusScaleName(ref errorMessage))
            {
                return false;
            }
            return true;
        }


        private bool ChangeModbusScaleName(ref string errorMessage)
        {
            if (!contentVm.SelectedDevice.IsBat2ScaleNameValid(contentVm.Name))
            {
                errorMessage = Bat2SensorViewModel.GetErrorMessageForBadBat2CableName(contentVm.SelectedDevice.Version);
                return false;
            }

            if (!deviceCommands.ChangeDeviceNameCommand().Invoke(contentVm.SelectedGateway, (byte)contentVm.SelectedDevice.Address, contentVm.Name))
            {
                errorMessage = Properties.Resources.ModbusScaleWriteNameFailed;
                return false;
            }
            return true;
        }
    }
}
