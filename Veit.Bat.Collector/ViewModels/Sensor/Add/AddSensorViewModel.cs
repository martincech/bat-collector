﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Communication.DataSource;
using Microsoft.Win32;
using NLog;
using Veit.Bat.Collector.IoC;
using Veit.Bat.Collector.Models;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Bat.Collector.Notification;
using Veit.Bat.Collector.Service_References.Modbus;
using Veit.Bat.Common.Units;
using Veit.Bat.Modbus.Common;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.Models;
using Veit.Wpf.Cursor;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Sensor.Add
{
    public class AddSensorViewModel : ObservableObject, IProgressBar
    {
        #region Private fields

        private DeviceType deviceType;
        private SensorType sensorType;
        private ScaleType scaleType;
        private string phoneNumber;
        private WeightUnits weightUnit;
        private SensorPlacement sensorPlacement;
        private string dataSourceFile;
        private ICommand searchDataSourceFileCommand;
        private ICommand scanGatewaysCommand;
        private ICommand scanDevicesCommand;
        private string name;
        private string house;
        private ObservableCollection<string> houses;
        private bool isDevicesLoaded;
        private bool isGatewaysLoaded;
        private ObservableCollection<string> gateways;
        private string selectedGateway;
        private ObservableCollection<Bat2ModbusSensorViewModel> devices;
        private Bat2ModbusSensorViewModel selectedDevice;
        private readonly IModbusCommands modbusCommands;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ISensorsWatcher sensors;
        private byte scanAddressFrom;
        private byte scanAddressTo;
        private const byte MINIMUM_MODBUS_ADDRESS = 1;
        private const byte MAXIMUM_MODBUS_ADDRESS = 247;
        private int scanGatewaysCount;
        private readonly Dictionary<string, string> uidMappings;

        #endregion

        #region Constructor

        public AddSensorViewModel(IEnumerable<string> houses,
                                  IModbusCommands modbusCommands,
                                  ISensorsWatcher sensors,
                                  Dictionary<string, string> uidMappings)
        {
            Houses = new ObservableCollection<string>(houses);
            Gateways = new ObservableCollection<string>();
            Devices = new ObservableCollection<Bat2ModbusSensorViewModel>();
            this.uidMappings = uidMappings;

            WeightUnit = WeightUnits.KG;
            SensorPlacement = SensorPlacement.In;
            SensorType = SensorType.Bat2;
            ScaleType = ScaleType.Bat2Modbus;
            this.modbusCommands = modbusCommands;
            this.sensors = sensors;
            sensors.ConnectedSensors.CollectionChanged += ConnectedSensors_CollectionChanged;
            ScanAddressFrom = MINIMUM_MODBUS_ADDRESS;
            ScanAddressTo = 10;
            DeviceType = DeviceType.Scale;
        }

        #endregion

        #region Public Properties

        public DeviceType DeviceType { get { return deviceType; } set { SetProperty(ref deviceType, value); } }
        public SensorType SensorType { get { return sensorType; } set { SetProperty(ref sensorType, value); } }
        public ScaleType ScaleType { get { return scaleType; } set { SetProperty(ref scaleType, value); } }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set
            {
                if (SetProperty(ref phoneNumber, value))
                {
                    SetName(value);
                }
            }
        }

        public WeightUnits WeightUnit { get { return weightUnit; } set { SetProperty(ref weightUnit, value); } }
        public SensorPlacement SensorPlacement { get { return sensorPlacement; } set { SetProperty(ref sensorPlacement, value); } }
        public string DataSourceFile { get { return dataSourceFile; } set { SetProperty(ref dataSourceFile, value); } }
        public string House { get { return house; } set { SetProperty(ref house, value); } }
        public ObservableCollection<string> Houses { get { return houses; } set { SetProperty(ref houses, value); } }
        public bool IsDevicesLoaded { get { return isDevicesLoaded; } set { SetProperty(ref isDevicesLoaded, value); } }
        public bool IsGatewaysLoaded { get { return isGatewaysLoaded; } set { SetProperty(ref isGatewaysLoaded, value); } }
        public ObservableCollection<string> Gateways { get { return gateways; } set { SetProperty(ref gateways, value); } }
        public string SelectedGateway { get { return selectedGateway; } set { SetProperty(ref selectedGateway, value); } }
        public ObservableCollection<Bat2ModbusSensorViewModel> Devices { get { return devices; } set { SetProperty(ref devices, value); } }
        public byte ScanAddressFrom { get { return scanAddressFrom; } set { SetProperty(ref scanAddressFrom, value); } }
        public byte ScanAddressTo { get { return scanAddressTo; } set { SetProperty(ref scanAddressTo, value); } }

        public byte MinimumModbusScanAddress
        {
            get { return MINIMUM_MODBUS_ADDRESS; }
        }

        public byte MaximumModbusScanAddress
        {
            get { return MAXIMUM_MODBUS_ADDRESS; }
        }


        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


        public Bat2ModbusSensorViewModel SelectedDevice
        {
            get { return selectedDevice; }
            set
            {
                //New Sensor "Name" can be other type of device then modbus scale,
                //therefore there is not direct binding and modbus when scale read it name
                //must be assign to global Name property
                if (selectedDevice != null)
                {
                    selectedDevice.PropertyChanged -= SelectedDevice_PropertyChanged;
                }

                if (SetProperty(ref selectedDevice, value))
                {
                    SetName(value.Name);
                }
                selectedDevice.PropertyChanged += SelectedDevice_PropertyChanged;
            }
        }

        private void SelectedDevice_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(selectedDevice.Name)))
            {
                SetName(selectedDevice.Name);
            }
        }


        #endregion

        #region Commands

        public ICommand SearchDataSourceFileCommand
        {
            get
            {
                return searchDataSourceFileCommand ?? (searchDataSourceFileCommand = new RelayCommand(
                () =>
                {
                    var dialog = new OpenFileDialog
                    {
                        Filter = "(.csv)|*.csv"
                    };

                    var result = dialog.ShowDialog();
                    if (result.HasValue && result.Value)
                    {
                        DataSourceFile = dialog.FileName;
                    }
                },
                () => true));
            }
        }

        public ICommand ScanGatewaysCommand
        {
            get
            {
                return scanGatewaysCommand ?? (scanGatewaysCommand = new RelayCommand(
                () =>
                {
                    var waitCursor = new WaitCursorScopeGuard();
                    try
                    {
                        logger.Debug("Start scan modbus gateways");
                        LoadAlreadyAddedGateways();
                        ModbusEthernetGateway.Discover(EthernetGatewayDiscoverAction());

                        if (scanGatewaysCount == 0)
                        {
                            const string msg = "No gateway has been found";
                            ToastNotifier.ShowInformation(msg);
                            logger.Info(msg);
                        }
                        else
                        {
                            logger.Debug($"{scanGatewaysCount} gateways was found.");
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Info(e, "Scan modbus gateway FAILED!");
                    }
                    finally
                    {
                        waitCursor.Dispose();
                    }
                },
                () => true));
            }
        }


        public ICommand ScanDevicesCommand
        {
            get
            {
                return scanDevicesCommand ?? (scanDevicesCommand = new RelayCommand(
                () =>
                {
                    logger.Debug("Start scan scales");
                    if (SelectedGateway == null)
                    {
                        var err = ContainerLocator.Resolve<IErrorView>();
                        err.DisplayError("Please, select a gateway");
                        return;
                    }

                    var waitCuror = new WaitCursorScopeGuard();
                    try
                    {
                        ScanModbusDevicesRoutine();
                    }
                    catch (Exception e)
                    {
                        logger.Warn(e, "Scan modbus devices error");
                    }
                    finally
                    {
                        waitCuror.Dispose();
                    }
                },
                () => true));
            }
        }

        private void ScanModbusDevicesRoutine()
        {
            var devicesToScan = new List<string> { SelectedGateway };
            var addressesForScan = Enumerable.Range(ScanAddressFrom, ScanAddressTo - ScanAddressFrom + 1).Select(i => (byte)i).ToList();

            //exclude already added scales from scanning addresses
            ExcludeAlreadyAddedScalesFromScan(ref addressesForScan);

            logger.Debug($"Gateway: {SelectedGateway}, scan for scales on addresses: [{string.Join(";", addressesForScan)}]");
            if (!modbusCommands.ScanComPortCommand().Invoke(devicesToScan, addressesForScan))
            {
                const string msg = "Scales scan was failed!";
                logger.Info(msg);
                ToastNotifier.ShowWarning(msg);
            }
            IsDevicesLoaded = true;
        }


        public double Value { get; set; }

        #endregion

        public SensorViewModel CreateSensor()
        {
            try
            {
                var sensor = CreateSensorInstance();

                //common sensor properties
                if (DeviceType == DeviceType.Sensor)
                {
                    ((RepositorySensorViewModel)sensor).DataSource = DataSourceFile;
                    sensor.Name = Name;  //repository sensor has already specific name by user
                    sensor.Uid = Name;
                    sensor.Parent = sensors.Sources.FirstOrDefault(f => f.Type == Communication.Connectivity.SourceType.External);
                }

                //common global properties
                //sensor.Where = House;
                sensor.State = SensorState.Inactive;
                sensor.Connection = SensorConnection.Inactive;
                TryActivateDevice(sensor);
                return sensor;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region Private helpers

        private void LoadAlreadyAddedGateways()
        {
            Gateways.Clear();
            var modbusSources = sensors.Sources.Where(w => w.Type == Communication.Connectivity.SourceType.Ethernet);
            if (modbusSources.Any())
            {
                foreach (var src in modbusSources)
                {
                    Gateways.Add(src.Name);
                }
                SelectedGateway = Gateways.FirstOrDefault();
            }
        }



        private Action<System.Net.IPEndPoint> EthernetGatewayDiscoverAction()
        {
            scanGatewaysCount = 0;

            return o =>
            {
                var gate = $"{o.Address}:{o.Port}";
                scanGatewaysCount++;
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    logger.Info($"Gateway: {gate} was discovered.");
                    if (!Gateways.Contains(gate) && !uidMappings.ContainsKey(gate))
                    {
                        AddSource(gate);
                    }
                    if (Gateways.Any())
                    {
                        IsGatewaysLoaded = true;
                    }
                }));
            };
        }


        private void ExcludeAlreadyAddedScalesFromScan(ref List<byte> addresses)
        {
            var modbusScales = sensors.ActivatedSensors.OfType<Bat2ModbusSensorViewModel>();
            var exceptAddresses = modbusScales.Select(s => (byte)s.Address);
            addresses = addresses.Except(exceptAddresses).ToList();
        }

        private void TryActivateDevice(SensorViewModel sensor)
        {
            if (!(sensor is Bat2ModbusSensorViewModel scale))
            {
                return;
            }

            var scales = new List<string> { scale.Uid };
            if (!modbusCommands.ActivateDeviceCommand().Invoke(SelectedGateway, scales))
            {   //scales activation failed
                var scalesFormatList = string.Join(";", scales);
                var msg = $"Cannot add devices: [{scalesFormatList}] for source: {SelectedGateway}";

                ToastNotifier.ShowWarning(msg);
                logger.Warn(msg);
            }
        }

        private void SetName(string value)
        {
            Name = value;
        }

        private void ConnectedSensors_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    var newSensors = e.NewItems.Cast<SensorViewModel>().ToList();
                    UpdateScannedDevices(newSensors);
                }));
            }
        }

        private void UpdateScannedDevices(IEnumerable<SensorViewModel> newSensors)
        {
            foreach (var item in newSensors.OfType<Bat2ModbusSensorViewModel>().Where(w => w.State == SensorState.Inactive))
            {
                Devices.Add(item);
            }

            if (SelectedDevice == null && Devices.Any())
            {
                SelectedDevice = Devices.FirstOrDefault();
            }
        }

        private void AddSource(string gate)
        {
            if (!modbusCommands.AddSourceCommand().Invoke(gate))
            {   //add source failed
                var msg = $"Add modbus source: {gate} failed!";
                logger.Info(msg);
                ToastNotifier.ShowWarning(msg);
            }
            else
            {
                Gateways.Add(gate);
                if (SelectedGateway == null)
                {
                    SelectedGateway = gate;
                }
            }
        }

        private SensorViewModel CreateSensorInstance()
        {
            SensorViewModel sensor = null;
            if (DeviceType == DeviceType.Sensor)
            {
                switch (SensorType)
                {
                    case SensorType.Thermometer:
                        sensor = new TemperatureSensorViewModel { Placement = SensorPlacement };
                        break;
                    case SensorType.Hydrometer:
                        sensor = new HumiditySensorViewModel();
                        break;
                    case SensorType.Co2:
                        sensor = new Co2SensorViewModel();
                        break;
                    default:
                        return null;
                }
            }
            else if (DeviceType == DeviceType.Scale)
            {
                sensor = CreateScaleInstance();
            }
            return sensor;
        }

        private SensorViewModel CreateScaleInstance()
        {
            SensorViewModel sensor = null;
           if (ScaleType == ScaleType.Bat2Modbus)
            {
                sensor = new Bat2ModbusSensorViewModel(SelectedGateway, SelectedDevice.Address)
                {
                    Uid = DataSourceExtensions.FormatIdOfDataSource((byte)SelectedDevice.Address, SelectedGateway),
                    Parent = new SourceDevice
                    {
                        Type = Communication.Connectivity.SourceType.Ethernet,
                        Name = SelectedGateway
                    }
                };
                AssignNameToScale(sensor, sensor.Uid);
            }
            return sensor;
        }

        private void AssignNameToScale(SensorViewModel sensor, string defaultName)
        {
            if (string.IsNullOrEmpty(Name))
            {
                sensor.Name = defaultName;
            }
            else
            {
                sensor.Name = Name;
            }
        }

        #endregion
    }
}
