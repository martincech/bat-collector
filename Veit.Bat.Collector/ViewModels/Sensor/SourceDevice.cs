﻿using System;
using Communication.Connectivity;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SourceDevice : IEquatable<SourceDevice>
    {
        public SourceType Type { get; set; }
        public string Name { get; set; }
        public object Settings { get; set; }


        #region Equality

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((SourceDevice)obj);
        }

        public virtual bool Equals(SourceDevice other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return
                 (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name, StringComparison.Ordinal)
                ) &&
                (
                    Type == other.Type ||
                    Type.Equals(other.Type)
                );
        }

        public static bool operator ==(SourceDevice left, SourceDevice right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SourceDevice left, SourceDevice right)
        {
            return !Equals(left, right);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)Type * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }

        #endregion
    }
}
