﻿using System.IO;
using System.Windows.Input;
using Microsoft.Win32;
using Veit.Bat.Collector.Models.Enums;
using Veit.Wpf.Commands;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public abstract class RepositorySensorViewModel : SensorViewModel
    {
        private string dataSource;
        private ICommand searchDataSourceCommand;

        protected RepositorySensorViewModel(SensorType type)
           : base(type)
        {
        }

        public string DataSource { get { return dataSource; } set { SetProperty(ref dataSource, value); } }

        public ICommand SearchDataSourceCommand
        {
            get
            {
                return searchDataSourceCommand ?? (searchDataSourceCommand = new RelayCommand(
                () =>
                {
                    var dialog = new OpenFileDialog
                    {
                        InitialDirectory = Path.GetDirectoryName(DataSource),
                        Filter = "(.csv)|*.csv"
                    };

                    var result = dialog.ShowDialog();
                    if (result.HasValue && result.Value)
                    {
                        DataSource = dialog.FileName;
                    }
                },
                () => true));
            }
        }
    }
}
