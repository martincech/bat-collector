﻿using System;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.ViewModels.Helper;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorDetailFilterViewModel : FilterViewModel
    {
        private DateTime? filterFrom;
        private DateTime? filterTo;
        private DateTime start;
        private DateTime end;
        private DataPeriod period;

        private const DataPeriod DEFAULT_PERIOD = DataPeriod.Hour;

        public SensorDetailFilterViewModel(DateTimeOffset from, DateTimeOffset to)
        {
            Start = from.DateTime;
            End = to.DateTime;
            PropertyChanged += SensorDetailFilterViewModel_PropertyChanged;
        }

        public DataPeriod Period { get { return period; } set { SetProperty(ref period, value); } }

        public DateTime Start
        {
            get { return start; }
            set { SetProperty(ref start, value); }
        }
        public DateTime End
        {
            get { return end; }
            set { SetProperty(ref end, value); }
        }


        public DateTime? FilterFrom
        {
            get { return filterFrom; }
            set { SetProperty(ref filterFrom, value); }
        }
        public DateTime? FilterTo
        {
            get { return filterTo; }
            set { SetProperty(ref filterTo, value); }
        }


        protected override void ResetFilter()
        {
            FilterFrom = new DateTime(Start.Year, Start.Month, Start.Day);
            FilterTo = new DateTime(End.Year, End.Month, End.Day);
            Period = DEFAULT_PERIOD;
        }

        private void SensorDetailFilterViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(Start)) || e.PropertyName.Equals(nameof(End)))
            {
                ResetFilter();
                return;
            }

            if (e.PropertyName.Equals(nameof(FilterFrom)) ||
                e.PropertyName.Equals(nameof(FilterTo)) ||
                e.PropertyName.Equals(nameof(Period)))
            {
                UpdateFilterChangeFlag();
            }
        }

        private void UpdateFilterChangeFlag()
        {
            if (!FilterFrom.HasValue || !FilterTo.HasValue)
            {
                return;
            }

            FilterIsChanged = DateTime.Compare(FilterFrom.Value.Date, Start.Date) != 0 ||
                              DateTime.Compare(FilterTo.Value.Date, End.Date) != 0 ||
                              Period != DEFAULT_PERIOD;
        }
    }
}
