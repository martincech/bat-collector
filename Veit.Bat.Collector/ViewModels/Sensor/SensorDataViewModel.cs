﻿using Veit.Bat.Common.Sample.Base;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorDataViewModel : ObservableObject
    {
        private TimeStampedSample lowLimit;
        private TimeStampedSample highLimit;
        private TimeStampedSample sample;
        private bool savedInCloud;

        public TimeStampedSample LowLimit { get { return lowLimit; } set { SetProperty(ref lowLimit, value); } }
        public TimeStampedSample HighLimit { get { return highLimit; } set { SetProperty(ref highLimit, value); } }
        public TimeStampedSample Sample { get { return sample; } set { SetProperty(ref sample, value); } }
        public bool SavedInCloud { get { return savedInCloud; } set { SetProperty(ref savedInCloud, value); } }
    }
}
