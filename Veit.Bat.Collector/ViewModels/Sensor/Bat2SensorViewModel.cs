﻿using System.Linq;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;
using Veit.Bat.Collector.Models.Enums;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public abstract class Bat2SensorViewModel : SensorViewModel
    {
        #region Private fields

        private const int BAT2_SCALE_NAME_MAX_LENGTH = 10; //maximum length of name for BAT2 v1.5x
        public const int BAT2_V111_ID_MINIMUM = 1;
        public const int BAT2_V111_ID_MAXIMUM = 32767;
        public const long BAT2_V153_ID_MAXIMUM = 9999999999;

        private ScaleType scaleType;

        #endregion

        protected Bat2SensorViewModel(ScaleType scale)
           : base(SensorType.Bat2)
        {
            ScaleType = scale;
        }

        public ScaleType ScaleType { get { return scaleType; } private set { SetProperty(ref scaleType, value); } }
        public DeviceVersion Version { get; set; }


        public bool IsBat2ScaleNameValid(string scaleName)
        {
            if (Version == DeviceVersion.Bat2V111)
            {
                return IsBat2ScaleNameVersion111Valid(scaleName);
            }
            if (Version == DeviceVersion.Bat2V150)
            {   //This version has no editable name!
                return false;
            }

            //other versions
            if (scaleName.Count() > BAT2_SCALE_NAME_MAX_LENGTH) return false;
            return scaleName.All(c => c >= '0' && c <= '9');
        }

        private static bool IsBat2ScaleNameVersion111Valid(string name)
        {
            if (!int.TryParse(name, out var value))
            {
                return false;
            }
            return value >= BAT2_V111_ID_MINIMUM && value <= BAT2_V111_ID_MAXIMUM;
        }


        /// <summary>
        /// Get error message for invalid scale name (BAT2 cable)
        /// corresponding with it version.
        /// </summary>
        /// <param name="version"></param>
        public static string GetErrorMessageForBadBat2CableName(DeviceVersion version)
        {
            const string messageBase = "Scale name is not valid! ";
            switch (version)
            {
                case DeviceVersion.Bat2V111:
                    return messageBase + $"For version: '{version}' must be in range: 1 - {BAT2_V111_ID_MAXIMUM}";
                case DeviceVersion.Bat2V153:
                    return messageBase + $"For version: '{version}' must be in range: 1 - {BAT2_V153_ID_MAXIMUM}";
                default:
                    return $"For scale version: '{version}' cannot change name!";
            }
        }
    }
}
