﻿namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class Bat2ModbusSensorViewModel : Bat2SensorViewModel
    {
        private string gateway;
        private int address;

        public Bat2ModbusSensorViewModel(string gateway, int address)
           : base(Models.Enums.ScaleType.Bat2Modbus)
        {
            Gateway = gateway;
            Address = address;
        }

        public string Gateway { get { return gateway; } private set { SetProperty(ref gateway, value); } }
        public int Address { get { return address; } private set { SetProperty(ref address, value); } }
    }
}
