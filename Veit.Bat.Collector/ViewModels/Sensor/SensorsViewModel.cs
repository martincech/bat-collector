﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Communication.Connectivity;
using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;
using Communication.Contract.Wcf.WebApi;
using NLog;
using Settings;
using Settings.Extensions;
using Veit.Bat.Collector.Helpers;
using Veit.Bat.Collector.Models;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Bat.Collector.Repository.Sensor;
using Veit.Bat.Collector.Service_References;
using Veit.Bat.Collector.Service_References.Modbus;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Sample.Weight;
using Veit.Bat.Common.Units;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorsViewModel : PageViewModel, IHardwareDevices, ISensorsWatcher
    {
        #region Private fields

        private readonly object lockObj = new object();
        private readonly ISensorDataRepository repository;
        private SensorsBarMenuViewModel overview;
        private readonly ObservableCollection<SensorViewModel> sensors;
        private ObservableCollection<SensorViewModel> filteredSensors;
        private SensorViewModel selectedSensor;

        private readonly Action showDetailAction;
        private ICommand showDetailCommand;
        private ICommand selectedSensorChangedCommand;
        private readonly SensorFilterViewModel filterVm;

        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private readonly IDeviceConfigurationCommands deviceConfigurationCommands;
        private readonly IManagerSettings managerSettings;
        private SensorViewModel previousSelectedSensor;
        private readonly IApiDevice apiDevice;

        #endregion

        public SensorsViewModel(PageModel model,
                                ISensorDataRepository repository,
                                Action showDetailAction,
                                SensorFilterViewModel filterVm,
                                IDeviceConfigurationCommands deviceConfigurationCommands,
                                IManagerSettings managerSettings,
                                IApiDevice apiDevice)
           : base(model)
        {
            this.filterVm = filterVm;
            this.showDetailAction = showDetailAction;
            this.repository = repository;
            this.deviceConfigurationCommands = deviceConfigurationCommands;
            this.managerSettings = managerSettings;
            this.apiDevice = apiDevice;

            Sources = new ObservableCollection<SourceDevice>();
            ConnectedSensors = new ObservableCollection<SensorViewModel>();
            FilteredSensors = new ObservableCollection<SensorViewModel>();
            sensors = new ObservableCollection<SensorViewModel>();
            sensors.CollectionChanged += Sensors_CollectionChanged;
            filterVm.PropertyChanged += SensorsViewModel_PropertyChanged;

            ThreadPool.QueueUserWorkItem(o =>
            {
                if (repository == null) return;
                LoadData();
                UpdateFilteredDevices();
            });
        }

        public event EventHandler<bool> SensorsStateChanged;

        public ICommand ShowDetailCommand
        {
            get
            {
                return showDetailCommand ?? (showDetailCommand = new RelayCommand(
                () =>
                {
                    showDetailAction.Invoke();
                },
                () => true));
            }
        }


        public ICommand SelectedSensorChangedCommand
        {
            get
            {
                return selectedSensorChangedCommand ?? (selectedSensorChangedCommand = new RelayCommand<SensorViewModel>(
                o =>
                {
                    if (o == null) return;
                    SelectedSensor = o;
                },
                o => true));
            }
        }

        public SensorsBarMenuViewModel Overview
        {
            get { return overview; }
            set
            {
                SetProperty(ref overview, value);
                if (value != null)
                {
                    UpdateOverview();
                }
            }
        }

        public ObservableCollection<SensorViewModel> FilteredSensors { get { return filteredSensors; } set { SetProperty(ref filteredSensors, value); } }
        public SensorViewModel SelectedSensor
        {
            get { return selectedSensor; }
            set
            {
                BackupSelection(value);
                SetProperty(ref selectedSensor, value);
                if (value == null)
                {
                    return;
                }
                ShowDetailCommand.Execute(null);
            }
        }


        /// <summary>
        /// Previous selected sensor. When you leave sensor detail,
        /// selected sensor is cancel, but in some cases you need to know
        /// which one was previously selected (e.g. in navigation between pages)
        /// </summary>
        public SensorViewModel PreviousSelectedSensor
        {
            get
            {
                if (previousSelectedSensor == null)
                {
                    return null;
                }

                var existedSensor = sensors.FirstOrDefault(f => f.Uid.Equals(previousSelectedSensor.Uid));
                if (existedSensor == null)
                {   //previous selected sensor is not already exist (deleted)
                    return null;
                }
                return previousSelectedSensor;
            }
            private set { previousSelectedSensor = value; }
        }



        public IEnumerable<SensorViewModel> ActivatedSensors
        {
            get { return sensors.Clone(); }
        }

        public ObservableCollection<SourceDevice> Sources { get; private set; }
        public ObservableCollection<SensorViewModel> ConnectedSensors { get; private set; }

        public void RemoveSensor(SensorViewModel removedSensor)
        {
            if (removedSensor == null) return;
            lock (lockObj)
            {
                DeleteDeviceFromSettings(removedSensor);
                sensors.Remove(removedSensor);
                UpdateFilteredDevices();
                CheckSensorsOverviewState();
            }
        }

        public void AddSensor(SensorViewModel newSensor)
        {
            if (newSensor == null) return;
            lock (lockObj)
            {
                var existSensor = sensors.FirstOrDefault(f => f.Uid.Equals(newSensor.Uid));
                if (existSensor != null)
                {   //update (modbus scales are added by activated event, but from add wizard are set additional properties)
                    existSensor.Where = newSensor.Where;
                    existSensor.Name = newSensor.Name;
                }
                else
                {   //add new
                    SaveDeviceToSettings(newSensor);
                    sensors.Add(newSensor);
                    UpdateFilteredDevices();

                    apiDevice.SyncDevice(new Communication.Contract.Wcf.DataContracts.Device.DeviceNotification
                    {
                        Action = Communication.Contract.Wcf.DataContracts.Device.DeviceAction.Add,
                        Uid = newSensor.Uid,
                        Name = newSensor.Name
                    });
                }
            }
        }

        #region Filtration

        private void SensorsViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(filterVm.SensorType)) ||
                e.PropertyName.Equals(nameof(filterVm.SensorState)) ||
                e.PropertyName.Equals(nameof(filterVm.SensorConnection)) ||
                e.PropertyName.Equals(nameof(filterVm.SelectedFilterWhere)))
            {
                UpdateFilteredDevices();
            }
        }

        private void UpdateFilteredDevices()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                var filtered = new List<SensorViewModel>();
                foreach (var sensor in GetSafeSensors())
                {
                    if (!filterVm.SensorTypeIsSelected(sensor.Type)) continue;
                    if (!filterVm.SensorWhereIsSelected(sensor.Where)) continue;
                    if (!filterVm.SensorStateIsSelected(sensor.State)) continue;
                    if (!filterVm.SensorConnectionIsSelected(sensor.Connection)) continue;
                    filtered.Add(sensor);
                }

                FilteredSensors = new ObservableCollection<SensorViewModel>(filtered);
                UpdateOverview();
            });
        }

        private IEnumerable<SensorViewModel> GetSafeSensors()
        {
            lock (lockObj)
            {
                return sensors.ToList();
            }
        }

        #endregion

        #region Implementation of IHardwareDevices

        public void ConnectDevice(SourceType sourceType, string deviceSource)
        {
            lock (lockObj)
            {
                logger.Debug($"Connect source: {sourceType}/{deviceSource}");
                var src = Sources.FirstOrDefault(s => s.Name.Equals(deviceSource) && s.Type == sourceType);
                if (src != null)
                {
                    return;
                }

                var source = new SourceDevice
                {
                    Name = deviceSource,
                    Type = sourceType,
                    Settings = GetSettings(sourceType)
                };
                repository.AddOrUpdateSource(source);
                Sources.Add(source);
            }
        }

        private object GetSettings(SourceType sourceType)
        {
            if (sourceType != SourceType.Ethernet)
            {
                return null;
            }
            return managerSettings.LoadModbusSettings();
        }

        public void ConnectDevice(Common.DeviceType deviceType, string source, string sensorUid)
        {
            lock (lockObj)
            {
                logger.Debug($"Connect Device: type: {deviceType}, source: {source}, uid: {sensorUid}");
                if (!IsKnownDevice(source, sensorUid))
                {
                    AddToKnownDevices(deviceType, source, sensorUid);
                }
                ChangeDeviceConnection(source, sensorUid, SensorConnection.Connected);
            }
        }


        public void DisconnectDevice(SourceType sourceType, string source)
        {
            lock (lockObj)
            {
                logger.Debug($"Disconnect source: {sourceType}/{source}");
                //disconnect all its children
                DisconnectKnownDevices(source);
            }
        }

        public void DisconnectDevice(Common.DeviceType deviceType, string source, string deviceId)
        {
            lock (lockObj)
            {
                logger.Debug($"Disconnect Device: {deviceType}/{source}/{deviceId}");
                ChangeDeviceConnection(source, deviceId, SensorConnection.Inactive);

                //Disconnect non activated modbus scale
                var connectedSensor = ConnectedSensors.FirstOrDefault(f => f.Uid.Equals(deviceId));
                if (connectedSensor != null)
                {
                    ConnectedSensors.Remove(connectedSensor);
                }
            }
        }

        public void DisconnectDevices(SourceType sourceType)
        {
            lock (lockObj)
            {
                logger.Debug($"Disconnect Devices - by source {sourceType}");
                foreach (var src in Sources.Where(w => w.Type == sourceType))
                {
                    DisconnectKnownDevices(src.Name);
                }
            }
        }

        public void ActivateDevice(Common.DeviceType deviceType, string source, string deviceId)
        {
            lock (lockObj)
            {
                logger.Debug($"Activate Device: {deviceType}/{source}/{deviceId}");

                //activated are only modbus cable scales
                var scale = ConnectedSensors.FirstOrDefault(f => f.Uid.Equals(deviceId));
                if (scale != null)
                {
                    ConnectedSensors.Remove(scale);
                    AddSensor(scale);
                }
                else
                {   //modbus scale which was already add - reload information from scale
                    ThreadPool.QueueUserWorkItem(o =>
                    {
                        var device = (Bat2ModbusSensorViewModel)sensors.FirstOrDefault(f => f.Uid.Equals(deviceId));
                        if (device == null)
                        {
                            return;
                        }
                        deviceConfigurationCommands.GetDeviceConfigurationCommand().Invoke(source, (byte)device.Address);
                    });
                }
                ChangeDeviceState(source, deviceId, SensorState.Active);
                ChangeDeviceConnection(source, deviceId, SensorConnection.Connected);
            }
        }



        public void DeactivateDevice(Common.DeviceType deviceType, string source, string deviceId)
        {
            lock (lockObj)
            {
                logger.Debug($"Deactivate Device: {deviceType}/{source}/{deviceId}");
                ChangeDeviceState(source, deviceId, SensorState.Inactive);
                apiDevice.SyncDevice(new Communication.Contract.Wcf.DataContracts.Device.DeviceNotification
                {
                    Action = Communication.Contract.Wcf.DataContracts.Device.DeviceAction.Delete,
                    Uid = deviceId
                });
            }
        }

        public void SampleRead(TimeSample sample)
        {
            lock (lockObj)
            {
                logger.Debug($"Sample read: {sample.SensorUid}/{sample.TimeStamp}");
                var uid = sample.SensorUid;

                var device = sensors.FirstOrDefault(f => f.Uid.Equals(uid));
                if (device == null)
                {
                    return;
                }
                SetDeviceSpecificParameters(device, sample);
                device.State = SensorState.Active;
                CheckSensorsOverviewState();
            }
        }

        public void Configuration(DeviceIdentification device, DeviceConfiguration configuration)
        {
            logger.Info($"Device {device.DeviceType}/{device.Uid} configuration read. Version: {configuration.Version}, name: {configuration.ScaleName}");

            var scale = GetModbusScale(device.Uid);
            if (scale == null)
            {
                return;
            }
            scale.Name = configuration.ScaleName;
            scale.Version = configuration.Version;
        }

        public void Name(DeviceIdentification device, string name)
        {
            logger.Info($"Device {device.DeviceType}/{device.Uid} name read. Name: {name}");
            var scale = GetModbusScale(device.Uid);
            if (scale == null)
            {
                return;
            }
            scale.Name = name;
        }

        public void Version(DeviceIdentification device, DeviceVersion version)
        {
            logger.Info($"Device {device.DeviceType}/{device.Uid} version read. Version: {version}");
            var scale = GetModbusScale(device.Uid);
            if (scale == null)
            {
                return;
            }
            scale.Version = version;
        }

        #region IHardwareDevices helpers

        private Bat2ModbusSensorViewModel GetModbusScale(string uid)
        {
            var scale = (Bat2ModbusSensorViewModel)sensors.FirstOrDefault(f => f.Uid.Equals(uid) && f is Bat2ModbusSensorViewModel);
            if (scale != null)
            {
                return scale;
            }
            var notActivateScale = (Bat2ModbusSensorViewModel)ConnectedSensors.FirstOrDefault(f => f.Uid.Equals(uid) && f is Bat2ModbusSensorViewModel);
            return notActivateScale;
        }

        private void SetDeviceSpecificParameters(SensorViewModel device, TimeSample sample)
        {
            //sensors and bat2 1.53 send samples with time, others (GSM and bat2 cable 1.11 and 1.50) need set to current time stamp
            if (!(sample is TimeStampedSample sensorSample))
            {
                //is it statistic sample from GSM scale?
                if (sample is Common.Sample.Statistics.StatisticSample stat)
                {
                    sensorSample = CreateLastStatisticSample(stat);
                    if (sensorSample == null)
                    {
                        return;
                    }

                    CheckDeviceNameFromReadData(device, stat.ScaleName);
                    device.Name = stat.ScaleName;
                }
                else
                {
                    return;
                }
            }

            SaveLastSampleTimeStampToSensor(device, sensorSample);
        }

        /// <summary>
        /// From read data we find out, that scale name was change
        /// (probably manually changed in scale).
        /// </summary>
        /// <param name="device"></param>
        /// <param name="scaleName"></param>
        private void CheckDeviceNameFromReadData(SensorViewModel device, string scaleName)
        {
            if (!(device is Bat2SensorViewModel bat2) || bat2.ScaleType != ScaleType.Bat2Modbus)
            {
                return;
            }
            if (device.Name == scaleName)
            {
                return;
            }

            apiDevice.SyncDevice(new Communication.Contract.Wcf.DataContracts.Device.DeviceNotification
            {
                Action = Communication.Contract.Wcf.DataContracts.Device.DeviceAction.Update,
                Uid = device.Uid,
                Name = scaleName
            });
        }

        private static void SaveLastSampleTimeStampToSensor(SensorViewModel device, TimeStampedSample sample)
        {
            if (device.HourSamples.Any())
            {
                device.HourSamples[0].Sample = sample;
            }
            else
            {
                device.HourSamples.Add(new SensorDataViewModel { Sample = sample });
            }
        }

        private static TimeStampedSample CreateLastStatisticSample(Common.Sample.Statistics.StatisticSample stat)
        {
            const WeightUnits units = WeightUnits.KG; //bat2 modbus scales (1.11 and 1.50) are always in KG

            //sample contains only date, not time => it is set to current time
            var statistic = new StatisticSample { TimeStamp = stat.TimeStamp.SetCurrentTime() };
            if (stat.MaleData != null && stat.FemaleData != null)
            {
                statistic.MaleValue = ConvertScaleValueToGrams(units, stat.MaleData.Average);
                statistic.FemaleValue = ConvertScaleValueToGrams(units, stat.FemaleData.Average);
            }
            else
            {
                statistic.UndefinedValue = ConvertScaleValueToGrams(units, stat.FemaleData.Average);
            }
            return statistic;
        }

        private static int ConvertScaleValueToGrams(WeightUnits unitsInScale, float average)
        {
            var weight = new Weight(average, unitsInScale);
            return (int)weight.AsG;
        }


        private bool IsKnownDevice(string source, string uid)
        {
            return sensors.Any(s => s.Parent.Name.Equals(source) && s.Uid.Equals(uid));
        }

        private void DisconnectKnownDevices(string source)
        {
            var devices = sensors.Where(w => w.Parent != null && w.Parent.Name.Equals(source));
            foreach (var device in devices)
            {
                device.Connection = SensorConnection.Inactive;
            }
        }

        private void AddToKnownDevices(Common.DeviceType deviceType, string source, string uid)
        {
            var src = Sources.FirstOrDefault(f => f.Name.Equals(source));
            var newDevice = SensorDataRepositoryExtension.CreateDevice(deviceType, src, uid);
            newDevice.Name = uid;   //new connected device has the same name as id


            if (newDevice is Bat2ModbusSensorViewModel scale)
            {   //cable scales are added after they are activated
                ConnectedSensors.Add(newDevice);
                ThreadPool.QueueUserWorkItem(o =>
                {
                    deviceConfigurationCommands.GetDeviceConfigurationCommand().Invoke(source, (byte)scale.Address);
                });
            }
            else
            {
                AddSensor(newDevice);
            }
        }


        private void ChangeDeviceConnection(string source, string uid, SensorConnection connection)
        {
            var device = sensors.FirstOrDefault(s => s.Parent != null && s.Parent.Name.Equals(source) && s.Uid.Equals(uid));
            if (device == null)
            {
                return;
            }
            device.Connection = connection;
            CheckSensorsOverviewState();
        }

        private void ChangeDeviceState(string source, string uid, SensorState state)
        {
            var device = sensors.FirstOrDefault(s => s.Parent != null && s.Parent.Name.Equals(source) && s.Uid.Equals(uid));
            if (device == null)
            {
                return;
            }
            device.State = state;
            CheckSensorsOverviewState();
        }

        #endregion

        #endregion

        #region Private helpers

        private void BackupSelection(SensorViewModel newSelection)
        {
            if (newSelection == null && selectedSensor != null)
            {
                PreviousSelectedSensor = selectedSensor;
            }
        }

        private void SaveDeviceToSettings(SensorViewModel device)
        {
            repository.AddOrUpdateSensor(device);
        }

        private void DeleteDeviceFromSettings(SensorViewModel device)
        {
            repository.DeleteSensor(device);
        }

        private void LoadData()
        {
            lock (lockObj)
            {
                var data = repository.GetSensors();
                if (data == null) return;
                GetSourcesFromDevices(data);
                sensors.Clear();
                foreach (var item in data)
                {
                    sensors.Add(item);
                }
            }
        }

        private void GetSourcesFromDevices(IEnumerable<SensorViewModel> data)
        {
            var distinctParents = data.GroupBy(x => x.Parent).Select(s => s.First());
            Sources.Clear();
            foreach (var parent in distinctParents.Select(s => s.Parent))
            {
                Sources.Add(parent);
            }
        }

        private void Sensors_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            lock(lockObj)
            {
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    SensorsChangedAdd(e.NewItems);
                }
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    SensorsChangedRemove(e.OldItems);
                }
                CheckSensorsOverviewState();
            }
        }

        private void SensorsChangedAdd(IList newItems)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                foreach (var item in newItems)
                {
                    if (!(item is SensorViewModel sensor)) continue;
                    filterVm.UpdateWhereCollection(sensor.Where);

                    sensor.PropertyChanged += Sensor_PropertyChanged;
                }
            }));
        }

        private void SensorsChangedRemove(IList oldItems)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                foreach (var item in oldItems)
                {
                    if (!(item is SensorViewModel sensor)) continue;
                    sensor.PropertyChanged -= Sensor_PropertyChanged;
                }
            }));
        }

        #region ISensorsWatcher implementation

        private void Sensor_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            lock(lockObj)
            {
                if (!(sender is SensorViewModel sensor))
                {
                    return;
                }

                if (e.PropertyName.Equals(nameof(sensor.Connection)) ||
                    e.PropertyName.Equals(nameof(sensor.State)))
                {
                    CheckSensorsOverviewState();
                }
            }
        }

        private void CheckSensorsOverviewState()
        {
            var errSensor = sensors.FirstOrDefault(f => f.State == SensorState.Inactive || f.Connection == SensorConnection.Inactive);
            SensorsStateChanged?.Invoke(this, errSensor == null);
        }

        #endregion


        private void UpdateOverview()
        {
            if (DesignerHelper.IsInDesignMode())
            {
                return;
            }

            ThreadPool.QueueUserWorkItem(o =>
            {
                var list = GetSafeSensors();
                var bat2s = list.Count(f => f.Type == SensorType.Bat2);
                var thermometers = list.Count(f => f.Type == SensorType.Thermometer);
                var hydrometers = list.Count(f => f.Type == SensorType.Hydrometer);
                var co2s = list.Count(f => f.Type == SensorType.Co2);

                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    Overview.Bat2Count = bat2s;
                    Overview.ThermometerCount = thermometers;
                    Overview.HydrometerCount = hydrometers;
                    Overview.Co2Count = co2s;
                }));
            });
        }

        #endregion
    }
}
