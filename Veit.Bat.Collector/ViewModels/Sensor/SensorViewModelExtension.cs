﻿using System.Collections.Generic;
using System.Linq;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public static class SensorViewModelExtension
    {
        public static IEnumerable<SensorViewModel> Clone(this IEnumerable<SensorViewModel> models)
        {
            return models.Select(s => s.Clone()).ToList();
        }

        public static SensorViewModel Clone(this SensorViewModel model)
        {
            if (model == null) return null;
            SensorViewModel clonedModel = null;
            if (model is TemperatureSensorViewModel tempModel)
            {
                clonedModel = new TemperatureSensorViewModel();
                tempModel.CopyTemperatureProperties((TemperatureSensorViewModel)clonedModel);
            }
            else if (model is HumiditySensorViewModel)
            {
                clonedModel = new HumiditySensorViewModel();
            }
            else if (model is Co2SensorViewModel)
            {
                clonedModel = new Co2SensorViewModel();
            }
            else if (model is Bat2ModbusSensorViewModel bat2ModbusModel)
            {
                clonedModel = new Bat2ModbusSensorViewModel(bat2ModbusModel.Gateway, bat2ModbusModel.Address)
                {
                    Version = bat2ModbusModel.Version
                };
            }

            //clone repository sensor properties
            if (clonedModel is RepositorySensorViewModel repositorySensor)
            {
                ((RepositorySensorViewModel)model).CopyRepositorySensorProperties(ref repositorySensor);
            }

            //clone common sensor properties
            if (clonedModel != null)
            {
                model.CopyCommonSensorProperties(ref clonedModel);
            }
            return clonedModel;
        }

        public static void Copy(this SensorViewModel source, SensorViewModel dest)
        {
            if (source == null || dest == null)
            {
                return;
            }

            //type specific
            if (source is TemperatureSensorViewModel tempSource && dest is TemperatureSensorViewModel tempDest)
            {
                tempSource.CopyTemperatureProperties(tempDest);
            }

            //common properties
            if (source is RepositorySensorViewModel repSource && dest is RepositorySensorViewModel repDest)
            {
                repSource.CopyRepositorySensorProperties(ref repDest);
            }
            source.CopyCommonSensorProperties(ref dest);
        }


        private static void CopyTemperatureProperties(this TemperatureSensorViewModel source, TemperatureSensorViewModel dest)
        {
            dest.Placement = source.Placement;
        }

        private static void CopyRepositorySensorProperties(this RepositorySensorViewModel source, ref RepositorySensorViewModel dest)
        {
            dest.DataSource = source.DataSource;
        }

        private static void CopyCommonSensorProperties(this SensorViewModel source, ref SensorViewModel dest)
        {
            dest.Name = source.Name;
            dest.Where = source.Where;
            dest.State = source.State;
            dest.Connection = source.Connection;
            dest.Uid = source.Uid;
        }
    }
}
