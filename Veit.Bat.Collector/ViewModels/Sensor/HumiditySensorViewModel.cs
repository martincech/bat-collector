﻿using Veit.Bat.Collector.Models.Enums;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class HumiditySensorViewModel : RepositorySensorViewModel
    {
        public HumiditySensorViewModel()
           : base(SensorType.Hydrometer)
        {
        }
    }
}
