﻿using System.Collections.Generic;
using System.Linq;
using Veit.Bat.Collector.Models.Enums;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorValidator
    {
        private readonly SensorsViewModel sensorsVm;

        public SensorValidator(SensorsViewModel sensorsVm)
        {
            this.sensorsVm = sensorsVm;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="excludeSelectedSensor">selected sensor (edit mode) will be discard from searching sensors</param>
        /// <returns></returns>
        public bool IsNameUnique(SensorType type, string name, bool excludeSelectedSensor = false)
        {
            var sensors = sensorsVm.ActivatedSensors.ToList();
            if (excludeSelectedSensor)
            {
                RemoveSelectedSensor(ref sensors);
            }
            return !sensors.Exists(e => e.Type == type && e.Name.Equals(name));
        }

        private void RemoveSelectedSensor(ref List<SensorViewModel> sensors)
        {
            var editingSensor = sensors.FirstOrDefault(f => f.Type == sensorsVm.SelectedSensor.Type && f.Name.Equals(sensorsVm.SelectedSensor.Name));
            if (editingSensor != null)
            {
                sensors.Remove(editingSensor);
            }
        }
    }
}
