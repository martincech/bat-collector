﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Veit.Bat.Collector.Models.Enums;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorDetailViewModel : PageViewModel
    {
        private SensorViewModel detail;
        private ObservableCollection<SensorDataViewModel> filteredData;
        private readonly SensorDetailFilterViewModel filterVm;

        public SensorDetailViewModel(PageModel model,
                                     SensorViewModel detail,
                                     SensorDetailFilterViewModel filterVm)
           : base(model)
        {
            this.filterVm = filterVm;
            Detail = detail ?? throw new ArgumentNullException(nameof(detail));
            filterVm.PropertyChanged += FilterVm_PropertyChanged;
            Detail.HourSamples.CollectionChanged += HourSamples_CollectionChanged;

            FilteredData = new ObservableCollection<SensorDataViewModel>();
            DateFiltersInitialization();
        }

        public SensorViewModel Detail { get { return detail; } private set { SetProperty(ref detail, value); } }
        public ObservableCollection<SensorDataViewModel> FilteredData { get { return filteredData; } set { SetProperty(ref filteredData, value); } }

        private void FilterVm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateFilteredData();
        }

        private void UpdateFilteredData()
        {
            if (!filterVm.FilterFrom.HasValue || !filterVm.FilterTo.HasValue)
            {
                return;
            }

            ThreadPool.QueueUserWorkItem(o =>
            {
                var from = filterVm.FilterFrom.Value.Date;
                var to = filterVm.FilterTo.Value.Date;
                var filtered = new List<SensorDataViewModel>();

                foreach (var data in GetSelectedPeriodSamples())
                {
                    var statTimeStamp = data.Sample.TimeStamp.Date;
                    if (DateTimeOffset.Compare(from, statTimeStamp) <= 0 &&
                        DateTimeOffset.Compare(to, statTimeStamp) >= 0)
                    {
                        filtered.Add(data);
                    }
                }
                FilteredData = new ObservableCollection<SensorDataViewModel>(filtered);
            });
        }

        private IEnumerable<SensorDataViewModel> GetSelectedPeriodSamples()
        {
            switch (filterVm.Period)
            {
                case DataPeriod.Day:
                    return Detail.DaySamples;
                case DataPeriod.Hour:
                    return Detail.HourSamples;
                case DataPeriod.Week:
                    return Detail.WeekSamples;
                default:
                    return new List<SensorDataViewModel>();
            }
        }

        private void HourSamples_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                UpdateDateFilter();
            }
        }

        private void UpdateDateFilter()
        {
            if (!Detail.HourSamples.Any())
            {
                return;
            }

            filterVm.FilterFrom = Detail.HourSamples.LastOrDefault().Sample.TimeStamp.LocalDateTime;
            filterVm.FilterTo = Detail.HourSamples.FirstOrDefault().Sample.TimeStamp.LocalDateTime;
        }

        private void DateFiltersInitialization()
        {
            var from = DateTimeOffset.Now.Date;
            var to = from;

            if (Detail.HourSamples.Any())
            {
                from = Detail.HourSamples.LastOrDefault().Sample.TimeStamp.LocalDateTime;
                to = Detail.HourSamples.FirstOrDefault().Sample.TimeStamp.LocalDateTime;
            }

            filterVm.Start = from;
            filterVm.End = to;
            filterVm.FilterFrom = from;
            filterVm.FilterTo = to;
        }
    }
}
