﻿using System;
using System.Windows.Input;
using Veit.Wpf.Commands;
using Veit.Wpf.Mvvm.Observable;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorDetailBarMenuViewModel : ObservableObject
    {
        private readonly Action editSensorAction;
        private ICommand showEditSensorCommand;

        public SensorDetailBarMenuViewModel(SensorViewModel detail, Action editSensorAction)
        {
            this.editSensorAction = editSensorAction;
            Detail = detail;
        }

        public StockIcon Icon { get { return StockIcon.DialMeter; } }

        public SensorViewModel Detail { get; private set; }


        public ICommand ShowEditSensorCommand
        {
            get
            {
                return showEditSensorCommand ?? (showEditSensorCommand = new RelayCommand(
                () =>
                {
                    editSensorAction.Invoke();
                },
                () => true));
            }
        }
    }
}
