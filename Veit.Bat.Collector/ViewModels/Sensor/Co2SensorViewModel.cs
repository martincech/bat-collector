﻿using Veit.Bat.Collector.Models.Enums;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class Co2SensorViewModel : RepositorySensorViewModel
    {
        public Co2SensorViewModel()
           : base(SensorType.Co2)
        {
        }
    }
}
