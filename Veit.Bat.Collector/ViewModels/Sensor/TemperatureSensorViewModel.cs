﻿using Veit.Bat.Collector.Models.Enums;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class TemperatureSensorViewModel : RepositorySensorViewModel
    {
        private SensorPlacement placement;

        public TemperatureSensorViewModel()
           : base(SensorType.Thermometer)
        {
        }

        public SensorPlacement Placement { get { return placement; } set { SetProperty(ref placement, value); } }
    }
}
