﻿using System.Collections.ObjectModel;
using Veit.Bat.Collector.Models.Enums;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public abstract class SensorViewModel : ObservableObject
    {
        private string name;
        private string uid;
        private SourceDevice parent;
        private string where;   //house name
        private SensorState state;
        private SensorConnection connection;

        private ObservableCollection<SensorDataViewModel> hourSamples;
        private ObservableCollection<SensorDataViewModel> daySamples;
        private ObservableCollection<SensorDataViewModel> weekSamples;

        private SensorType type;

        protected SensorViewModel(SensorType type)
        {
            Type = type;
            HourSamples = new ObservableCollection<SensorDataViewModel>();
            DaySamples = new ObservableCollection<SensorDataViewModel>();
            WeekSamples = new ObservableCollection<SensorDataViewModel>();
        }


        public string Name { get { return name; } set { SetProperty(ref name, value); } }
        public string Uid { get { return uid; } set { SetProperty(ref uid, value); } }
        public SourceDevice Parent { get { return parent; } set { SetProperty(ref parent, value); } }
        public string Where { get { return where; } set { SetProperty(ref where, value); } }
        public SensorState State { get { return state; } set { SetProperty(ref state, value); } }
        public SensorConnection Connection
        {
            get { return connection; }
            set
            {
                SetProperty(ref connection, value);
                if (value == SensorConnection.Inactive)
                {
                    State = SensorState.Inactive;
                }
            }
        }
        public ObservableCollection<SensorDataViewModel> HourSamples { get { return hourSamples; } set { SetProperty(ref hourSamples, value); } }
        public ObservableCollection<SensorDataViewModel> DaySamples { get { return daySamples; } set { SetProperty(ref daySamples, value); } }
        public ObservableCollection<SensorDataViewModel> WeekSamples { get { return weekSamples; } set { SetProperty(ref weekSamples, value); } }
        public SensorType Type { get { return type; } private set { SetProperty(ref type, value); } }
    }
}
