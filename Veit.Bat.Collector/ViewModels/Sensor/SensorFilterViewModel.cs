﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.ViewModels.Helper;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorFilterViewModel : FilterViewModel
    {
        #region Private fields

        private SensorType sensorType;
        private ObservableCollection<string> filterWheres;
        private string selectedFilterWhere;
        private SensorState sensorState;
        private SensorConnection sensorConnection;
        private const string allHousesItem = "All houses";
        private readonly List<string> wheres;

        private const SensorType DEFAULT_SENSOR_TYPE = SensorType.All;
        private const SensorState DEFAULT_SENSOR_STATE = SensorState.All;
        private const SensorConnection DEFAULT_SENSOR_CONNECTION = SensorConnection.All;

        #endregion

        public SensorFilterViewModel()
        {
            wheres = new List<string>();
            FilterWheres = new ObservableCollection<string> { allHousesItem };
            ResetFilter();

            PropertyChanged += SensorFilterViewModel_PropertyChanged;
        }

        public SensorType SensorType { get { return sensorType; } set { SetProperty(ref sensorType, value); } }
        public ObservableCollection<string> FilterWheres { get { return filterWheres; } set { SetProperty(ref filterWheres, value); } }
        public string SelectedFilterWhere { get { return selectedFilterWhere; } set { SetProperty(ref selectedFilterWhere, value); } }
        public SensorState SensorState { get { return sensorState; } set { SetProperty(ref sensorState, value); } }
        public SensorConnection SensorConnection { get { return sensorConnection; } set { SetProperty(ref sensorConnection, value); } }


        public bool SensorTypeIsSelected(SensorType type)
        {
            switch (SensorType)
            {
                case SensorType.All:
                    return true;
                case SensorType.Bat2:
                    return type == SensorType.Bat2;
                case SensorType.Co2:
                    return type == SensorType.Co2;
                case SensorType.Hydrometer:
                    return type == SensorType.Hydrometer;
                case SensorType.Thermometer:
                    return type == SensorType.Thermometer;
                default:
                    return false;
            }
        }

        public bool SensorWhereIsSelected(string where)
        {
            if (SelectedFilterWhere.Equals(allHousesItem) || (SelectedFilterWhere.Equals(where)))
            {
                return true;
            }
            return false;
        }

        public bool SensorStateIsSelected(SensorState state)
        {
            switch (SensorState)
            {
                case SensorState.All:
                    return true;
                case SensorState.Active:
                    return state == SensorState.Active;
                case SensorState.Inactive:
                    return state == SensorState.Inactive;
                default:
                    return false;
            }
        }

        public bool SensorConnectionIsSelected(SensorConnection connection)
        {
            switch (SensorConnection)
            {
                case SensorConnection.All:
                    return true;
                case SensorConnection.Connected:
                    return connection == SensorConnection.Connected;
                case SensorConnection.Inactive:
                    return connection == SensorConnection.Inactive;
                default:
                    return false;
            }
        }

        public void UpdateWhereCollection(string where)
        {
            if (wheres.Contains(where)) return;
            var count = wheres.Count;
            if (count == 0)   //empty list
            {
                wheres.Add(where);
                FilterWheres.Insert(0, where);
            }
            else if (wheres[count - 1].CompareTo(where) <= 0)  //belongs to the end of the collection
            {
                wheres.Add(where);
                FilterWheres.Insert(count, where);
            }
            else if (wheres[0].CompareTo(where) >= 0) //belongs to the begin of the collection
            {
                wheres.Insert(0, where);
                FilterWheres.Insert(0, where);
            }
            else   //find index
            {
                var index = wheres.BinarySearch(where);
                if (index < 0)
                {
                    index = ~index;
                }
                wheres.Insert(index, where);
                FilterWheres.Insert(index, where);
            }
        }

        protected override void ResetFilter()
        {
            SensorType = DEFAULT_SENSOR_TYPE;
            SensorState = DEFAULT_SENSOR_STATE;
            SensorConnection = DEFAULT_SENSOR_CONNECTION;
            SelectedFilterWhere = FilterWheres.Last();
        }

        private void SensorFilterViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(FilterWheres)) || e.PropertyName.Equals(nameof(FilterIsChanged)))
            {
                return;
            }

            FilterIsChanged = (e.PropertyName.Equals(nameof(SensorType)) && SensorType != DEFAULT_SENSOR_TYPE) ||
                (e.PropertyName.Equals(nameof(SensorState)) && SensorState != DEFAULT_SENSOR_STATE) ||
                (e.PropertyName.Equals(nameof(SensorConnection)) && SensorConnection != DEFAULT_SENSOR_CONNECTION) ||
                (e.PropertyName.Equals(nameof(SelectedFilterWhere)) && !SelectedFilterWhere.Equals(allHousesItem));
        }
    }
}
