﻿using System;
using System.Linq;
using System.Windows.Input;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.ViewModels.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.ViewModels.Sensor
{
    public class SensorsBarMenuViewModel : CardSmallViewModel
    {
        private int bat2Count;
        private int thermometerCount;
        private int hydrometerCount;
        private int co2Count;
        private readonly Action addSensorAction;
        private ICommand showAddSensorCommand;
        private readonly ISensorsWatcher watcher;
        private readonly object lockObj = new object();

        public SensorsBarMenuViewModel(Action switchToDetailAction, Action addSensorAction, ISensorsWatcher watcher)
           : base(StockIcon.DialMeter, "Sensors", switchToDetailAction)
        {
            this.addSensorAction = addSensorAction;
            this.watcher = watcher;
            watcher.SensorsStateChanged += Watcher_SensorsStateChanged;
            SetInvalidState();
        }


        public int Bat2Count { get { return bat2Count; } set { SetProperty(ref bat2Count, value); } }
        public int ThermometerCount { get { return thermometerCount; } set { SetProperty(ref thermometerCount, value); } }
        public int HydrometerCount { get { return hydrometerCount; } set { SetProperty(ref hydrometerCount, value); } }
        public int Co2Count { get { return co2Count; } set { SetProperty(ref co2Count, value); } }

        public ICommand ShowAddSensorCommand
        {
            get
            {
                return showAddSensorCommand ?? (showAddSensorCommand = new RelayCommand(
                () =>
                {
                    addSensorAction.Invoke();
                },
                () => true));
            }
        }


        private void Watcher_SensorsStateChanged(object sender, bool e)
        {
            lock(lockObj)
            {
                if (e)
                {
                    SetValidState();
                }
                else
                {
                    SetInvalidState();
                }
            }
        }


        private void SetValidState()
        {
            if (!watcher.ActivatedSensors.Any() && !watcher.ConnectedSensors.Any())
            {
                SetNoSensorState();
            }
            else
            {
                SetOkState();
            }
        }

        private void SetNoSensorState()
        {
            Status = Properties.Resources.SensorStateNoSensor;
            StateIcon = StockIcon.CheckMark;
            Flag = CardState.Warning;
        }

        private void SetOkState()
        {
            Status = Properties.Resources.SensorStateOk;
            StateIcon = StockIcon.CheckMark;
            Flag = CardState.Ok;
        }


        private void SetInvalidState()
        {
            Status = Properties.Resources.SensorStateError;
            StateIcon = StockIcon.ErrorMark;
            Flag = CardState.Error;
        }
    }
}
