﻿using Veit.Wpf.Mvvm.Observable;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.ViewModels.Feedback
{
    public class FeedbackBarMenuViewModel : ObservableObject
    {
        public FeedbackBarMenuViewModel(StockIcon icon)
        {
            Icon = icon;
        }

        public StockIcon Icon { get; private set; }
    }
}
