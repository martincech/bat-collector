﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using NLog;
using Settings.Core;
using Veit.Bat.Collector.Models.Enums;
using Veit.Bat.Collector.Notification;
using Veit.Bat.Collector.ViewModels.Services;
using Veit.Wpf.Commands;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;

namespace Veit.Bat.Collector.ViewModels.Feedback
{
    public class FeedbackDetailViewModel : PageViewModel
    {
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private string feedback;
        private bool includeDiagnostics;
        private ICommand sendFeedbackCommand;
        private readonly SystemInfo systemInfo;
        private readonly FeedbackType type;
        private readonly IDiagnostic diagnostic;
        private readonly IServiceBaseViewModel serviceVm;
        private readonly Action<string, string> displayAction;

        private const string DIAGNOSTIC_NAME_DELIMITER = "_";
        private const string UNKNOWN_USER = "unknown";


        public FeedbackDetailViewModel(PageModel model,
                                       FeedbackType type,
                                       SystemInfo systemInfo,
                                       IDiagnostic diagnostic,
                                       IServiceBaseViewModel serviceVm,
                                       Action<string, string> displayAction)
           : base(model)
        {
            this.type = type;
            this.systemInfo = systemInfo ?? throw new ArgumentNullException(nameof(systemInfo));
            this.diagnostic = diagnostic ?? throw new ArgumentNullException(nameof(diagnostic));
            this.serviceVm = serviceVm ?? throw new ArgumentNullException(nameof(serviceVm));
            this.displayAction = displayAction;

            IncludeDiagnostics = true;
        }

        public string Feedback
        {
            get { return feedback; }
            set
            {
                SetProperty(ref feedback, value);
                UpdateCanSendFeedback();
            }
        }
        public bool IncludeDiagnostics
        {
            get { return includeDiagnostics; }
            set
            {
                SetProperty(ref includeDiagnostics, value);
                UpdateCanSendFeedback();
            }
        }


        public ICommand SendFeedbackCommand
        {
            get
            {
                return sendFeedbackCommand ?? (sendFeedbackCommand = new RelayCommand(
                   () =>
                   {
                       if (!serviceVm.IsRunning)
                       {
                           ToastNotifier.ShowError(Properties.Resources.CannotSendFeedback);
                           logger.Warn(Properties.Resources.CannotSendFeedback);
                           return;
                       }

                       diagnostic.SendFeedback(GetDiagnosticName(), Feedback, IncludeDiagnostics);
                       displayAction?.Invoke(Properties.Resources.ThanksForFeedback, Properties.Resources.WeWillInformYou);
                   },
                   () => IncludeDiagnostics || !string.IsNullOrEmpty(Feedback)
                   ));
            }
        }

        #region Private helpers

        private void UpdateCanSendFeedback()
        {
            ((RelayCommand)SendFeedbackCommand).RaiseCanExecuteChanged();
        }

        private string GetDiagnosticName()
        {
            var properties = new List<string>
            {
                systemInfo.WindowsProductName.Replace(" ", DIAGNOSTIC_NAME_DELIMITER),
                systemInfo.SystemLanguage.Replace(" ", DIAGNOSTIC_NAME_DELIMITER),
                systemInfo.TerminalId.ToString()
            };

            var loggedUserName = UNKNOWN_USER;
            if (systemInfo.LoggedUserName != null && !systemInfo.LoggedUserName.Equals(string.Empty))
            {
                loggedUserName = systemInfo.LoggedUserName;
            }
            properties.Add(loggedUserName);
            properties.Add(type.ToString());

            return string.Join(DIAGNOSTIC_NAME_DELIMITER, properties);
        }

        #endregion
    }
}
