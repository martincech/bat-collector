﻿using Veit.Wpf.Controls.Models.Card;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;

namespace Veit.Bat.Collector.ViewModels.Feedback
{
    public class FeedbackViewModel : PageViewModel
    {
        public FeedbackViewModel(PageModel model, ICardSmall positiveFeedbackCard, ICardSmall negativeFeedbackCard)
           : base(model)
        {
            PositiveFeedbackCard = positiveFeedbackCard;
            NegativeFeedbackCard = negativeFeedbackCard;
        }


        public ICardSmall PositiveFeedbackCard { get; private set; }
        public ICardSmall NegativeFeedbackCard { get; private set; }
    }
}
