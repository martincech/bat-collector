﻿using System.Collections.Generic;
using System.Linq;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Home
{
    public class HomeBarMenuViewModel : ObservableObject
    {
        #region Private fields

        private string title;

        private const string ALL_IS_OK = "Everything is working perfectly!";
        private const string CONNECTION_ERROR = "Network & Settings needs your attention!";
        private const string SENSORS_ERROR = "Sensors need your attention!";
        private const int MAXIMUM_ERRORS = 2;
        private bool connectionState;
        private bool sensorsState;

        #endregion

        public HomeBarMenuViewModel(IConnectionWatcher connectionWatcher, ISensorsWatcher sensorWatcher)
        {
            ConnectionWatcher_CloudConnectionChanged(null, false);
            SensorWatcher_SensorsStateChanged(null, false);
            connectionWatcher.CloudConnectionChanged += ConnectionWatcher_CloudConnectionChanged;
            sensorWatcher.SensorsStateChanged += SensorWatcher_SensorsStateChanged;
        }

        public string Title { get { return title; } set { SetProperty(ref title, value); } }


        #region Private helpers

        private void ConnectionWatcher_CloudConnectionChanged(object sender, bool e)
        {
            connectionState = e;
            UpdateTitle();
        }

        private void SensorWatcher_SensorsStateChanged(object sender, bool e)
        {
            sensorsState = e;
            UpdateTitle();
        }


        private void UpdateTitle()
        {
            var errors = new List<string>();

            if (!connectionState)
            {
                errors.Add(CONNECTION_ERROR);
            }
            if (!sensorsState)
            {
                errors.Add(SENSORS_ERROR);
            }

            Title = !errors.Any() ? ALL_IS_OK : ErrorMessagesSerialization(errors);
        }


        private static string ErrorMessagesSerialization(List<string> messages)
        {
            var permitMountMessages = messages.Take(MAXIMUM_ERRORS);
            return string.Join("\n", permitMountMessages);
        }

        #endregion
    }
}
