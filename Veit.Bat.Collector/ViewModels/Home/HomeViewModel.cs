﻿using Veit.Wpf.Controls.Models.Card;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;

namespace Veit.Bat.Collector.ViewModels.Home
{
    public class HomeViewModel : PageViewModel
    {
        public HomeViewModel(PageModel model, ICardSmall sensorCard, ICardSmall cloudCard)
         : base(model)
        {
            SensorCard = sensorCard;
            CloudCard = cloudCard;
        }

        public ICardSmall SensorCard { get; private set; }
        public ICardSmall CloudCard { get; private set; }
    }
}
