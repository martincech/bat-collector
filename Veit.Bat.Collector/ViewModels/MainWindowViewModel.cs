﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Veit.Bat.Collector.ViewModels.Cloud;
using Veit.Bat.Collector.ViewModels.Helper;
using Veit.Wpf.Controls.Models.Navigation;
using Veit.Wpf.Controls.ViewModels;
using Veit.Wpf.Controls.ViewModels.Navigation;

namespace Veit.Bat.Collector.ViewModels
{
    public class MainWindowViewModel : StandardNavigationViewModel
    {
        private bool isDialogCoordinatorRegistered;

        public MainWindowViewModel(LoginDialog dialog, NavigationService navService, IEnumerable<PageViewModel> pages, CloudConnectionViewModel connectionVm)
            : base(navService, pages)
        {
            Dialog = dialog;
            connectionVm.CloudConnectionChanged += ConnectionWatcher_CloudConnectionChanged;
        }

        public void RegisterDialogCoordinator()
        {
            isDialogCoordinatorRegistered = true;
            Task.Run(async () => { await ShowLoginDialogAsync(); });
        }

        public LoginDialog Dialog { get; }

        #region Private helpers

        private async Task ShowLoginDialogAsync()
        {
            if (!isDialogCoordinatorRegistered || Dialog.IsVisible)
            {
                return;
            }

            await Dialog.ShowDialogAsync();
        }

        private async Task HideLoginDialogAsync()
        {
            if (!isDialogCoordinatorRegistered || !Dialog.IsVisible)
            {
                return;
            }

            await Dialog.HideDialogAsync();
        }

        private void ConnectionWatcher_CloudConnectionChanged(object sender, bool e)
        {
            Task.Run( async () =>
            {
                if (e)
                {
                    await HideLoginDialogAsync();
                }
                else
                {
                    await ShowLoginDialogAsync();
                }
            });
        }

        #endregion
    }
}
