﻿using System.Diagnostics;
using System.Reflection;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Dialogs
{
    public class AboutDialogViewModel : ObservableObject
    {
        public string ProductName
        {
            get => "BAT Collector";
        }

        public string Version
        {
            get => Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public string Copyright
        {
            get => FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).LegalCopyright;
        }

        public string Weblink
        {
            get => "https://www.veit.cz/";
        }
    }
}
