﻿using System;
using System.Collections.Generic;
using System.Threading;
using Settings;
using Settings.Core;
using Veit.Bat.Collector.Models.Enums;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Cloud.Settings
{
    public class ApplicationSettingsViewModel : ObservableObject
    {
        #region Private fields

        private const double EXAMPLE_WEIGHT_G = 1234.56;
        private const double EXAMPLE_TEMPERATURE_C = 30;

        private int decimals;
        private int maxDecimals;
        private Language language;
        private WeightUnit weightUnit;
        private TemperatureUnit temperatureUnit;
        private DateFormat dateFormat;
        private bool diagnosticMode;
        private int hideFinishedAfter;
        private readonly IDiagnosticSettingsManager settingsRepository;
        private readonly IEnumerable<IDiagnostic> diagObjects;
        private readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private global::Settings.DiagnosticSettings.Diagnostics diagnostics;
        private bool isInit;

        #endregion

        public ApplicationSettingsViewModel(IDiagnosticSettingsManager settingsRepository, IEnumerable<IDiagnostic> diagObjects)
        {
            this.settingsRepository = settingsRepository ?? throw new ArgumentNullException(nameof(settingsRepository));
            this.diagObjects = diagObjects ?? throw new ArgumentNullException(nameof(diagObjects));

            ThreadPool.QueueUserWorkItem(o =>
            {
                diagnostics = settingsRepository.LoadDiagnostics();
                DiagnosticMode = diagnostics.UseDebugMode;
                isInit = true;
            });
        }

        #region Properties

        public int Decimals
        {
            get => decimals;
            set
            {
                SetProperty(ref decimals, value);
                RaisePropertyChanged(nameof(WeightExample));
            }
        }

        public int MaxDecimals
        {
            get => maxDecimals;
            set => SetProperty(ref maxDecimals, value);
        }


        public Language Language
        {
            get => language;
            set => SetProperty(ref language, value);
        }

        public WeightUnit WeightUnit
        {
            get => weightUnit;
            set
            {
                SetProperty(ref weightUnit, value);
                MaxDecimals = value.GetMaxDecimals();
                Decimals = MaxDecimals;
                RaisePropertyChanged(nameof(Decimals));
            }
        }

        public string WeightExample
        {
            get => WeightUnit.ConvertToString(EXAMPLE_WEIGHT_G, Decimals);
        }

        public TemperatureUnit TemperatureUnit
        {
            get => temperatureUnit;
            set
            {
                SetProperty(ref temperatureUnit, value);
                RaisePropertyChanged(nameof(TemperatureExample));
            }
        }

        public string TemperatureExample
        {
            get => TemperatureUnit.ConvertToString(EXAMPLE_TEMPERATURE_C);
        }

        public DateFormat DateFormat
        {
            get => dateFormat;
            set
            {
                SetProperty(ref dateFormat, value);
                RaisePropertyChanged(nameof(DateFormatExample));
            }
        }

        public string DateFormatExample
        {
            get => DateFormat.CovertToString(DateTime.Now);
        }

        public bool DiagnosticMode
        {
            get => diagnosticMode;
            set
            {
                if (SetProperty(ref diagnosticMode, value))
                {
                    UpdateDiagnosticMode();
                }
            }
        }

        public int HideFinishedAfter
        {
            get => hideFinishedAfter;
            set => SetProperty(ref hideFinishedAfter, value);
        }

        #endregion


        private void UpdateDiagnosticMode()
        {
            if (!isInit)
            {
                return;
            }

            logger.Info($"Set diagnostic mode: {DiagnosticMode}");
            diagnostics.UseDebugMode = DiagnosticMode;
            settingsRepository.SaveDiagnostic(diagnostics);

            foreach (var obj in diagObjects)
            {
                obj.SetDebugMode(DiagnosticMode);
            }
        }

    }
}
