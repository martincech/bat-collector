﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Communication.Contract.Wcf.DataContracts;
using NLog;
using Settings.Core;
using Veit.Bat.Collector.Helpers;
using Veit.Bat.Collector.Models;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Bat.Collector.Service_References.WebApi;
using Veit.Bat.Collector.ViewModels.Services;
using Veit.Bat.Collector.Views.WebBrowser;
using Veit.Cognito.Service;
using Veit.Wpf.Commands;
using Veit.Wpf.Mvvm.Observable;

namespace Veit.Bat.Collector.ViewModels.Cloud
{
    public class CloudConnectionViewModel : ValidatableObservableObject, IConnectionWatcher
    {
        #region Private fields

        private const string CODE_QP = "?code=";

        private readonly IWebApiCommands commands;
        private string batApi;
        private string sensorsApi;
        private ICommand logoutCommand;
        private bool isLogged;

        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private readonly SystemInfo systemInfo;

        private ICommand pageLoadedCommand;
        private ICommand pageNavigatingCommand;
        private Login loginInfo;
        private string navigateAddress;
        private readonly Action successLoginAction;
        private bool isBusy;
        private readonly ServicesManagerViewModel servicesManager;

        #endregion

        #region Public interfaces

        public event EventHandler<bool> CloudConnectionChanged;

        #region Constructor

        public CloudConnectionViewModel(IWebApiCommands commands,
                                        SystemInfo systemInfo,
                                        Action successLoginAction,
                                        ServicesManagerViewModel servicesManager)
        {
            this.systemInfo = systemInfo;
            if (DesignerHelper.IsInDesignMode())
            {
                return;
            }

            IsBusy = true;
            this.commands = commands ?? throw new ArgumentNullException($"{nameof(commands)} cannot be null");
            CloudConnectionChanged += CloudConnectionViewModel_CloudConnectionChanged;
            this.successLoginAction = successLoginAction;
            this.servicesManager = servicesManager ?? throw new ArgumentNullException($"{nameof(servicesManager)} cannot be null");
            servicesManager.ServiceRunningChanged += ServicesManager_ServiceRunningChanged;

            SetInitialConnection();
        }

        #endregion

        public WebApiConnectionInfo GetInitialConnection()
        {
            return commands.GetConnectionInfo();
        }

        public string BatApi
        {
            get { return batApi; }
            set { SetPropertyAndValidate(ref batApi, value); }
        }

        public string SensorsApi
        {
            get { return sensorsApi; }
            set { SetPropertyAndValidate(ref sensorsApi, value); }
        }

        public bool IsLogged
        {
            get { return isLogged; }
            set
            {
                SetProperty(ref isLogged, value);
                UpdateCommands();

                if (systemInfo != null) systemInfo.LoggedUserName = isLogged ? loginInfo.UserName : "";
                RaisePropertyChanged(() => LoggedUserName);
                InvokeConnectionchanged();
            }
        }

        public string LoggedUserName
        {
            get
            {
                if (systemInfo == null)
                {
                    return string.Empty;
                }
                return systemInfo.LoggedUserName;
            }
        }

        public string LoginWebAddress { get; private set; }
        public string NavigateAddress { get { return navigateAddress; } set { SetProperty(ref navigateAddress, value); } }
        public bool IsBusy
        {
            get { return isBusy; }
            private set { SetProperty(ref isBusy, value); }
        }

        public void SetConnection(WebApiConnectionInfo connectionInfo)
        {
            //if refresh token exist, it will be trying log in to cloud
            IsBusy = !string.IsNullOrEmpty(connectionInfo.Login.RefreshToken);

            loginInfo = connectionInfo.Login;
            BatApi = connectionInfo.BatApi?.Value;
            SensorsApi = connectionInfo.SensorsApi?.Value;
            LoginWebAddress = connectionInfo.Login.CognitoSettings.LoginAddress;
            IsLogged = (connectionInfo.Logged == LoggedStatus.Logged || connectionInfo.Logged == LoggedStatus.BadRequest)
               && !string.IsNullOrEmpty(connectionInfo.Login?.AccessToken);
            if (!IsLogged)
            {
                NavigateAddress = loginInfo.CognitoSettings.LogoutAddress;
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(500);
                    NavigateAddress = loginInfo.CognitoSettings.LoginAddress;
                });
            }
        }

        public ICommand LogoutCommand
        {
            get
            {
                return logoutCommand ?? (logoutCommand = new RelayCommand(
                   () =>
                   {
                       logger.Info($"{loginInfo.UserName} is logging out from BAT Cloud");
                       LogOutAction();
                   },
                   () => true
                   ));
            }
        }

        public ICommand PageLoadedCommand
        {
            get
            {
                return pageLoadedCommand ?? (pageLoadedCommand = new RelayCommand<WebDocument>(
                   async o =>
                   {
                       var document = (System.Windows.Forms.HtmlDocument)o.Document;
                       await TryGetExchangeForTokensAsync(document.Url);
                   },
                   o => true
                   ));
            }
        }

        public ICommand PageNavigatingCommand
        {
            get
            {
                return pageNavigatingCommand ?? (pageNavigatingCommand = new RelayCommand<WebDocument>(
                   async o =>
                   {
                       await TryGetExchangeForTokensAsync(o?.Uri);
                   },
                   o => true
                   ));
            }
        }

        #endregion

        #region Private helpers

        private void ServicesManager_ServiceRunningChanged(object sender, IServiceBaseViewModel e)
        {
            if (!e.IsRunning && !servicesManager.IsAnyServiceRunning)
            {
                IsLogged = false;
            }
        }

        private void SetInitialConnection()
        {
            try
            {
                var connection = GetInitialConnection();
                if (connection == null) return;
                SetConnection(connection);
            }
            catch (Exception e)
            {
                logger.Warn(e, "Cloud connection, SetInitialConnection error");
            }
        }


        private async Task TryGetExchangeForTokensAsync(Uri uri)
        {
            if (IsLogged || uri == null) return;
            var index = uri.AbsoluteUri.LastIndexOf(CODE_QP);
            if (index < 0) return;
            var codeIndex = index + CODE_QP.Length;
            var lengh = uri.AbsoluteUri.Length - codeIndex;
            var code = uri.AbsoluteUri.Substring(codeIndex, lengh);

            if (!string.IsNullOrEmpty(code))
            {
                await GetTokensAsync(code);
            }
        }

        private async Task GetTokensAsync(string code)
        {
            try
            {
                var tokens = await loginInfo.CognitoSettings.Oauth2GetTokensAsync(code);
                SaveTokens(tokens);

                loginInfo.UserName = TokenHelper.GetEmailFromToken(loginInfo.IdToken);
                commands.ChangeServerConnection(GetConnectionInfo());
            }
            catch (Exception e)
            {
                logger.Warn(e, "Trying get tokens error!");
            }
        }

        private void SaveTokens(Dictionary<string, string> tokens)
        {
            loginInfo.IdToken = TokenHelper.ParseToken(tokens, TokenHelper.IdTokenKey);
            loginInfo.AccessToken = TokenHelper.ParseToken(tokens, TokenHelper.AccessTokenKey);

            //if trying valid "refresh token" it is not return, therefore is not in collection
            var refreshToken = TokenHelper.ParseToken(tokens, TokenHelper.RefreshTokenKey);
            if (refreshToken != null)
            {
                loginInfo.RefreshToken = refreshToken;
            }
        }

        private void DeleteTokensInfo()
        {
            loginInfo.UserName = string.Empty;
            loginInfo.IdToken = string.Empty;
            loginInfo.AccessToken = string.Empty;
            loginInfo.RefreshToken = string.Empty;

            Thread.Sleep(500);
            commands.ChangeServerConnection(GetConnectionInfo());
            IsLogged = false;
        }

        private WebApiConnectionInfo GetConnectionInfo()
        {
            var connectionInfo = new WebApiConnectionInfo
            {
                Login = loginInfo,
                BatApi = string.IsNullOrEmpty(BatApi) ? null : new Url(BatApi),
                SensorsApi = string.IsNullOrEmpty(SensorsApi) ? null : new Url(SensorsApi)
            };
            return connectionInfo;
        }

        private void LogOutAction()
        {
            if (NavigateAddress != null && NavigateAddress.Equals(loginInfo.CognitoSettings.LogoutAddress))
            {  //force update
                NavigateAddress = string.Empty;
            }
            NavigateAddress = loginInfo.CognitoSettings.LogoutAddress;
            DeleteTokensInfo();
            IsBusy = false;
        }

        private void UpdateCommands()
        {
            ((RelayCommand)LogoutCommand).RaiseCanExecuteChanged();
        }

        private void InvokeConnectionchanged()
        {
            CloudConnectionChanged?.Invoke(this, isLogged);
        }

        private void CloudConnectionViewModel_CloudConnectionChanged(object sender, bool e)
        {
            if (!e)
            {
                return;
            }

            successLoginAction?.Invoke();
        }

        #endregion
    }
}
