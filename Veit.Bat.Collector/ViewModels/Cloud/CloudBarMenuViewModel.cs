﻿using System;
using Veit.Bat.Collector.Models.Watcher;
using Veit.Wpf.Controls.ViewModels.Card;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.ViewModels.Cloud
{
    public class CloudBarMenuViewModel : CardSmallViewModel
    {
        private string detailStatus;


        public CloudBarMenuViewModel(Action switchToDetailAction, IConnectionWatcher watcher)
           : base(StockIcon.CloudUpload, "Network & Settings", switchToDetailAction)
        {
            SetDisconnectedState();
            watcher.CloudConnectionChanged += Watcher_CloudConnectionChanged;
        }

        public string DetailTitle => "Network & Settings";
        public string DetailStatus { get { return detailStatus; } set { SetProperty(ref detailStatus, value); } }


        private void Watcher_CloudConnectionChanged(object sender, bool e)
        {
            if (e)
            {
                SetConnectedState();
            }
            else
            {
                SetDisconnectedState();
            }
        }

        private void SetConnectedState()
        {
            Status = "CONNECTED";
            DetailStatus = "EVERYTHING IS SET SUCCESSFULLY";
            StateIcon = StockIcon.CheckMark;
            Flag = CardState.Ok;
        }

        private void SetDisconnectedState()
        {
            Status = "DISCONNECTED";
            DetailStatus = "PLEASE LOGIN TO BAT CLOUD";
            StateIcon = StockIcon.ErrorMark;
            Flag = CardState.Error;
        }
    }
}
