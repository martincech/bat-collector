﻿using Veit.Wpf.Controls.Models.Card;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Controls.ViewModels;
using Veit.Wpf.Controls.ViewModels.Card;

namespace Veit.Bat.Collector.ViewModels.Cloud
{
    public class CloudViewModel : PageViewModel
    {
        private CardSmallViewModel overview;

        public CloudViewModel(PageModel model,
                              ICardSmall batCloudCard,
                              ICardSmall connectionCard,
                              ICardSmall sensorsCard,
                              ICardSmall settingsCard)
           : base(model)
        {
            BatCloudCard = batCloudCard;
            ConnectionCard = connectionCard;
            SensorsCard = sensorsCard;
            SettingsCard = settingsCard;
        }


        public CardSmallViewModel Overview { get { return overview; } set { SetProperty(ref overview, value); } }

        public ICardSmall BatCloudCard { get; private set; }
        public ICardSmall ConnectionCard { get; private set; }
        public ICardSmall SensorsCard { get; private set; }
        public ICardSmall SettingsCard { get; private set; }
    }
}
