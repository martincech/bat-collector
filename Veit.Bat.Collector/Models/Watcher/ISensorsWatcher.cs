﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.Models.Watcher
{
    public interface ISensorsWatcher
    {
        ObservableCollection<SourceDevice> Sources { get; }

        /// <summary>
        /// Connected sensors which are not activated yet.
        /// Typically modbus scale, it is connected after scan has finished,
        /// but activated is after user select it.
        /// </summary>
        ObservableCollection<SensorViewModel> ConnectedSensors { get; }

        /// <summary>
        /// Activated scales. For modbus scales it means that they periodically
        /// read statistics or raw weights (BAT2 v1.53)
        /// </summary>
        IEnumerable<SensorViewModel> ActivatedSensors { get; }

        /// <summary>
        /// Check if some of sensor has any problem (connection, data, ...).
        /// </summary>
        event EventHandler<bool> SensorsStateChanged;
    }
}
