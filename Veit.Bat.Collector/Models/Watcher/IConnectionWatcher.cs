﻿using System;

namespace Veit.Bat.Collector.Models.Watcher
{
    public interface IConnectionWatcher
    {
        event EventHandler<bool> CloudConnectionChanged;
    }
}
