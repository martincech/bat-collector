﻿using System;
using Veit.Bat.Collector.Views.Feedback;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.Models.Pages
{
    public class FeedbackModel : IMenuPage
    {
        public FeedbackModel()
        {
            Source = ModelHelper.GetUriFromType(typeof(FeedbackView));
            BarMenu = ModelHelper.GetUriFromType(typeof(FeedbackBarMenuView));
        }

        public bool IsOptional { get { return true; } }
        public StockIcon Icon { get { return StockIcon.Feedback; } }
        public string Title { get { return "Feedback"; } }
        public Uri Source { get; set; }
        public Uri BarMenu { get; set; }


        public Uri DetailFeedbackContent => ModelHelper.GetUriFromType(typeof(FeedbackDetailView));
        public Uri DetailPositiveFeedbackBarMenu => ModelHelper.GetUriFromType(typeof(FeedbackDetailPositiveBarMenuView));
        public Uri DetailNegativeFeedbackBarMenu => ModelHelper.GetUriFromType(typeof(FeedbackDetailNegativeBarMenuView));
    }
}
