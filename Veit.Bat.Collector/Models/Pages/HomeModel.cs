﻿using System;
using Veit.Bat.Collector.Views.Home;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.Models.Pages
{
    public class HomeModel : IMenuPage
    {
        public HomeModel()
        {
            Source = ModelHelper.GetUriFromType(typeof(HomeView));
            BarMenu = ModelHelper.GetUriFromType(typeof(HomeBarMenuView));
        }

        public bool IsOptional { get { return false; } }
        public StockIcon Icon { get { return StockIcon.Home; } }
        public string Title { get { return "Home"; } }
        public Uri Source { get; set; }
        public Uri BarMenu { get; set; }
    }
}
