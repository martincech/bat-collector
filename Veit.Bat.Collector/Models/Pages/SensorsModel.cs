﻿using System;
using Veit.Bat.Collector.Views.Sensor;
using Veit.Bat.Collector.Views.Sensor.Add;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.Models.Pages
{
    public class SensorsModel : IMenuPage
    {
        public SensorsModel()
        {
            Source = ModelHelper.GetUriFromType(typeof(SensorsView));
            BarMenu = ModelHelper.GetUriFromType(typeof(SensorsBarMenuView));
        }

        public bool IsOptional { get { return false; } }
        public StockIcon Icon { get { return StockIcon.DialMeter; } }
        public string Title { get { return "Sensors"; } }
        public Uri Source { get; set; }
        public Uri BarMenu { get; set; }

        public Uri DetailContent => ModelHelper.GetUriFromType(typeof(SensorDetailView));
        public Uri DetailBarMenu => ModelHelper.GetUriFromType(typeof(SensorDetailBarMenuView));

        public Uri AddSensor => ModelHelper.GetUriFromType(typeof(SensorAddWizardView));
        public Uri EditSensor => ModelHelper.GetUriFromType(typeof(SensorEditView));
    }
}
