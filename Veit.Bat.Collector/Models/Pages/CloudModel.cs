﻿using System;
using Veit.Bat.Collector.Views.Cloud;
using Veit.Bat.Collector.Views.Cloud.Settings;
using Veit.Wpf.Controls.Models.Page;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.Models.Pages
{
    public class CloudModel : IMenuPage
    {
        public CloudModel()
        {
            Source = ModelHelper.GetUriFromType(typeof(CloudView));
            BarMenu = ModelHelper.GetUriFromType(typeof(CloudBarMenuView));
        }

        public bool IsOptional { get { return false; } }
        public StockIcon Icon { get { return StockIcon.CloudUpload; } }
        public string Title { get { return "Cloud"; } }
        public Uri Source { get; set; }
        public Uri BarMenu { get; set; }


        public Uri LoginContent => ModelHelper.GetUriFromType(typeof(CloudNotLoggedView));

        public Uri ApplicationSettingsContent => ModelHelper.GetUriFromType(typeof(ApplicationSettingsView));
        public Uri ApplicationSettingsBarMenu => ModelHelper.GetUriFromType(typeof(ApplicationSettingsBarMenuView));
    }
}
