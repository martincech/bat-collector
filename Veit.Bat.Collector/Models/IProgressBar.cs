﻿namespace Veit.Bat.Collector.Models
{
    public interface IProgressBar
    {
        double Value { get; set; }
    }
}
