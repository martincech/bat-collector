﻿using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Collector.Models
{
    public class StatisticSample : TimeStampedSample
    {
        public int? MaleValue { get; set; }
        public int? FemaleValue { get; set; }
        public int? UndefinedValue { get; set; }
    }
}
