﻿using System.Collections.Generic;

namespace Veit.Bat.Collector.Models.Enums
{
    public enum WeightUnit
    {
        Gram,
        Kilogram,
        Pound
    }

    public static class WeightUnitExtensions
    {
        private static readonly Dictionary<WeightUnit, string> units = new Dictionary<WeightUnit, string>
        {
            { WeightUnit.Gram, "g" },
            { WeightUnit.Kilogram, "kg" },
            { WeightUnit.Pound, "lb" }
        };

        private static readonly Dictionary<WeightUnit, int> maxDecimals = new Dictionary<WeightUnit, int>
        {
            { WeightUnit.Gram, 0 },
            { WeightUnit.Kilogram, 3 },
            { WeightUnit.Pound, 3 }
        };

        public static string GetUnit(this WeightUnit unit) => units[unit];
        public static int GetMaxDecimals(this WeightUnit unit) => maxDecimals[unit];

        public static double Convert(this WeightUnit to, double value, WeightUnit from = WeightUnit.Gram)
        {
            if (from == to) return value;
            value = from.ConvertToBaseUnit(value);
            if (to == WeightUnit.Kilogram) return value / 1000;
            if (to == WeightUnit.Pound) return value / 453.59237;
            return value;
        }

        public static string ConvertToString(this WeightUnit to, double value, int decimals, WeightUnit from = WeightUnit.Gram)
        {
            return to.Convert(value, from).ToString("N" + decimals) + " " + units[to];
        }

        private static double ConvertToBaseUnit(this WeightUnit from, double value)
        {
            if (from == WeightUnit.Kilogram) return value * 1000;
            if (from == WeightUnit.Pound) return value * 453.59237;
            return value;
        }
    }
}
