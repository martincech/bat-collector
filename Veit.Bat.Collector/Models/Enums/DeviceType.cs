﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum DeviceType
    {
        Scale,
        Sensor
    }
}
