﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum SensorConnection
    {
        Inactive,
        Connected,
        All = int.MaxValue
    }
}
