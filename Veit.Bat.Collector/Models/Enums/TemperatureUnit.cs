﻿using System.Collections.Generic;

namespace Veit.Bat.Collector.Models.Enums
{
    public enum TemperatureUnit
    {
        Celsius,
        Fahrenheit
    }

    public static class TemperatureUnitExtensions
    {
        private static readonly Dictionary<TemperatureUnit, string> units = new Dictionary<TemperatureUnit, string>
        {
            { TemperatureUnit.Celsius, "°C" },
            { TemperatureUnit.Fahrenheit, "°F" }
        };

        public static string GetUnit(this TemperatureUnit unit) => units[unit];

        public static double Convert(this TemperatureUnit to, double value, TemperatureUnit from = TemperatureUnit.Celsius)
        {
            if (from == to) return value;
            value = from.ConvertToBaseUnit(value);
            if (to == TemperatureUnit.Fahrenheit) return value * 1.8 + 32;
            return value;
        }

        private static double ConvertToBaseUnit(this TemperatureUnit from, double value)
        {
            if (from == TemperatureUnit.Fahrenheit) return (value - 32) / 1.8;
            return value;
        }

        public static string ConvertToString(this TemperatureUnit to, double value, TemperatureUnit from = TemperatureUnit.Celsius)
        {
            return to.Convert(value, from).ToString("N0") + " " + units[to];
        }
    }
}
