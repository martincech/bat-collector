﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum FeedbackType
    {
        Neutral,
        Positive,
        Negative
    }
}
