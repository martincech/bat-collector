﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum Language
    {
        English,
        German,
        French,
        Czech
    }
}
