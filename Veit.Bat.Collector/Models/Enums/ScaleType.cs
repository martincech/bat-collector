﻿using System;

namespace Veit.Bat.Collector.Models.Enums
{
    public enum ScaleType
    {
        [Obsolete("GSM scale is no longer supported due to modem device is also obsoleted")]
        Bat2Gsm,
        Bat2Modbus
    }
}
