﻿using System;
using System.Collections.Generic;

namespace Veit.Bat.Collector.Models.Enums
{
    public enum DateFormat
    {
        DDMMYY,
        YYYYMMDD,
        DDMMYYYY
    }

    public static class DateFormatExtension
    {
        public static Dictionary<DateFormat, string> Formats { get; set; } = new Dictionary<DateFormat, string>
          {
             { DateFormat.DDMMYY, "dd/MM/yy" },
             { DateFormat.YYYYMMDD, "yyyy-MM-dd" },
             { DateFormat.DDMMYYYY, "dd. MM. yyyy" }
          };

        public static string GetFromat(this DateFormat dateFormat) => Formats[dateFormat];

        public static string CovertToString(this DateFormat dateFormat, DateTime dateTime)
        {
            return dateTime.ToString(Formats[dateFormat]);
        }
    }
}
