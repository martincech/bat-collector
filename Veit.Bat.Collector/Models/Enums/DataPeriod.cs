﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum DataPeriod
    {
        Hour,
        Day,
        Week
    }
}
