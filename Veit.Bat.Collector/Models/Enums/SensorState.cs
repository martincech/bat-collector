﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum SensorState
    {
        Inactive,
        Active,
        All = int.MaxValue
    }
}
