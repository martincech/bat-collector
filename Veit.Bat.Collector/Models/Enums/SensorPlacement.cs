﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum SensorPlacement
    {
        In,
        Out
    }
}
