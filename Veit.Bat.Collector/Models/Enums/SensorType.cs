﻿namespace Veit.Bat.Collector.Models.Enums
{
    public enum SensorType
    {
        Bat2,
        Thermometer,
        Hydrometer,
        Co2,
        All = int.MaxValue
    }
}
