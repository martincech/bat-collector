﻿using System;

namespace Veit.Bat.Collector.Models
{
    public class WebDocument
    {
        public Uri Uri { get; set; }
        public object Document { get; set; }
    }
}
