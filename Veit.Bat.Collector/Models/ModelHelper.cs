﻿using System;

namespace Veit.Bat.Collector.Models
{
    public static class ModelHelper
    {
        public static Uri GetUriFromType(Type type)
        {
            var assembly = type.Assembly.GetName().Name;
            var fullName = type.FullName.Replace(assembly, "");
            fullName = fullName.Replace('.', '/');
            return new Uri("pack://application:,,," + fullName + ".xaml");
        }
    }
}
