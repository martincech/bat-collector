﻿using Veit.Bat.Common;

namespace Veit.Bat.Collector.Models
{
    public class DeviceIdentification
    {
        public DeviceType DeviceType { get; set; }
        public string Source { get; set; }
        public string Uid { get; set; }
    }
}
