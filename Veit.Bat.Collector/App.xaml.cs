﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows;
using Ninject;
using NLog;
using Settings;
using Veit.Bat.Collector.IoC;
using Veit.Bat.Collector.Logger;
using Veit.Bat.Collector.Service_References.Watchers;
using Veit.Bat.Collector.Views;
using Veit.Wpf;
using Veit.Wpf.Helpers;

namespace Veit.Bat.Collector
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly NLog.Logger LOGGER = LogManager.GetCurrentClassLogger();
        private LoggerConfiguration configHelper;

        private const string APP_GUID = "fb6a0899-3587-4744-88e6-7ecc853a4e4f";
        private EventWaitHandle shutdownEvent;
        private Bindings bindings;
        private IKernel iocKernel;

        protected override void OnStartup(StartupEventArgs e)
        {
            DispatcherHelper.Initialize();
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            EnsureSingleInstance(e);
        }

        private void EnsureSingleInstance(StartupEventArgs e)
        {
            var startupEvent = new EventWaitHandle(false, EventResetMode.ManualReset, $"Local\\{APP_GUID}", out var created);
            if (created)
            {
                shutdownEvent = new EventWaitHandle(false, EventResetMode.ManualReset);
                ThreadPool.QueueUserWorkItem(o =>
                {
                    while (true)
                    {
                        var idx = WaitHandle.WaitAny(new WaitHandle[] { startupEvent, shutdownEvent });
                        if (idx == 0)
                        {
                            SetForeground();
                            startupEvent.Reset();
                        }
                        else
                        {
                            startupEvent.Dispose();
                            shutdownEvent.Dispose();
                            break;
                        }
                    }
                });

                //first start of application
                base.OnStartup(e);
            }
            else
            {   //other instance - send signal and exit
                startupEvent.Set();
                Environment.Exit(0);
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            shutdownEvent?.Set();
            bindings?.Dispose();
            iocKernel?.Dispose();

            base.OnExit(e);
        }

        private void SetForeground()
        {
            Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (MainWindow.WindowState == WindowState.Minimized)
                {
                    MainWindow.WindowState = WindowState.Normal;
                }
                MainWindow.Activate();
            }));
        }



        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Setup.InitResources();

            bindings = new Bindings();
            iocKernel = new StandardKernel(bindings);
            ContainerLocator.Init(iocKernel);
            InitLogger();
            LogProgramInfo();

            var culture = CultureInfo.GetCultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;

            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            MainWindow = ContainerLocator.Resolve<MainWindow>();
            MainWindow.Show();

            InitServiceWatchers();
        }

        private void InitLogger()
        {
            configHelper = ContainerLocator.Resolve<LoggerConfiguration>();
            LogManager.Configuration = configHelper.InitLoggerConfiguration();

            //load previous settings
            var diag = ContainerLocator.Resolve<ManagerSettings>().LoadDiagnostics();
            configHelper.SetDebugMode(diag.UseDebugMode);
        }

        private void LogProgramInfo()
        {
            //Application version
            var version = typeof(App).Assembly.GetName().Version;
            LOGGER.Debug("Application is started");
            LOGGER.Debug($"Program version: {version.Major}.{version.Minor}.{version.Build}.{version.Revision}");
            LOGGER.Debug($"Terminal ID: {configHelper.SystemInfo.TerminalId}");

            //OS version
            LOGGER.Debug($"OS version: {configHelper.SystemInfo.OsVersion}");
            LOGGER.Debug($"System: {configHelper.SystemInfo.SystemType}");
            LOGGER.Debug($"System Language: {configHelper.SystemInfo.SystemLanguage}");
        }

        private static void InitServiceWatchers()
        {
            ContainerLocator.Resolve<ModbusSystemServiceWatcher>();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var e = (Exception)args.ExceptionObject;
            LOGGER.Fatal(e, "Unhandled exception");
            MessageBox.Show(e.Message + "\n\nTrace:\n" + e.StackTrace);
        }
    }
}
