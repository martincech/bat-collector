﻿using System;
using System.Windows.Input;

namespace Veit.Bat.Collector.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for TryLoginDialog.xaml
    /// </summary>
    public partial class TryLoginDialog
    {
        public TryLoginDialog()
        {
            InitializeComponent();
            KeyDown += TryLoginDialog_KeyDown;
        }

        public event EventHandler CloseRequest;

        private void TryLoginDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.F2) && Keyboard.IsKeyDown(Key.F4))
            {
                CloseRequest?.Invoke(this, new EventArgs());
            }
        }
    }
}
