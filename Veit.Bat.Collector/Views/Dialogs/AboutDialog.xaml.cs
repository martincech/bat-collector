﻿using System;
using System.Diagnostics;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Veit.Bat.Collector.IoC;

namespace Veit.Bat.Collector.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AboutDialog.xaml
    /// </summary>
    public partial class AboutDialog
    {
        public AboutDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Close();
        }


        private void Close()
        {
            if (!(ContainerLocator.Resolve<MainWindow>() is MetroWindow win))
            {
                throw new InvalidOperationException($"Main window is not a type: {typeof(MetroWindow)}");
            }
            win.HideMetroDialogAsync(this);
        }

#pragma warning disable CC0091 // Cannot be static for correct function of hyper link
        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
#pragma warning restore CC0091
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
