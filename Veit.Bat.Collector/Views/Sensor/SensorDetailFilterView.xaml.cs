﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Sensor
{
    /// <summary>
    /// Interaction logic for SensorDetailFilterView.xaml
    /// </summary>
    public partial class SensorDetailFilterView : UserControl
    {
        public SensorDetailFilterView()
        {
            InitializeComponent();
            resetFilterButton.Focus();
        }
    }
}
