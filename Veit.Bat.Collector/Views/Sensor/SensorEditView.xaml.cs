﻿using System.Windows;
using System.Windows.Controls;
using Veit.Bat.Collector.IoC;
using Veit.Wpf.Controls.Models;
using Veit.Wpf.Controls.ViewModels;

namespace Veit.Bat.Collector.Views.Sensor
{
    /// <summary>
    /// Interaction logic for SensorEditView.xaml
    /// </summary>
    public partial class SensorEditView : UserControl
    {
        public SensorEditView()
        {
            InitializeComponent();
            Loaded += SensorEditView_Loaded;
        }

        private void SensorEditView_Loaded(object sender, RoutedEventArgs e)
        {
            if (!(DataContext is ErrorValidationViewModel vm))
            {
                return;
            }
            vm.PropertyChanged += Vm_PropertyChanged;
        }

        private void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!(DataContext is ErrorValidationViewModel vm))
            {
                return;
            }

            if (e.PropertyName.Equals(nameof(vm.IsValidationError)) && vm.IsValidationError)
            {
                var err = ContainerLocator.Resolve<IErrorView>();
                err.DisplayError(vm.ValidationErrorText);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            content.Visibility = Visibility.Hidden;
            confirm.Visibility = Visibility.Visible;
        }
    }
}
