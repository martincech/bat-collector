﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Sensor
{
    /// <summary>
    /// Interaction logic for SensorDetailBarMenuView.xaml
    /// </summary>
    public partial class SensorDetailBarMenuView : UserControl
    {
        public SensorDetailBarMenuView()
        {
            InitializeComponent();
        }
    }
}
