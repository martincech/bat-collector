﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Sensor
{
    /// <summary>
    /// Interaction logic for SensorFilterView.xaml
    /// </summary>
    public partial class SensorFilterView : UserControl
    {
        public SensorFilterView()
        {
            InitializeComponent();
        }
    }
}
