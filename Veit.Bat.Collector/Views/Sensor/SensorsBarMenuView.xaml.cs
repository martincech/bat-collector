﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Sensor
{
    /// <summary>
    /// Interaction logic for SensorsBarMenuView.xaml
    /// </summary>
    public partial class SensorsBarMenuView : UserControl
    {
        public SensorsBarMenuView()
        {
            InitializeComponent();
        }
    }
}
