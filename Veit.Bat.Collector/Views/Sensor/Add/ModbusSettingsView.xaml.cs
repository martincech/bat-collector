﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Sensor.Add
{
    /// <summary>
    /// Interaction logic for ModbusSettingsView.xaml
    /// </summary>
    public partial class ModbusSettingsView : UserControl
    {
        public ModbusSettingsView()
        {
            InitializeComponent();
        }
    }
}
