﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Sensor.Add
{
    /// <summary>
    /// Interaction logic for SensorAddWizardView.xaml
    /// </summary>
    public partial class SensorAddWizardView : UserControl
    {
        public SensorAddWizardView()
        {
            InitializeComponent();
        }
    }
}
