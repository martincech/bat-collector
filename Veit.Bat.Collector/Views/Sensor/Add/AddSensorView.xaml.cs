﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Sensor.Add
{
    /// <summary>
    /// Interaction logic for AddSensorView.xaml
    /// </summary>
    public partial class AddSensorView : UserControl
    {
        public AddSensorView()
        {
            InitializeComponent();
        }
    }
}
