﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Veit.Bat.Collector.Views.WebBrowser
{
    public static class IeCookieHelper
    {
        private static bool InternetGetCookieExWrapper(
            string url,
            string cookieName,
            StringBuilder cookieData,
            ref int size,
            int dwFlags,
            IntPtr lpReserved)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException(nameof(url));
            }
            return InternetGetCookieEx(url, cookieName, cookieData, ref size, dwFlags, lpReserved);
        }

        [DllImport("wininet.dll", SetLastError = true)]
        private  static extern bool InternetGetCookieEx(
            string url,
            string cookieName,
            StringBuilder cookieData,
            ref int size,
            int dwFlags,
            IntPtr lpReserved);

        private const int InternetCookieHttponly = 0x2000;

        /// <summary>
        /// Gets the URL cookie data(with session)
        /// </summary>
        /// <param name="url">The URL</param>
        /// <param name="cookieName">Cookie parameter name</param>
        /// <returns></returns>
        public static string GetCookieData(string url, string cookieName = null)
        {
            // Determine the size of the cookie
            var datasize = 8192 * 16;
            var cookieData = new StringBuilder(datasize);
            if (!InternetGetCookieExWrapper(url, cookieName, cookieData, ref datasize, InternetCookieHttponly, IntPtr.Zero))
            {
                if (datasize < 0)
                    return null;
                // Allocate string builder large enough to hold the cookie
                cookieData = new StringBuilder(datasize);
                if (!InternetGetCookieExWrapper(
                    url,
                    cookieName, cookieData,
                    ref datasize,
                    InternetCookieHttponly,
                    IntPtr.Zero))
                    return null;
            }
            return cookieData.ToString();
        }
    }
}
