﻿using NLog;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using Veit.Bat.Collector.Models;

namespace Veit.Bat.Collector.Views.WebBrowser
{
    public class WebBrowserOverlay : ContentControl
    {
        #region DefaultAddress

        public static readonly DependencyProperty DefaultAddressProperty =
          DependencyProperty.RegisterAttached(nameof(DefaultAddress), typeof(string), typeof(WebBrowserOverlay), new PropertyMetadata(string.Empty));

        public string DefaultAddress
        {
            get { return (string)GetValue(DefaultAddressProperty); }
            set { SetValue(DefaultAddressProperty, value); }
        }

        #endregion

        #region Navigate

        public static readonly DependencyProperty NavigateProperty =
          DependencyProperty.RegisterAttached(nameof(Navigate), typeof(string), typeof(WebBrowserOverlay), new PropertyMetadata(string.Empty, OnNavigatePropertyChanged));


        public string Navigate
        {
            get { return (string)GetValue(NavigateProperty); }
            set { SetValue(NavigateProperty, value); }
        }

        private static void OnNavigatePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender == null || e.NewValue == null) return;
            var address = e.NewValue.ToString();
            if (sender is WebBrowserOverlay wb && wb.webBrowserHandler != null && UriIsOk(address))
            {
                wb.webBrowserHandler.Navigate(address);
            }
        }

        private static bool UriIsOk(string uri)
        {
            try
            {
                return Uri.TryCreate(uri, UriKind.RelativeOrAbsolute, out _);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region PageLoaded command

        /// <summary>
        /// Occurs when page is completely downloaded.
        /// </summary>
        public static readonly DependencyProperty PageLoadedCommandProperty =
           DependencyProperty.Register(nameof(PageLoadedCommand), typeof(ICommand), typeof(WebBrowserOverlay), new PropertyMetadata(null));

        public ICommand PageLoadedCommand
        {
            get { return (ICommand)GetValue(PageLoadedCommandProperty); }
            set { SetValue(PageLoadedCommandProperty, value); }
        }

        #endregion

        #region PageNavigating command

        /// <summary>
        /// Occurs just before load page.
        /// </summary>
        public static readonly DependencyProperty PageNavigatingCommandProperty =
           DependencyProperty.Register(nameof(PageNavigatingCommand), typeof(ICommand), typeof(WebBrowserOverlay), new PropertyMetadata(null));

        public ICommand PageNavigatingCommand
        {
            get { return (ICommand)GetValue(PageNavigatingCommandProperty); }
            set { SetValue(PageNavigatingCommandProperty, value); }
        }

        #endregion

        private WebBrowserOverlayWindow webBrowserOverlay;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();
        private System.Windows.Forms.WebBrowser webBrowserHandler;

        public WebBrowserOverlay()
        {
            Loaded += WebBrowserOverlay_Loaded;
        }

        #region Private helpers

        private void WebBrowserOverlay_Loaded(object sender, RoutedEventArgs e)
        {
            if (webBrowserOverlay == null)
            {
                InitBrowser(this);
            }

            var address = DefaultAddress;
            logger.Info($"Cloud login address: {address}");

            //always redirect to main page when control is displayed
            webBrowserHandler.Navigate(address);
        }

        private void InitBrowser(FrameworkElement target)
        {
            webBrowserOverlay = new WebBrowserOverlayWindow(target);
            webBrowserHandler = webBrowserOverlay.WebBrowser;
            webBrowserOverlay.WebBrowser.DocumentCompleted += WebBrowser_LoadCompleted;
            webBrowserOverlay.WebBrowser.Navigating += WebBrowser_Navigating;
        }

        private void WebBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            var doc = GetDocument(e.Url);
            if (PageNavigatingCommand.CanExecute(doc))
            {
                PageNavigatingCommand.Execute(doc);
            }
        }

        private void WebBrowser_LoadCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var doc = GetDocument(e.Url);

            if (PageLoadedCommand.CanExecute(doc))
            {
                PageLoadedCommand.Execute(doc);
            }
            if (!e.Url.Host.Contains("amazoncognito") && !string.IsNullOrEmpty(e.Url.Host)) webBrowserOverlay.WebBrowser.DocumentText =
                  "<!DOCTYPE html>" +
                  "<html> " +
                  "<head> " +
                  "</head> " +
                  "<body style=\"background-color:#909090\"> " +
                  "</body> " +
                  "</html>";
        }



        private WebDocument GetDocument(Uri uri)
        {
            return new WebDocument
            {
                Uri = uri,
                Document = webBrowserOverlay.WebBrowser.Document
            };
        }

        #endregion
    }
}
