﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using Veit.Bat.Collector.IoC;

namespace Veit.Bat.Collector.Views.WebBrowser
{
    /// <summary>
    /// Displays a WebBrowser control over a given placement target element in a WPF Window.
    /// The owner window can be transparent, but not this one, due mixing DirectX and GDI drawing.
    /// WebBrowserOverlayWF uses WinForms to avoid this limitation.
    /// </summary>
    public partial class WebBrowserOverlayWindow
    {
        private readonly FrameworkElement placementTarget;
        private FrameworkElement PlacementTargetParent { get { return placementTarget.Parent as FrameworkElement; } }
        private Win32.Point lastScreenLocation;
        private Win32.Point lastScreenSize;


        public System.Windows.Forms.WebBrowser WebBrowser { get; private set; }


        public WebBrowserOverlayWindow(FrameworkElement placementTarget)
        {
            InitializeComponent();

            WebBrowser = (wfhSample.Child as System.Windows.Forms.WebBrowser);

            WebBrowser.IsWebBrowserContextMenuEnabled = false;
            WebBrowser.AllowWebBrowserDrop = false;

            this.placementTarget = placementTarget;

            if (DesignerProperties.GetIsInDesignMode(this))
            {  //ContainerLocator don´t work in design mode
                return;
            }

            Owner = ContainerLocator.Resolve<MainWindow>();
            Debug.Assert(Owner != null);

            Owner.LocationChanged += OnSizeLocationChanged; //react on owner/window change location (drag window)
            Owner.SizeChanged += OnSizeLocationChanged;     //react on change window size (minimize/maximize)
            this.placementTarget.SizeChanged += OnSizeLocationChanged;  //react on placement size changed

            PlacementTargetParent.IsVisibleChanged += PlacementTargetParent_IsVisibleChanged;
            if (PlacementTargetParent.IsVisible)
            {
                Show();
                OnSizeLocationChanged(null, null);
            }
        }

        private void PlacementTargetParent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (PlacementTargetParent.IsVisible)
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {  // Delayed call to avoid crash due to Window bug.
                Dispatcher.BeginInvoke((Action)delegate
                {
                    Owner.Close();
                });
            }
        }


        private void OnSizeLocationChanged(object sender, EventArgs e)
        {
            var offset = placementTarget.TranslatePoint(new Point(), Owner);
            var size = new Point(placementTarget.ActualWidth, placementTarget.ActualHeight);
            var hwndSource = (HwndSource)PresentationSource.FromVisual(Owner);
            CompositionTarget ct = hwndSource.CompositionTarget;
            offset = ct.TransformToDevice.Transform(offset);
            size = ct.TransformToDevice.Transform(size);

            var screenLocation = new Win32.Point(offset);
            Win32.ClientToScreen(hwndSource.Handle, ref screenLocation);
            var screenSize = new Win32.Point(size);

            if (!IsScreenChanged(screenLocation, screenSize))
            {   //do not redraw object when size and location is not changed
                return;
            }

            var helper = new WindowInteropHelper(this);
            var source = HwndSource.FromHwnd(helper.EnsureHandle());
            Win32.MoveWindow(source.Handle, screenLocation.X, screenLocation.Y, screenSize.X, screenSize.Y, true);
        }

        private bool IsScreenChanged(Win32.Point location, Win32.Point size)
        {
            var result = false;
            if (IsPointDifferent(location, lastScreenLocation))
            {
                result = true;
                lastScreenLocation = new Win32.Point(location.X, location.Y);
            }
            if (IsPointDifferent(size, lastScreenSize))
            {
                result = true;
                lastScreenSize = new Win32.Point(size.X, size.Y);
            }
            return result;
        }

        private static bool IsPointDifferent(Win32.Point p1, Win32.Point p2)
        {
            return p1.X != p2.X ||
                   p1.Y != p2.Y;
        }
    }
}
