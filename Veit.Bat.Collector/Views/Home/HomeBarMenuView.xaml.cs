﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Home
{
    /// <summary>
    /// Interaction logic for HomeBarMenuView.xaml
    /// </summary>
    public partial class HomeBarMenuView : UserControl
    {
        public HomeBarMenuView()
        {
            InitializeComponent();
        }
    }
}
