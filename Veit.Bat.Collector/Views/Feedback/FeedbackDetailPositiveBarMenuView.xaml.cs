﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Feedback
{
    /// <summary>
    /// Interaction logic for FeedbackDetailPositiveBarMenuView.xaml
    /// </summary>
    public partial class FeedbackDetailPositiveBarMenuView : UserControl
    {
        public FeedbackDetailPositiveBarMenuView()
        {
            InitializeComponent();
        }
    }
}
