﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Feedback
{
    /// <summary>
    /// Interaction logic for FeedbackBarMenuView.xaml
    /// </summary>
    public partial class FeedbackBarMenuView : UserControl
    {
        public FeedbackBarMenuView()
        {
            InitializeComponent();
        }
    }
}
