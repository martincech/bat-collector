﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Feedback
{
    /// <summary>
    /// Interaction logic for FeedbackDetailNegativeBarMenuView.xaml
    /// </summary>
    public partial class FeedbackDetailNegativeBarMenuView : UserControl
    {
        public FeedbackDetailNegativeBarMenuView()
        {
            InitializeComponent();
        }
    }
}
