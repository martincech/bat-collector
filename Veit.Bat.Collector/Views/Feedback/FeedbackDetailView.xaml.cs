﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Feedback
{
    /// <summary>
    /// Interaction logic for FeedbackDetailView.xaml
    /// </summary>
    public partial class FeedbackDetailView : UserControl
    {
        public FeedbackDetailView()
        {
            InitializeComponent();
        }
    }
}
