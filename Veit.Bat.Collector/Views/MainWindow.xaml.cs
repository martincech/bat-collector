﻿using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Veit.Bat.Collector.ViewModels;
using Veit.Wpf.Controls.Models;

namespace Veit.Bat.Collector.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : IErrorView
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
            KeyDown += MainWindow_KeyDown;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!(DataContext is MainWindowViewModel vm))
            {
                return;
            }

            //due to used view model locator, dialog coordinator is registered here
            DialogParticipation.SetRegister(this, vm.Dialog);
            vm.RegisterDialogCoordinator();
        }

        /// <summary>
        /// Display error fly out menu.
        /// </summary>
        /// <param name="message"></param>
        public void DisplayError(string message)
        {
            ErrorFlyout.Text = message;
            if (Flyouts.Items.Count < 1 || !(Flyouts.Items[0] is Flyout flyoutView))
            {
                return;
            }
            flyoutView.IsOpen = !flyoutView.IsOpen;
        }



#region About dialog

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.F1))
            {
                ShowAboutDialog();
            }
        }

        private void ShowAboutDialog()
        {
            if (!(Resources["AboutDialog"] is BaseMetroDialog dialog) || dialog.IsVisible)
            {
                return;
            }

            var win = this as MetroWindow;
            win.ShowMetroDialogAsync(dialog);
        }

#endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ShowAboutDialog();
        }
    }
}
