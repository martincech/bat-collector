﻿using System.Windows;
using System.Windows.Controls;
using Veit.Wpf.Resources.StockIcons;

namespace Veit.Bat.Collector.Views.Helper
{
    /// <summary>
    /// Interaction logic for IconTitleView.xaml
    /// </summary>
    public partial class IconTitleView : UserControl
    {
        public IconTitleView()
        {
            InitializeComponent();
        }


        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(nameof(Title), typeof(string), typeof(IconTitleView), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(nameof(Icon), typeof(StockIcon), typeof(IconTitleView), new PropertyMetadata(StockIcon.CloudUpload));


        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public StockIcon Icon
        {
            get { return (StockIcon)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }
    }
}
