﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Veit.Bat.Collector.Views.Helper
{
    /// <summary>
    /// Interaction logic for ConfirmView.xaml
    /// </summary>
    public partial class ConfirmView : UserControl
    {
        #region Default settings

        private const string DEFAULT_TITLE = "Are you sure?";
        private const string DEFAULT_LABEL_CONFIRM = "Delete";
        private const string DEFAULT_LABEL_CANCEL = "Don't do it";

        #endregion

        #region Dependency properties

        public static readonly DependencyProperty ConfirmCommandProperty = DependencyProperty.Register(nameof(ConfirmCommand), typeof(ICommand), typeof(ConfirmView), new PropertyMetadata(null));
        public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register(nameof(CancelCommand), typeof(ICommand), typeof(ConfirmView), new PropertyMetadata(null));
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(nameof(Title), typeof(string), typeof(ConfirmView), new PropertyMetadata(DEFAULT_TITLE));
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof(Text), typeof(string), typeof(ConfirmView), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty LabelConfirmProperty = DependencyProperty.Register(nameof(LabelConfirm), typeof(string), typeof(ConfirmView), new PropertyMetadata(DEFAULT_LABEL_CONFIRM));
        public static readonly DependencyProperty LabelCancelProperty = DependencyProperty.Register(nameof(LabelCancel), typeof(string), typeof(ConfirmView), new PropertyMetadata(DEFAULT_LABEL_CANCEL));

        public ICommand ConfirmCommand
        {
            get { return (ICommand)GetValue(ConfirmCommandProperty); }
            set { SetValue(ConfirmCommandProperty, value); }
        }

        public ICommand CancelCommand
        {
            get { return (ICommand)GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public string LabelConfirm
        {
            get { return (string)GetValue(LabelConfirmProperty); }
            set { SetValue(LabelConfirmProperty, value); }
        }
        public string LabelCancel
        {
            get { return (string)GetValue(LabelCancelProperty); }
            set { SetValue(LabelCancelProperty, value); }
        }

        #endregion


        public ConfirmView()
        {
            InitializeComponent();
        }
    }
}
