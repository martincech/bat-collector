﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Cloud
{
    /// <summary>
    /// Interaction logic for CloudBarMenuView.xaml
    /// </summary>
    public partial class CloudBarMenuView : UserControl
    {
        public CloudBarMenuView()
        {
            InitializeComponent();
        }
    }
}
