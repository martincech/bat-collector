﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Cloud
{
    /// <summary>
    /// Interaction logic for CloudLoggedView.xaml
    /// </summary>
    public partial class CloudLoggedView : UserControl
    {
        public CloudLoggedView()
        {
            InitializeComponent();
        }
    }
}
