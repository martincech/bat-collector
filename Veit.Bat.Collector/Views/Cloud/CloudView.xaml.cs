﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Cloud
{
    /// <summary>
    /// Interaction logic for CloudView.xaml
    /// </summary>
    public partial class CloudView : UserControl
    {
        public CloudView()
        {
            InitializeComponent();
        }
    }
}
