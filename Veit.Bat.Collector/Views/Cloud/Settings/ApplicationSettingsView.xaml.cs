﻿using System.Windows.Controls;

namespace Veit.Bat.Collector.Views.Cloud.Settings
{
    /// <summary>
    /// Interaction logic for ApplicationSettingsView.xaml
    /// </summary>
    public partial class ApplicationSettingsView : UserControl
    {
        public ApplicationSettingsView()
        {
            InitializeComponent();
        }
    }
}
