﻿namespace Veit.Bat.Collector.Helpers
{
    public static class DesignerHelper
    {
        public static bool IsInDesignMode()
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject()))
            {
                return true;
            }
            return false;
        }
    }
}
