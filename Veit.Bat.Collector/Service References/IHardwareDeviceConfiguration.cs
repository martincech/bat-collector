﻿using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;
using Veit.Bat.Collector.Models;

namespace Veit.Bat.Collector.Service_References
{
    public interface IHardwareDeviceConfiguration
    {
        void Configuration(DeviceIdentification device, DeviceConfiguration configuration);

        void Name(DeviceIdentification device, string name);
        void Version(DeviceIdentification device, DeviceVersion version);
    }
}
