﻿using System;
using System.Collections.Generic;
using Communication.Connectivity;
using Communication.Contract.Wcf.DataSources;
using NLog;
using Services.PublishSubscribe.Contracts;
using Veit.Bat.Collector.IoC;
using Veit.Bat.Common;
using Veit.Bat.Common.DeviceStatusInfo;
using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Collector.Service_References
{
    public abstract class DeviceCommands : IServiceCommands
    {
        private IHardwareDevices hardwareDevices;
        private static readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();

        protected IHardwareDevices HardwareDevice
        {
            get { return hardwareDevices ?? (hardwareDevices = ContainerLocator.Resolve<IHardwareDevices>()); }
        }

        public void SampleReaded(TimeSample sample)
        {
            HardwareDevice.SampleRead(sample);
        }

        public virtual void Connected(SourceConnectivityInfo device)
        {
            ExecuteActionForSourceInfo(
               device.Device.Type, device.Device.SensorUid, device.Source,
               (sourceType, source) => HardwareDevice.ConnectDevice(sourceType, source),
               (deviceType, source, deviceId) => HardwareDevice.ConnectDevice(deviceType, source, deviceId));

        }

        protected static void ExecuteActionForSourceInfo(
           string type, string uid, string source,
           Action<SourceType, string> sourceTypeAction,
           Action<DeviceType, string, string> deviceTypeAction)
        {
            if (string.IsNullOrEmpty(uid))
            {
                //connectivity - elo etc
                if (Enum.TryParse(type, true, out SourceType sourceType))
                {
                    sourceTypeAction?.Invoke(sourceType, source);
                }
            }
            else
            {
                //concrete devices
                if (Enum.TryParse(type, out DeviceType deviceType))
                {
                    deviceTypeAction?.Invoke(deviceType, source, uid);
                }
            }
        }

        /// <inheritdoc />
        public virtual void Disconnected(SourceConnectivityInfo device)
        {
            ExecuteActionForSourceInfo(
               device.Device.Type, device.Device.SensorUid, device.Source,
               (sourceType, source) => HardwareDevice.DisconnectDevice(sourceType, source),
               (deviceType, source, deviceId) => HardwareDevice.DisconnectDevice(deviceType, source, deviceId));

        }

        /// <inheritdoc />
        public abstract bool Connect(string address);

        public abstract bool Disconnect(string address);


        public static T Connect<T>(Func<T> serviceFactory) where T : class, IDataSource
        {
            try
            {
                var service = serviceFactory?.Invoke();
                if (service == null)
                {
                    throw new ArgumentNullException(nameof(serviceFactory));
                }
                service.Subscribe();
                service.GetConnected();
                return service;
            }
            catch (Exception e)
            {
                logger.Info(e, "Connect service factory exception");
                return null;
            }
        }

        public bool Disconnect(ISubscriptionContract service, SourceType sourceType)
        {
            return Disconnect(service, new[] { sourceType });
        }

        public bool Disconnect(ISubscriptionContract service, IEnumerable<SourceType> sourceTypes)
        {
            try
            {
                service.Unsubscribe();
                foreach (var sourceType in sourceTypes)
                {
                    HardwareDevice.DisconnectDevices(sourceType);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Warn(e, "Hardware device disconnect error");
                return false;
            }
        }
    }
}
