﻿namespace Veit.Bat.Collector.Service_References
{
    public interface IServiceCommands
    {
        bool Connect(string address);
        bool Disconnect(string address);
    }
}
