﻿using System;
using Communication.Contract.Wcf.DataContracts;

namespace Veit.Bat.Collector.Service_References.WebApi
{
    public interface IWebApiCommands
    {
        Func<WebApiConnectionInfo, bool> ChangeServerConnection { get; }
        Func<bool> GetExistingConnectionsRequest { get; }
        Func<WebApiConnectionInfo> GetConnectionInfo { get; }
    }
}
