﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.DataContracts.Device;
using Communication.Contract.Wcf.WebApi;
using Settings;
using Veit.Bat.Collector.Core.Service_References.WebApiConnection;
using Veit.Bat.Collector.IoC;
using Veit.Bat.Collector.Notification;
using Veit.Bat.Collector.ViewModels.Cloud;

namespace Veit.Bat.Collector.Service_References.WebApi
{
    public class WebApiConnectionCommands : IWebApiConnectionCallbacks, IWebApiCommands, IServiceCommands, IApiDevice
    {
        private readonly IManagerSettings settingsManager;
        private readonly ConcurrentDictionary<string, IWebApiConnection> webApiConnections;
        public WebApiConnectionCommands(IManagerSettings settingsManager)
        {
            this.settingsManager = settingsManager;
            webApiConnections = new ConcurrentDictionary<string, IWebApiConnection>();
        }

        private void CreateClientAndSubscribe(string address)
        {
            webApiConnections.GetOrAdd(address, o =>
            {
                var client = new WebApiConnectionWcfClient(this, address, ToastNotifier.ShowError);
                client.Subscribe();
                return client;
            });
        }

        public void ExistingConnections(IEnumerable<WebApiConnectionInfo> connections)
        {
            if (connections == null) return;
            foreach (var conn in connections)
            {
                ConnectionStatusChanged(conn);
            }
        }

        public void ConnectionStatusChanged(WebApiConnectionInfo webApiConnectionInfo)
        {
            if (webApiConnectionInfo == null) return;
            settingsManager.SaveConnection(webApiConnectionInfo);
            SetViewModel(webApiConnectionInfo);

            if (webApiConnectionInfo.Logged != LoggedStatus.Logged)
            {

                var message = "";
                switch (webApiConnectionInfo.Logged)
                {
                    case LoggedStatus.UnAuthorized:
                        message = Properties.Resources.StatusUnauthorized;
                        break;
                    case LoggedStatus.Unknown:
                        break;
                    case LoggedStatus.Forbidden:
                        message = Properties.Resources.StatusForbidden;
                        break;
                    case LoggedStatus.BadRequest:
                        message = Properties.Resources.StatusBadRequest;
                        break;
                    default:
                        message = Properties.Resources.StatusOther;
                        break;
                }
                if (!string.IsNullOrEmpty(message)) ToastNotifier.ShowOnce(nameof(ConnectionStatusChanged), message, ToastNotifier.ShowError);
            }
        }

        #region Implementation of IWebApiCommands

        public Func<WebApiConnectionInfo, bool> ChangeServerConnection
        {
            get
            {
                return connectionInfo =>
                {
                    try
                    {
                        if (connectionInfo == null) return false;
                        settingsManager.SaveConnection(connectionInfo);

                        if (webApiConnections == null || !webApiConnections.Any())
                        {
                            connectionInfo.Logged = LoggedStatus.Unknown;
                            SetViewModel(connectionInfo);
                            return false;
                        }
                        foreach (var conn in webApiConnections.Select(s => s.Value))
                        {
                            conn.ChangeServerConnection(connectionInfo);
                        }

                        return true;
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                };
            }
        }

        public Func<bool> GetExistingConnectionsRequest
        {
            get
            {
                return () =>
                {
                    foreach (var conn in webApiConnections.Select(s => s.Value))
                    {
                        conn.GetExistingConnectionsRequest();
                    }
                    return true;
                };
            }
        }

        public Func<WebApiConnectionInfo> GetConnectionInfo
        {
            get
            {
                return () =>
                {
                    return settingsManager.LoadConnections().FirstOrDefault();
                };
            }
        }

        #endregion

        private static void SetViewModel(WebApiConnectionInfo webApiConnectionInfo)
        {
            if (webApiConnectionInfo == null) return;

            var viewModel = ContainerLocator.Resolve<CloudConnectionViewModel>();
            if (viewModel != null)
            {
                viewModel.SetConnection(webApiConnectionInfo);
            }
        }

        #region Implementation of IServicePart

        public bool Connect(string address)
        {
            try
            {
                CreateClientAndSubscribe(address);
                GetExistingConnectionsRequest?.Invoke();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Disconnect(string address)
        {
            if (!webApiConnections.ContainsKey(address)) return true;
            try
            {
                webApiConnections[address].Unsubscribe();
                return true;
            }
            catch (Exception)
            {
                return true;

            }
            finally
            {
                webApiConnections.TryRemove(address, out _);
            }
        }


        public void SyncDevice(DeviceNotification device)
        {
            foreach (var conn in webApiConnections)
            {
                conn.Value.SyncDevice(device);
            }
        }

        #endregion
    }
}
