﻿using Communication.Connectivity;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Base;

namespace Veit.Bat.Collector.Service_References
{
    public interface IHardwareDevices : IHardwareDeviceConfiguration
    {
        void ConnectDevice(SourceType sourceType, string deviceSource);
        void ConnectDevice(DeviceType deviceType, string source, string sensorUid);

        void DisconnectDevice(SourceType sourceType, string source);
        void DisconnectDevice(DeviceType deviceType, string source, string deviceId);
        void DisconnectDevices(SourceType sourceType);

        void ActivateDevice(DeviceType deviceType, string source, string deviceId);
        void DeactivateDevice(DeviceType deviceType, string source, string deviceId);
        void SampleRead(TimeSample sample);
    }
}
