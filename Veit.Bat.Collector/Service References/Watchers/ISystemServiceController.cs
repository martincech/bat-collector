﻿using System;

namespace Veit.Bat.Collector.Service_References.Watchers
{
    public interface ISystemServiceController : IDisposable
    {
        bool Init(string serviceName);
        bool IsRunning();
    }
}
