﻿using System;
using System.Collections.Generic;
using System.Threading;
using Utilities.Timer;
using Veit.Bat.Collector.ViewModels.Services;

namespace Veit.Bat.Collector.Service_References.Watchers
{
    public abstract class SystemServiceWatcher : IDisposable
    {
        private readonly IServiceBaseViewModel serviceViewModel;
        private readonly Dictionary<string, IServiceCommands> services;

        #region Private fields

        private readonly TimeSpan serviceCheckPeriod = TimeSpan.FromSeconds(3);
        private readonly ISystemServiceController serviceController;
        private readonly ITimer healthCheckTimer;

        #endregion

        #region Public interfaces

        #region Constructor

        protected SystemServiceWatcher(
           IServiceBaseViewModel serviceViewModel,
           ISystemServiceController serviceController,
           Func<ITimer> timerFactory = null)
        {
            services = new Dictionary<string, IServiceCommands>();

            this.serviceViewModel = serviceViewModel;
            this.serviceController = serviceController;
            serviceController.Init(serviceViewModel.ServiceName);

            healthCheckTimer = (timerFactory ?? TimerAdapter.Factory)();
            healthCheckTimer.AutoReset = true;
            healthCheckTimer.Interval = serviceCheckPeriod.TotalMilliseconds;
            healthCheckTimer.Elapsed += (sender, args) => ServiceHealthCheck();
            healthCheckTimer.Start();
        }

        protected void AddService(string address, IServiceCommands service)
        {
            if (string.IsNullOrEmpty(address) || service == null || services.ContainsKey(address)) return;
            services.Add(address, service);
        }

        private void ServiceHealthCheck()
        {
            if (!Monitor.TryEnter(healthCheckTimer))
            {
                return;
            }
            try
            {
#if DEBUG
                //for test with console version of services -> disable check if service
                //is running (console app must runs first due to pipe connection)

#pragma warning disable CC0037 // Remove commented code.
                //if (serviceViewModel is ModbusServiceViewModel /*||
                //   serviceViewModel is ExternalSensorsServiceViewModel ||
                //   serviceViewModel is GsmServiceViewModel*/)
                //{
                //    var isFirstInit = IsRunning == false;
                //    IsRunning = true;
                //    if (isFirstInit)
                //    {
                //        StateChanged();
                //    }
                //}
#pragma warning restore CC0037 // Remove commented code.
#endif

                var isRunning = serviceController.IsRunning();
                if (IsRunning == isRunning) return;
                IsRunning = isRunning;
                StateChanged();
            }
            finally
            {
                Monitor.Exit(healthCheckTimer);
            }
        }

        private void StateChanged()
        {
            serviceViewModel.IsRunning = IsRunning;
            if (IsRunning)
            {
                foreach (var s in services)
                {
                    s.Value.Connect(s.Key);
                }
            }
            else
            {
                foreach (var s in services)
                {
                    s.Value.Disconnect(s.Key);
                }
            }
        }

        #endregion

        public bool IsRunning { get; private set; }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            serviceController?.Dispose();
            healthCheckTimer.Stop();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
