﻿using System;
using System.Linq;
using System.ServiceProcess;
using ServiceCommon;

namespace Veit.Bat.Collector.Service_References.Watchers
{
    public sealed class SystemServiceController : ISystemServiceController
    {
        private ServiceController serviceController;
        private string service;

        #region Implementation of ISystemServiceController

        public bool Init(string serviceName)
        {
            service = serviceName;
            if (serviceController != null) DisposeServiceController();
            return InitServiceController();
        }

        public bool IsRunning()
        {
            if (string.IsNullOrEmpty(service) || !ServiceExists)
            {
                DisposeServiceController();
                return false;
            }
            if (serviceController == null && !Init(service)) return false;
            return GetServiceState() == CommonServiceBase.ServiceState.SERVICE_RUNNING;
        }

        #endregion

        private CommonServiceBase.ServiceState GetServiceState()
        {
            var state = CommonServiceBase.ServiceState.SERVICE_SHUTDOWN;
            if (serviceController == null) return state;
            try
            {
                serviceController.Refresh();
                state = (CommonServiceBase.ServiceState)serviceController.Status;
            }
            catch (Exception)
            {
                DisposeServiceController();
            }
            return state;
        }

        private bool ServiceExists
        {
            get { return ServiceController.GetServices().Any(s => s.ServiceName == service); }
        }

        private bool InitServiceController()
        {
            try
            {
                serviceController = new ServiceController(service);
                serviceController.Refresh();
                return true;
            }
            catch (Exception)
            {
                DisposeServiceController();
                return false;
            }
        }

        private void DisposeServiceController()
        {
            serviceController?.Dispose();
            serviceController = null;
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            DisposeServiceController();
        }

        #endregion
    }
}
