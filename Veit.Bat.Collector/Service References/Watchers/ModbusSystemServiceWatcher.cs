﻿using Communication.Contract.Wcf.BindingConstants;
using Veit.Bat.Collector.Service_References.Modbus;
using Veit.Bat.Collector.Service_References.WebApi;
using Veit.Bat.Collector.ViewModels.Services;

namespace Veit.Bat.Collector.Service_References.Watchers
{
    public class ModbusSystemServiceWatcher : SystemServiceWatcher
    {
        public ModbusSystemServiceWatcher(
           ModbusServiceViewModel serviceViewModel,
           ISystemServiceController serviceController,
           ModbusConnectionCommands modbusCommands,
            WebApiConnectionCommands webapi)
           : base(serviceViewModel, serviceController)
        {
            AddService(ServiceNetPipeBindingConstants.ModbusPipeAddress + ServiceNetPipeBindingConstants.ConnectionSubAddress, webapi);
            AddService(ServiceNetPipeBindingConstants.ModbusPipeAddress, modbusCommands);
        }
    }
}
