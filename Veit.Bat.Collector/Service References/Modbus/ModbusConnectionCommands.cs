﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using Communication.Connectivity;
using Communication.Contract.Wcf.DataContracts;
using Communication.Contract.Wcf.DataContracts.Device;
using Communication.Contract.Wcf.DataContracts.Device.Configuration;
using Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums;
using Communication.Contract.Wcf.DataSources;
using NLog;
using Settings.Core;
using Veit.Bat.Collector.Core.Service_References.ModbusConnection;
using Veit.Bat.Collector.IoC;
using Veit.Bat.Collector.Models;
using Veit.Bat.Collector.Notification;
using Veit.Bat.Common;
using Veit.Bat.Common.DeviceStatusInfo;

namespace Veit.Bat.Collector.Service_References.Modbus
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public class ModbusConnectionCommands : DeviceCommands, IModbusServiceCallback, IModbusCommands, IDiagnostic
    {
        private IModbusService commandClient;
        private string expectedComPortCallback;
        private string expectedDataSourceCallback;
        private string expectedCallbackName;
        private readonly object callbackLock = new object();
        private readonly TimeSpan callbackTimeout = TimeSpan.FromSeconds(3);
        private ManualResetEvent callbackReady;
        private readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();


        public Func<string, bool> AddSourceCommand()
        {
            return portName =>
            {
                return ExecuteActionWithPortAndWaitForCompletition(
                   portName,
                   client => client.AddSource(portName),
                   client =>
                   {
                       if (client != null)
                       {
                           client.RemoveSource(portName);
                       }
                       return false;
                   },
                   () => true, new List<string>
                      {nameof(Connected)});
            };
        }

        /// <inheritdoc />
        public Func<IEnumerable<string>, IEnumerable<byte>, bool> ScanComPortCommand()
        {
            return (portNames, addresses) =>
            {
                try
                {
                    commandClient.ScanPortAddresses(portNames, addresses);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        /// <inheritdoc />
        public Func<string, IEnumerable<string>, bool> ActivateDeviceCommand()
        {
            return (portName, addresses) =>
            {
                try
                {
                    commandClient.ActivateDevices(new List<string> { portName }, addresses);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        /// <inheritdoc />
        public Func<string, IEnumerable<string>, bool> DeactivateDeviceCommand()
        {
            return (portName, addresses) =>
            {
                try
                {
                    commandClient.DeactivateDevices(new List<string> { portName }, addresses);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        public Func<string, CommunicationParameters, bool> ChangeCommunicationParamsForPort()
        {
            return (portName, comParams) =>
            {
                try
                {
                    commandClient.SetPortSettings(portName, comParams);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        public Func<string, byte, bool> GetDeviceNameCommand()
        {
            return (portName, address) =>
            {
                try
                {
                    commandClient.GetDeviceName(portName, address);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        public Func<string, byte, string, bool> ChangeDeviceNameCommand()
        {
            return (portName, address, name) =>
            {
                try
                {
                    commandClient.SetDeviceName(portName, address, name);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        public Func<string, byte, bool> GetDeviceConfigurationCommand()
        {
            return (portName, address) =>
            {
                try
                {
                    commandClient.GetConfiguration(portName, address);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            };
        }

        public Func<string, bool> RemoveSourceCommand()
        {
            return portName =>
            {
                return ExecuteActionWithPortAndWaitForCompletition(
                   portName,
                   client => client.RemoveSource(portName),
                   client => false,
                   () => true,
                   new List<string>
                      {nameof(Disconnected)});
            };
        }

        private T ExecuteActionWithPortAndWaitForCompletition<T>(string portName,
           Action<IModbusService> commandAction,
           Func<IModbusService, T> commandErrAction,
           Func<T> commandOkAction, IEnumerable<string> callbackOperations)
        {
            var actionToDo = new Action<IModbusService>(client =>
            {
                expectedComPortCallback = portName;
                expectedDataSourceCallback = null;
                expectedCallbackName = callbackOperations.Last();
                commandAction?.Invoke(client);
            });

            return WcfLockedAction(commandErrAction, commandOkAction, actionToDo);
        }

        private T WcfLockedAction<T>(Func<IModbusService, T> commandErrAction, Func<T> commandOkAction,
           Action<IModbusService> actionToDo)
        {
            if (commandClient == null && commandErrAction != null)
            {
                return commandErrAction.Invoke(null);
            }

            lock (callbackLock)
            {
                try
                {
                    callbackReady = new ManualResetEvent(false);

                    actionToDo?.Invoke(commandClient);
                    // wait for info or timeout
                    if (!callbackReady.WaitOne(callbackTimeout))
                    {
                        if (commandErrAction != null)
                            return commandErrAction(commandClient);
                    }
                    else
                    {
                        if (commandOkAction != null)
                            return commandOkAction();
                    }

                    return default;
                }
                finally
                {
                    callbackReady.Dispose();
                    callbackReady = null;
                }
            }
        }


        /// <inheritdoc />
        public override void Connected(SourceConnectivityInfo device)
        {
            base.Connected(device);
            SetExpectedCallbackReady(nameof(Connected), device.Source, device.Device.SensorUid);
        }

        /// <inheritdoc />
        public override void Disconnected(SourceConnectivityInfo device)
        {
            base.Disconnected(device);
            SetExpectedCallbackReady(nameof(Disconnected), device.Source, device.Device.SensorUid);
        }

        private void SetExpectedCallbackReady(string callbackName, string connectedSourceId, string dataSourceId)
        {
            if (expectedCallbackName != callbackName ||
                connectedSourceId != expectedComPortCallback ||
                dataSourceId != expectedDataSourceCallback)
            {
                return;
            }
            if (callbackReady != null)
            {
                callbackReady.Set();
            }
        }

        /// <inheritdoc />
        public void ScanProgress(Percent percent)
        {
            var progressBar = ContainerLocator.Resolve<IProgressBar>();
            if (progressBar != null)
            {
                lock (progressBar)
                {
                    progressBar.Value = percent;
                    if (percent >= 100 && callbackReady != null)
                    {
                        callbackReady.Set();
                    }
                }
            }
        }

        /// <inheritdoc />
        public void Activated(string portId, string deviceAddress)
        {
            ExecuteActionForSourceInfo(
               DeviceType.Bat2Cable.ToString(), deviceAddress, portId,
               (sourceType, source) => { },
               (deviceType, source, deviceId) => HardwareDevice.ActivateDevice(deviceType, source, deviceId));
        }

        /// <inheritdoc />
        public void Deactivated(string portId, string deviceAddress)
        {
            ExecuteActionForSourceInfo(
               DeviceType.Bat2Cable.ToString(), deviceAddress, portId,
               (sourceType, source) => { },
               (deviceType, source, deviceId) => HardwareDevice.DeactivateDevice(deviceType, source, deviceId));
        }


        public void DeviceNameRead(string portId, string deviceAddress, string name)
        {
            HardwareDevice.Name(CreateIdentification(DeviceType.Bat2Cable, portId, deviceAddress), name);
        }

        public void DeviceVersion(string portId, string deviceAddress, DeviceVersion version)
        {
            HardwareDevice.Version(CreateIdentification(DeviceType.Bat2Cable, portId, deviceAddress), version);
        }

        public void DeviceConfiguration(string portId, string deviceAddress, DeviceConfiguration configuration)
        {
            HardwareDevice.Configuration(CreateIdentification(DeviceType.Bat2Cable, portId, deviceAddress), configuration);
        }

        private static DeviceIdentification CreateIdentification(DeviceType devicetype, string portId, string uid)
        {
            return new DeviceIdentification
            {
                DeviceType = devicetype,
                Source = portId,
                Uid = uid
            };
        }

        public override bool Connect(string address)
        {
            if (commandClient != null) return true;
            commandClient = Connect(() => new ModbusConnectionWcfClient(this, address, ToastNotifier.ShowError));
            commandClient.GetActive();
            return commandClient != null;
        }

        public override bool Disconnect(string address)
        {
            var s = commandClient;
            commandClient = null;
            return Disconnect(s, new[] { SourceType.Ethernet });
        }

        #region Implementation of IScaleFlockCallback

        public void DeviceFlock(string portId, string deviceAddress, Flock flock)
        {
            if (string.IsNullOrWhiteSpace(portId) || string.IsNullOrWhiteSpace(deviceAddress) || flock == null)
            {
                logger.Info("Read device flock: one or more arguments are invalid!");
                return;
            }
            logger.Info($"Read device flock, number: {flock.Number}, for source: {portId}, address: {deviceAddress}");
        }

        #endregion

        #region Implementation of IDiagnostic

        public void SetDebugMode(bool enable)
        {
            try
            {
                commandClient?.SetDebugMode(enable);
            }
            catch (Exception e)
            {
                logger.Warn(e, "Set debug mode exception");
            }
        }

        public void SendFeedback(string feedbackFileName, string message, bool includeDiagnosticFiles)
        {
            try
            {
                commandClient?.SendFeedback(feedbackFileName, message, includeDiagnosticFiles);
            }
            catch (Exception e)
            {
                logger.Warn(e, "Send diagnostics exception");
            }
        }

        #endregion
    }
}
