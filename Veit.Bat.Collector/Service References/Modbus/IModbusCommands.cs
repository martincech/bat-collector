﻿using System;
using System.Collections.Generic;
using Communication.Contract.Wcf.DataContracts;

namespace Veit.Bat.Collector.Service_References.Modbus
{
    public interface IModbusCommands : IDeviceConfigurationCommands
    {
        Func<string, bool> AddSourceCommand();
        Func<string, bool> RemoveSourceCommand();
        Func<IEnumerable<string>, IEnumerable<byte>, bool> ScanComPortCommand();

        Func<string, IEnumerable<string>, bool> ActivateDeviceCommand();
        Func<string, IEnumerable<string>, bool> DeactivateDeviceCommand();
        Func<string, CommunicationParameters, bool> ChangeCommunicationParamsForPort();
    }
}
