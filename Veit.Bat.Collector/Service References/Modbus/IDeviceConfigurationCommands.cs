﻿using System;

namespace Veit.Bat.Collector.Service_References.Modbus
{
    public interface IDeviceConfigurationCommands
    {
        Func<string, byte, bool> GetDeviceNameCommand();
        Func<string, byte, string, bool> ChangeDeviceNameCommand();
        Func<string, byte, bool> GetDeviceConfigurationCommand();
    }
}
