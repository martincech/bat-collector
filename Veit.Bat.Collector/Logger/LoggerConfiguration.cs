﻿using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using NLog.Targets;
using Settings;
using Settings.Core.Logger;
using Veit.Bat.Collector.IoC;

namespace Veit.Bat.Collector.Logger
{
    public class LoggerConfiguration : BaseLoggerConfiguration
    {
        public FileTarget UiTarget { get; private set; }

        public LoggingConfiguration InitLoggerConfiguration()
        {
            RegisterRenderers();
            var config = new LoggingConfiguration();

            //targets
            var mainTarget = InitMainFileTarget(ref config, "BatCollector");
            var sentryTarget = InitSentryTarget(ref config);

            //define rules
            var mainRule = new LoggingRule("*", DefaultLevel, mainTarget);
            config.LoggingRules.Add(mainRule);
            DebugRules.Add(mainRule);

            var sentryRule = new LoggingRule("*", LogLevel.Error, sentryTarget);
            config.LoggingRules.Add(sentryRule);

            return config;
        }


        #region Private helpers

        protected override void RegisterRenderers()
        {
            var terminalId = ContainerLocator.Resolve<ManagerSettings>().Settings.TerminalId;

            LayoutRenderer.Register("appName", logEvent => "BAT Terminal");
            LayoutRenderer.Register(nameof(terminalId), logEvent => terminalId);
            base.RegisterRenderers();
        }

        #endregion
    }
}
