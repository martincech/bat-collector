﻿using System.Collections.Generic;

namespace Veit.Bat.Collector.Repository.House
{
    public interface IHouseDataRepository
    {
        IEnumerable<string> GetHouses();
    }
}
