﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Connectivity;
using Settings.DeviceSettings;
using Settings.DeviceSettings.SourceSettings;
using Veit.Bat.Collector.Models;
using Veit.Bat.Collector.ViewModels.Sensor;
using Veit.Bat.Common;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Sample.Co2;
using Veit.Bat.Common.Sample.Humidity;
using Veit.Bat.Common.Sample.Temperature;
using Veit.Bat.Common.Sample.Weight;

namespace Veit.Bat.Collector.Repository.Sensor
{
    public static class SensorDataRepositoryExtension
    {
        private static readonly Dictionary<Type, SourceType> settingsToTypeMapping = new Dictionary<Type, SourceType>
        {
            {typeof(EthernetSourceSettings), SourceType.Ethernet},
        };

        private static readonly Dictionary<SourceType, Func<DeviceSourceSettings>> typeToSettingsMapping = new Dictionary<SourceType, Func<DeviceSourceSettings>>
        {
            {SourceType.Ethernet, () => new EthernetSourceSettings() },
        };

        public static IEnumerable<SensorViewModel> MapTo(this DeviceSettingsTree settings)
        {
            var deviceTree = new List<SensorViewModel>();
            if (settings == null) return deviceTree;

            var ethernetSources = settings.EthernetSources.LoadSources();
            deviceTree.AddRange(ethernetSources);

            return deviceTree;
        }

        public static DeviceSettings MapFrom(this SensorViewModel device)
        {
            if (device == null) return null;

            DateTimeOffset? lastSample = null;
            if (device.HourSamples.Any())
            {
                lastSample = device.HourSamples.FirstOrDefault().Sample.TimeStamp;
            }

            return new DeviceSettings
            {
                Name = device.Name,
                Uid = device.Uid,
                Type = device.Type.MapToSettings(device),
                LastSampleTimeStamp = lastSample
            };
        }


        public static DeviceSourceSettings MapFrom(this SourceDevice device)
        {
            if (device == null || !typeToSettingsMapping.TryGetValue(device.Type, out var settings))
            {
                return null;
            }
            var sourceSettings = settings.Invoke();
            sourceSettings.Name = device.Name;
            sourceSettings.Uid = device.Name;
            sourceSettings.SetSettings(device.Settings);
            return sourceSettings;
        }


        public static SensorViewModel CreateDevice(DeviceType deviceType, SourceDevice source, string uid)
        {
            SensorViewModel vm;
            switch (deviceType)
            {
                case DeviceType.Bat2Cable:
                case DeviceType.Bat3:
                    var (gateway, address) = ParseGatewayInfo(uid);
                    vm = new Bat2ModbusSensorViewModel(gateway, address);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(deviceType), "Unknown device!");
            }

            vm.Uid = uid;
            vm.Parent = source;
            return vm;
        }

        #region Private helpers

        private static DeviceType MapToSettings(this Models.Enums.SensorType model, SensorViewModel device)
        {
            switch (model)
            {
                case Models.Enums.SensorType.Thermometer:
                    return DeviceType.ExternalTemperature;
                case Models.Enums.SensorType.Hydrometer:
                    return DeviceType.ExternalHumidity;
                case Models.Enums.SensorType.Co2:
                    return DeviceType.ExternalCo2;
                case Models.Enums.SensorType.Bat2:
                    return GetScaleType(device);
                default:
                    throw new ArgumentOutOfRangeException(nameof(model));
            }
        }

        private static DeviceType GetScaleType(SensorViewModel device)
        {
            if (!(device is Bat2SensorViewModel scale))
            {
                throw new ArgumentException("Device is not Scale!");
            }

            switch (scale.ScaleType)
            {
                case Models.Enums.ScaleType.Bat2Modbus:
                    if (scale.Version == Communication.Contract.Wcf.DataContracts.Device.Configuration.Enums.DeviceVersion.Bat2V153)
                    {   //BAT2 version: 1.53 send raw values => "BAT3"
                        return DeviceType.Bat3;
                    }
                    return DeviceType.Bat2Cable;
                default:
                    throw new InvalidOperationException("Unknown scale type");
            }
        }

        private static List<SensorViewModel> LoadScales(IEnumerable<DeviceSettings> devices, SourceDevice source)
        {
            var viewModels = new List<SensorViewModel>();
            if (devices == null)
            {
                return viewModels;
            }

            foreach (var deviceSettings in devices)
            {
                var vm = CreateScaleViewModelFromSettings(deviceSettings, source);
                if (vm == null)
                {
                    continue;
                }
                viewModels.Add(vm);
            }
            return viewModels;
        }

        private static SensorViewModel CreateScaleViewModelFromSettings(DeviceSettings deviceSettings, SourceDevice source)
        {
            var vm = CreateDevice(deviceSettings.Type, source, deviceSettings.Uid);
            vm.Name = deviceSettings.Name;
            if (deviceSettings.LastSampleTimeStamp.HasValue)
            {
                vm.HourSamples.Add(new SensorDataViewModel
                {
                    Sample = CreateSampleForSpecificSensor(deviceSettings.Type, deviceSettings.LastSampleTimeStamp.Value)
                });
            }
            return vm;
        }

        private static (string gateway, int address) ParseGatewayInfo(string uid)
        {
            var parts = uid.Split('-');
            if (parts.Count() != 2 || !int.TryParse(parts[1], out var address))
            {
                throw new ArgumentException(nameof(uid));
            }
            return (gateway: parts[0], address);
        }

        private static List<SensorViewModel> LoadSources<T>(this IEnumerable<DeviceSourceSettings<T>> deviceSources)
        {
            if (deviceSources == null) return new List<SensorViewModel>();
            var viewModels = new List<SensorViewModel>();
            foreach (var source in deviceSources)
            {
                viewModels.AddRange(source.LoadSourceDevices());
            }
            return viewModels;
        }

        private static List<SensorViewModel> LoadSourceDevices<T>(this DeviceSourceSettings<T> deviceSource)
        {
            var emptyList = new List<SensorViewModel>();
            if (deviceSource == null)
            {
                return emptyList;
            }
            var source = GetSource(deviceSource);

            if (deviceSource.GetType() == typeof(EthernetSourceSettings))
            {
                return LoadScales(deviceSource.Devices, source);
            }
            return emptyList;
        }

        private static SourceDevice GetSource<T>(DeviceSourceSettings<T> deviceSource)
        {
            if (!settingsToTypeMapping.TryGetValue(deviceSource.GetType(), out var sourceType))
            {
                throw new MemberAccessException($"Unknown source type: {sourceType}");
            }
            return new SourceDevice
            {
                Name = deviceSource.Uid,
                Type = sourceType,
                Settings = deviceSource.Settings
            };
        }


        private static TimeStampedSample CreateSampleForSpecificSensor(DeviceType sensorType, DateTimeOffset timeStamp)
        {
            TimeStampedSample sample = null;
            switch (sensorType)
            {
                case DeviceType.ExternalTemperature:
                    sample = new TemperatureSample(timeStamp);
                    break;
                case DeviceType.ExternalCo2:
                    sample = new Co2Sample(timeStamp);
                    break;
                case DeviceType.ExternalHumidity:
                    sample = new HumiditySample(timeStamp);
                    break;
                case DeviceType.Bat3:
                    sample = new BirdWeight(0, timeStamp);
                    break;
                case DeviceType.Bat2Cable:
                case DeviceType.Bat2Gsm:
                    sample = new StatisticSample
                    {
                        UndefinedValue = 0,
                        TimeStamp = timeStamp
                    };
                    break;
                default:
                    break;
            }
            return sample;
        }

        #endregion
    }
}
