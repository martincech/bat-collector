﻿using System.Collections.Generic;
using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.Repository.Sensor
{
    public interface ISensorDataRepository
    {
        IEnumerable<SensorViewModel> GetSensors();
        void AddOrUpdateSensor(SensorViewModel sensor);
        void DeleteSensor(SensorViewModel sensor);
        void AddOrUpdateSource(SourceDevice source);
        void DeleteSource(SourceDevice source);
    }
}
