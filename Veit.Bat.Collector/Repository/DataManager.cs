﻿using System;
using System.Collections.Generic;
using System.IO;
using Settings;
using Veit.Bat.Collector.Repository.House;
using Veit.Bat.Collector.Repository.Sensor;
using Veit.Bat.Collector.ViewModels.Sensor;

namespace Veit.Bat.Collector.Repository
{
    public class DataManager : ISensorDataRepository,
                               IHouseDataRepository,
                               IDiagnosticSettingsManager
    {
        private readonly IManagerSettings settings;


        public DataManager(IManagerSettings settings)
        {
            //if application is run as different user or in kiosk mode, you must set current directory for finding correctly demo data
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
        }

        #region Implementation of ISensorDataRepository

        public IEnumerable<SensorViewModel> GetSensors()
        {
            return settings.LoadDevices(true).MapTo();
        }
        public void AddOrUpdateSensor(SensorViewModel sensor)
        {
            settings.AddOrUpdateDevice(sensor.MapFrom(), sensor.Parent.MapFrom());
        }

        public void DeleteSensor(SensorViewModel sensor)
        {
            settings.DeleteDevice(sensor.MapFrom(), sensor.Parent.MapFrom());
        }

        public void AddOrUpdateSource(SourceDevice source)
        {
            settings.AddOrUpdateDevice(source.MapFrom(), null);
        }

        public void DeleteSource(SourceDevice source)
        {
            settings.DeleteDevice(source.MapFrom(), null);
        }

        #endregion

        #region Implementation of IHouseDataRepository

        public IEnumerable<string> GetHouses()
        {
            return new List<string>();
        }

        public bool SaveDiagnostic(Settings.DiagnosticSettings.Diagnostics diagnostic)
        {
            return settings.SaveDiagnostic(diagnostic);
        }

        public Settings.DiagnosticSettings.Diagnostics LoadDiagnostics()
        {
            return settings.LoadDiagnostics();
        }

        #endregion
    }
}
