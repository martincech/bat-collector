[cmdletbinding()]
Param(
    [Parameter(Mandatory=$true)]
    [ValidateScript({
        if( -Not ($_ | Test-Path  -PathType leaf) ){
            throw "File does not exist"
            exit 1
        }
        return $true
    })]$InFile,
    [Parameter(Mandatory=$false)]
    [System.IO.FileInfo]$OutFile
)

if ($null -eq $OutFile) {
    $OutFile = $InFile;
}

$url = $env:VEIT_CI_SERVICE_BASE_URL + '/sign'
$contentType = 'application/octet-stream'
$headers = @{
    'X-API-Key' = $env:VEIT_CI_SERVICE_API_KEY
}

#Set global paramter $ProgressPreference to disable progress bar when uploading file
$ProgressPreference = 'SilentlyContinue'

try {	
    Invoke-RestMethod -Uri $url -Method Post -ContentType $contentType -Headers $headers -InFile $inFile -OutFile $outFile -TimeoutSec 600
    exit 0
} catch {
    if ($null -eq $_.Exception.Response) {
        Write-Host "Exception:" $_.Exception
    }
    else {
        Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__
        Write-Host "StatusDescription:" $_.Exception.Response.StatusDescription
    }
    exit 2
}
