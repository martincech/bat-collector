﻿using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Settings.Core
{
    public static class DirectoryHelper
    {
        public static void GrantAccess(string directory)
        {
            var dInfo = new DirectoryInfo(directory);
            var dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.Modify, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
        }
    }
}
