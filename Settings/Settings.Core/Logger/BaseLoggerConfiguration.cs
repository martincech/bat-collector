﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;
using NLog.Layouts;
using NLog.Targets;
using NLog.Targets.Wrappers;

namespace Settings.Core.Logger
{
    public abstract class BaseLoggerConfiguration
    {
        #region Private fields

        private const string SentryDns =
           "https://98e2bb67344e4227ae1d1270d335e464:4e17cedaf4634eea8b10fcaa84dec527@sentry.io/280662";

        private const int MaxArchiveFiles = 14;
        private const int MaxInnerExceptionLevel = 10;

        protected static string LogSuffix => ".log";
        protected ICollection<LoggingRule> DebugRules { get; set; }

        public LogLevel DefaultLevel { get; protected set; } =
#if !DEBUG
         LogLevel.Info;
#else
         LogLevel.Debug;
#endif

        #endregion

        public static string BaseFolder => "Logs";
        public static string ArchiveFolderName => "Archive";

        public static string RootFolder
        {
            get { return Path.Combine(AppFolders.Data, BaseFolder); }
        }
        protected BaseLoggerConfiguration()
        {
            DebugRules = new List<LoggingRule>();
            SystemInfo = new SystemInfo();
        }

        public static string ArchiveFolderFullPath
        {
            get { return Path.Combine(RootFolder, ArchiveFolderName); }
        }

        public SystemInfo SystemInfo { get; private set; }

        protected Layout SentryLayout;

        protected virtual void RegisterRenderers()
        {
            LayoutRenderer.Register("osVersion", logEvent => SystemInfo.OsVersion);
            LayoutRenderer.Register("system", logEvent => SystemInfo.SystemType);
            LayoutRenderer.Register("systemLang", logEvent => SystemInfo.SystemLanguage);
            LayoutRenderer.Register("uiLang", logEvent => SystemInfo.UiLanguage);
        }


        protected static Target InitMainFileTarget(ref LoggingConfiguration config, string name)
        {
            var fileLogName = "log" + name;
            var fileTarget = new FileTarget(fileLogName);
            config.AddTarget(fileLogName, fileTarget);

            var asyncTargetName = "asyncTarget" + name + "Target";
            var asyncTarget = new AsyncTargetWrapper(asyncTargetName, fileTarget);
            config.AddTarget(asyncTarget);

            //target properties
            fileTarget.CreateDirs = true;
            fileTarget.FileName = Path.Combine(RootFolder, name + LogSuffix);
            fileTarget.Encoding = Encoding.UTF8;
            fileTarget.Layout =
               @"[${date:format=yyyy-MM-ddTHH\:mm\:ss.fffK}]|${uppercase:${level}}" +
#if DEBUG
            "|${callsite}:${callsite-linenumber}" +
#endif
            "|${message}\t${exception:format=toString,Data:maxInnerExceptionLevel=" + MaxInnerExceptionLevel + "}";
            fileTarget.ArchiveFileName = Path.Combine(ArchiveFolderFullPath, "${shortdate}-" + name + LogSuffix);
            fileTarget.MaxArchiveFiles = MaxArchiveFiles;
            fileTarget.ArchiveEvery = FileArchivePeriod.Day;

            return asyncTarget;

        }

        protected Target InitSentryTarget(ref LoggingConfiguration config)
        {
            const string sentryLogName = "logSentry";
            var sentryTarget = new SentryTarget();
            config.AddTarget(sentryLogName, sentryTarget);

            const string asyncTargetName = "asyncSentryTarget";
            var asyncTarget = new AsyncTargetWrapper(asyncTargetName, sentryTarget);
            config.AddTarget(asyncTarget);

            //target properties
            sentryTarget.Dsn = SentryDns;
            sentryTarget.Environment =
#if !DEBUG
         "production";
#else
         "test";
#endif
            sentryTarget.Layout = @"${message}${processid:sentrytag=pid}${appName:sentrytag=application}${osVersion:sentrytag=os_version}${system:sentrytag=system}${systemLang:sentrytag=system_language}${uiLang:sentrytag=ui_language}${terminalId:sentrytag=terminal_id}";
            SentryLayout = sentryTarget.Layout;
            return asyncTarget;
        }

        public void SetDebugMode(bool enable)
        {
            var level = LogLevel.Debug;
            foreach (var rule in DebugRules)
            {
                if (enable)
                {
                    rule.EnableLoggingForLevel(level);
                }
                else
                {
                    rule.DisableLoggingForLevel(level);
                }
            }

            LogManager.ReconfigExistingLoggers();
        }
    }
}
