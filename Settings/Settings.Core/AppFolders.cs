﻿using System;               //DON'T DELETE IT! It's used in release build
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;    //DON'T DELETE IT! It's used in debug build

namespace Settings.Core
{
    public static class AppFolders
    {
        public const string COMPANY_SUB_FOLDER = "Veit";
        public const string PROJECT_SUB_FOLDER = "BAT_Terminal";

        public static string BaseFolder
        {
            get
            {
#if DEBUG
                return Path.GetDirectoryName((Assembly.GetEntryAssembly() ?? Assembly.GetAssembly(typeof(AppFolders))).Location);
#else
                return Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
#endif
            }
        }

        public static string IntoData(string what)
        {
            return Path.Combine(Data, what);
        }

        public static string Data => Settings;

        public static string IntoSettings(string what)
        {
            return Path.Combine(Settings, what);
        }

        public static string Settings
        {
            get { return Path.Combine(BaseFolder, COMPANY_SUB_FOLDER, PROJECT_SUB_FOLDER); }
        }
    }
}
