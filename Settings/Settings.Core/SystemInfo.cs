﻿using System;
using System.Globalization;
using System.Management;
using HardwareId;

namespace Settings.Core
{
    public class SystemInfo
    {
        public SystemInfo()
        {
            LoadWindowsVersion();
            TerminalId = HwId.SystemId;
        }

        public string OsVersion
        {
            get { return $"{WindowsProductName} ({WindowsVersionNumber})"; }
        }

        public string WindowsVersionNumber { get; private set; }
        public string WindowsProductName { get; private set; }

        public string SystemType
        {
            get { return Environment.Is64BitOperatingSystem ? "64bit" : "32bit"; }
        }

        public string SystemLanguage
        {
            get { return CultureInfo.InstalledUICulture.EnglishName; }
        }

        public string UiLanguage
        {
            get { return CultureInfo.CurrentUICulture.EnglishName; }
        }

        public Guid TerminalId { get; set; }
        public string LoggedUserName { get; set; }


        private void LoadWindowsVersion()
        {
            using (var searcher = new ManagementObjectSearcher("SELECT Caption, Version FROM Win32_OperatingSystem"))
            {
                foreach (var os in searcher.Get())
                {
                    WindowsVersionNumber = os["Version"].ToString();
                    WindowsProductName = os["Caption"].ToString();
                }
            }
        }
    }
}
