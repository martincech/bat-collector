﻿namespace Settings.Core.Repository
{
    public interface ICoreEngine<T>
    {
        void Save(T settings);
        T Load();
    }
}
