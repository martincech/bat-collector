﻿using System.IO;
using Newtonsoft.Json;

namespace Settings.Core.Repository
{
    public abstract class JsonCoreEngine<T> : ICoreEngine<T> where T : class
    {
        #region Private fields

        private const string DEFAULT_NAME = "Settings.json";
        private readonly string name;
        private string fullName;

        #endregion

        #region Constructor

        protected JsonCoreEngine(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = DEFAULT_NAME;
            }
            name = fileName;
            CheckFolder();
        }

        #endregion

        #region Implementation of ICoreEngine<T>

        public virtual void Save(T settings)
        {
            var text = JsonConvert.SerializeObject(settings);
            File.WriteAllText(fullName, text);
        }

        public virtual T Load()
        {
            return Load(null);
        }

        public virtual T Load(JsonConverter converter)
        {
            if (!File.Exists(fullName))
            {
                return null;
            }
            var json = File.ReadAllText(fullName);
            return converter == null ? JsonConvert.DeserializeObject<T>(json) : JsonConvert.DeserializeObject<T>(json, converter);
        }

        #endregion

        #region Private helpers

        private void CheckFolder()
        {
            var folder = AppFolders.Settings;
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
                DirectoryHelper.GrantAccess(folder);
            }
            fullName = AppFolders.IntoSettings(name);
        }

        #endregion
    }
}
