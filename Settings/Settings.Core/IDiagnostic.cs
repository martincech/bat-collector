﻿namespace Settings.Core
{
    public interface IDiagnostic
    {
        void SetDebugMode(bool enable);
        void SendFeedback(string feedbackFileName, string message, bool includeDiagnosticFiles);
    }
}
