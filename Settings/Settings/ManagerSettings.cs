﻿using System;
using System.Collections.Generic;
using System.Linq;
using Communication.Contract.Wcf.DataContracts;
using HardwareId;
using Heartbeat;
using Settings.Core;
using Settings.DataSourceSettings.Modbus;
using Settings.DataStorageSettings;
using Settings.DeviceSettings;
using Settings.DeviceSettings.SourceSettings;
using Settings.DiagnosticSettings;
using Settings.StorageEngine;
using Veit.Cognito.Service;

namespace Settings
{
    public class ManagerSettings : DefaultAppConfigParser, IManagerSettings
    {
        private const string BAT_API_URL = "BAT_API_URL";
        private const string SENSORS_API_URL = "SENSORS_API_URL";
        private const string HEARTBEAT_URL = "HEARTBEAT_URL";
        private static readonly Dictionary<string, string> DEFAULT_APP_TAGS = new Dictionary<string, string>
        {
#if !DEBUG
            {nameof(BAT_API_URL),"https://batapi.bat.veit.cz/"},
            {nameof(SENSORS_API_URL),"https://sensorsapi.bat.veit.cz/"},
            {nameof(HEARTBEAT_URL),"https://sensorsapi.bat.veit.cz/v1/heartbeat"}
#else
            {nameof(BAT_API_URL),"http://localhost:60083/"},
            {nameof(SENSORS_API_URL),"https://localhost:44398"},
            {nameof(HEARTBEAT_URL),"http://localhost:44398/api/v1/heartbeat"}

            // staging
            //{nameof(CLOUD_SERVER_URL),"https://batapi.internal.bat.veit.cz/"},
            //{nameof(SENSORS_API_URL),"https://sensorsapi.internal.bat.veit.cz/"},
            //{nameof(HEARTBEAT_URL),"https://sensorsapi.internal.bat.veit.cz/v1/heartbeat"}
#endif
        };


        #region Private fields

        private readonly IEngine engine;
        private readonly IDeviceSettingsManager deviceSettings;

        #endregion

        #region Constructors

        public ManagerSettings(IEngine engine, IDeviceSettingsManager deviceSettings, SystemInfo systemInfo)
           : base(DEFAULT_APP_TAGS)
        {
            SystemInfo = systemInfo ?? throw new ArgumentNullException(nameof(systemInfo));
            this.engine = engine;
            this.deviceSettings = deviceSettings;
            Load();
        }

        public ManagerSettings()
           : this(new JsonSettingsEngine(), new DeviceSettingsManager(new DeviceSettingsEngine()), new SystemInfo())
        {
        }

        #endregion

        public Settings Settings { get; private set; }

        public SystemInfo SystemInfo { get; private set; }

        public string HeartbeatUrl
        {
            get
            {
                var connection = Settings.Storages.OfType<WebApiRepositorySettings>().FirstOrDefault();
                if (connection == null || string.IsNullOrEmpty(connection.HeartbeatUrl))
                {
                    return FromAppSettingsOrDefault(HEARTBEAT_URL);
                }
                return connection.HeartbeatUrl;
            }
        }

        public void Save()
        {
            engine.Save(Settings);
        }

        public bool SaveConnection(WebApiConnectionInfo connectionInfo)
        {
            Load();
            var connection = Settings.Storages.OfType<WebApiRepositorySettings>().FirstOrDefault();
            if (connection == null) return false;
            if (connectionInfo.Login != null)
            {
                connection.IdToken = connectionInfo.Login.IdToken;
                connection.AccessToken = connectionInfo.Login.AccessToken;
                connection.RefreshToken = connectionInfo.Login.RefreshToken;
                connection.Email = connectionInfo.Login.UserName;
                connection.CognitoSettings = connectionInfo.Login.CognitoSettings;
            }
            connection.BatApi = connectionInfo.BatApi == null ? "" : connectionInfo.BatApi.Value;
            connection.SensorsApi = connectionInfo.SensorsApi == null ? "" : connectionInfo.SensorsApi.Value;
            engine.Save(Settings);
            return true;
        }

        public void Load()
        {
            var settings = engine.Load();
            if (settings == null)
            {
                settings = CreateDefaultSettings();
                settings.TerminalId = HwId.SystemId;
                engine.Save(settings);
            }
            settings.TerminalId = HwId.SystemId;
            MigrateSettings(settings);

            Settings = settings;
            SystemInfo.TerminalId = settings.TerminalId;
        }

        /// <summary>
        /// Load default values for properties added later.
        /// </summary>
        /// <param name="settings"></param>
        private void MigrateSettings(Settings settings)
        {
            var connection = settings.Storages.OfType<WebApiRepositorySettings>().FirstOrDefault();
            if (connection == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(connection.HeartbeatUrl))
            {   //heartbeat URL property was added later, therefore is assigned after load
                connection.HeartbeatUrl = FromAppSettingsOrDefault(HEARTBEAT_URL);
            }
            if (string.IsNullOrEmpty(connection.SensorsApi))
            {
                connection.SensorsApi = FromAppSettingsOrDefault(SENSORS_API_URL);
            }
            if (string.IsNullOrEmpty(connection.BatApi))
            {
                connection.BatApi = FromAppSettingsOrDefault(BAT_API_URL);
            }

            engine.Save(settings);
        }


        public List<WebApiConnectionInfo> LoadConnections()
        {
            Load();
            return Settings.Storages.OfType<WebApiRepositorySettings>().Select(s => s.ConnectionInfo).ToList();
        }


        /// <inheritdoc />
        public bool SaveModbusSettings(ModbusSettings settings)
        {
            Load();
            var source = Settings.Sources.OfType<ModbusManagerSettings>().FirstOrDefault();
            if (source == null) return false;
            source.DefaultConnectivitySettings = settings;
            Save();
            return true;
        }

        /// <inheritdoc />
        public ModbusSettings LoadModbusSettings()
        {
            Load();
            var source = Settings.Sources.OfType<ModbusManagerSettings>().FirstOrDefault() ?? DefaultSettings.DefaultModbusSettings() as ModbusManagerSettings;
            return source?.DefaultConnectivitySettings;
        }

        public Settings CreateDefaultSettings()
        {
            return new Settings
            {
                Storages = new List<RepositorySettings>
                {
                   DefaultWebApiStorage(),
                   DefaultSettings.DefaultLocalCsvStorage()
                },
                Sources = new List<DataSourceSettings.DataSourceSettings>
                {
                   DefaultSettings.DefaultModbusSettings(),
                },
                Weighing = DefaultSettings.DefaultWeighingParametersCsv(),
                Diagnostics = DefaultSettings.DefaultDiagnostics()
            };
        }

        private WebApiRepositorySettings DefaultWebApiStorage()
        {
            return DefaultSettings.DefaultWebApiStorage(
                         FromAppSettingsOrDefault(BAT_API_URL),
                         FromAppSettingsOrDefault(SENSORS_API_URL),
                         FromAppSettingsOrDefault(HEARTBEAT_URL),
                         "Cloud"); //Cloud fixed address
        }

        public DeviceSettingsTree LoadDevices(bool excludeDeleted)
        {
            return deviceSettings.LoadDevices(excludeDeleted);
        }

        public bool AddOrUpdateDevice<T>(T device, DeviceSourceSettings parent = null)
           where T : DeviceSettings.DeviceSettings
        {
            return deviceSettings.AddOrUpdateDevice(device, parent);
        }

        public bool AddOrUpdateDevices<T>(Dictionary<T, DeviceSourceSettings> devices)
           where T : DeviceSettings.DeviceSettings
        {
            return deviceSettings.AddOrUpdateDevices(devices);
        }

        public bool UpdateDevice(string uid, string name, DateTimeOffset timeStamp, Guid? guid = null)
        {
            return deviceSettings.UpdateDevice(uid, name, timeStamp, guid);
        }

        public bool DeleteDevice<T>(T device, DeviceSourceSettings parent = null)
           where T : DeviceSettings.DeviceSettings
        {
            return deviceSettings.DeleteDevice(device, parent);
        }

        public bool DeleteDevices<T>(Dictionary<T, DeviceSourceSettings> devices)
           where T : DeviceSettings.DeviceSettings
        {
            return deviceSettings.DeleteDevices(devices);
        }

        #region Implementation of IDiagnosticSettingsManager

        public bool SaveDiagnostic(Diagnostics diagnostic)
        {
            if (diagnostic == null)
            {
                return false;
            }
            Settings.Diagnostics = diagnostic;
            Save();
            return true;
        }

        public Diagnostics LoadDiagnostics()
        {
            if (Settings.Diagnostics == null)
            {
                Settings.Diagnostics = DefaultSettings.DefaultDiagnostics();
                Save();
            }
            return Settings.Diagnostics;
        }

        #region Update endpoints

        public bool UpdateEndpoints(List<Endpoint> endpoints)
        {
            if (endpoints == null || endpoints.Count == 0) return false;
            return endpoints.Select(s => UpdateEndpoint(s)).Any(a => a);
        }

        private bool UpdateEndpoint(Endpoint endpoint)
        {
            if (endpoint == null) return false;
            try
            {
                var settings = Settings.Storages.OfType<WebApiRepositorySettings>().FirstOrDefault();
                if (settings == null)
                {
                    settings = DefaultWebApiStorage();
                    (Settings.Storages as List<RepositorySettings>).Add(settings);
                }
                var result = false;
                switch (endpoint.Type)
                {
                    case EndpointType.BatApi:
                        result = UpdateEndpointAddress(settings.BatApi, endpoint.Address, () => settings.BatApi = endpoint.Address);
                        break;
                    case EndpointType.SensorsApi:
                        result = UpdateEndpointAddress(settings.SensorsApi, endpoint.Address, () => settings.SensorsApi = endpoint.Address);
                        break;
                    case EndpointType.Heartbeat:
                        result = UpdateEndpointAddress(settings.HeartbeatUrl, endpoint.Address, () => settings.HeartbeatUrl = endpoint.Address);
                        break;
                    case EndpointType.Identity:
                        result = UpdateIdentityEndpoint(endpoint.Attributes);
                        break;
                    default:
                        return false;
                }
                if (result)
                {
                    Save();
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error(e, $"Failed to update endpoint {endpoint.Type}.");
                return false;
            }
        }

        private bool UpdateEndpointAddress(string current, string update, Action change)
        {
            if (string.IsNullOrEmpty(update) || string.Equals(current, update, StringComparison.OrdinalIgnoreCase)) return false;
            logger.Info($"Endpoint changed: {current} => {update}");
            change?.Invoke();
            return true;
        }

        private bool UpdateIdentityEndpoint(Dictionary<string, string> attributes)
        {
            if (attributes == null || attributes.Count == 0) return false;
            var settings = Settings.Storages.OfType<WebApiRepositorySettings>().FirstOrDefault();
            if (settings != null)
            {
                if (settings.CognitoSettings == null)
                {
                    settings.CognitoSettings = DefaultSettings.DefaultCognitoSettings();
                }
            }
            else
            {
                logger.Error($"Collecotor settings does not contain {nameof(WebApiRepositorySettings)}.");
            }
            return attributes.Select(attr => UpdateIdentityValue(settings.CognitoSettings, attr.Key, attr.Value)).Any(a => a);
        }

        private bool UpdateIdentityValue(CognitoSettings cognitoSettings, string key, string value)
        {
            if (string.IsNullOrEmpty(key)) return false;
            try
            {
                var property = typeof(CognitoSettings).GetProperty(key);
                if (property != null)
                {
                    var originalValue = property.GetValue(cognitoSettings) as string;
                    if (string.Equals(originalValue, value, StringComparison.OrdinalIgnoreCase)) return false;
                    property.SetValue(cognitoSettings, value);
                    return true;
                }
            }
            catch (Exception e)
            {
                logger.Error(e, $"Failed to update identity value. {key}:\'{value}\'");
            }
            return false;
        }

        public int GetDeviceRelativeId(string uid)
        {
            return deviceSettings.GetDeviceRelativeId(uid);
        }
        #endregion
        #endregion
    }
}
