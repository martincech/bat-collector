﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using NLog;

namespace Settings
{
    public class DefaultAppConfigParser
    {
        protected readonly Dictionary<string, string> DefaultTagValues;
        protected readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public DefaultAppConfigParser(Dictionary<string, string> defaultTagValues)
        {
            DefaultTagValues = defaultTagValues;
        }

        protected long NumberFromAppSettings(string tag)
        {
            var str = FromAppSettingsOrDefault(tag);
            if (long.TryParse(str, out var id))
            {
                return id;
            }
            throw new ArrayTypeMismatchException();
        }

        protected string FromAppSettingsOrDefault(string appConfigTag)
        {
            string tagValue;
            if (ConfigurationManager.AppSettings.AllKeys.Contains(appConfigTag))
            {
                tagValue = ConfigurationManager.AppSettings[appConfigTag];
            }
            else
            {
                logger.Debug("No {0} settings found in App.config, using default", appConfigTag);
                try
                {
                    tagValue = DefaultTagValues[appConfigTag];
                }
                catch (Exception)
                {
                    throw new InvalidOperationException("Wrong default values specified for app config!");
                }
            }
            return tagValue;
        }
    }
}
