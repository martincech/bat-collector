﻿using System;

namespace Settings.Extensions
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// Replace time in source by current time stamp.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DateTimeOffset SetCurrentTime(this DateTimeOffset source)
        {
            var currentTime = CurrentTime();
            return new DateTimeOffset(source.Year, source.Month, source.Day, currentTime.Hours, currentTime.Minutes, currentTime.Seconds, source.Offset);
        }

        /// <summary>
        /// Add current time to source and return new date time.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DateTimeOffset AddCurrentTime(this DateTimeOffset source)
        {
            return source.Add(CurrentTime());
        }

        private static TimeSpan CurrentTime()
        {
            var currentDate = DateTime.Now;
            return new TimeSpan(0, currentDate.Hour, currentDate.Minute, currentDate.Second);
        }


        /// <summary>
        /// Determine if date time is midnight.
        /// Do not check only the exact value of midnight,
        /// but with small range on both sides.
        /// </summary>
        /// <param name="dateTime"></param>
        public static bool TimeIsMidnight(this DateTimeOffset dateTime)
        {
            var beforeMidnightLimit = new TimeSpan(23, 59, 0);
            var afterMidnightLimit = new TimeSpan(0, 1, 59);
            return TimeBetween(dateTime, beforeMidnightLimit, afterMidnightLimit);
        }

        private static bool TimeBetween(DateTimeOffset dateTime, TimeSpan start, TimeSpan end)
        {
            // convert date time to a TimeSpan
            var now = dateTime.TimeOfDay;
            // see if start comes before end
            if (start < end)
                return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
        }
    }
}
