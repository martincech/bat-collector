﻿using Communication.Contract.Wcf.DataContracts;
using Settings.Core;
using Settings.DataSourceSettings.Modbus;
using Settings.DataStorageSettings;
using Settings.DiagnosticSettings;
using Veit.Cognito.Service;

namespace Settings
{
    public static class DefaultSettings
    {

        public static WebApiRepositorySettings DefaultWebApiStorage(string batApi, string sensorsApi, string heartbeatUrl, string cachefileName)
        {
            return new WebApiRepositorySettings
            {
                Path = AppFolders.IntoSettings(cachefileName),
                BatApi = batApi,
                SensorsApi = sensorsApi,
                HeartbeatUrl = heartbeatUrl,
                CognitoSettings = DefaultCognitoSettings(),
                Email = string.Empty,
                IdToken = string.Empty,
                AccessToken = string.Empty,
                RefreshToken = string.Empty
            };
        }

        public static CognitoSettings DefaultCognitoSettings()
        {
            return new CognitoSettings
            {
#if !DEBUG
                Domain = "veit",
                Region = "eu-central-1",
                UserPoolId = "eu-central-1_B4838C0fV",
                ClientId = "36tb2hpt0gpvg651dp68bdf5m9",
                CallbackSite = "http://localhost:49720",
                CallbackPath = "/signin-cognito",
                Scopes = "openid veit.api/write"
#else
                Domain = "batid-dev",
                Region = "eu-central-1",
                UserPoolId = "eu-central-1_271lLNsx2",
                ClientId = "1ghp7fs5jt4mk8f25fo3ovsuvu",
                CallbackSite = "http://localhost:49720",
                CallbackPath = "/signin-cognito",
                Scopes = "openid veit.api/write"
#endif
            };
        }

        public static RepositorySettings DefaultLocalCsvStorage()
        {
            var settings = new LocalCsvRepositorySettings
            {
                Csv = { Path = AppFolders.IntoData("CsvStorage") }
            };
            //default configuration doesn't saved any samples to CSV file
            return settings;
        }

        public static ExternalWeighingInfo DefaultWeighingParametersCsv()
        {
            return null;
        }

        public static Diagnostics DefaultDiagnostics()
        {
            return new Diagnostics
            {
                UseDebugMode =
#if !DEBUG
         false
#else
         true
#endif
            };
        }

        public static DataSourceSettings.DataSourceSettings DefaultModbusSettings()
        {
            return new ModbusManagerSettings();
        }
    }
}
