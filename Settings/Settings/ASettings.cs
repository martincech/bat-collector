﻿using System.Runtime.Serialization;

namespace Settings
{
    [DataContract]
    public abstract class ASettings
    {
        protected ASettings(object type)
        {
            Type = type;
        }

        [DataMember(Order = 1)]
        public object Type { get; private set; }
    }
}
