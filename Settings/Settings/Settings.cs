﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Communication.Contract.Wcf.DataContracts;
using Settings.DataStorageSettings;
using Settings.DiagnosticSettings;

namespace Settings
{
    [DataContract]
    public class Settings
    {
        public Settings()
        {
            Storages = new List<RepositorySettings>();
        }

        public Guid TerminalId { get; set; }

        [DataMember]
        public IEnumerable<RepositorySettings> Storages { get; set; }
        [DataMember]
        public IEnumerable<DataSourceSettings.DataSourceSettings> Sources { get; set; }
        [DataMember]
        public ExternalWeighingInfo Weighing { get; set; }
        [DataMember]
        public Diagnostics Diagnostics { get; set; }
    }
}
