﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Communication.Contract.Wcf.DataContracts;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Settings.DeviceSettings.SourceSettings
{
    public static class ModbusConstansts
    {
        public const int MINIMUM_ADDRESS = 1;
        public const int MAXIMUM_ADDRESS = 247;
        public const int REPLY_DELAY_MINIMUM = 1000;
        public const int REPLY_DELAY_MAXIMUM = 10000;
        public const int REPLY_DELAY_DEFAULT = 2000;

        public const int SILENT_INTERVAL_MINIMUM = 50;
        public const int SILENT_INTERVAL_MAXIMUM = 500;
        public const int SILENT_INTERVAL_DEFAULT = SILENT_INTERVAL_MINIMUM;

        public const int REPETITIONS_MINIMUM = 1;
        public const int REPETITIONS_MAXIMUM = 9;
        public const int REPETITIONS_DEFAULT = 3;

        public const Parity PARITY_DEFAULT = Parity.Even;
        public const PacketMode PROTOCOL_DEFAULT = PacketMode.ASCII;

        public static readonly IEnumerable<uint> BAUD_RATES = new List<uint> { 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 };
        public static readonly uint DEFAULT_BAUD_RATE = ((List<uint>)BAUD_RATES)[3];
    }

    [DataContract]
    public class ModbusSettings
    {
        [DataMember]
        [JsonConverter(typeof(StringEnumConverter))]
        public Parity Parity { get; set; } = ModbusConstansts.PARITY_DEFAULT;

        [DataMember]
        [JsonConverter(typeof(StringEnumConverter))]
        public PacketMode Protocol { get; set; } = ModbusConstansts.PROTOCOL_DEFAULT;

        [DataMember]
        public int Repetitions { get; set; } = ModbusConstansts.REPETITIONS_DEFAULT;

        [DataMember]
        public int BaudRate { get; set; } = (int)ModbusConstansts.DEFAULT_BAUD_RATE;

        [DataMember]
        public int ReplyDelay { get; set; } = ModbusConstansts.REPLY_DELAY_DEFAULT;

        [DataMember]
        public int SilentInterval { get; set; } = ModbusConstansts.SILENT_INTERVAL_DEFAULT;
    }
}
