﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Settings.DeviceSettings.SourceSettings;

namespace Settings.DeviceSettings
{
    [DataContract]
    public class DeviceSettingsTree
    {
        [DataMember]
        public List<EthernetSourceSettings> EthernetSources
        {
            get { return ethernetSources ?? (ethernetSources = new List<EthernetSourceSettings>()); }
            set { ethernetSources = value; }
        }

        private List<EthernetSourceSettings> ethernetSources;
    }
}
