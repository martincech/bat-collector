﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NLog;

namespace Settings.DeviceSettings
{
    [DataContract]
    public abstract class DeviceSourceSettings<T> : DeviceSourceSettings
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        [DataMember]
        public T Settings { get; set; }

        public override void SetSettings(object settings)
        {
            if (settings == null) return;
            var value = default(T);
            try
            {
                value = (T)settings;
            }
            catch (Exception e)
            {
                logger.Warn(e, "Set settings exception");
            }
            Settings = value;
        }

        public override object GetSettings()
        {
            return Settings;
        }
    }

    [DataContract]
    public abstract class DeviceSourceSettings : DeviceSettings
    {
        /// <inheritdoc />
        protected DeviceSourceSettings()
        {
            Devices = new List<DeviceSettings>();
        }

        [DataMember]
        public string Description { get; set; }

        [DataMember(Order = 1)]
        public List<DeviceSettings> Devices
        {
            get { return devices ?? (devices = new List<DeviceSettings>()); }
            set { devices = value; }
        }

        public abstract void SetSettings(object settings);
        public abstract object GetSettings();

        private List<DeviceSettings> devices;
    }
}
