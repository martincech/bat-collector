﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common;

namespace Settings.DeviceSettings
{
    [DataContract]
    public class DeviceSettings
    {
        [DataMember]
        public string Uid { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int? LocalId { get; set; }

        [DataMember]
        public Guid? Guid { get; set; }

        [DataMember]
        public DateTimeOffset? LastSampleTimeStamp { get; set; }

        [DataMember]
        public DeviceType Type { get; set; }

        [DataMember]
        public bool IsDeleted { get; set; }
    }
}
