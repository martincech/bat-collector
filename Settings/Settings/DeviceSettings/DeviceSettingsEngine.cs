﻿using System.IO;
using Newtonsoft.Json;
using Settings.Core;
using Settings.Core.Repository;

namespace Settings.DeviceSettings
{
    public class DeviceSettingsEngine : ICoreEngine<DeviceSettingsTree>
    {
        private string SettingsPath { get { return AppFolders.IntoSettings(DEVICE_SETTINGS_FILE); } }
        public const string DEVICE_SETTINGS_FILE = "DeviceSettings.json";

        #region Implementation of ICoreEngine<DeviceSettingsTree>

        public void Save(DeviceSettingsTree settings)
        {
            File.WriteAllText(SettingsPath, JsonConvert.SerializeObject(settings));
        }

        public DeviceSettingsTree Load()
        {
            if (!File.Exists(SettingsPath)) return new DeviceSettingsTree();
            var text = File.ReadAllText(SettingsPath);
            return JsonConvert.DeserializeObject<DeviceSettingsTree>(text) ?? new DeviceSettingsTree();
        }

        #endregion
    }
}
