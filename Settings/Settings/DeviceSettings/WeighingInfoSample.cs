﻿using System;
using System.Runtime.Serialization;
using Veit.Bat.Common.Sample.Base;
using Veit.Bat.Common.Units;

namespace Settings.DeviceSettings
{
    [DataContract]
    public class WeighingInfoSample : TimeSample
    {
        /// <summary>
        /// Actual start of flock
        /// </summary>
        [DataMember]
        public DateTimeOffset Start { get; set; }

        [DataMember]
        public WeightUnits Unit { get; set; }
    }
}
