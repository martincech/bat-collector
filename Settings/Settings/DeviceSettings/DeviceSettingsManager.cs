﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using NLog;
using Settings.Core.Repository;
using Settings.DeviceSettings.SourceSettings;
using Settings.Extensions;

namespace Settings.DeviceSettings
{
    public class DeviceSettingsManager : IDeviceSettingsManager
    {
        #region Private fields

        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ICoreEngine<DeviceSettingsTree> storageEngine;
        private const string MUTEX_ID = "Global\\Veit_BatTerminal_SettingsMutex";
        private readonly MutexSecurity securitySettings;
        private const int MUTEX_TIMEOUT = 1000 * 60 * 2; //2min
        private const int INVALID_LOCAL_ID = -1;

        #endregion

        #region Constructor

        public DeviceSettingsManager(ICoreEngine<DeviceSettingsTree> storageEngine)
        {
            this.storageEngine = storageEngine;

            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null),
               MutexRights.FullControl, AccessControlType.Allow);
            securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);
        }

        #endregion


        public void Save(DeviceSettingsTree deviceTree)
        {
            EnsureExclusiveAccessForAction(() =>
            {
                if (deviceTree != null) storageEngine.Save(deviceTree);
                return true;
            });
        }

        #region Implementation of IDeviceSettingsManager

        public DeviceSettingsTree LoadDevices(bool excludeDeleted)
        {
            var deviceTree = EnsureExclusiveAccessForAction(() => storageEngine.Load() ?? new DeviceSettingsTree());
            if (excludeDeleted)
            {
                ExcludeRemovedDevices(deviceTree.EthernetSources);
            }
            return deviceTree;
        }

        private static void ExcludeRemovedDevices<T>(List<T> sources) where T : DeviceSourceSettings
        {
            for (var i = sources.Count - 1; i >= 0; i--)
            {
                if (sources[i].IsDeleted)
                {
                    sources.RemoveAt(i);
                    continue;
                }
                ExcludeRemovedDevices(sources[i].Devices);
            }
        }

        private static void ExcludeRemovedDevices(List<DeviceSettings> devices)
        {
            if (devices == null)
            {
                return;
            }

            for (var i = devices.Count - 1; i >= 0; i--)
            {
                if (devices[i].IsDeleted)
                {
                    devices.RemoveAt(i);
                }
            }
        }


        public bool AddOrUpdateDevice<T>(T device, DeviceSourceSettings parent = null)
           where T : DeviceSettings
        {
            return AddOrUpdateDevices(new Dictionary<T, DeviceSourceSettings> { { device, parent } });
        }

        public bool AddOrUpdateDevices<T>(Dictionary<T, DeviceSourceSettings> devices)
           where T : DeviceSettings
        {
            if (devices == null) return false;
            return EnsureExclusiveAccessForAction(() =>
            {
                var deviceTree = LoadDevices(false);
                foreach (var device in devices)
                {
                    if (!TryGetDeviceSources(device.Key, device.Value, deviceTree, out var deviceList, out var sourceType)) continue;

                    if (device.Value == null) AddOrUpdateDeviceSource(device.Key as DeviceSourceSettings, deviceList);
                    else AddOrUpdateDevice(device.Key, device.Value, deviceList);

                    SetDeviceSourcesByType(sourceType, deviceList, deviceTree);
                }
                Save(deviceTree);
                return true;
            });
        }

        private T EnsureExclusiveAccessForAction<T>(Func<T> action)
        {
            var result = default(T);
            using (var mutex = new Mutex(false, MUTEX_ID, out _, securitySettings))
            {
                var hasHandle = false;
                try
                {
                    try
                    {
                        hasHandle = mutex.WaitOne(MUTEX_TIMEOUT, false);
                        if (!hasHandle)
                        {
                            logger.Debug("Timeout waiting for exclusive access");
                            return result;
                        }
                    }
                    catch (AbandonedMutexException)
                    {
                        logger.Debug("Abandoned mutex");
                        // Log the fact that the mutex was abandoned in another process,
                        // it will still get acquired
                        hasHandle = true;
                    }
                    result = action.Invoke();
                }
                finally
                {
                    if (hasHandle)
                    {
                        mutex.ReleaseMutex();
                    }
                }
            }
            return result;
        }


        public bool UpdateDevice(string uid, string name, DateTimeOffset timeStamp, Guid? guid = null)
        {
            return EnsureExclusiveAccessForAction(() =>
            {
                var deviceTree = LoadDevices(false);
                var parentType = Device.Unknown;
                var dev = FindDevice(uid, deviceTree, ref parentType);
                if (dev == null) return false;
                if (name != null)
                {
                    dev.Name = name;
                }
                if (guid != null)
                {
                    dev.Guid = guid;
                }
                dev.LastSampleTimeStamp = ContainsSampleTimeStamp(parentType, timeStamp) ? timeStamp : timeStamp.SetCurrentTime();

                Save(deviceTree);
                return true;
            });
        }

        private static bool ContainsSampleTimeStamp(Device parentType, DateTimeOffset timeStamp)
        {
            if ((parentType == Device.Com || parentType == Device.Elo || parentType == Device.Ethernet) && !timeStamp.TimeIsMidnight())
            {  //Bat2 v1.53 sends sample with timespan, but from settings you cannot able to determine device version => check if time is almost midnight
                return true;
            }
            return false;
        }


        public bool DeleteDevice<T>(T device, DeviceSourceSettings parent = null)
           where T : DeviceSettings
        {
            return DeleteDevices(new Dictionary<T, DeviceSourceSettings> { { device, parent } });
        }

        public bool DeleteDevices<T>(Dictionary<T, DeviceSourceSettings> devices)
           where T : DeviceSettings
        {
            if (devices == null) return false;
            return EnsureExclusiveAccessForAction(() =>
            {
                var deviceTree = LoadDevices(false);
                foreach (var device in devices)
                {
                    if (!TryGetDeviceSources(device.Key, device.Value, deviceTree, out var deviceList, out var sourceType)) continue;

                    DeleteDevice(device.Key, device.Value, deviceList);

                    SetDeviceSourcesByType(sourceType, deviceList, deviceTree);
                }
                Save(deviceTree);
                return true;
            });
        }

        #endregion

        #region Private methods

        private static IEnumerable<DeviceSourceSettings> GetDeviceSourcesByType(Type type, DeviceSettingsTree deviceTree)
        {
            var emptyList = new List<DeviceSourceSettings>();
            if (deviceTree == null) return emptyList;
            if (type == typeof(EthernetSourceSettings)) return deviceTree.EthernetSources ?? new List<EthernetSourceSettings>();
            return emptyList;
        }

        private static void SetDeviceSourcesByType<T>(Type type, List<T> deviceSources, DeviceSettingsTree deviceTree)
        {
            if (deviceTree == null || deviceSources == null) return;
            if (type == typeof(EthernetSourceSettings)) deviceTree.EthernetSources = deviceSources.Cast<EthernetSourceSettings>().ToList();
        }

        private static bool TryGetDeviceSources(DeviceSettings device, DeviceSourceSettings deviceSource,
         DeviceSettingsTree deviceTree, out List<DeviceSourceSettings> deviceList, out Type sourceType)
        {
            deviceList = null;
            sourceType = null;
            if (device == null) return false;
            sourceType = deviceSource == null ? device.GetType() : deviceSource.GetType();
            var sourceDevices = GetDeviceSourcesByType(sourceType, deviceTree);
            if (sourceDevices == null) return false;
            deviceList = sourceDevices.ToList();
            return true;
        }

        private static void AddOrUpdateDevice(DeviceSettings device, DeviceSourceSettings deviceSource, List<DeviceSourceSettings> deviceSources)
        {
            if (device == null || deviceSource == null) return;
            var deviceSourceOriginal = deviceSources.FirstOrDefault(f => f.Uid == deviceSource.Uid);
            if (deviceSourceOriginal == null)
            {
                deviceSource.Devices = new List<DeviceSettings> { device };
                deviceSources.Add(deviceSource);
            }
            else
            {
                var deviceOriginal = deviceSourceOriginal.Devices.FirstOrDefault(f => f.Uid == device.Uid);
                if (deviceOriginal == null)
                {
                    deviceSourceOriginal.Devices.Add(device);
                }
                else
                {
                    deviceOriginal.Name = device.Name;
                    deviceOriginal.LocalId = device.LocalId;
                    deviceOriginal.LastSampleTimeStamp = device.LastSampleTimeStamp;
                    deviceOriginal.Type = device.Type;
                    deviceOriginal.IsDeleted = false;
                }
            }
        }

        private static void AddOrUpdateDeviceSource(DeviceSourceSettings deviceSource, List<DeviceSourceSettings> deviceSources)
        {
            if (deviceSource == null) return;
            var deviceSourceOriginal = deviceSources.FirstOrDefault(f => f.Uid == deviceSource.Uid);
            if (deviceSourceOriginal == null)
            {
                deviceSources.Add(deviceSource);
            }
            else
            {
                deviceSourceOriginal.Description = deviceSource.Description;
                deviceSourceOriginal.Name = deviceSource.Name;
                deviceSourceOriginal.SetSettings(deviceSource.GetSettings());
                deviceSourceOriginal.IsDeleted = false;
            }
        }

        private static void DeleteDevice(DeviceSettings device, DeviceSourceSettings deviceSource, List<DeviceSourceSettings> deviceSources)
        {
            var sourceDevice = deviceSources.FirstOrDefault(f => f.Uid == (deviceSource == null ? device.Uid : deviceSource.Uid));
            if (sourceDevice == null) return;

            if (deviceSource == null)
            {
                if (!(device is DeviceSourceSettings)) return;
                DeleteSourceDevice(sourceDevice);
            }
            else
            {
                if (sourceDevice.Devices == null) return;
                var originalDevice = sourceDevice.Devices.FirstOrDefault(f => f.Uid == device.Uid);
                if (originalDevice == null) return;

                originalDevice.IsDeleted = true;
                originalDevice.LastSampleTimeStamp = null;
            }
        }

        private static void DeleteSourceDevice(DeviceSourceSettings sourceDevice)
        {
            sourceDevice.IsDeleted = true;
            sourceDevice.LastSampleTimeStamp = null;
            foreach (var dev in sourceDevice.Devices)
            {
                dev.IsDeleted = true;
                dev.LastSampleTimeStamp = null;
            }
        }

        private static DeviceSettings FindDevice(string uid, DeviceSettingsTree deviceTree, ref Device parentType)
        {
            return FindDevice(uid, deviceTree.EthernetSources, ref parentType);
        }

        private static DeviceSettings FindDevice(string uid, IEnumerable<DeviceSourceSettings> sources, ref Device parentType)
        {
            DeviceSettings dev = null;
            foreach (var source in sources)
            {
                dev = source.Devices.FirstOrDefault(f => f.Uid.Equals(uid));
                if (dev != null)
                {
                    parentType = MapSourceType(source);
                    break;
                }
            }
            return dev;
        }

        private static Device MapSourceType(object source)
        {
            if (source is EthernetSourceSettings)
            {
                return Device.Ethernet;
            }
            return Device.Unknown;
        }

        public int GetDeviceRelativeId(string uid)
        {
            return EnsureExclusiveAccessForAction(() =>
            {
                var deviceTree = LoadDevices(false);
                var parentType = Device.Unknown;
                var dev = FindDevice(uid, deviceTree, ref parentType);
                if (dev == null)
                {
                    throw new InvalidOperationException($"Device not found: {uid}");
                }
                if (dev.LocalId.HasValue)
                {
                    return dev.LocalId.Value;
                }

                var lastId = GetLastUsedLocalId(deviceTree);
                dev.LocalId = lastId + 1;
                Save(deviceTree);
                return dev.LocalId.Value;
            });
        }

        /// <summary>
        /// Search in all devices for last used local ID a return it.
        /// If no local Id has been used yet, return -1.
        /// </summary>
        /// <param name="devicetree"></param>
        /// <returns></returns>
        private static int GetLastUsedLocalId(DeviceSettingsTree devicetree)
        {
            var devices = new List<DeviceSettings>();
            devices.AddRange(GetAllSourceDevices(devicetree.EthernetSources));
            return GetLastLocalId(devices);
        }


        private static IEnumerable<DeviceSettings> GetAllSourceDevices(IEnumerable<DeviceSourceSettings> sources)
        {
            var devs = new List<DeviceSettings>();
            foreach (var source in sources)
            {
                devs.AddRange(source.Devices);
            }
            return devs;
        }


        private static int GetLastLocalId(IEnumerable<DeviceSettings> devices)
        {
            var id = INVALID_LOCAL_ID;
            foreach (var device in devices)
            {
                if (!device.LocalId.HasValue)
                {
                    continue;
                }
                if (device.LocalId.Value > id)
                {
                    id = device.LocalId.Value;
                }
            }
            return id;
        }


        private enum Device
        {
            Unknown,
            Com,
            Elo,
            [Obsolete("Modem is replace by SMS gateway")]
            Modem,
            Ethernet
        }

        #endregion
    }
}
