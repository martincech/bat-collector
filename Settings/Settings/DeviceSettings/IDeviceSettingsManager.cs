﻿using System;
using System.Collections.Generic;

namespace Settings.DeviceSettings
{
    public interface IDeviceSettingsManager
    {
        DeviceSettingsTree LoadDevices(bool excludeDeleted);

        bool AddOrUpdateDevice<T>(T device, DeviceSourceSettings parent = null)
           where T : DeviceSettings;

        bool AddOrUpdateDevices<T>(Dictionary<T, DeviceSourceSettings> devices)
           where T : DeviceSettings;

        bool UpdateDevice(string uid, string name, DateTimeOffset timeStamp, Guid? guid = null);

        bool DeleteDevice<T>(T device, DeviceSourceSettings parent = null)
           where T : DeviceSettings;

        bool DeleteDevices<T>(Dictionary<T, DeviceSourceSettings> devices)
           where T : DeviceSettings;

        /// <summary>
        /// Returns device ID relative to collector.
        /// The first device get 0, the second 1 etc.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        int GetDeviceRelativeId(string uid);


    }
}
