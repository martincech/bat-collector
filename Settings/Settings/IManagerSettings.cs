﻿using System.Collections.Generic;
using Communication.Contract.Wcf.DataContracts;
using Heartbeat;
using Settings.Core;
using Settings.DeviceSettings;

namespace Settings
{
    public interface IManagerSettings : IModbusSettings, IDeviceSettingsManager, IDiagnosticSettingsManager
    {
        bool SaveConnection(WebApiConnectionInfo connectionInfo);
        List<WebApiConnectionInfo> LoadConnections();
        bool UpdateEndpoints(List<Endpoint> endpoints);
        string HeartbeatUrl { get; }
        SystemInfo SystemInfo { get; }
    }
}
