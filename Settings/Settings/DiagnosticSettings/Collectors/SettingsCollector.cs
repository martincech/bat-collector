﻿using System.IO;
using Settings.Core;

namespace Settings.DiagnosticSettings.Collectors
{
    public class SettingsCollector : BaseCollector
    {
        private const string OUTPUT_SETTINGS_FOLDER_NAME = nameof(Settings);
        private const string SETTINGS_FILE_SEARCH_PATTERN = "*.json";

        public SettingsCollector()
        {
            InputPath = AppFolders.Settings;
            OutputPath = Path.Combine(OutputFolder, OUTPUT_SETTINGS_FOLDER_NAME);
        }

        #region Overrides of BaseCollector

        /// <summary>
        /// Collect diagnostics files to selected folder
        /// </summary>
        /// <param name="outputDir"></param>
        public override void Collect(string outputDir)
        {
            CopyFiles(InputPath, outputDir, SETTINGS_FILE_SEARCH_PATTERN);
        }

        #endregion
    }
}
