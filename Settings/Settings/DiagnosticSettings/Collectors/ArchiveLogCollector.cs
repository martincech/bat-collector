﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Settings.Core.Logger;

namespace Settings.DiagnosticSettings.Collectors
{
    public class ArchiveLogCollector : BaseCollector
    {
        private readonly CollectingArchiveLogMode collectingArchiveLogMode;

        public ArchiveLogCollector(CollectingArchiveLogMode collectingArchiveLogMode)
        {
            this.collectingArchiveLogMode = collectingArchiveLogMode;
            InputPath = BaseLoggerConfiguration.ArchiveFolderFullPath;
            OutputPath = Path.Combine(OutputFolder, BaseLoggerConfiguration.BaseFolder, BaseLoggerConfiguration.ArchiveFolderName);
        }

        #region Overrides of BaseCollector

        /// <summary>
        /// Collect diagnostics files to selected folder
        /// </summary>
        /// <param name="outputDir"></param>
        public override void Collect(string outputDir)
        {
            switch (collectingArchiveLogMode)
            {
                case CollectingArchiveLogMode.All:
                    CollectAllArchive(outputDir);
                    break;
                case CollectingArchiveLogMode.OnlyLastDay:
                    CollectLastDayArchive(outputDir);
                    break;
                default:
                    throw new NotSupportedException($"Unsupported collecting mode: {collectingArchiveLogMode}");
            }
        }

        private void CollectAllArchive(string output)
        {
            CopyFiles(InputPath, output);
        }

        private void CollectLastDayArchive(string output)
        {
            var allArchives = GetFileList(InputPath);
            var archives = GetArchiveFilesInfo(allArchives);

            foreach (var arch in archives)
            {
                //filter only last day of archived files
                var lastArchive = arch.Value.OrderBy(g => g.TimeStamp).Last();
                CopyFile(lastArchive.FullName, output);
            }
        }

        private static Dictionary<string, List<ArchiveLogInfo>> GetArchiveFilesInfo(IEnumerable<string> allArchives)
        {
            var archives = new Dictionary<string, List<ArchiveLogInfo>>();

            foreach (var item in allArchives)
            {
                var rootName = "";
                var archiveInfo = GetArchiveFileInfo(item, ref rootName);
                if (archiveInfo == null)
                {
                    continue;
                }

                //save archive
                if (!archives.ContainsKey(rootName))
                {
                    archives.Add(rootName, new List<ArchiveLogInfo>());
                }
                archives[rootName].Add(archiveInfo);
            }
            return archives;
        }

        private static ArchiveLogInfo GetArchiveFileInfo(string fileFullName, ref string rootName)
        {
            var fileName = Path.GetFileName(fileFullName);
            if (fileName == null)
            {
                return null;
            }

            var timeStamp = DateTimeOffset.MinValue;
            if (!ParseFileInfoFromName(fileName, ref timeStamp, ref rootName))
            {
                return null;
            }

            return new ArchiveLogInfo { TimeStamp = timeStamp, FullName = fileFullName };
        }

        private static bool ParseFileInfoFromName(string value, ref DateTimeOffset timeStamp, ref string rootName)
        {
            if (!TryParseDateTimeFromName(value, ref timeStamp, out var timeStampEndIndex))
            {
                return false;
            }

            return TryParseRootNameFromName(value, ref rootName, timeStampEndIndex + 1);
        }

        private static bool TryParseDateTimeFromName(string value, ref DateTimeOffset timeStamp, out int timeStampEndIndex)
        {
            timeStampEndIndex = value.LastIndexOf('-');
            if (timeStampEndIndex == -1)
            {
                return false;
            }

            var timeStampString = value.Substring(0, timeStampEndIndex);
            return DateTimeOffset.TryParse(timeStampString, out timeStamp);
        }

        private static bool TryParseRootNameFromName(string value, ref string rootName, int startIndexForSearch)
        {
            var rootNameEndIndex = value.IndexOf('.');
            if (rootNameEndIndex == -1)
            {
                return false;
            }
            rootName = value.Substring(startIndexForSearch, rootNameEndIndex - startIndexForSearch);
            return !string.IsNullOrEmpty(rootName);
        }


        private class ArchiveLogInfo
        {
            public DateTimeOffset TimeStamp { get; set; }
            public string FullName { get; set; }
        }

        #endregion
    }
}
