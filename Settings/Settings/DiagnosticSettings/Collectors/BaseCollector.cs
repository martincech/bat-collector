﻿using System;
using System.Collections.Generic;
using System.IO;
using NLog;
using Settings.Core;

namespace Settings.DiagnosticSettings.Collectors
{
    public abstract class BaseCollector : IDiagnosticCollector
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private const string DEFAULT_OUTPUT_FOLDER = "tmp";
        public static string OutputFolder
        {
            get { return Path.Combine(AppFolders.Settings, DEFAULT_OUTPUT_FOLDER); }
        }


        protected static void CopyFiles(string sourceFolder, string destinationFolder, string searchFilePattern = "")
        {
            var files = GetFileList(sourceFolder, searchFilePattern);
            foreach (var file in files)
            {
                CopyFile(file, destinationFolder);
            }
        }

        protected static void CopyFile(string file, string destinationFolder)
        {
            try
            {
                Directory.CreateDirectory(destinationFolder);
                var name = Path.GetFileName(file);
                if (name == null)
                {
                    return;
                }
                File.Copy(file, Path.Combine(destinationFolder, name), true);
            }
            catch (Exception e)
            {
                logger.Warn(e, "CopyFile exception");
            }
        }

        protected static IEnumerable<string> GetFileList(string folder, string searchFilePattern = "")
        {
            if (!Directory.Exists(folder)) return new List<string>();
            return string.IsNullOrEmpty(searchFilePattern) ? Directory.GetFiles(folder) : Directory.GetFiles(folder, searchFilePattern);
        }


        #region Implementation of IDiagnosticCollector

        public string InputPath { get; set; }
        public string OutputPath { get; set; }

        /// <summary>
        /// Collect diagnostics files to selected folder
        /// </summary>
        /// <param name="outputDir"></param>
        public abstract void Collect(string outputDir);

        #endregion
    }
}
