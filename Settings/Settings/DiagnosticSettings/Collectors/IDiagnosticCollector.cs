﻿namespace Settings.DiagnosticSettings.Collectors
{
    public interface IDiagnosticCollector
    {
        string InputPath { get; set; }
        string OutputPath { get; set; }

        /// <summary>
        /// Collect diagnostics files to selected folder
        /// </summary>
        void Collect(string outputDir);
    }
}
