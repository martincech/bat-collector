﻿using System.IO;
using Settings.Core.Logger;

namespace Settings.DiagnosticSettings.Collectors
{
    public class LogCollector : BaseCollector
    {
        public LogCollector()
        {
            InputPath = BaseLoggerConfiguration.RootFolder;
            OutputPath = Path.Combine(OutputFolder, BaseLoggerConfiguration.BaseFolder);
        }

        #region Overrides of BaseCollector

        /// <summary>
        /// Collect diagnostics files to selected folder
        /// </summary>
        /// <param name="outputDir"></param>
        public override void Collect(string outputDir)
        {
            CopyFiles(InputPath, outputDir);
        }

        #endregion
    }
}
