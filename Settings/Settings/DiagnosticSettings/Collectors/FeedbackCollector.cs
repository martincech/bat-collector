﻿using System;
using System.IO;
using NLog;

namespace Settings.DiagnosticSettings.Collectors
{
    public class FeedbackCollector : BaseCollector
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private const string FILE_NAME = "feedback.txt";

        public FeedbackCollector()
        {
            OutputPath = OutputFolder;
        }

        public string Message { get; set; }

        #region Overrides of BaseCollector

        /// <summary>
        /// Collect diagnostics files to selected folder
        /// </summary>
        /// <param name="outputDir"></param>
        public override void Collect(string outputDir)
        {
            try
            {
                var fileName = Path.Combine(outputDir, FILE_NAME);
                Clear(fileName);
                if (Message.Length == 0)
                {
                    return;
                }

                Directory.CreateDirectory(outputDir);
                File.WriteAllText(fileName, Message);
            }
            catch (Exception e)
            {
                logger.Warn(e, "Collect feedback exception");
            }
        }

        private static void Clear(string name)
        {
            if (File.Exists(name))
            {
                File.Delete(name);
            }
        }

        #endregion
    }
}
