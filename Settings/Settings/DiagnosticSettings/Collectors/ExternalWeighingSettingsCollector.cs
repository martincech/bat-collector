﻿using System;
using System.Collections.Generic;
using System.IO;
using NLog;

namespace Settings.DiagnosticSettings.Collectors
{
    public class ExternalWeighingSettingsCollector : BaseCollector
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly Settings settings;
        private const string OUTPUT_EXTERNAL_WEIGHING_SETTINGS_FOLDER = "ExternalWeighingSettings";

        public ExternalWeighingSettingsCollector(Settings settings)
        {
            this.settings = settings ?? throw new ArgumentNullException(nameof(settings));
            OutputPath = Path.Combine(OutputFolder, OUTPUT_EXTERNAL_WEIGHING_SETTINGS_FOLDER);
        }

        #region Overrides of BaseCollector

        /// <summary>
        /// Collect diagnostics files to selected folder
        /// </summary>
        /// <param name="outputDir"></param>
        public override void Collect(string outputDir)
        {
            try
            {
                if (settings.Weighing == null)
                {
                    return;
                }
                CopyExternalWeighingFiles(new List<string>
            {
               settings.Weighing.CurveFileParams,
               settings.Weighing.WeighingConfigurationFileParams,
               settings.Weighing.WeighingParametersFileParams
            }, outputDir);
            }
            catch (Exception e)
            {
                logger.Warn(e, "Collect external settings exception");
            }
        }

        private void CopyExternalWeighingFiles(IEnumerable<string> names, string output)
        {
            try
            {
                InputPath = settings.Weighing.Csv.Path;
                foreach (var name in names)
                {
                    CopyFile(Path.Combine(InputPath, name), output);
                }
            }
            catch (Exception e)
            {
                logger.Warn(e, "Copy external weighing files exception");
            }
        }

        #endregion

    }
}
