﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Settings.Core;
using Settings.DiagnosticSettings.Collectors;

namespace Settings.DiagnosticSettings
{
    public class Collector : ICollectorSettings
    {
        #region Private fields

        private const string ARCHIVE_FILE_NAME_PREFIX = nameof(Diagnostics);
        private const string ARCHIVE_FILE_NAME_SUFFIX = ".zip";
        private readonly Settings applicationSettings;

        #endregion

        #region Public interfaces

        #region Constructor

        public Collector(Settings applicationSettings)
        {
            this.applicationSettings = applicationSettings ?? throw new ArgumentNullException(nameof(applicationSettings));
            InitDefaultSettings();
        }

        #endregion

        /// <summary>
        /// Collect all files depending on Flag settings into
        /// inner collection: filesForArchive
        /// </summary>
        public bool CollectDiagnosticData()
        {
            try
            {
                Flush();

                var collectors = InitCollectors();
                foreach (var collector in collectors)
                {
                    collector.Collect(collector.OutputPath);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete all temporary files.
        /// </summary>
        public void Flush()
        {
            if (!Directory.Exists(OutputFolder))
            {
                return;
            }
            Directory.Delete(OutputFolder, true);
        }


        public static bool Archive(string outputFileName)
        {
            return Archive(BaseCollector.OutputFolder, outputFileName);
        }

        /// <summary>
        /// Pack all collected files into zip file.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="outputFileName"></param>
        public static bool Archive(string directory, string outputFileName)
        {
            try
            {
                if (!Directory.Exists(directory))
                {
                    return false;
                }
                if (File.Exists(outputFileName))
                {
                    File.Delete(outputFileName);
                }

                ZipFile.CreateFromDirectory(directory, outputFileName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Private helpers

        private IEnumerable<IDiagnosticCollector> InitCollectors()
        {
            var collectors = new List<IDiagnosticCollector>
             {
                new FeedbackCollector { Message = FeedbackMessage}
             };

            if (UseLogs)
            {
                collectors.Add(new LogCollector());
                collectors.Add(new ArchiveLogCollector(CollectingArchiveLogMode));
            }
            if (UseApplicationSettings)
            {
                collectors.Add(new SettingsCollector());
            }
            if (UseExternalWeighingSettings)
            {
                collectors.Add(new ExternalWeighingSettingsCollector(applicationSettings));
            }

            return collectors;
        }

        private void InitDefaultSettings()
        {
            OutputArchiveName = Path.Combine(AppFolders.Settings, CreateArchiveName(applicationSettings.TerminalId));
            CollectingArchiveLogMode = CollectingArchiveLogMode.All;
            OutputFolder = BaseCollector.OutputFolder;
            UseLogs = true;
            UseApplicationSettings = true;
            UseExternalWeighingSettings = true;
        }


        private static string CreateArchiveName(Guid terminalId)
        {
            return $"{ARCHIVE_FILE_NAME_PREFIX}-{terminalId}{ARCHIVE_FILE_NAME_SUFFIX}";
        }

        #endregion

        #region Implementation of ICollectorSettings

        public string OutputFolder { get; set; }
        public string OutputArchiveName { get; set; }
        public CollectingArchiveLogMode CollectingArchiveLogMode { get; set; }
        public bool UseLogs { get; set; }
        public bool UseApplicationSettings { get; set; }
        public bool UseExternalWeighingSettings { get; set; }
        public string FeedbackMessage { get; set; }

        #endregion
    }
}
