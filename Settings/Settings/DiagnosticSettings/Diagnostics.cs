﻿using System.Runtime.Serialization;

namespace Settings.DiagnosticSettings
{
    [DataContract]
    public class Diagnostics
    {
        [DataMember]
        public bool UseDebugMode { get; set; }
    }
}
