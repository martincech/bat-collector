﻿namespace Settings.DiagnosticSettings
{
    public interface ICollectorSettings
    {
        /// <summary>
        /// Full name of archive zip file.
        /// </summary>
        string OutputArchiveName { get; set; }
        CollectingArchiveLogMode CollectingArchiveLogMode { get; set; }

        bool UseLogs { get; set; }
        bool UseApplicationSettings { get; set; }
        bool UseExternalWeighingSettings { get; set; }

        string FeedbackMessage { get; set; }
    }
}
