﻿namespace Settings.DiagnosticSettings
{
    public enum CollectingArchiveLogMode
    {
        None,
        OnlyLastDay,
        All
    }
}
