﻿using System.Runtime.Serialization;
using System.Security.Policy;
using Communication.Contract.Wcf.DataContracts;
using Settings.StorageEngine;
using Veit.Cognito.Service;

namespace Settings.DataStorageSettings
{
    [DataContract]
    public class WebApiRepositorySettings : RepositorySettings
    {
        public WebApiRepositorySettings()
           : base(StorageType.WebApi)
        {
        }

        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public string BatApi { get; set; }
        [DataMember]
        public string SensorsApi { get; set; }
        [DataMember]
        public string HeartbeatUrl { get; set; }

        [DataMember]
        public CognitoSettings CognitoSettings { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string IdToken { get; set; }
        [DataMember]
        public string AccessToken { get; set; }
        [DataMember]
        public string RefreshToken { get; set; }

        [IgnoreDataMember]
        public WebApiConnectionInfo ConnectionInfo
        {
            get
            {
                return new WebApiConnectionInfo
                {
                    Logged = LoggedStatus.Unknown,
                    Login = new Login
                    {
                        UserName = Email,
                        IdToken = IdToken,
                        AccessToken = AccessToken,
                        RefreshToken = RefreshToken,
                        CognitoSettings = CognitoSettings
                    },
                    BatApi = string.IsNullOrEmpty(BatApi) ? null : new Url(BatApi),
                    SensorsApi = string.IsNullOrEmpty(SensorsApi) ? null : new Url(SensorsApi)
                };
            }
        }
    }
}
