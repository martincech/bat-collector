﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Communication.Contract.Wcf.DataContracts;
using Settings.StorageEngine;
using Veit.Bat.Common;

namespace Settings.DataStorageSettings
{
    [DataContract]
    public class LocalCsvRepositorySettings : RepositorySettings
    {
        public LocalCsvRepositorySettings() : base(StorageType.LocalCsv)
        {
            Csv = new Csv();
            SensorsWithItsFileNames = new Dictionary<DataType, string>();
        }
        [DataMember]
        public Csv Csv { get; private set; }
        [DataMember]
        public Dictionary<DataType, string> SensorsWithItsFileNames { get; private set; }
    }
}
