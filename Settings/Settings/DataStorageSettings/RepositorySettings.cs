﻿using System.Runtime.Serialization;
using Settings.StorageEngine;

namespace Settings.DataStorageSettings
{
    [DataContract]
    public abstract class RepositorySettings : ASettings
    {
        protected RepositorySettings(StorageType type) : base(type)
        {
        }
    }
}
