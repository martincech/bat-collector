﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Settings.DeviceSettings.SourceSettings;
using Settings.StorageEngine;

namespace Settings.DataSourceSettings.Modbus
{
    [DataContract]
    public class ModbusManagerSettings : DataSourceSettings
    {
        [DataMember]
        public ModbusSettings DefaultConnectivitySettings { get; set; }

        [DataMember]
        public Dictionary<string, string> UidMapping { get; private set; }

        public ModbusManagerSettings() : base(SourcesType.ModbusSource)
        {
            DefaultConnectivitySettings = new ModbusSettings();
            UidMapping = new Dictionary<string, string>();
        }
    }
}
