﻿using System.Runtime.Serialization;
using Settings.StorageEngine;

namespace Settings.DataSourceSettings
{
    [DataContract]
    public abstract class DataSourceSettings : ASettings
    {
        protected DataSourceSettings(SourcesType type) : base(type)
        {
        }
    }
}
