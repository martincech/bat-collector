﻿using Settings.DeviceSettings.SourceSettings;

namespace Settings
{
    public interface IModbusSettings
    {
        bool SaveModbusSettings(ModbusSettings settings);
        ModbusSettings LoadModbusSettings();
    }
}
