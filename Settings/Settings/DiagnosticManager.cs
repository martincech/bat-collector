﻿using System;
using System.IO;
using NLog;
using Settings.Core;
using Settings.Core.Logger;
using Settings.DiagnosticSettings;
using Veit.Diagnostics.AWS;

namespace Settings
{
    public class DiagnosticManager : IDiagnostic
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();
        private readonly BaseLoggerConfiguration loggerConfiguration;
        private readonly Collector collector;

        public DiagnosticManager(BaseLoggerConfiguration loggerConfiguration,
                                 Collector collector)
        {
            this.collector = collector ?? throw new ArgumentNullException(nameof(collector));
            this.loggerConfiguration = loggerConfiguration;
        }


        public void SendFeedback(string feedbackFileName, string message, bool includeDiagnosticFiles)
        {
            if (!Savediagnostics(collector.OutputArchiveName, message, includeDiagnosticFiles))
            {
                logger.Warn("Send feedback: collect diagnostic failed");
                return;
            }

            try
            {
                S3.Put(collector.OutputArchiveName, feedbackFileName);
                File.Delete(collector.OutputArchiveName);
            }
            catch (Exception e)
            {
                logger.Warn(e, "Send diagnostic error");
            }
        }

        public void SetDebugMode(bool enable)
        {
            loggerConfiguration?.SetDebugMode(enable);
        }

        private bool Savediagnostics(string outputFullName, string feedback, bool includediagnostics)
        {
            collector.OutputArchiveName = outputFullName;
            collector.FeedbackMessage = feedback;

            InitDiagnosticCollector(includediagnostics);
            collector.CollectDiagnosticData();
            return Collector.Archive(outputFullName);
        }

        private void InitDiagnosticCollector(bool flag)
        {
            collector.UseApplicationSettings = flag;
            collector.UseExternalWeighingSettings = flag;
            collector.UseLogs = flag;
        }

    }
}
