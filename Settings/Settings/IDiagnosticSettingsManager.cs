﻿using Settings.DiagnosticSettings;

namespace Settings
{
    public interface IDiagnosticSettingsManager
    {
        bool SaveDiagnostic(Diagnostics diagnostic);
        Diagnostics LoadDiagnostics();
    }
}
