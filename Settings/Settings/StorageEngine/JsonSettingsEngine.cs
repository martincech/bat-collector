﻿using System;
using System.Collections.Generic;
using Communication.Contract.Wcf.DataContracts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Settings.Core.Repository;
using Settings.DataStorageSettings;
using Settings.DiagnosticSettings;

namespace Settings.StorageEngine
{
    public class JsonSettingsEngine : JsonCoreEngine<Settings>, IEngine
    {
        #region Private fields

        private const string NAME = "Settings.json";

        #endregion

        #region Constructor

        public JsonSettingsEngine()
           : base(NAME)
        {
        }

        #endregion


        #region Overrides of JsonCoreEngine<Settings>

        public override Settings Load()
        {
            return base.Load(new SettingsConverter());
        }

        #endregion

        #region Custom json converter for deserialization

        private class SettingsConverter : JsonConverter
        {
            #region Overrides of JsonConverter

            /// <summary>
            ///    Writes the JSON representation of the object.
            /// </summary>
            /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
            /// <param name="value">The value.</param>
            /// <param name="serializer">The calling serializer.</param>
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            /// <summary>
            ///    Reads the JSON representation of the object.
            /// </summary>
            /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
            /// <param name="objectType">Type of the object.</param>
            /// <param name="existingValue">The existing value of object being read.</param>
            /// <param name="serializer">The calling serializer.</param>
            /// <returns>
            ///    The object value.
            /// </returns>
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
               JsonSerializer serializer)
            {
                var obj = JObject.Load(reader);
                return DeserializeSettings(obj);
            }


            private static Settings DeserializeSettings(JObject obj)
            {
                return new Settings
                {
                    Storages = DeserializeStorages(obj),
                    Sources = DeserializeSources(obj),
                    Weighing = DeserializeWeighing(obj),
                    Diagnostics = DeserializeDiagnostics(obj)
                };
            }

            private static ExternalWeighingInfo DeserializeWeighing(JObject obj)
            {
                var weighingObj = obj["Weighing"];
                return weighingObj?.ToObject<ExternalWeighingInfo>();
            }

            private static Diagnostics DeserializeDiagnostics(JObject obj)
            {
                var diagObj = obj[nameof(Diagnostics)];
                return diagObj?.ToObject<Diagnostics>();
            }


            private static IEnumerable<T> DeserializeStorageType<T>(JObject jObject, string propName, Func<JProperty, Type> mapToTypeFunction)
               where T : ASettings
            {
                var list = new List<T>();
                foreach (var storage in jObject[propName].Children<JObject>())
                {
                    var prop = storage.Property(nameof(Type));
                    var classType = mapToTypeFunction?.Invoke(prop);
                    if (classType == null) continue;

                    var jsonInObject = storage.ToObject(classType);
                    dynamic concreteTypedStorage = Convert.ChangeType(jsonInObject, classType);
                    list.Add(concreteTypedStorage);
                }
                return list;
            }

            private static IEnumerable<RepositorySettings> DeserializeStorages(JObject jObject)
            {
                return DeserializeStorageType<RepositorySettings>(jObject, "Storages", prop =>
                {
                    var storageType = prop.Value.ToObject<StorageType>();
                    return storageType.ConvertToType();
                });
            }

            private static IEnumerable<DataSourceSettings.DataSourceSettings> DeserializeSources(JObject jObject)
            {
                return DeserializeStorageType<DataSourceSettings.DataSourceSettings>(jObject, "Sources", prop =>
                {
                    var storageType = prop.Value.ToObject<SourcesType>();
                    return storageType.ConvertToType();
                });
            }


            /// <summary>
            ///    Determines whether this instance can convert the specified object type.
            /// </summary>
            /// <param name="objectType">Type of the object.</param>
            /// <returns>
            ///    <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
            /// </returns>
            public override bool CanConvert(Type objectType)
            {
                return objectType == typeof(Settings);
            }

            #endregion
        }

        #endregion
    }
}
