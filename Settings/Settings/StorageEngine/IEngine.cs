﻿using Settings.Core.Repository;

namespace Settings.StorageEngine
{
    public interface IEngine : ICoreEngine<Settings>
    {
    }
}
