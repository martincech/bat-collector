﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Settings.DataSourceSettings.Modbus;
using Settings.DataStorageSettings;

namespace Settings.StorageEngine
{
    [DataContract]
    public enum StorageType
    {
        //repositories
        [EnumMember]
        [Obsolete("Not in use anymore", true)]
        Kafka,
        [EnumMember]
        WebApi,
        [EnumMember]
        [Obsolete("Not in use anymore", true)]
        LocalDb,
        [EnumMember]
        LocalCsv
    }

    [DataContract]
    public enum SourcesType
    {
        [EnumMember]
        [Obsolete("Not in use anymore", true)]
        ExternalCsv,
        [EnumMember]
        ModbusSource,
        [EnumMember]
        [Obsolete("Not in use anymore", true)]
        ModemSource
    }

    public static class StorageTypeExtensions
    {
        public static Type ConvertToType(this StorageType storage)
        {
            if (TypeMapper.ContainsKey(storage))
            {
                return TypeMapper[storage];
            }
            return null;
        }

        public static Type ConvertToType(this SourcesType source)
        {
            if (TypeMapper.ContainsKey(source))
            {
                return TypeMapper[source];
            }
            return null;
        }
        private static readonly Dictionary<object, Type> TypeMapper = new Dictionary<object, Type>
      {
         { StorageType.WebApi, typeof(WebApiRepositorySettings)},
         { StorageType.LocalCsv, typeof(LocalCsvRepositorySettings)},
         { SourcesType.ModbusSource, typeof(ModbusManagerSettings)},
      };
    }
}
