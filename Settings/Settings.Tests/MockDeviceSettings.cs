﻿using System.Collections.Generic;
using Communication.Contract.Wcf.DataContracts;
using Settings.DeviceSettings;
using Settings.DeviceSettings.SourceSettings;

namespace Settings.Tests
{
    public class MockDeviceSettings
    {
        public static DeviceSettings.DeviceSettings Device1 { get { return CreateDevice(nameof(Device1), "Device1 Name"); } }
        public static DeviceSettings.DeviceSettings Device2 { get { return CreateDevice(nameof(Device2), "Device2 Name"); } }
        public static DeviceSettings.DeviceSettings Device3 { get { return CreateDevice(nameof(Device3), "Device3 Name"); } }
        public static DeviceSettings.DeviceSettings Device4 { get { return CreateDevice(nameof(Device4), "Device4 Name"); } }
        public static DeviceSettings.DeviceSettings Device5 { get { return CreateDevice(nameof(Device5), "Device5 Name"); } }
        public static DeviceSettings.DeviceSettings Device6 { get { return CreateDevice(nameof(Device6), "Device6 Name"); } }
        public static DeviceSettings.DeviceSettings Device7 { get { return CreateDevice(nameof(Device7), "Device7 Name"); } }
        public static DeviceSettings.DeviceSettings Device8 { get { return CreateDevice(nameof(Device8), "Device8 Name"); } }

        public static EthernetSourceSettings Eth1
        {
            get { return CreateDeviceSource<EthernetSourceSettings>(nameof(Eth1), new List<DeviceSettings.DeviceSettings> { Device1, Device2 }, CreateSettings()); }
        }

        public static EthernetSourceSettings Eth2
        {
            get { return CreateDeviceSource<EthernetSourceSettings>(nameof(Eth2), new List<DeviceSettings.DeviceSettings> { Device2, Device3 }, CreateSettings(baudRate: 38400)); }
        }

        public static T CreateDeviceSource<T>(string uid, List<DeviceSettings.DeviceSettings> devices, ModbusSettings settings = null) where T : DeviceSourceSettings
        {
            return CreateDeviceSource<T>(uid, devices, uid + " Name", uid + " Description", settings);
        }

        public static T CreateDeviceSource<T>(string uid, List<DeviceSettings.DeviceSettings> devices, string name, string description, ModbusSettings settings = null) where T : DeviceSourceSettings
        {
            var type = typeof(T);
            DeviceSourceSettings result = null;
            if (type == typeof(EthernetSourceSettings)) result = new EthernetSourceSettings { Settings = settings };
            if (result == null) return null;
            result.Uid = uid;
            result.Description = description;
            result.Name = name;
            result.Devices = devices ?? new List<DeviceSettings.DeviceSettings>();
            return result as T;
        }

        public static DeviceSettings.DeviceSettings CreateDevice(string uid, string name = null)
        {
            return new DeviceSettings.DeviceSettings
            {
                Uid = uid,
                Name = name
            };
        }

        public static ModbusSettings CreateSettings(Parity parity = Parity.Even, PacketMode protocol = PacketMode.RTU, int repetitions = 3, int baudRate = 9600, int replyDelay = 2000, int silentInterval = 50)
        {
            return new ModbusSettings
            {
                Parity = parity,
                Protocol = protocol,
                Repetitions = repetitions,
                BaudRate = baudRate,
                ReplyDelay = replyDelay,
                SilentInterval = silentInterval
            };
        }
    }
}
