﻿using System;
using System.Collections.Generic;
using System.Linq;
using Settings.DataStorageSettings;
using Settings.StorageEngine;

namespace Settings.Tests
{
    internal static class MockSettings
    {
        public static Settings GetDemoSettings()
        {
            var demoStorage1 = new MockDemoStorage { AdditionalProperty = "super value xyz" };
            var demoStorage2 = new MockEmptyDemoStorage();
            MockDemoStorage.Init();
            MockEmptyDemoStorage.Init();

            var settings = new Settings
            {
                TerminalId = Guid.NewGuid(),
                Storages = new List<RepositorySettings>
            {
               demoStorage1,
               demoStorage2
            }
            };
            return settings;
        }

        //use demo data and add new not registered storage
        public static Settings GetDemoSettingsWithNotRegisteredType()
        {
            var settings = GetDemoSettings();
            var list = settings.Storages.ToList();
            list.Add(new MockNotRegisteredStorage());
            settings.Storages = list;
            return settings;
        }

        public static bool IsSettingsSame(Settings s1, Settings s2)
        {
            if (!CheckOnNull(s1, s2)) return false;
            if (s1.Weighing != s2.Weighing) return false;

            return AreCollectionSame(s1.Storages, s2.Storages);
        }

        #region Private helpers

        private static bool AreCollectionSame<T>(IEnumerable<T> col1, IEnumerable<T> col2)
        {
            if (col1 == null && col2 == null) return true;
            if (col1 == null || col2 == null) return false;

            var t1 = col1.ToList();
            var t2 = col2.ToList();
            if (t1.Count != t2.Count) return false;
            return true;
        }

        private static bool CheckOnNull<T>(T obj1, T obj2)
        {
            if (obj1 == null && obj2 == null) return true;
            return obj1 != null && obj2 != null;
        }

        #endregion

        #region Mock data

        internal static readonly StorageType MockDemoStorageType = (StorageType)222;
        internal static readonly StorageType MockEmptyDemoStorageType = (StorageType)333;
        internal static readonly StorageType MockNotRegisteredStorageType = (StorageType)456;

        internal class MockDemoStorage : RepositorySettings
        {
            public MockDemoStorage() : base(MockDemoStorageType)
            {
            }

            public static void Init()
            {

            }

            public string AdditionalProperty { get; set; }
        }

        internal class MockDemoStorageWithoutAdditionalProperty : RepositorySettings
        {
            public MockDemoStorageWithoutAdditionalProperty() : base(MockDemoStorageType)
            {
            }

            public static void Init()
            {
            }
        }


        internal class MockEmptyDemoStorage : RepositorySettings
        {
            public MockEmptyDemoStorage() : base(MockEmptyDemoStorageType)
            {
            }

            public static void Init()
            {

            }
        }

        internal class MockNotRegisteredStorage : RepositorySettings
        {
            public MockNotRegisteredStorage() : base(MockNotRegisteredStorageType)
            {
            }
        }

        public class MockTransferObject
        {
        }

        public static readonly Dictionary<object, Type> MockTypeMapper = new Dictionary<object, Type>
      {
         { MockDemoStorageType, typeof(MockDemoStorage)},
         { MockEmptyDemoStorageType, typeof(MockEmptyDemoStorage)},
      };

        #endregion
    }
}
