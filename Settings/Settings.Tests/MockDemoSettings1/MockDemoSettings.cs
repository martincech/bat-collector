﻿using System.Runtime.Serialization;
using Settings.DataStorageSettings;
using Settings.StorageEngine;

namespace Settings.Tests.MockDemoSettings1
{
    [DataContract]
    public class MockDemoSettings : RepositorySettings
    {
        public static void Init()
        {
        }

        [DataMember]
        public string AdditionalProperty { get; set; }

        public MockDemoSettings() : base((StorageType)888)
        {
        }
    }
}
