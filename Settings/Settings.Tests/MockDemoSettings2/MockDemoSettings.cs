﻿using Settings.DataStorageSettings;
using Settings.StorageEngine;

namespace Settings.Tests.MockDemoSettings2
{
    internal class MockDemoSettings : RepositorySettings
    {
        public static void Init()
        {
        }

        public MockDemoSettings() : base((StorageType)888)
        {
        }
    }
}
