﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Settings.DiagnosticSettings;
using Settings.DiagnosticSettings.Collectors;
using Xunit;

namespace Settings.Tests.UnitTests.DiagnosticSettings.Collectors
{
    public class ArchiveLogCollectorTests : BaseCollectorTests
    {
        private static readonly Func<CollectingArchiveLogMode, IDiagnosticCollector> GET_INSTANCE = m => new ArchiveLogCollector(m);

        public ArchiveLogCollectorTests()
           : base(GET_INSTANCE.Invoke(CollectingArchiveLogMode.All))
        {
        }

        [Fact]
        public void ArchiveLogCollect_NoArchive()
        {
            Collector = GET_INSTANCE.Invoke(CollectingArchiveLogMode.None);
            Collector.InputPath = DemoArchiveFolder;
            var output = Path.Combine(OutputFolder, DemoArchiveFolder);
#pragma warning disable IDE0039 // Use local function
            Action act = () => Collector.Collect(output);
#pragma warning restore IDE0039 // Use local function
            Assert.Throws<NotSupportedException>(act);
        }

        [Fact]
        public void ArchiveLogCollect_LastDayOnly()
        {
            Collector = GET_INSTANCE.Invoke(CollectingArchiveLogMode.OnlyLastDay);
            Collector.InputPath = DemoArchiveFolder;
            var output = Path.Combine(OutputFolder, DemoArchiveFolder);
            Collector.Collect(output);

            CheckFileMapping(output, new List<string> { LAST_ARCHIVE });
        }

        [Fact]
        public void ArchiveLogCollect_AllArchive()
        {
            Collector = GET_INSTANCE.Invoke(CollectingArchiveLogMode.All);
            Collector.InputPath = DemoArchiveFolder;
            var output = Path.Combine(OutputFolder, DemoArchiveFolder);
            Collector.Collect(output);

            CheckFileMapping(output, DemoArchiveLogs);
        }
    }
}
