﻿using System.IO;
using Settings.DiagnosticSettings.Collectors;
using Xunit;

namespace Settings.Tests.UnitTests.DiagnosticSettings.Collectors
{
    public class LogCollectorTests : BaseCollectorTests
    {
        public LogCollectorTests()
           : base(new LogCollector())
        {
        }

        [Fact]
        public void LogCollect()
        {
            Collector.InputPath = DemoLogFolder;
            var output = Path.Combine(OutputFolder, FOLDER_LOG);
            Collector.Collect(output);

            CheckFileMapping(output, DemoLogs);
        }
    }
}
