﻿using System.Collections.Generic;
using System.IO;
using Communication.Contract.Wcf.DataContracts;
using Settings.DiagnosticSettings.Collectors;
using Xunit;

namespace Settings.Tests.UnitTests.DiagnosticSettings.Collectors
{
    public class ExternalWeighingSettingsCollectorTests : BaseCollectorTests
    {
        private static readonly Settings APP_SETTINGS;
        private static readonly ExternalWeighingInfo WEIGHING_INFO;

        static ExternalWeighingSettingsCollectorTests()
        {
            WEIGHING_INFO = new ExternalWeighingInfo
            {
                Csv = new Csv { Path = DemoExternalWeighingFolder },
                CurveFileParams = DEMO_EXTERNAL_WEIGHING_CURVE_FILE_NAME,
                WeighingConfigurationFileParams = DEMO_EXTERNAL_WEIGHING_CONFIGURATION_FILE_NAME,
                WeighingParametersFileParams = DEMO_EXTERNAL_WEIGHING_PARAMETERS_FILE_NAME
            };
            APP_SETTINGS = new Settings { Weighing = WEIGHING_INFO };
        }

        public ExternalWeighingSettingsCollectorTests()
           : base(new ExternalWeighingSettingsCollector(APP_SETTINGS))
        {
        }

        [Fact]
        public void ExternalWeighingSettingsCollect_EnabledBySettings()
        {
            APP_SETTINGS.Weighing = WEIGHING_INFO;
            Collector.InputPath = DemoExternalWeighingFolder;
            var output = Path.Combine(OutputFolder, DemoExternalWeighingFolder);
            Collector.Collect(output);

            var demoExternals = new List<string>
             {
                DEMO_EXTERNAL_WEIGHING_CURVE_FILE_NAME,
                DEMO_EXTERNAL_WEIGHING_CONFIGURATION_FILE_NAME,
                DEMO_EXTERNAL_WEIGHING_PARAMETERS_FILE_NAME
             };

            CheckFileMapping(output, demoExternals);
        }

        [Fact]
        public void ExternalWeighingSettingsCollect_DisabledBySettings()
        {
            APP_SETTINGS.Weighing = null;
            Collector.InputPath = DemoExternalWeighingFolder;
            var output = Path.Combine(OutputFolder, DemoExternalWeighingFolder);
            Collector.Collect(output);

            Assert.False(Directory.Exists(output));
        }
    }
}
