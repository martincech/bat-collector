﻿using System.IO;
using Settings.DiagnosticSettings.Collectors;
using Xunit;

namespace Settings.Tests.UnitTests.DiagnosticSettings.Collectors
{
    public class SettingsCollectorTests : BaseCollectorTests
    {
        public SettingsCollectorTests()
           : base(new SettingsCollector())
        {
        }

        [Fact]
        public void SettingsCollect()
        {
            Collector.InputPath = BASE_DEMO_DATA_FOLDER;
            var output = Path.Combine(OutputFolder, nameof(Settings));
            Collector.Collect(output);

            CheckFileMapping(output, DemoSettings);
        }
    }
}
