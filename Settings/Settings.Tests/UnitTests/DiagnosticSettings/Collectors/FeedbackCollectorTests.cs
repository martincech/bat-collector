﻿using System.IO;
using System.Reflection;
using Settings.DiagnosticSettings.Collectors;
using Xunit;

namespace Settings.Tests.UnitTests.DiagnosticSettings.Collectors
{
    public class FeedbackCollectorTests : BaseCollectorTests
    {
        private const string TEST_FEEDBACK_MESSAGE = "Some test feedback message";

        public FeedbackCollectorTests()
           : base(new FeedbackCollector())
        {
        }


        [Fact]
        public void FeedbackCollect_ContainsMessage()
        {
            CheckCollector(TEST_FEEDBACK_MESSAGE, true);
        }


        [Fact]
        public void FeedbackCollect_NoMessage()
        {
            CheckCollector("", false);
        }


        #region Private helpers

        private void CheckCollector(string feedbackMessage, bool expectFeddbackFileWillCreate)
        {
            var collector = (FeedbackCollector)Collector;
            collector.Message = feedbackMessage;

            collector.Collect(OutputFolder);
            Assert.Equal(expectFeddbackFileWillCreate, CheckFeedbackFile());
        }

        private bool CheckFeedbackFile()
        {
            var fieldInfo = typeof(FeedbackCollector).GetField("FILE_NAME",
               BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            Assert.NotNull(fieldInfo);
            var expectedFileName = fieldInfo.GetValue(Collector).ToString();

            var fullName = Path.Combine(OutputFolder, expectedFileName);
            if (!File.Exists(fullName))
            {
                return false;
            }

            var content = File.ReadAllText(fullName);
            return content.Equals(TEST_FEEDBACK_MESSAGE);
        }

        #endregion

    }
}
