﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Settings.DiagnosticSettings.Collectors;
using Xunit;

namespace Settings.Tests.UnitTests.DiagnosticSettings.Collectors
{
    public abstract class BaseCollectorTests : IDisposable
    {
        #region Constants

        protected IDiagnosticCollector Collector;
        protected const string BASE_DEMO_DATA_FOLDER = "TestSettingsFolder";

        protected const string FOLDER_LOG = "testLog";
        protected const string FOLDER_ARCHIVE = "testArchive";
        protected static readonly string DemoLogFolder = Path.Combine(BASE_DEMO_DATA_FOLDER, FOLDER_LOG);
        protected static readonly string DemoArchiveFolder = Path.Combine(DemoLogFolder, FOLDER_ARCHIVE);

        protected static readonly string DemoExternalWeighingFolder = Path.Combine(BASE_DEMO_DATA_FOLDER, "Data",
           "Ext-configuration");

        protected const string DEMO_EXTERNAL_WEIGHING_CURVE_FILE_NAME = "Curve-test.csv";
        protected const string DEMO_EXTERNAL_WEIGHING_CONFIGURATION_FILE_NAME = "Configuration-test.csv";
        protected const string DEMO_EXTERNAL_WEIGHING_PARAMETERS_FILE_NAME = "Weighing-test.csv";

        protected readonly List<string> DemoLogs = new List<string> { "testFile001", "testF02.txt" };
        protected const string LAST_ARCHIVE = "2018-01-24-service01.log";
        protected readonly List<string> DemoArchiveLogs = new List<string> { LAST_ARCHIVE, "2018-01-23-service01.log" };
        protected readonly List<string> DemoSettings = new List<string> { "testSettings.json", "someSettings.json" };
        protected const string NO_SETTINGS_FILE = "NoSettingsFile.txt";

        protected readonly string OutputFolder = Path.Combine(BASE_DEMO_DATA_FOLDER, "testTemp");

        #endregion

        protected BaseCollectorTests(IDiagnosticCollector collector)
        {
            Collector = collector;
            CreateDemoData();
        }


        protected BaseCollectorTests()
        {
            CleanUp();
            CreateDemoData();
        }

        private static void CleanUp()
        {
            if (Directory.Exists(BASE_DEMO_DATA_FOLDER))
            {
                Directory.Delete(BASE_DEMO_DATA_FOLDER, true);
            }
        }

        public void Dispose()
        {
            CleanUp();
            GC.SuppressFinalize(this);
        }

        #region Private helpers

        private void CreateDemoData()
        {
            Directory.CreateDirectory(BASE_DEMO_DATA_FOLDER);

            //create settings
            foreach (var file in DemoSettings)
            {
                File.WriteAllText(Path.Combine(BASE_DEMO_DATA_FOLDER, file), file);
            }
            File.WriteAllText(Path.Combine(BASE_DEMO_DATA_FOLDER, NO_SETTINGS_FILE), NO_SETTINGS_FILE);

            //create logs
            Directory.CreateDirectory(DemoLogFolder);
            foreach (var file in DemoLogs)
            {
                File.WriteAllText(Path.Combine(DemoLogFolder, file), file);
            }

            //create archive
            Directory.CreateDirectory(DemoArchiveFolder);
            foreach (var file in DemoArchiveLogs)
            {
                File.WriteAllText(Path.Combine(DemoArchiveFolder, file), file);
            }

            //create external weighing settings
            Directory.CreateDirectory(DemoExternalWeighingFolder);
            WriteExternalWeighingFile(DEMO_EXTERNAL_WEIGHING_CURVE_FILE_NAME);
            WriteExternalWeighingFile(DEMO_EXTERNAL_WEIGHING_CONFIGURATION_FILE_NAME);
            WriteExternalWeighingFile(DEMO_EXTERNAL_WEIGHING_PARAMETERS_FILE_NAME);
        }

        private static void WriteExternalWeighingFile(string fileName)
        {
            File.WriteAllText(Path.Combine(DemoExternalWeighingFolder, fileName), fileName);
        }

        protected static void CheckFileMapping(string folder, ICollection<string> expectedFileNames)
        {
            var actualFiles = Directory.GetFiles(folder);
            Assert.Equal(expectedFileNames.Count, actualFiles.Count());

            foreach (var file in actualFiles)
            {
                var name = Path.GetFileName(file);
                Assert.NotNull(name);
                Assert.True(expectedFileNames.Contains(name));
            }
        }

        #endregion
    }
}
