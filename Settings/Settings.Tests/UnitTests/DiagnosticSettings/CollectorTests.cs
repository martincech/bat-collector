﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Settings.DiagnosticSettings;
using Xunit;

namespace Settings.Tests.UnitTests.DiagnosticSettings
{
    public class CollectorTests : IDisposable
    {
        #region Initialization & De-initialization

        private const string ARCHIVE_NAME = "testArchive.zip";

        private const string BASE_DEMO_DATA_FOLDER = "TestSettingsFolder";
        private static readonly string OUTPUT_PATH = Path.Combine(BASE_DEMO_DATA_FOLDER, "output");
        private static readonly string COLLECTED_PATH = Path.Combine(BASE_DEMO_DATA_FOLDER, "TEMP");

        private readonly List<string> demoFiles = new List<string>
          {
             "dir01/file01",
             "fileX",
             "fileY",
             "dir02/subdir02/file02.log"
          };

        public CollectorTests()
        {
            CleanUp();
            CreateDemoData();
        }

        private static void CleanUp()
        {
            if (Directory.Exists(BASE_DEMO_DATA_FOLDER))
            {
                Directory.Delete(BASE_DEMO_DATA_FOLDER, true);
            }
            if (File.Exists(ARCHIVE_NAME))
            {
                File.Delete(ARCHIVE_NAME);
            }
        }

        public void Dispose()
        {
            CleanUp();
            GC.SuppressFinalize(this);
        }

        #endregion


        [Fact]
        public void ArchiveDiagnosticData_NoData()
        {
            //no collected data
            Assert.False(Collector.Archive(COLLECTED_PATH, ARCHIVE_NAME));
            Assert.False(IsArchiveExist(ARCHIVE_NAME));
        }

        [Fact]
        public void ArchiveDiagnosticData_CollectedData()
        {
            Assert.True(Collector.Archive(BASE_DEMO_DATA_FOLDER, ARCHIVE_NAME));
            Assert.True(IsArchiveExist(ARCHIVE_NAME));
            CheckZipFile();
        }


        #region Private helpers

        private void CreateDemoData()
        {
            foreach (var demoFile in demoFiles)
            {
                var fullPath = Path.Combine(BASE_DEMO_DATA_FOLDER, demoFile);
                var dir = Path.GetDirectoryName(fullPath);
                Assert.NotNull(dir);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                File.WriteAllText(fullPath, fullPath);
            }
        }

        private void CheckZipFile()
        {
            UnzipDiagnostics();
            foreach (var demofile in demoFiles)
            {
                var file = Path.Combine(OUTPUT_PATH, demofile);
                if (!File.Exists(file))
                {
                    Assert.True(false, "File is missing in output zip");
                }
            }
        }

        private static bool IsArchiveExist(string archiveName)
        {
            return File.Exists(archiveName);
        }

        private static void UnzipDiagnostics()
        {
            try
            {
                ZipFile.ExtractToDirectory(ARCHIVE_NAME, OUTPUT_PATH);
            }
            catch (Exception)
            {
                Assert.True(false);
            }
        }

        #endregion
    }
}
