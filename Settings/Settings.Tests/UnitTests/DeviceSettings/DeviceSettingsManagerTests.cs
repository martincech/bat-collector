﻿using System.Collections.Generic;
using System.Linq;
using Castle.Components.DictionaryAdapter;
using Moq;
using Settings.Core.Repository;
using Settings.DeviceSettings;
using Settings.DeviceSettings.SourceSettings;
using Xunit;

namespace Settings.Tests.UnitTests.DeviceSettings
{
    public class DeviceSettingsManagerTests
    {
        delegate IEnumerable<DeviceSourceSettings> GetDevices();

        private readonly Mock<ICoreEngine<DeviceSettingsTree>> mockStorageEngine;
        private readonly DeviceSettingsManager manager;
        private DeviceSettingsTree settings;

        public DeviceSettingsManagerTests()
        {
            mockStorageEngine = new Mock<ICoreEngine<DeviceSettingsTree>>();
            manager = new DeviceSettingsManager(mockStorageEngine.Object);
        }

        private void SetupStorageEngine()
        {
            settings = new DeviceSettingsTree();
            mockStorageEngine.Setup(m => m.Load()).Returns(settings);
            mockStorageEngine.Setup(m => m.Save(It.IsAny<DeviceSettingsTree>()))
               .Callback((DeviceSettingsTree item) => settings = item);
        }

        [Fact]
        public void LoadDevices_WhenSettingsNotExist()
        {
            mockStorageEngine.Setup(m => m.Load()).Returns((DeviceSettingsTree)null);

            var result = manager.LoadDevices(true);

            mockStorageEngine.Verify(v => v.Load(), Times.Once);
            Assert.NotNull(result);
        }

        [Fact]
        public void LoadDevices_WhenSettingsExist()
        {
            var expected = new DeviceSettingsTree();
            mockStorageEngine.Setup(m => m.Load()).Returns(expected);

            var result = manager.LoadDevices(true);

            mockStorageEngine.Verify(v => v.Load(), Times.Once);
            Assert.NotNull(result);
            Assert.Equal(expected, result);
        }

        [Fact]
        public void AddOrUpdateDevice_DeviceSource_WhenDeviceSourceNotExist()
        {
            SetupStorageEngine();

            AddOrUpdateDevice_DeviceSource_WhenDeviceSourceNotExist_For(MockDeviceSettings.Eth1, () => settings.EthernetSources);

            mockStorageEngine.Verify(v => v.Save(It.IsAny<DeviceSettingsTree>()), Times.Exactly(1));
        }

        private void AddOrUpdateDevice_DeviceSource_WhenDeviceSourceNotExist_For(DeviceSourceSettings source, GetDevices getSources)
        {
            manager.AddOrUpdateDevice(source);

            var sources = getSources?.Invoke().ToList();
            Assert.Single(sources);
            Equal(source, sources.FirstOrDefault());
        }

        [Fact]
        public void AddOrUpdateDevice_WhenDeviceSourceExist()
        {
            SetupStorageEngine();

            AddOrUpdateDevice_DeviceSource_WhenDeviceSourceExist(MockDeviceSettings.Eth1, MockDeviceSettings.Eth2, () => settings.EthernetSources);

            mockStorageEngine.Verify(v => v.Save(It.IsAny<DeviceSettingsTree>()), Times.Exactly(2));
        }

        private void AddOrUpdateDevice_DeviceSource_WhenDeviceSourceExist(DeviceSourceSettings original, DeviceSourceSettings update, GetDevices getSources)
        {
            manager.AddOrUpdateDevice(original);

            update.Uid = original.Uid;

            manager.AddOrUpdateDevice(update);

            var sources = getSources?.Invoke().ToList();
            Assert.Single(sources);
            Equal(update, sources.FirstOrDefault());
        }

        [Fact]
        public void AddOrUpdateDevice_Device_WhenDeviceSourceNotExist()
        {
            SetupStorageEngine();

            AddOrUpdateDevice_Device_WhenDeviceSourceNotExist_For(MockDeviceSettings.Device1, MockDeviceSettings.Eth1, () => settings.EthernetSources);

            mockStorageEngine.Verify(v => v.Save(It.IsAny<DeviceSettingsTree>()), Times.Exactly(1));
        }

        private void AddOrUpdateDevice_Device_WhenDeviceSourceNotExist_For(global::Settings.DeviceSettings.DeviceSettings device, DeviceSourceSettings source, GetDevices getSources)
        {
            manager.AddOrUpdateDevice(device, source);

            var sources = getSources?.Invoke().ToList();
            Assert.Single(sources);
            Equal(source, sources.FirstOrDefault());
            Assert.Single(sources.First().Devices);
        }


        private void SetupStorageEngineAndData()
        {
            SetupStorageEngine();
            settings.EthernetSources = new EditableList<EthernetSourceSettings> { MockDeviceSettings.Eth1 };
        }

        [Fact]
        public void Delete_DeviceSource_WhenDeviceSourceExist()
        {
            SetupStorageEngineAndData();

            Delete_DeviceSource_WhenDeviceSourceExist_For(MockDeviceSettings.Eth1, () => settings.EthernetSources);

            mockStorageEngine.Verify(v => v.Save(It.IsAny<DeviceSettingsTree>()), Times.Exactly(1));
        }

        [Fact]
        public void Delete_DeviceSource_WhenDeviceSourceNotExist()
        {
            SetupStorageEngine();

            Delete_DeviceSource_WhenDeviceSourceExist_For(MockDeviceSettings.Eth1, () => settings.EthernetSources, 0);

            mockStorageEngine.Verify(v => v.Save(It.IsAny<DeviceSettingsTree>()), Times.Exactly(1));
        }

        private void Delete_DeviceSource_WhenDeviceSourceExist_For(DeviceSourceSettings source, GetDevices getSources, int expected = 1)
        {
            var sources = getSources?.Invoke().ToList();
            Assert.Equal(expected, sources.Count());

            manager.DeleteDevice(source);

            sources = getSources?.Invoke().ToList();
            Assert.Empty(sources.Where(w => !w.IsDeleted));
        }


        [Fact]
        public void Delete_Device_WhenDeviceAndDeviceSourceSourceExist()
        {
            SetupStorageEngineAndData();

            Delete_Device_WhenDeviceAndDeviceSourceExist_For(MockDeviceSettings.Eth1, () => settings.EthernetSources);

            mockStorageEngine.Verify(v => v.Save(It.IsAny<DeviceSettingsTree>()), Times.Exactly(1));
        }

        [Fact]
        public void Delete_Device_WhenDeviceAndDeviceSourceSourceNotExist()
        {
            SetupStorageEngine();

            Delete_Device_WhenDeviceAndDeviceSourceExist_For(MockDeviceSettings.Eth1, () => settings.EthernetSources, MockDeviceSettings.Eth1.Devices.First());

            mockStorageEngine.Verify(v => v.Save(It.IsAny<DeviceSettingsTree>()), Times.Exactly(1));
        }

        private void Delete_Device_WhenDeviceAndDeviceSourceExist_For(DeviceSourceSettings source, GetDevices getSources, global::Settings.DeviceSettings.DeviceSettings device = null)
        {
            var sources = getSources?.Invoke().ToList();
            Assert.Equal(device == null ? 1 : 0, sources.Count());
            if (device == null) Assert.Equal(2, sources.First().Devices.Count);

            manager.DeleteDevice(device ?? source.Devices.First(), source);

            sources = getSources?.Invoke().ToList();
            Assert.Equal(device == null ? 1 : 0, sources.Count());
            if (device == null) Assert.Single(sources.First().Devices.Where(w => !w.IsDeleted));
        }


        private static void Equal(DeviceSourceSettings expected, DeviceSourceSettings actual)
        {
            Assert.Equal(expected != null, actual != null);
            if (expected == null || actual == null) return;
            Assert.Equal(expected.Uid, actual.Uid);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.Description, actual.Description);
        }
    }
}
