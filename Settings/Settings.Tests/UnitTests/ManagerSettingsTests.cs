﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Settings.Core;
using Settings.StorageEngine;
using Xunit;

namespace Settings.Tests.UnitTests
{
    public class ManagerSettingsTests
    {
        private readonly Mock<IEngine> mockEngine;
        private readonly Mock<SystemInfo> mockInfo;

        public ManagerSettingsTests()
        {
            mockEngine = new Mock<IEngine>();
            mockInfo = new Mock<SystemInfo>();
        }

        /// <summary>
        /// Save settings in manager raised save in engine.
        /// </summary>
        [Fact]
        public void SaveSettings_CallInEngine()
        {
            mockEngine.Verify(m => m.Save(It.IsAny<Settings>()), Times.Never);

            var settingsManager = new ManagerSettings(mockEngine.Object, null, mockInfo.Object);
            settingsManager.Save();

            mockEngine.Verify(m => m.Save(It.IsAny<Settings>()), Times.AtLeastOnce);
        }

        /// <summary>
        /// Load settings in engine is raised in manager constructor.
        /// </summary>
        [Fact]
        public void LoadSettings_CallsInConstructor()
        {
            mockEngine.Verify(m => m.Load(), Times.Never);

            var settingsManager = new ManagerSettings(mockEngine.Object, null, mockInfo.Object);

            mockEngine.Verify(m => m.Load(), Times.Once);
            Assert.NotNull(settingsManager.Settings);
        }

        [Fact]
        public void CallEngineLoadSettings_SettingsExisted()
        {
            var demoSettings = MockSettings.GetDemoSettings();
            mockEngine.Setup(s => s.Load()).Returns(demoSettings);

            var settingsManager = new ManagerSettings(mockEngine.Object, null, mockInfo.Object);
            settingsManager.Load();

            var loadedSettings = settingsManager.Settings;
            Assert.True(MockSettings.IsSettingsSame(demoSettings, loadedSettings));
        }

        /// <summary>
        /// Engine load settings return null data, manager use some default, do not returns null.
        /// </summary>
        [Fact]
        public void CallEngineLoadSettings_SettingsNotExisted_UseDefault()
        {
            mockEngine.Setup(s => s.Load()).Returns((Settings)null);

            var settingsManager = new ManagerSettings(mockEngine.Object, null, mockInfo.Object);
            settingsManager.Load();
            Assert.NotNull(settingsManager.Settings);
        }

        /// <summary>
        /// Default settings is not null and contains some initial data.
        /// </summary>
        [Fact]
        public void DefaultSettings_ContainsSomeData()
        {
            var settingsManager = new ManagerSettings(mockEngine.Object, null, mockInfo.Object);
            var defaultSettings = settingsManager.CreateDefaultSettings();

            Assert.NotNull(defaultSettings);
            Assert.NotNull(defaultSettings.Storages);
            Assert.NotEmpty(defaultSettings.Storages);
        }

        [Fact]
        public void DefaultSettings_ContainsDataForAllSamples()
        {
            var settingsManager = new ManagerSettings(mockEngine.Object, null, mockInfo.Object);
            var defaultSettings = settingsManager.CreateDefaultSettings();

            Assert.NotNull(defaultSettings);
            Assert.NotNull(defaultSettings.Sources);

            var expected = GetEnumValuesWithoutObsoleteAttributes(typeof(SourcesType));
            Assert.Equal(expected.Length, defaultSettings.Sources.Count());
        }

        private static Array GetEnumValuesWithoutObsoleteAttributes(Type type)
        {
            var values = Enum.GetValues(type);
            var checkedValues = new List<object>();
            foreach (var item in values)
            {
                if (IsObsolete((Enum)item))
                {
                    continue;
                }
                checkedValues.Add(item);
            }
            return checkedValues.ToArray();
        }

        private static bool IsObsolete(Enum value)
        {
            var enumType = value.GetType();
            var enumName = enumType.GetEnumName(value);
            var fieldInfo = enumType.GetField(enumName);
            return Attribute.IsDefined(fieldInfo, typeof(ObsoleteAttribute));
        }

    }
}
