﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Settings.DataStorageSettings;
using Settings.StorageEngine;
using Xunit;

namespace Settings.Tests.UnitTests.Repository
{
    public class JsonEngineTests : IDisposable
    {
        #region Private fields

        private static IEngine _uut;
        private readonly string testFile;

        #endregion

        #region Tests Initialization

        public JsonEngineTests()
        {
            _uut = new JsonSettingsEngine();
            var baseType = _uut.GetType().BaseType;
            Assert.NotNull(baseType);
            testFile = baseType.GetField("fullName", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_uut) as string;
            Assert.NotNull(testFile);
            DeleteTestFile();
            Assert.False(TestFileExist());
            SetTestsTypeMapperForStorages(MockSettings.MockTypeMapper);
        }

        public void Dispose()
        {
            DeleteTestFile();
            GC.SuppressFinalize(this);
        }

        private void DeleteTestFile()
        {
            if (File.Exists(testFile))
            {
                File.Delete(testFile);
            }
        }
        #endregion


        /// <summary>
        /// save null data to file
        /// </summary>
        [Fact]
        public void SaveToJson_Null()
        {
            _uut.Save(null);

            Assert.True(TestFileExist());
            var text = LoadFileData();
            Assert.True(text.Equals("") || text.Equals("null"));
        }

        /// <summary>
        /// save demo data to file
        /// </summary>
        [Fact]
        public void SaveToJson_SettingsData()
        {
            var demoSettings = MockSettings.GetDemoSettings();
            _uut.Save(demoSettings);

            Assert.True(TestFileExist());
            var text = LoadFileData();

            //Not test concrete data -> for this are tests for load
            Assert.NotEqual(string.Empty, text);
        }

        /// <summary>
        /// Try load file which is not exist
        /// </summary>
        [Fact]
        public void LoadFromJson_FileNotFound()
        {
            var settings = _uut.Load();
            Assert.Null(settings);
        }

        /// <summary>
        /// Load existing file.
        /// </summary>
        [Fact]
        public void LoadFromJson_FileExist()
        {
            SaveToJson_SettingsData();

            var settings = _uut.Load();
            Assert.True(MockSettings.IsSettingsSame(MockSettings.GetDemoSettings(), settings));
        }


        [Fact]
        public void LoadFromJson_NotRegisteredType()
        {
            var demoSettings = MockSettings.GetDemoSettingsWithNotRegisteredType();
            _uut.Save(demoSettings);
            Assert.True(TestFileExist());

            var loadedSettings = _uut.Load();
            Assert.False(MockSettings.IsSettingsSame(demoSettings, loadedSettings));
        }

        /// <summary>
        /// Saved old storage implementation with more properties.
        /// Load new storage version with smaller number of properties.
        /// </summary>
        [Fact]
        public void LoadFromJson_NewVersion_LessInfo()
        {
            var oldStorage = new MockDemoSettings1.MockDemoSettings { AdditionalProperty = "qwerty" };
            var newStorage = new MockDemoSettings2.MockDemoSettings();
            MigrationTest(oldStorage, newStorage);
        }

        /// <summary>
        /// Saved old storage implementation with smaller number of properties.
        /// Load new storage version with higher number of properties.
        /// </summary>
        [Fact]
        public void LoadFromJson_NewVersion_MoreInfo()
        {
            var oldStorage = new MockDemoSettings2.MockDemoSettings();
            var newStorage = new MockDemoSettings1.MockDemoSettings { AdditionalProperty = "qwerty" };
            MigrationTest(oldStorage, newStorage);
        }

        #region Private helpers


        private static void MigrationTest(RepositorySettings oldSettings, RepositorySettings newSettings)
        {
            var demoSettings = MockSettings.GetDemoSettings();
            demoSettings.Storages = new List<RepositorySettings> { oldSettings };
            _uut.Save(demoSettings);

            //edit mapper for new storage
            var mapper = new Dictionary<object, Type>
             {
                { oldSettings.Type, newSettings.GetType()}
             };
            SetTestsTypeMapperForStorages(mapper);

            //load
            var loadedSettings = _uut.Load();
            Assert.Single(loadedSettings.Storages);
            var loadedStorageType = loadedSettings.Storages.First().GetType();

            Assert.NotEqual(oldSettings.GetType(), loadedStorageType);
            Assert.Equal(newSettings.GetType(), loadedStorageType);
        }

        private bool TestFileExist()
        {
            return File.Exists(testFile);
        }

        private string LoadFileData()
        {
            return File.ReadAllText(testFile);
        }

        private static void SetTestsTypeMapperForStorages(object value)
        {
            var mapper = typeof(StorageTypeExtensions).GetField("TypeMapper", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
            if (mapper == null)
            {
                throw new Exception("Class not contains required field!");
            }
            mapper.SetValue(null, value);
        }

        #endregion
    }
}
