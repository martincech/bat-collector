﻿using System;
using Xunit;

namespace HardwareId.Tests.UnitTests
{
    public class HwIdTests
    {
        [Fact]
        public void GetSystemGuid()
        {
            var uuid = HwId.SystemId;
            Assert.True(uuid != Guid.Empty);
        }

    }
}
