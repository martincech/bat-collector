﻿namespace HardwareId.Legacy.FingerPrint
{
    public interface IFingerPrint
    {
        bool Exist();
        string Load();
    }
}
