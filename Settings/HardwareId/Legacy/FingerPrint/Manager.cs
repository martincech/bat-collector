﻿namespace HardwareId.Legacy.FingerPrint
{
    public class Manager : IFingerPrint
    {
        private readonly IFingerPrint fingerPrintImpl;

        public Manager(string settingsFolder)
           : this(new FingerPrintFile(settingsFolder))
        {
        }

        public Manager(IFingerPrint fingerPrint)
        {
            fingerPrintImpl = fingerPrint;
        }


        public bool Exist()
        {
            return fingerPrintImpl.Exist();
        }

        public string Load()
        {
            return fingerPrintImpl.Load();
        }
    }
}
