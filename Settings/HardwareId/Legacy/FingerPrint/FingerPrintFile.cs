﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace HardwareId.Legacy.FingerPrint
{
    /// <summary>
    /// Using file to store fingerprint. The fingerprint is saved using symmetric
    /// cryptography (<see cref="Rijndael"/>) and then data are saved as Base64 string.
    /// </summary>
    internal class FingerPrintFile : IFingerPrint
    {
        #region Private fields

        private const string FILE_NAME = "hwid";
        private string fullName;
        private readonly PaddingMode paddingMode;

        //32 bit key
        private readonly byte[] cryptedKey =
        {
         1, 50, 7, 7, 29, 4, 25, 37, 11, 74, 78, 1, 5, 73, 147, 9, 14, 25, 37, 111, 74,
         78, 11, 250, 77, 47, 9, 4, 25, 37, 11, 44
      };

        //16 bit key
        private readonly byte[] cryptedVector = { 8, 250, 7, 27, 74, 155, 25, 37, 111, 74, 78, 13, 50, 77, 47, 29 };

        #endregion

        #region Constructor

        public FingerPrintFile(string settingsFolder)
        {
            paddingMode = PaddingMode.Zeros;
            CheckFolder(settingsFolder);
        }

        #endregion

        #region Implementation of IFingerPrint

        public bool Exist()
        {
            return File.Exists(fullName);
        }

        public string Load()
        {
            if (!Exist())
            {
                throw new FileNotFoundException();
            }

            var cryptedText = File.ReadAllText(fullName);
            var bytes = Convert.FromBase64String(cryptedText);
            return DecryptStringFromBytes(bytes, cryptedKey, cryptedVector, paddingMode);
        }

        #endregion

        #region Private helpers

        private void CheckFolder(string settingsFolder)
        {
            fullName = Path.Combine(settingsFolder, FILE_NAME);
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv, PaddingMode mode)
        {
            // Check arguments
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException(nameof(cipherText));
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException(nameof(key));
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException(nameof(iv));

            // Create an RijndaelManaged object with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = key;
                rijAlg.IV = iv;
                rijAlg.Padding = mode;

                // Create a decryptor to perform the stream transform.
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                var output = new byte[0x100];
                var len = decryptor.TransformBlock(cipherText, 0, cipherText.Length, output, 0);
                return System.Text.Encoding.ASCII.GetString(output, 0, len).TrimEnd(new char[] { '\0' });
            }
        }

        #endregion

    }
}
