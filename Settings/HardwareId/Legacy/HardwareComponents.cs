﻿using System;

namespace HardwareId.Legacy
{
    [Flags]
    public enum HardwareComponents
    {
        Cpu = 1,
        MotherBoard = 2,
        Disk = 4,
        MacAddress = 8,
        Bios = 16,
    }
}
