﻿using System;
using HardwareId.Legacy.FingerPrint;

namespace HardwareId.Legacy
{
    /// <summary>
    /// Generator hardware ID - from predefined hardware components take their
    /// serial number, manufacturer or other identification and transform it
    /// to hexadecimal number.
    /// </summary>
    public class HwIdLegacy
    {
        #region Private fields

        private readonly IFingerPrint fingerPrint; //previous saved hardware ID
        private const string CUSTOM_ID_PREFIX = "00074924571247654-";
        public const int MINIMAL_ID_LENGTH = 19;

        #endregion

        #region Public interfaces

        #region Constructor

        public HwIdLegacy(string settingsFolder)
        {
            fingerPrint = new Manager(settingsFolder);
            Components = HardwareComponents.Cpu | HardwareComponents.MotherBoard | HardwareComponents.Disk;
        }

        #endregion

        public bool UseFingerPrint { get; } = true;
        public HardwareComponents Components { get; }


        /// <summary>
        /// Generate hardware ID from predefined components by property "Components".
        /// </summary>
        /// <returns></returns>
        public string Generate()
        {
            if (UseFingerPrint && fingerPrint.Exist())
            {
                var loadedId = fingerPrint.Load();
                if (IsValidAsHwId(loadedId))
                {
                    return loadedId;
                }
                if (IsCustomId(loadedId))
                {
                    return ParseCustomId(loadedId);
                }
            }

            throw new NotSupportedException("Generating a new HWID is not supported. Use new HwId implementation using iSMBIOS data.");
        }

        #endregion

        #region Private helpers

        private static bool IsValidAsHwId(string str)
        {
            //check length
            var length = str.Length;
            if (length < MINIMAL_ID_LENGTH || (length - MINIMAL_ID_LENGTH) % 5 != 0)
            {
                //not valid
                return false;
            }

            for (var i = 0; i < length; i++)
            {
                if ((i - 4) % 5 == 0)
                {
                    //is dash
                    if (str[i] != '-') return false;
                }
                else
                {
                    //is hex number
                    if (!IsHexNumber(str[i]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private static bool IsCustomId(string loadedId)
        {
            return loadedId.Contains(CUSTOM_ID_PREFIX);
        }

        private static string ParseCustomId(string loadedId)
        {
            return loadedId.Replace(CUSTOM_ID_PREFIX, string.Empty);
        }

        private static bool IsHexNumber(char letter)
        {
            return (letter >= '0' && letter <= '9') || (letter >= 'a' && letter <= 'f') || (letter >= 'A' && letter <= 'F');
        }

        #endregion
    }
}
