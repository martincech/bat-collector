﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using iTin.Core.Hardware;
using iTin.Core.Hardware.Specification;
using iTin.Core.Hardware.Specification.Dmi;
using iTin.Core.Hardware.Specification.Dmi.Property;
using iTin.Core.Hardware.Specification.Smbios;

namespace HardwareId
{
    public static class HwId
    {
        private static Guid id;

        public static Guid SystemId
        {
            get
            {
                if (id == Guid.Empty)
                {
                    id = GetSystemId();
                }
                return id;
            }
        }

        private static Guid GetSystemId()
        {
            var structures = DMI.Instance.Structures;
            var identifier = GetStructureInfo(structures[SmbiosStructure.System], KnownDmiProperty.System.UUID);
            if (Guid.TryParse(identifier.ToString(), out var uuid))
            {
                return uuid;
            }
            throw new InvalidCastException($"{identifier} is not a valid GUID.");
        }


        private static object GetStructureInfo(DmiStructure structure, PropertyKey propertyKey)
        {
            if (structure == null)
            {
                throw new ArgumentNullException(nameof(structure));
            }

            foreach (var element in structure.Elements)
            {
                var dictionary = element.Properties
                    .Cast<DictionaryEntry>()
                    .ToDictionary(o => (PropertyKey)o.Key, o => o.Value);

                if (dictionary.TryGetValue(propertyKey, out var value))
                {
                    return value;
                }
            }

            throw new InvalidOperationException($"Property: '{propertyKey.PropertyId.ToString()}' not found in selected DmiStructure!");
        }
    }
}
